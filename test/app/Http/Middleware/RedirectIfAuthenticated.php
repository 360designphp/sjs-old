<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
         $pname=DB::select('select role from users where email = ?' , [$request->email]);
         
  if(count($pname)>0){
    //dd($pname[0]->role);
    if ($pname[0]->role=='User'){
        return redirect()->to('/');
      //return  header('Location: '.'http://topcar-premier.ru/'); 
    }
    
  }
        
        if (Auth::guard($guard)->check()) {
            return redirect('/admin/news/list');
        }

        return $next($request);
    }
}
