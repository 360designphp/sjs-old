<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Post;
use App\Settings;
use App\Taxonomy;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index(Request $request){
        switch ($request->intent){
            case 'saveSettings':

                $this->saveSettings($request);
                break;
            case 'deleteTaxonomy':
                Taxonomy::find($request->id)->delete();
                break;
            case 'addTaxonomy':
               return(Taxonomy::addTaxonomy($request));

                break;
            case 'deleteImage':
                $post = Post::find($request->id);
                $post->deleteImage();
                break;
            case 'addMenuItem':
                $menu = new Menu();
                $menu->save();
                return $menu->id;
            break;
            case 'deleteMenuItem':
                Menu::find($request->id)->delete();
                break;
            case 'updateMenuItem':
                $menu = Menu::find($request->id);
                $menu->updateItem($request);
                break;
            case 'sendMessage':
                $to = 'dev.vladimirkarpenko@gmail.com';
                $subject = 'Модерн Люкс | '.$request->subject;
                $message = 'Новое сообщение от пользователя ' . $request->name . '
                ' . $request->text . '
                E-mail для обратной связи: ' . $request->email;
                mail($to,$subject,$message);
                break;
        }
    }

    public function saveSettings(Request $request){
        $settings = Settings::first();
        $settings->site_name = $request->site_name;
        $settings->keywords = $request->keywords;
        $settings->description = $request->description;
        $settings->vk_link = $request->vk_link;
        $settings->fb_link = $request->fb_link;
        $settings->inst_link = $request->insta_link;
        $settings->twitter_link = $request->twitter_link;

        if($settings->update()){
            echo 'Настройки успешно сохранены';
        } else {
            echo 'Возникла ошибка при сохранении настроек';
        }
    }
}
