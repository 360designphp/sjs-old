<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Taxonomy;
use App\Settings;
use Hamcrest\Core\Set;
use App\Menu;
use Laravel\Scout\Searchable;
class HomeController extends Controller
{

    public $data = array();

    public function __construct()
    {
        $this->data['top_menu'] = Menu::getMenu('top_menu');
        $this->data['bottom_menu'] = Menu::getMenu('bottom_menu');
        $this->data['settings'] = Settings::first();
        //$this->middleware('auth');
    }

    //Страница входа
    public function index()
    {
        return view('home');
    }

    public function searchPost(Request $request){
        $posts = Post::search($request->search_string)->where('type', 'news')->orWhere('type', 'post')->get();
        dd($posts);
    }

    public function getHomepage() {
        //DB::SELECT();
     
       $this->data['posts'] = Post::where('type', 'news')->where('status','published' )->orderBy('created_at')->take(5)->get();
        if(count($this->data['posts'])==0){
            
             return view('errors.404', $this->data);
        } 
       // dd(count($this->data['posts']));
        
        
        $this->data['main_post'] =  $this->data['posts']->shift();
       
        if($this->data['main_post'] != null ) {
            $thumb = $this->data['main_post']->thumbnail();
        }
        return view('layouts.front.home', $this->data);
    }
    public function getPage($slug){
        $this->data['page'] = Post::where('slug', $slug)->first();
        return view('layouts.front.page', $this->data);
    }
    public function getNewsList(){
        $this->data['posts'] = Post::where('type', 'news')->paginate(12);
        return view('layouts.front.news_list', $this->data);
    }
    public function getCategory($category){
        $this->data['tax'] = Taxonomy::where('slug',$category)->first();
         $this->data['posts'] = Post::whereHas('taxonomy', function($query) use ($category){
            $query->where('slug', 'LIKE', $category);})->paginate(12);

        return view('layouts.front.news_list', $this->data);
    }
    public function getNewsSingle($slug){
        //Для активации фоновой пикчи или видео переименовать в main_post
        $this->data['post'] = Post::where('type', 'news')->where('slug', $slug)->first();
        return view('layouts.front.single', $this->data);
    }
}
