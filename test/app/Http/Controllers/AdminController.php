<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Relation;
use App\Taxonomy;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Settings;
use App\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use DB;
class AdminController extends Controller
{
    
    public function __construct()
    {
        $this->data = array();
        $this->data['settings'] = Settings::first();
       // dd($this->middleware('auth'));//
        $this->middleware('auth');
        $this->data['filterNewsDell']='/admin/news/dell';
            $this->data['filterNewsDruft']='/admin/news/druft';
            $this->data['filterNewsPub']='/admin/news/pub';
            
            $this->data['filterPageDell']='/admin/page/dell';
            $this->data['filterPageDruft']='/admin/page/druft';
            $this->data['filterPagePub']='/admin/page/pub';
    }

    //Настройки
    public function settings() {    
        $this->data['ad_script'] = '/assets/js/ajax/settings_saver.js';
        
        $this->data['settings'] = Settings::first();
      
        return view('layouts.admin.settings', $this->data);
    }

    //Галерея
    public function gallery() {
        $this->data['ad_script'] = '/assets/plugins/shufflejs/jquery.shuffle.min.js';
        return view('layouts.admin.gallery',$this->data);
    }

    //Управление меню
    public function menu() {
        $this->data['menu_items'] = Menu::all();
        //$this->data['parent']=DB::select('select id, caption, parent_id from menus');
        $this->data['parent'] = Menu::all();
        $this->data['ad_script'] = '/assets/js/ajax/menu.js';
        return view('layouts.admin.menu',$this->data);
    }

    //Создание/редактирование поста
    public function editPost($type, $slug){
        $this->data['categories'] = Taxonomy::where('type','category')->get();
        $this->data['tags'] = Taxonomy::where('type','tag')->get();
        $this->data['type_name'] ='';

        switch ($type) {
            case 'news':
                $this->data['type_name'] = 'новости';
                $this->data['type'] = 'news';
                break;
            case 'page':
                $this->data['type_name'] = 'страницы';
                $this->data['type'] = 'page';
                break;
        }
        if($slug != 'new'){
            $post = Post::where('slug', $slug)->first();
            $this->data['post'] = $post;
            $this->data['post_tags'] =  $post->taxonomy()->where('type','tag')->get()->toArray();
            $this->data['post_categories'] =  $post->taxonomy()->where('type','category')->get()->toArray();
            $this->data['thumbnail'] = $post->thumbnail();
        } else {
            $post = new Post();
            $post->type = $type;
            $post->title = 'Новая запись';
            $post->slug = 'new_page';
            $post->content = 'Введите текст страницы';
            $post->save();
            $post->slug = 'new_post_'+ $post->id;
            $post->save();
            return redirect('/admin/'.$type.'/edit/'.$post->slug);

        }
        $this->data['ad_script']='/assets/js/ajax/dropzone.js';
        return view('layouts.admin.editor', $this->data);
    }
    
    
    
    
    
    
    
   //Снятие с публикации 
   public function  UnpublishPost($id,$page){
     $post = Post::find($id);
     $post->status='drafted';
     $post->save();
   
        switch ($page)
      {
             case 'ShowAll':
              return redirect()->to('/admin/news/list/news');
               break;
              case 'ShowPub':
              return redirect()->to('/admin/news/pub/news');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/news/dell/news');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/news/druft/news');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
    
    }
    //Удалить
     public function  deletethePost($id,$page){
     $post = Post::find($id);
     $post->status='deleted';
     $post->save();
  
       switch ($page)
      {
              case 'ShowPub':
              return redirect()->to('/admin/news/pub/news');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/news/dell/news');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/news/druft/news');
                break;
                case 'ShowAll':
               return redirect()->to('/admin/news/list/news');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
    
    }
    
    //Опубликовать
     public function  publishPost($id,$page){
     $post = Post::find($id);
     $post->status='published';
     $post->save();
  
     switch ($page)
      {
              case 'ShowPub':
              return redirect()->to('/admin/news/pub/news');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/news/dell/news');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/news/druft/news');
                break;
                case 'ShowAll':
               return redirect()->to('/admin/news/list/news');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
  
        
    
    }
    
    
    
    
    
    
    //////////////////////////////////////////////////
    
    
    
    
    //Снятие с публикации 
   public function  UnpublishPostPage($id,$page){
     $post = Post::find($id);
     $post->status='drafted';
     $post->save();
   
        switch ($page)
      {
             case 'ShowAll':
              return redirect()->to('/admin/page/list');
               break;
              case 'ShowPub':
              return redirect()->to('/admin/page/pub/page');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/page/dell/page');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/page/druft/page');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
    
    }
    //Удалить
     public function  deletethePostPage($id,$page){
     $post = Post::find($id);
     $post->status='deleted';
     $post->save();
  
       switch ($page)
      {
              case 'ShowPub':
              return redirect()->to('/admin/page/pub/page');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/page/dell/page');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/page/druft/page');
                break;
                case 'ShowAll':
               return redirect()->to('/admin/page/list');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
    
    }
    
    //Опубликовать
     public function  publishPostPage($id,$page){
     $post = Post::find($id);
     $post->status='published';
     $post->save();
  
     switch ($page)
      {
              case 'ShowPub':
              return redirect()->to('/admin/page/pub/page');
               break;
               case 'ShowDell':
              return redirect()->to('/admin/page/dell/page');
                   break;
               case 'ShoDruft':
               return redirect()->to('/admin/page/druft/page');
                break;
                case 'ShowAll':
               return redirect()->to('/admin/page/list');
                break;
default:
$this->data['posts'] = Post::where('type', 'news')->where('status','!=', 'deleted')->orderBy('status')->get();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        return view('layouts/admin/list', $this->data);
break;
}
  
        
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    //Удаление поста
    public function deletePost($slug){
        $post = Post::where('slug', $slug)->first();
        $type = $post->type;
        if($post->delete()){
            return redirect('/admin/'.$type.'/list');
        }
    }

    //Список постов
    public function postsList($type){
            $this->data['posts'] = Post::where('type', $type)->where('status','!=', 'deleted')->orderBy('status')->get();
           
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Ppage']='ShowAll'; 
            $this->data['Type']=$type;
        return view('layouts/admin/list', $this->data);
        
    }
    
    
    
    
    
     public function ShowPub($type){
        
        
        
            $this->data['posts'] = Post::where('type', 'news')->where('status','published')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();       
            $this->data['Ppage']='ShowPub';
            $this->data['Type']=$type; 
            return view('layouts/admin/list', $this->data);
            
            
            
    }
    
     public function ShowDell($type){
            $this->data['posts'] = Post::where('type', 'news')->where('status','deleted')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Ppage']='ShowDell'; 
            $this->data['Type']=$type; 
        return view('layouts/admin/list', $this->data);
    }
     public function ShoDruft($type){
            $this->data['posts'] = Post::where('type', 'news')->where('status','drafted')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Ppage']='ShoDruft';
            $this->data['Type']=$type; 
        return view('layouts/admin/list', $this->data);
    }
    ///////////////////////////////////////////////////////////////////////////////PAGE
     public function ShowPubPage($type){
        
        
        
            $this->data['posts'] = Post::where('type', 'page')->where('status','published')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();       
            $this->data['Ppage']='ShowPub'; 
             $this->data['Type']=$type; 
            return view('layouts/admin/list', $this->data);
            
            
            
    }
    
     public function ShowDellPage($type){
            $this->data['posts'] = Post::where('type', 'page')->where('status','deleted')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Ppage']='ShowDell'; 
             $this->data['Type']=$type; 
        return view('layouts/admin/list', $this->data);
    }
     public function ShoDruftPage($type){
            $this->data['posts'] = Post::where('type', 'page')->where('status','drafted')->get();
            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Ppage']='ShoDruft';
             $this->data['Type']=$type; 
        return view('layouts/admin/list', $this->data);
    }
    
    
    
    
    

    public function postsCategory($category, $type,$Ppage){
        $this->data['tax'] = Taxonomy::where('slug',$category)->first();
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
         $this->data['Type']=$type;
         $this->data['Ppage']=$Ppage;
        $this->data['posts'] = Post::whereHas('taxonomy', function($query) use ($category){
            $query->where('slug', 'LIKE', $category);})->get();
        return view('layouts/admin/list', $this->data);
    }

    public function savePost(Request $request){
        Post::savePost($request);
        return redirect()->to('/admin/'.$request->type.'/list');
    }


    //Сохранение слайдов/картинок галереи
    public function saveImages(Request $request) {
        
        
         $final_width_of_image = 360; //Размер изображения которые Вы хотели бы получить (И ШИРИНА И ВЫСОТА)
   //Папка, куда будут загружаться полноразмерные изображения
   //Папка, куда будут загружаться миниатюры
  if(isset($_FILES['file'])) {
    
  if(preg_match('/[.](jpg)|(gif)|(png)$/', //Ставим допустимые форматы изображений для загрузки
  $_FILES['file']['name'])) {
        //Вынести в AjaxController!
        $file = $_FILES['file']['name'];
        $upDir = "../public/front/images/";
        $path_to_image_directory=$upDir;
        $test= explode('.', $file);
        $after=$test[1];

       $file=self::encodestring($test[0]);
        $slug=$request->parent_id."_".$request->type."_".$file.".".$after;
        $file=$upDir.$request->parent_id."_".$request->type."_".$file.".".$after;


      //  @copy($_FILES['file']['tmp_name'],$file);
       
        ///////////////////////////////////////////////////////////////////////////////
      
       
       //Записываем в базу
        $image = new Post();
        $image->title = $image->slug = $image->content =$image->slug_small=$slug;
        $image->type = $request->type;
        $image->parent_id = $request->parent_id;
        $image->save();
        
        
    
        
        
        
       
         
         
  
  
 // $filename = $_FILES['file']['name'];
  $filename=self::encodestring($test[0]);
  $filename=$request->parent_id."_".$request->type."_".$filename.".".$after;
  $source = $_FILES['file']['tmp_name']; 
  $target = $path_to_image_directory . $filename;
  
  move_uploaded_file($source, $target);
  
 $this-> createThumbnail($filename,$upDir,$final_width_of_image); 
  }else{
    
    //return redirect()->to('/admin/news/list');
    //die("Возникли проблемы! попробуйте снова!");
    return false;
    
  }
  
  
  
  
  
  }


 
        
        
      
 
}

        
        


    //Вынести в класс-предок
    public static function encodestring($str){
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',  'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        $str = strtr($str, $converter);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        return $str;
    }

    public function userSettings(){
        return view('layouts.admin.profile_settings', $this->data);
    }

    public function userUpdate($request){
        $user = User::find(Auth::user()->id);
        $user->updateUser($request);

    }

    public function registerAdmin(){
        //if(Auth::user()!= null && Auth::user()->role == 'Admin');

       
        return view('layouts.admin.reg_form', $this->data);
         
    }



 
public function createThumbnail($filename,$upDir,$final_width_of_image) {
    
 // require 'config.php'; //Подключаем файл конфигурации
  $path_to_thumbs_directory = "../public/front/thumbnail/";
  if(preg_match('/[.](jpg)$/', $filename)) {
  $im = imagecreatefromjpeg($upDir . $filename);
  } else if (preg_match('/[.](gif)$/', $filename)) {
  $im = imagecreatefromgif($upDir . $filename);
  } else if (preg_match('/[.](png)$/', $filename)) {
  $im = imagecreatefrompng($upDir . $filename);
  } //Определяем формат изображения
  
  $ox = imagesx($im);
  $oy = imagesy($im);
  
  $nx = $final_width_of_image;
  $ny = floor($oy * ($final_width_of_image / $ox));
  
  $nm = imagecreatetruecolor($nx, $ny);
  
  imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
  
  if(!file_exists($path_to_thumbs_directory)) {
  if(!mkdir($path_to_thumbs_directory)) {
  die("Возникли проблемы! попробуйте снова!");
  } 
  }
 imagejpeg($nm, $path_to_thumbs_directory . $filename);
 // $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
 // $tn .= '<br />Поздравляем! Ваше изображение успешно загружено и его миниатюра удачно выполнена. Выше Вы можете просмотреть результат:';
 // echo $tn;
  }//Сжимаем изображение, если есть оишибки, то говорим о них, если их нет, то выводим получившуюся миниатюру


    
    
    
    
}






