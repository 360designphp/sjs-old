<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
     
     
    protected $redirectTo = '/admin/news/list';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }




    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required|max:10',
        ]);
        
       
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        
        
        
        
    $pname=DB::select('select name from users where role = :cn', ['cn' =>'Admin']);
	if(count($pname)>0){
    
      return  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role'=>$data['role'],
        ]);
       
       }
      else{
        
      return  User::create([
            'name' => $data['name'],
           'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role'=>'Admin',
        ]);
        
        
       }
       
       
   }
    
    /*
    protected function createAdmin(Request $request)
    {
          $role = $request->input('role');
		 $email= $request->input('email');
		 $name= $request->input('name');
		 $password= $request->input('password');
		 $password= bcrypt($password);
  $pname=DB::select('select * from users where email = ?' , [$email]);
  if(count($pname)>0){
$data=array('message'=>'����������� � ����� email �������');

return view('layouts.admin.index',$data);
 }
  else{
$userAdd=DB::insert("insert into users(`name`,`password`,`email`,`role`) values(?,?,?,?)",
 [$name,$password,$email,$role]);



	return view('layouts.admin.index');

  }
       
    }
    */
   
    
}
