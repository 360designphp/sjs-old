<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;


class Post extends Model
{
    protected $fillable = ['type', 'title', 'slug', 'content', 'parent_id'];
    protected $softDelete = true;

    //Возвращает категории и теги к которым относится пост
    public function taxonomy() {
        return $this->belongsToMany('App\Taxonomy');
    }

    //Возвращает дочерние посты
    public function childs() {
        return $this->where('parent_id', $this->id);
    }

    //Возвращает родительский пост
    public function parent() {
        return $this->find($this->parent_id);
    }

    //Получаем миниатюру изображения
    public function thumbnail() {
        return $this->childs()->where('type', 'thumbnail')->first();
    }

    //получаем все слайды поста
    public function slides() {
        return $this->childs()->where('type', 'slide')->get();
    }

    //Получаем все изображения для галереи поста
    public function gallery() {
        return $this->childs()->where('type', 'gallery')->get();
    }
    //Получаем все документы поста
      public function documents() {
        return $this->childs()->where('type', 'document')->get();
    }

    //Сохраняем пост
    public static function savePost(Request $request){
        $save = false;
        if($request->id == ''){
            $post = new Post();
            $save = true;
        } else {
            $post = Post::find($request->id);
        }
        $post->type = $request->type;
        $post->title = $request->title;
        if($request->slug == null){
            $post->slug = self::encodestring($request->title);
        }else {
            $post->slug = self::encodestring($request->slug);
        }
        $post->content = $request->text;
        $post->parent_id = $request->parent_id;
        $post->youtube_link = $request->youtube_link;
        if(isset($request->taxonomy)) {
            $post->taxonomy()->sync($request->taxonomy);
        }

        if($save){
            $post->save();
        } else {
            $post->update();

        }
        //Создаем/изменяем пост миниатюры
        if(preg_match('/[.](jpg)|(gif)|(png)$/',$_FILES['thumbnail']['name'])) {
        if(!empty($_FILES['thumbnail']['tmp_name'])){
            $thumb = $post->childs()->where('type','thumbnail')->first();
            $save = false;
            if($thumb == ''){
                $thumb = new Post();
                $save = true;
            }
            
            
            
            $filename = $_FILES['thumbnail']['name'];
            $test= explode('.', $filename);
            $after=$test[1];
            $test=self::encodestring($test[0]);

            $thumbslug=$post->id."_thumbnail_".$test.".".$after;
            $upDir = "../public/front/images/";
           ////////////////////////////////////////////
            $final_width_of_image=360;
           
           $filenameTH=$post->id."_thumbnail_".$test.".".$after;
            
           $ThumbDir = "../public/front/thumbnail/";
           ////////////////////////////////////////////
        $path_to_image_directory=$upDir;
            $filename = $upDir.$post->id."_thumbnail_".$test.".".$after;
            if($thumb->slug != ''){
                @unlink($upDir.$thumb->slug);
                @unlink($ThumbDir.$thumb->slug);
            }
          @copy($_FILES['thumbnail']['tmp_name'],$filename);
           
            $post-> createThumbnail($filenameTH,$upDir,$final_width_of_image);
            
            $thumb->content = ($_FILES['thumbnail']['name']);
            $thumb->type = 'thumbnail';
            $thumb->title = $_FILES['thumbnail']['name'] + $post->id;
            $thumb->slug = $thumbslug;
            $thumb->slug_small=$filenameTH;
            $thumb->parent_id = $post->id;
            if($save) {
                $thumb->save();
            } else {
                $thumb->update();
            }
        }
        
        }

    }

    //
    public function deleteImage() {
        $upDir = "../public/front/images/";
         $upDirTH = "../public/front/thumbnail/";
        $filename = $this->slug;
        $this->delete();

        unlink($upDir.$filename);
         unlink($upDirTH.$filename);
       
    }

    public static function encodestring($str){
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '',  'ы' => 'y',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        $str = strtr($str, $converter);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, '-');
        return $str;
    }



public function createThumbnail($filenameTH,$upDir,$final_width_of_image) {
    
 $path_to_thumbs_directory = "../public/front/thumbnail/";
  if(preg_match('/[.](jpg)$/', $filenameTH)) {
  $im = imagecreatefromjpeg($upDir . $filenameTH);
  } else if (preg_match('/[.](gif)$/', $filenameTH)) {
  $im = imagecreatefromgif($upDir . $filenameTH);
  } else if (preg_match('/[.](png)$/', $filenameTH)) {
  $im = imagecreatefrompng($upDir . $filenameTH);
  } //Определяем формат изображения
  
  $ox = imagesx($im);
  $oy = imagesy($im);
  
  $nx = $final_width_of_image;
  $ny = floor($oy * ($final_width_of_image / $ox));
  
  $nm = imagecreatetruecolor($nx, $ny);
  
  imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
  
  if(!file_exists($path_to_thumbs_directory)) {
  if(!mkdir($path_to_thumbs_directory)) {
  die("Возникли проблемы! попробуйте снова!");
  } 
  }
 imagejpeg($nm, $path_to_thumbs_directory . $filenameTH);
 // $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
 // $tn .= '<br />Поздравляем! Ваше изображение успешно загружено и его миниатюра удачно выполнена. Выше Вы можете просмотреть результат:';
 // echo $tn;
  }//Сжимаем изображение, если есть оишибки, то говорим о них, если их нет, то выводим получившуюся миниатюру








}
