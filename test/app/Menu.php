<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Menu extends Model
{
    protected $fillable = ['caption', 'link', 'zone', 'weight', 'parent_id', 'visible'];

    public function childs() {
        return $this->where('parent_id', $this->id)->get();
    }

    public function parent() {
        return $this->find($this->parent_id)->first();
    }

    public static function getMenu($zone) {
        //where('visible', true)
        return Menu::where('zone', $zone)->orderBy('weight')->get();
    }
    public function updateItem(Request $request) {
        $this->caption = $request->caption;
        $this->link = $request->link;
        $this->zone = $request->zone;
        $this->weight = $request->weight;
        $this->visible = $request->visible;
        $this->parent_id = $request->parent;
        $this->update();

    }
}
