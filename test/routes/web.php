<?php
use App\Settings;
use App\Post;
use App\Relation;

Route::get('/', ['uses' => 'HomeController@getHomepage','as'=>'main']);

//Список новостей
Route::get('/news',  ['uses' =>'HomeController@getNewsList','as'=>'news']);

//Определенная новость
Route::get('/news/{slug}',  ['uses' =>'HomeController@getNewsSingle']);

//Галерея
Route::get('/gallery', function () {
    return view('layouts.front.news_list');
});

Route::get('/logout',function(){
  Auth::logout();
  return View('auth/login'); 
});


//Список страниц
Route::get('/pages', function () {
    return view('layouts.front.news_list');
});

//Поиск
Route::post('/search/', ['uses' => 'HomeController@searchPost']);

//Определенная страница
Route::get('/page/{slug}', ['uses' =>'HomeController@getPage']);

//Список категорий
Route::get('/category/{category}', ['uses' =>'HomeController@getCategory']);
Route::post('/register/admin', ['uses' =>'regController@setUser']);
Auth::routes();

/*
Route::get('all/register',['uses' =>'AdminController@registerAdmin'],function(){
    
    return view('layouts.admin.reg_form');
});

Route::get('/regfromadmin',['uses' =>'regController@setUserForm']);
Route::post('/setuserfromadmin',['uses' =>'regController@setUser']);
*/

Route::get('/home', 'HomeController@index');

/*---------РОУТЫ АДМИН-ПАНЕЛИ------*/


//Todo: Вынести в контроллер
//Route::get('/registeradmin', ['uses' => 'AdminController@registerAdmin']);

Route::group(['prefix'=>'admin','middleware'=>'auth'], function() {
// Главная страница панели
    Route::get('/', function () {
        return redirect()->to('/admin/news/list');
    });


// Настройки пользователя
    Route::get('/profile_settings',['uses' => 'AdminController@userSettings']);

//Сохраняет настройки авторизованного пользователя
    Route::post('/update_user',['uses' => 'AdminController@userUpdate']);


//Создание/редактирование поста
    Route::get('/{type}/edit/{slug}', ['uses' => 'AdminController@editPost']);

//Регистрация администратора
    Route::get('/register', ['uses' => 'AdminController@registerAdmin']);

//Список постов без постов-фотографий
    Route::get('/{type}/list', ['uses' => 'AdminController@postsList']);

//Список постов определенной категории
    Route::get('/category/{slug}/{type}/{Ppage}', ['uses' => 'AdminController@postsCategory']);

//Управление меню
    Route::get('/menu', ['uses' => 'AdminController@menu']);

//Список постов-фотографий
    Route::get('/gallery', ['uses' => 'AdminController@gallery']);

//Сохранение постов
    Route::post('/save_post', ['uses' => 'AdminController@savePost']);

//Удаление постов
    Route::get('/delete_post/{slug}', ['uses' => 'AdminController@deletePost']);

//Редактирование настроек
    Route::get('/settings', ['uses' => 'AdminController@settings']);

 //Сохранение изображений (удалить?)
    Route::post('/save_images', ['uses' => 'AdminController@saveImages']);




Route::get('/news/pub/{Type}',['uses' => 'AdminController@ShowPub']);
Route::get('/news/dell/{Type}',['uses' => 'AdminController@ShowDell']);
Route::get('/news/druft/{Type}',['uses' => 'AdminController@ShoDruft']);

Route::get('/news/UnpublishPost/{id}/{page}',['uses' => 'AdminController@UnpublishPost']);
Route::get('/news/deletethePost/{id}/{page}',['uses' => 'AdminController@deletethePost']);
Route::get('/news/publishPost/{id}/{page}',['uses' => 'AdminController@publishPost']);


Route::get('/page/UnpublishPost/{id}/{page}',['uses' => 'AdminController@UnpublishPostPage']);
Route::get('/page/deletethePost/{id}/{page}',['uses' => 'AdminController@deletethePostPage']);
Route::get('/page/publishPost/{id}/{page}',['uses' => 'AdminController@publishPostPage']);






Route::get('/page/pub/{Type}',['uses' => 'AdminController@ShowPubPage']);
Route::get('/page/dell/{Type}',['uses' => 'AdminController@ShowDellPage']);
Route::get('/page/druft/{Type}',['uses' => 'AdminController@ShoDruftPage']);





});