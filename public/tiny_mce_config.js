tinymce.init({
	selector: ".editor",
	theme: "modern",
	language : 'ru',
	file_browser_callback : elFinderBrowser,
	relative_urls : false,
	convert_urls : false,
	remove_script_host : false,
	autosave_ask_before_unload: false,
	height : 180,
	width: "100%", 
	extended_valid_elements : 'object[width|height|classid|codebase|embed|param],param[name|value],embed[param|src|type|width|height|flashvars|wmode],textarea,div[*],span[*],p[*],input[*]',
	plugins: [
		"advlist autolink lists link image charmap print preview anchor",
		"searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste"
	],
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertdatetime link image media fullpage | forecolor backcolor emoticons | code fullscreen", 
	style_formats: [
		{title: 'Красный текст', inline: 'span', styles: {color: '#ff0000'}},
		{title: 'Красный заголовок', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'H1', block: 'h1',},
        {title: 'H2', block: 'h2',},
        {title: 'H3', block: 'h3',},
        {title: 'H4', block: 'h4',},
        {title: 'H5', block: 'h5',},
        {title: 'H6', block: 'h6',},
	]
}); 

function elFinderBrowser (field_name, url, type, win) {
	tinymce.activeEditor.windowManager.open({
		file: '/cp/elfinder/elfinder.htm?remote='+document.body.getAttribute('remote')+'&data-host='+document.body.getAttribute('data-host'),// use an absolute path!
		title: 'Выбрать файл',
		width: 900,  
		height: 450,
		resizable: 'yes'
	}, {
		setUrl: function (url) {
			win.document.getElementById(field_name).value = url;
		}
	});
	return false;
}