$('.press-tab').on('shown.bs.tab', function (e) {

    if ($('.slick_press.slick-initialized').length == 0) {
            $('.slick_press').slick({
            dots: false,
            speed: 300,
            adaptiveHeight: true,
            arrows: true,
            slidesToShow: 1,
            infinite: true
        });
    }

});