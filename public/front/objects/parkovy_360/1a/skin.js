// Garden Gnome Software - Skin
// Pano2VR 5.2.0/15969
// Filename: ???????? 3D.ggsk
// Generated Сб авг 11 15:21:03 2018

function pano2vrSkin(player,base) {
	var ggSkinVars = [];
	ggSkinVars['ht_ani'] = false;
	var me=this;
	var flag=false;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=me.player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		this.__24=document.createElement('div');
		this.__24.ggId="\u0417\u0430\u043b";
		this.__24.ggLeft=-144;
		this.__24.ggTop=-81;
		this.__24.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__24.ggVisible=false;
		this.__24.className='ggskin ggskin_container ';
		this.__24.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__24.setAttribute('style',hs);
		this.__24.style[domTransform + 'Origin']='50% 50%';
		me.__24.ggIsActive=function() {
			return false;
		}
		me.__24.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__24.ggCurrentLogicStateVisible = -1;
		this.__24.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Зал") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__24.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__24.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__24.style[domTransition]='';
				if (me.__24.ggCurrentLogicStateVisible == 0) {
					me.__24.style.visibility=(Number(me.__24.style.opacity)>0||!me.__24.style.opacity)?'inherit':'hidden';
					me.__24.ggVisible=true;
				}
				else {
					me.__24.style.visibility="hidden";
					me.__24.ggVisible=false;
				}
			}
		}
		this.__24.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__24.ggNodeChange=function () {
			me.__24.ggUpdateConditionNodeChange();
		}
		this.__28=document.createElement('div');
		this.__28__img=document.createElement('img');
		this.__28__img.className='ggskin ggskin_button';
		this.__28__img.setAttribute('src',basePath + 'images/_28.png');
		this.__28__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__28__img.className='ggskin ggskin_button';
		this.__28__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__28__img);
		this.__28.appendChild(this.__28__img);
		this.__28.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__28.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__28.ggVisible=true;
		this.__28.className='ggskin ggskin_button ';
		this.__28.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__28.setAttribute('style',hs);
		this.__28.style[domTransform + 'Origin']='50% 50%';
		me.__28.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__28.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__28.onclick=function (e) {
			me.player.openNext("{node8}","$cur");
		}
		this.__28.onmouseover=function (e) {
			me.elementMouseOver['_28']=true;
		}
		this.__28.onmouseout=function (e) {
			me.__28.style[domTransition]='none';
			me.__28.style.opacity='0.5';
			me.__28.style.visibility=me.__28.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_28']=false;
		}
		this.__28.ontouchend=function (e) {
			me.elementMouseOver['_28']=false;
		}
		this.__28.ggUpdatePosition=function (useTransition) {
		}
		this.__24.appendChild(this.__28);
		this.__27=document.createElement('div');
		this.__27__img=document.createElement('img');
		this.__27__img.className='ggskin ggskin_button';
		this.__27__img.setAttribute('src',basePath + 'images/_27.png');
		this.__27__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__27__img.className='ggskin ggskin_button';
		this.__27__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__27__img);
		this.__27.appendChild(this.__27__img);
		this.__27.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__27.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__27.ggVisible=true;
		this.__27.className='ggskin ggskin_button ';
		this.__27.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__27.setAttribute('style',hs);
		this.__27.style[domTransform + 'Origin']='50% 50%';
		me.__27.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__27.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__27.onclick=function (e) {
			me.player.openNext("{node6}","$cur");
		}
		this.__27.onmouseover=function (e) {
			me.elementMouseOver['_27']=true;
		}
		this.__27.onmouseout=function (e) {
			me.__27.style[domTransition]='none';
			me.__27.style.opacity='0.5';
			me.__27.style.visibility=me.__27.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_27']=false;
		}
		this.__27.ontouchend=function (e) {
			me.elementMouseOver['_27']=false;
		}
		this.__27.ggUpdatePosition=function (useTransition) {
		}
		this.__24.appendChild(this.__27);
		this.__26=document.createElement('div');
		this.__26__img=document.createElement('img');
		this.__26__img.className='ggskin ggskin_button';
		this.__26__img.setAttribute('src',basePath + 'images/_26.png');
		this.__26__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__26__img.className='ggskin ggskin_button';
		this.__26__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__26__img);
		this.__26.appendChild(this.__26__img);
		this.__26.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__26.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__26.ggVisible=true;
		this.__26.className='ggskin ggskin_button ';
		this.__26.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__26.setAttribute('style',hs);
		this.__26.style[domTransform + 'Origin']='50% 50%';
		me.__26.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__26.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__26.onclick=function (e) {
			me.player.openNext("{node7}","$cur");
		}
		this.__26.onmouseover=function (e) {
			me.elementMouseOver['_26']=true;
		}
		this.__26.onmouseout=function (e) {
			me.__26.style[domTransition]='none';
			me.__26.style.opacity='0.5';
			me.__26.style.visibility=me.__26.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_26']=false;
		}
		this.__26.ontouchend=function (e) {
			me.elementMouseOver['_26']=false;
		}
		this.__26.ggUpdatePosition=function (useTransition) {
		}
		this.__24.appendChild(this.__26);
		this.__25=document.createElement('div');
		this.__25__img=document.createElement('img');
		this.__25__img.className='ggskin ggskin_button';
		this.__25__img.setAttribute('src',basePath + 'images/_25.png');
		this.__25__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__25__img.className='ggskin ggskin_button';
		this.__25__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__25__img);
		this.__25.appendChild(this.__25__img);
		this.__25.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__25.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__25.ggVisible=true;
		this.__25.className='ggskin ggskin_button ';
		this.__25.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__25.setAttribute('style',hs);
		this.__25.style[domTransform + 'Origin']='50% 50%';
		me.__25.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__25.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__25.onclick=function (e) {
			me.player.openNext("{node5}","$cur");
		}
		this.__25.onmouseover=function (e) {
			me.elementMouseOver['_25']=true;
		}
		this.__25.onmouseout=function (e) {
			me.__25.style[domTransition]='none';
			me.__25.style.opacity='0.5';
			me.__25.style.visibility=me.__25.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_25']=false;
		}
		this.__25.ontouchend=function (e) {
			me.elementMouseOver['_25']=false;
		}
		this.__25.ggUpdatePosition=function (useTransition) {
		}
		this.__24.appendChild(this.__25);
		this.divSkin.appendChild(this.__24);
		this.__19=document.createElement('div');
		this.__19.ggId="\u041f\u0440\u0438\u0445\u043e\u0436\u0430\u044f";
		this.__19.ggLeft=-144;
		this.__19.ggTop=-81;
		this.__19.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__19.ggVisible=false;
		this.__19.className='ggskin ggskin_container ';
		this.__19.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__19.setAttribute('style',hs);
		this.__19.style[domTransform + 'Origin']='50% 50%';
		me.__19.ggIsActive=function() {
			return false;
		}
		me.__19.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__19.ggCurrentLogicStateVisible = -1;
		this.__19.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Прихожая") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__19.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__19.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__19.style[domTransition]='';
				if (me.__19.ggCurrentLogicStateVisible == 0) {
					me.__19.style.visibility=(Number(me.__19.style.opacity)>0||!me.__19.style.opacity)?'inherit':'hidden';
					me.__19.ggVisible=true;
				}
				else {
					me.__19.style.visibility="hidden";
					me.__19.ggVisible=false;
				}
			}
		}
		this.__19.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__19.ggNodeChange=function () {
			me.__19.ggUpdateConditionNodeChange();
		}
		this.__23=document.createElement('div');
		this.__23__img=document.createElement('img');
		this.__23__img.className='ggskin ggskin_button';
		this.__23__img.setAttribute('src',basePath + 'images/_23.png');
		this.__23__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__23__img.className='ggskin ggskin_button';
		this.__23__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__23__img);
		this.__23.appendChild(this.__23__img);
		this.__23.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__23.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__23.ggVisible=true;
		this.__23.className='ggskin ggskin_button ';
		this.__23.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__23.setAttribute('style',hs);
		this.__23.style[domTransform + 'Origin']='50% 50%';
		me.__23.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__23.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__23.onclick=function (e) {
			me.player.openNext("{node20}","$cur");
		}
		this.__23.onmouseover=function (e) {
			me.elementMouseOver['_23']=true;
		}
		this.__23.onmouseout=function (e) {
			me.__23.style[domTransition]='none';
			me.__23.style.opacity='0.5';
			me.__23.style.visibility=me.__23.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_23']=false;
		}
		this.__23.ontouchend=function (e) {
			me.elementMouseOver['_23']=false;
		}
		this.__23.ggUpdatePosition=function (useTransition) {
		}
		this.__19.appendChild(this.__23);
		this.__22=document.createElement('div');
		this.__22__img=document.createElement('img');
		this.__22__img.className='ggskin ggskin_button';
		this.__22__img.setAttribute('src',basePath + 'images/_22.png');
		this.__22__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__22__img.className='ggskin ggskin_button';
		this.__22__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__22__img);
		this.__22.appendChild(this.__22__img);
		this.__22.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__22.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__22.ggVisible=true;
		this.__22.className='ggskin ggskin_button ';
		this.__22.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__22.setAttribute('style',hs);
		this.__22.style[domTransform + 'Origin']='50% 50%';
		me.__22.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__22.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__22.onclick=function (e) {
			me.player.openNext("{node18}","$cur");
		}
		this.__22.onmouseover=function (e) {
			me.elementMouseOver['_22']=true;
		}
		this.__22.onmouseout=function (e) {
			me.__22.style[domTransition]='none';
			me.__22.style.opacity='0.5';
			me.__22.style.visibility=me.__22.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_22']=false;
		}
		this.__22.ontouchend=function (e) {
			me.elementMouseOver['_22']=false;
		}
		this.__22.ggUpdatePosition=function (useTransition) {
		}
		this.__19.appendChild(this.__22);
		this.__21=document.createElement('div');
		this.__21__img=document.createElement('img');
		this.__21__img.className='ggskin ggskin_button';
		this.__21__img.setAttribute('src',basePath + 'images/_21.png');
		this.__21__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__21__img.className='ggskin ggskin_button';
		this.__21__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__21__img);
		this.__21.appendChild(this.__21__img);
		this.__21.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__21.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__21.ggVisible=true;
		this.__21.className='ggskin ggskin_button ';
		this.__21.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__21.setAttribute('style',hs);
		this.__21.style[domTransform + 'Origin']='50% 50%';
		me.__21.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__21.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__21.onclick=function (e) {
			me.player.openNext("{node19}","$cur");
		}
		this.__21.onmouseover=function (e) {
			me.elementMouseOver['_21']=true;
		}
		this.__21.onmouseout=function (e) {
			me.__21.style[domTransition]='none';
			me.__21.style.opacity='0.5';
			me.__21.style.visibility=me.__21.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_21']=false;
		}
		this.__21.ontouchend=function (e) {
			me.elementMouseOver['_21']=false;
		}
		this.__21.ggUpdatePosition=function (useTransition) {
		}
		this.__19.appendChild(this.__21);
		this.__20=document.createElement('div');
		this.__20__img=document.createElement('img');
		this.__20__img.className='ggskin ggskin_button';
		this.__20__img.setAttribute('src',basePath + 'images/_20.png');
		this.__20__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__20__img.className='ggskin ggskin_button';
		this.__20__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__20__img);
		this.__20.appendChild(this.__20__img);
		this.__20.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__20.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__20.ggVisible=true;
		this.__20.className='ggskin ggskin_button ';
		this.__20.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__20.setAttribute('style',hs);
		this.__20.style[domTransform + 'Origin']='50% 50%';
		me.__20.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__20.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__20.onclick=function (e) {
			me.player.openNext("{node17}","$cur");
		}
		this.__20.onmouseover=function (e) {
			me.elementMouseOver['_20']=true;
		}
		this.__20.onmouseout=function (e) {
			me.__20.style[domTransition]='none';
			me.__20.style.opacity='0.5';
			me.__20.style.visibility=me.__20.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_20']=false;
		}
		this.__20.ontouchend=function (e) {
			me.elementMouseOver['_20']=false;
		}
		this.__20.ggUpdatePosition=function (useTransition) {
		}
		this.__19.appendChild(this.__20);
		this.divSkin.appendChild(this.__19);
		this.__14=document.createElement('div');
		this.__14.ggId="\u041a\u0443\u0445\u043d\u044f";
		this.__14.ggLeft=-144;
		this.__14.ggTop=-81;
		this.__14.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__14.ggVisible=false;
		this.__14.className='ggskin ggskin_container ';
		this.__14.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__14.setAttribute('style',hs);
		this.__14.style[domTransform + 'Origin']='50% 50%';
		me.__14.ggIsActive=function() {
			return false;
		}
		me.__14.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__14.ggCurrentLogicStateVisible = -1;
		this.__14.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Кухня") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__14.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__14.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__14.style[domTransition]='';
				if (me.__14.ggCurrentLogicStateVisible == 0) {
					me.__14.style.visibility=(Number(me.__14.style.opacity)>0||!me.__14.style.opacity)?'inherit':'hidden';
					me.__14.ggVisible=true;
				}
				else {
					me.__14.style.visibility="hidden";
					me.__14.ggVisible=false;
				}
			}
		}
		this.__14.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__14.ggNodeChange=function () {
			me.__14.ggUpdateConditionNodeChange();
		}
		this.__18=document.createElement('div');
		this.__18__img=document.createElement('img');
		this.__18__img.className='ggskin ggskin_button';
		this.__18__img.setAttribute('src',basePath + 'images/_18.png');
		this.__18__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__18__img.className='ggskin ggskin_button';
		this.__18__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__18__img);
		this.__18.appendChild(this.__18__img);
		this.__18.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__18.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__18.ggVisible=true;
		this.__18.className='ggskin ggskin_button ';
		this.__18.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__18.setAttribute('style',hs);
		this.__18.style[domTransform + 'Origin']='50% 50%';
		me.__18.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__18.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__18.onclick=function (e) {
			me.player.openNext("{node12}","$cur");
		}
		this.__18.onmouseover=function (e) {
			me.elementMouseOver['_18']=true;
		}
		this.__18.onmouseout=function (e) {
			me.__18.style[domTransition]='none';
			me.__18.style.opacity='0.5';
			me.__18.style.visibility=me.__18.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_18']=false;
		}
		this.__18.ontouchend=function (e) {
			me.elementMouseOver['_18']=false;
		}
		this.__18.ggUpdatePosition=function (useTransition) {
		}
		this.__14.appendChild(this.__18);
		this.__17=document.createElement('div');
		this.__17__img=document.createElement('img');
		this.__17__img.className='ggskin ggskin_button';
		this.__17__img.setAttribute('src',basePath + 'images/_17.png');
		this.__17__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__17__img.className='ggskin ggskin_button';
		this.__17__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__17__img);
		this.__17.appendChild(this.__17__img);
		this.__17.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__17.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__17.ggVisible=true;
		this.__17.className='ggskin ggskin_button ';
		this.__17.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__17.setAttribute('style',hs);
		this.__17.style[domTransform + 'Origin']='50% 50%';
		me.__17.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__17.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__17.onclick=function (e) {
			me.player.openNext("{node10}","$cur");
		}
		this.__17.onmouseover=function (e) {
			me.elementMouseOver['_17']=true;
		}
		this.__17.onmouseout=function (e) {
			me.__17.style[domTransition]='none';
			me.__17.style.opacity='0.5';
			me.__17.style.visibility=me.__17.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_17']=false;
		}
		this.__17.ontouchend=function (e) {
			me.elementMouseOver['_17']=false;
		}
		this.__17.ggUpdatePosition=function (useTransition) {
		}
		this.__14.appendChild(this.__17);
		this.__16=document.createElement('div');
		this.__16__img=document.createElement('img');
		this.__16__img.className='ggskin ggskin_button';
		this.__16__img.setAttribute('src',basePath + 'images/_16.png');
		this.__16__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__16__img.className='ggskin ggskin_button';
		this.__16__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__16__img);
		this.__16.appendChild(this.__16__img);
		this.__16.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__16.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__16.ggVisible=true;
		this.__16.className='ggskin ggskin_button ';
		this.__16.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__16.setAttribute('style',hs);
		this.__16.style[domTransform + 'Origin']='50% 50%';
		me.__16.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__16.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__16.onclick=function (e) {
			me.player.openNext("{node11}","$cur");
		}
		this.__16.onmouseover=function (e) {
			me.elementMouseOver['_16']=true;
		}
		this.__16.onmouseout=function (e) {
			me.__16.style[domTransition]='none';
			me.__16.style.opacity='0.5';
			me.__16.style.visibility=me.__16.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_16']=false;
		}
		this.__16.ontouchend=function (e) {
			me.elementMouseOver['_16']=false;
		}
		this.__16.ggUpdatePosition=function (useTransition) {
		}
		this.__14.appendChild(this.__16);
		this.__15=document.createElement('div');
		this.__15__img=document.createElement('img');
		this.__15__img.className='ggskin ggskin_button';
		this.__15__img.setAttribute('src',basePath + 'images/_15.png');
		this.__15__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__15__img.className='ggskin ggskin_button';
		this.__15__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__15__img);
		this.__15.appendChild(this.__15__img);
		this.__15.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__15.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__15.ggVisible=true;
		this.__15.className='ggskin ggskin_button ';
		this.__15.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__15.setAttribute('style',hs);
		this.__15.style[domTransform + 'Origin']='50% 50%';
		me.__15.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__15.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__15.onclick=function (e) {
			me.player.openNext("{node9}","$cur");
		}
		this.__15.onmouseover=function (e) {
			me.elementMouseOver['_15']=true;
		}
		this.__15.onmouseout=function (e) {
			me.__15.style[domTransition]='none';
			me.__15.style.opacity='0.5';
			me.__15.style.visibility=me.__15.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_15']=false;
		}
		this.__15.ontouchend=function (e) {
			me.elementMouseOver['_15']=false;
		}
		this.__15.ggUpdatePosition=function (useTransition) {
		}
		this.__14.appendChild(this.__15);
		this.divSkin.appendChild(this.__14);
		this.__9=document.createElement('div');
		this.__9.ggId="\u041b\u043e\u0434\u0436\u0438\u044f";
		this.__9.ggLeft=-144;
		this.__9.ggTop=-81;
		this.__9.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__9.ggVisible=false;
		this.__9.className='ggskin ggskin_container ';
		this.__9.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__9.setAttribute('style',hs);
		this.__9.style[domTransform + 'Origin']='50% 50%';
		me.__9.ggIsActive=function() {
			return false;
		}
		me.__9.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__9.ggCurrentLogicStateVisible = -1;
		this.__9.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Лоджия") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__9.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__9.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__9.style[domTransition]='';
				if (me.__9.ggCurrentLogicStateVisible == 0) {
					me.__9.style.visibility=(Number(me.__9.style.opacity)>0||!me.__9.style.opacity)?'inherit':'hidden';
					me.__9.ggVisible=true;
				}
				else {
					me.__9.style.visibility="hidden";
					me.__9.ggVisible=false;
				}
			}
		}
		this.__9.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__9.ggNodeChange=function () {
			me.__9.ggUpdateConditionNodeChange();
		}
		this.__13=document.createElement('div');
		this.__13__img=document.createElement('img');
		this.__13__img.className='ggskin ggskin_button';
		this.__13__img.setAttribute('src',basePath + 'images/_13.png');
		this.__13__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__13__img.className='ggskin ggskin_button';
		this.__13__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__13__img);
		this.__13.appendChild(this.__13__img);
		this.__13.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__13.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__13.ggVisible=true;
		this.__13.className='ggskin ggskin_button ';
		this.__13.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__13.setAttribute('style',hs);
		this.__13.style[domTransform + 'Origin']='50% 50%';
		me.__13.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__13.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__13.onclick=function (e) {
			me.player.openNext("{node16}","$cur");
		}
		this.__13.onmouseover=function (e) {
			me.elementMouseOver['_13']=true;
		}
		this.__13.onmouseout=function (e) {
			me.__13.style[domTransition]='none';
			me.__13.style.opacity='0.5';
			me.__13.style.visibility=me.__13.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_13']=false;
		}
		this.__13.ontouchend=function (e) {
			me.elementMouseOver['_13']=false;
		}
		this.__13.ggUpdatePosition=function (useTransition) {
		}
		this.__9.appendChild(this.__13);
		this.__12=document.createElement('div');
		this.__12__img=document.createElement('img');
		this.__12__img.className='ggskin ggskin_button';
		this.__12__img.setAttribute('src',basePath + 'images/_12.png');
		this.__12__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__12__img.className='ggskin ggskin_button';
		this.__12__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__12__img);
		this.__12.appendChild(this.__12__img);
		this.__12.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__12.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__12.ggVisible=true;
		this.__12.className='ggskin ggskin_button ';
		this.__12.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__12.setAttribute('style',hs);
		this.__12.style[domTransform + 'Origin']='50% 50%';
		me.__12.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__12.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__12.onclick=function (e) {
			me.player.openNext("{node14}","$cur");
		}
		this.__12.onmouseover=function (e) {
			me.elementMouseOver['_12']=true;
		}
		this.__12.onmouseout=function (e) {
			me.__12.style[domTransition]='none';
			me.__12.style.opacity='0.5';
			me.__12.style.visibility=me.__12.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_12']=false;
		}
		this.__12.ontouchend=function (e) {
			me.elementMouseOver['_12']=false;
		}
		this.__12.ggUpdatePosition=function (useTransition) {
		}
		this.__9.appendChild(this.__12);
		this.__11=document.createElement('div');
		this.__11__img=document.createElement('img');
		this.__11__img.className='ggskin ggskin_button';
		this.__11__img.setAttribute('src',basePath + 'images/_11.png');
		this.__11__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__11__img.className='ggskin ggskin_button';
		this.__11__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__11__img);
		this.__11.appendChild(this.__11__img);
		this.__11.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__11.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__11.ggVisible=true;
		this.__11.className='ggskin ggskin_button ';
		this.__11.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__11.setAttribute('style',hs);
		this.__11.style[domTransform + 'Origin']='50% 50%';
		me.__11.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__11.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__11.onclick=function (e) {
			me.player.openNext("{node15}","$cur");
		}
		this.__11.onmouseover=function (e) {
			me.elementMouseOver['_11']=true;
		}
		this.__11.onmouseout=function (e) {
			me.__11.style[domTransition]='none';
			me.__11.style.opacity='0.5';
			me.__11.style.visibility=me.__11.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_11']=false;
		}
		this.__11.ontouchend=function (e) {
			me.elementMouseOver['_11']=false;
		}
		this.__11.ggUpdatePosition=function (useTransition) {
		}
		this.__9.appendChild(this.__11);
		this.__10=document.createElement('div');
		this.__10__img=document.createElement('img');
		this.__10__img.className='ggskin ggskin_button';
		this.__10__img.setAttribute('src',basePath + 'images/_10.png');
		this.__10__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__10__img.className='ggskin ggskin_button';
		this.__10__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__10__img);
		this.__10.appendChild(this.__10__img);
		this.__10.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__10.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__10.ggVisible=true;
		this.__10.className='ggskin ggskin_button ';
		this.__10.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__10.setAttribute('style',hs);
		this.__10.style[domTransform + 'Origin']='50% 50%';
		me.__10.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__10.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__10.onclick=function (e) {
			me.player.openNext("{node13}","$cur");
		}
		this.__10.onmouseover=function (e) {
			me.elementMouseOver['_10']=true;
		}
		this.__10.onmouseout=function (e) {
			me.__10.style[domTransition]='none';
			me.__10.style.opacity='0.5';
			me.__10.style.visibility=me.__10.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_10']=false;
		}
		this.__10.ontouchend=function (e) {
			me.elementMouseOver['_10']=false;
		}
		this.__10.ggUpdatePosition=function (useTransition) {
		}
		this.__9.appendChild(this.__10);
		this.divSkin.appendChild(this.__9);
		this.__4=document.createElement('div');
		this.__4.ggId="\u0422\u0443\u0430\u043b\u0435\u0442";
		this.__4.ggLeft=-144;
		this.__4.ggTop=-81;
		this.__4.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__4.ggVisible=false;
		this.__4.className='ggskin ggskin_container ';
		this.__4.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__4.setAttribute('style',hs);
		this.__4.style[domTransform + 'Origin']='50% 50%';
		me.__4.ggIsActive=function() {
			return false;
		}
		me.__4.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__4.ggCurrentLogicStateVisible = -1;
		this.__4.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Туалет") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__4.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__4.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__4.style[domTransition]='';
				if (me.__4.ggCurrentLogicStateVisible == 0) {
					me.__4.style.visibility=(Number(me.__4.style.opacity)>0||!me.__4.style.opacity)?'inherit':'hidden';
					me.__4.ggVisible=true;
				}
				else {
					me.__4.style.visibility="hidden";
					me.__4.ggVisible=false;
				}
			}
		}
		this.__4.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__4.ggNodeChange=function () {
			me.__4.ggUpdateConditionNodeChange();
		}
		this.__8=document.createElement('div');
		this.__8__img=document.createElement('img');
		this.__8__img.className='ggskin ggskin_button';
		this.__8__img.setAttribute('src',basePath + 'images/_8.png');
		this.__8__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__8__img.className='ggskin ggskin_button';
		this.__8__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__8__img);
		this.__8.appendChild(this.__8__img);
		this.__8.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__8.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__8.ggVisible=true;
		this.__8.className='ggskin ggskin_button ';
		this.__8.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__8.setAttribute('style',hs);
		this.__8.style[domTransform + 'Origin']='50% 50%';
		me.__8.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__8.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__8.onclick=function (e) {
			me.player.openNext("{node24}","$cur");
		}
		this.__8.onmouseover=function (e) {
			me.elementMouseOver['_8']=true;
		}
		this.__8.onmouseout=function (e) {
			me.__8.style[domTransition]='none';
			me.__8.style.opacity='0.5';
			me.__8.style.visibility=me.__8.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_8']=false;
		}
		this.__8.ontouchend=function (e) {
			me.elementMouseOver['_8']=false;
		}
		this.__8.ggUpdatePosition=function (useTransition) {
		}
		this.__4.appendChild(this.__8);
		this.__7=document.createElement('div');
		this.__7__img=document.createElement('img');
		this.__7__img.className='ggskin ggskin_button';
		this.__7__img.setAttribute('src',basePath + 'images/_7.png');
		this.__7__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__7__img.className='ggskin ggskin_button';
		this.__7__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__7__img);
		this.__7.appendChild(this.__7__img);
		this.__7.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__7.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__7.ggVisible=true;
		this.__7.className='ggskin ggskin_button ';
		this.__7.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__7.setAttribute('style',hs);
		this.__7.style[domTransform + 'Origin']='50% 50%';
		me.__7.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__7.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__7.onclick=function (e) {
			me.player.openNext("{node22}","$cur");
		}
		this.__7.onmouseover=function (e) {
			me.elementMouseOver['_7']=true;
		}
		this.__7.onmouseout=function (e) {
			me.__7.style[domTransition]='none';
			me.__7.style.opacity='0.5';
			me.__7.style.visibility=me.__7.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_7']=false;
		}
		this.__7.ontouchend=function (e) {
			me.elementMouseOver['_7']=false;
		}
		this.__7.ggUpdatePosition=function (useTransition) {
		}
		this.__4.appendChild(this.__7);
		this.__6=document.createElement('div');
		this.__6__img=document.createElement('img');
		this.__6__img.className='ggskin ggskin_button';
		this.__6__img.setAttribute('src',basePath + 'images/_6.png');
		this.__6__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__6__img.className='ggskin ggskin_button';
		this.__6__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__6__img);
		this.__6.appendChild(this.__6__img);
		this.__6.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__6.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__6.ggVisible=true;
		this.__6.className='ggskin ggskin_button ';
		this.__6.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__6.setAttribute('style',hs);
		this.__6.style[domTransform + 'Origin']='50% 50%';
		me.__6.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__6.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__6.onclick=function (e) {
			me.player.openNext("{node23}","$cur");
		}
		this.__6.onmouseover=function (e) {
			me.elementMouseOver['_6']=true;
		}
		this.__6.onmouseout=function (e) {
			me.__6.style[domTransition]='none';
			me.__6.style.opacity='0.5';
			me.__6.style.visibility=me.__6.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_6']=false;
		}
		this.__6.ontouchend=function (e) {
			me.elementMouseOver['_6']=false;
		}
		this.__6.ggUpdatePosition=function (useTransition) {
		}
		this.__4.appendChild(this.__6);
		this.__5=document.createElement('div');
		this.__5__img=document.createElement('img');
		this.__5__img.className='ggskin ggskin_button';
		this.__5__img.setAttribute('src',basePath + 'images/_5.png');
		this.__5__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__5__img.className='ggskin ggskin_button';
		this.__5__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__5__img);
		this.__5.appendChild(this.__5__img);
		this.__5.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__5.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__5.ggVisible=true;
		this.__5.className='ggskin ggskin_button ';
		this.__5.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__5.setAttribute('style',hs);
		this.__5.style[domTransform + 'Origin']='50% 50%';
		me.__5.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__5.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__5.onclick=function (e) {
			me.player.openNext("{node21}","$cur");
		}
		this.__5.onmouseover=function (e) {
			me.elementMouseOver['_5']=true;
		}
		this.__5.onmouseout=function (e) {
			me.__5.style[domTransition]='none';
			me.__5.style.opacity='0.5';
			me.__5.style.visibility=me.__5.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_5']=false;
		}
		this.__5.ontouchend=function (e) {
			me.elementMouseOver['_5']=false;
		}
		this.__5.ggUpdatePosition=function (useTransition) {
		}
		this.__4.appendChild(this.__5);
		this.divSkin.appendChild(this.__4);
		this.__=document.createElement('div');
		this.__.ggId="\u0412\u0430\u043d\u043d\u0430\u044f";
		this.__.ggLeft=-144;
		this.__.ggTop=-81;
		this.__.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__.ggVisible=false;
		this.__.className='ggskin ggskin_container ';
		this.__.ggType='container';
		hs ='';
		hs+='height : 49px;';
		hs+='left : -144px;';
		hs+='position : absolute;';
		hs+='top : -81px;';
		hs+='visibility : hidden;';
		hs+='width : 287px;';
		hs+='pointer-events:none;';
		this.__.setAttribute('style',hs);
		this.__.style[domTransform + 'Origin']='50% 50%';
		me.__.ggIsActive=function() {
			return false;
		}
		me.__.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		me.__.ggCurrentLogicStateVisible = -1;
		this.__.ggUpdateConditionNodeChange=function () {
			var newLogicStateVisible;
			if (
				(me.ggUserdata.tags.indexOf("Ванная") != -1)
			)
			{
				newLogicStateVisible = 0;
			}
			else {
				newLogicStateVisible = -1;
			}
			if (me.__.ggCurrentLogicStateVisible != newLogicStateVisible) {
				me.__.ggCurrentLogicStateVisible = newLogicStateVisible;
				me.__.style[domTransition]='';
				if (me.__.ggCurrentLogicStateVisible == 0) {
					me.__.style.visibility=(Number(me.__.style.opacity)>0||!me.__.style.opacity)?'inherit':'hidden';
					me.__.ggVisible=true;
				}
				else {
					me.__.style.visibility="hidden";
					me.__.ggVisible=false;
				}
			}
		}
		this.__.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.__.ggNodeChange=function () {
			me.__.ggUpdateConditionNodeChange();
		}
		this.__3=document.createElement('div');
		this.__3__img=document.createElement('img');
		this.__3__img.className='ggskin ggskin_button';
		this.__3__img.setAttribute('src',basePath + 'images/_3.png');
		this.__3__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__3__img.className='ggskin ggskin_button';
		this.__3__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__3__img);
		this.__3.appendChild(this.__3__img);
		this.__3.ggId="\u0421\u0435\u0442\u043a\u0430";
		this.__3.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__3.ggVisible=true;
		this.__3.className='ggskin ggskin_button ';
		this.__3.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 0px;';
		hs+='opacity : 0.49999;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 49px;';
		hs+='pointer-events:auto;';
		this.__3.setAttribute('style',hs);
		this.__3.style[domTransform + 'Origin']='50% 50%';
		me.__3.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__3.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__3.onclick=function (e) {
			me.player.openNext("{node4}","$cur");
		}
		this.__3.onmouseover=function (e) {
			me.elementMouseOver['_3']=true;
		}
		this.__3.onmouseout=function (e) {
			me.__3.style[domTransition]='none';
			me.__3.style.opacity='0.5';
			me.__3.style.visibility=me.__3.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_3']=false;
		}
		this.__3.ontouchend=function (e) {
			me.elementMouseOver['_3']=false;
		}
		this.__3.ggUpdatePosition=function (useTransition) {
		}
		this.__.appendChild(this.__3);
		this.__2=document.createElement('div');
		this.__2__img=document.createElement('img');
		this.__2__img.className='ggskin ggskin_button';
		this.__2__img.setAttribute('src',basePath + 'images/_2.png');
		this.__2__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__2__img.className='ggskin ggskin_button';
		this.__2__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__2__img);
		this.__2.appendChild(this.__2__img);
		this.__2.ggId="\u0411\u0435\u0442\u043e\u043d";
		this.__2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__2.ggVisible=true;
		this.__2.className='ggskin ggskin_button ';
		this.__2.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 54px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 65px;';
		hs+='pointer-events:auto;';
		this.__2.setAttribute('style',hs);
		this.__2.style[domTransform + 'Origin']='50% 50%';
		me.__2.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__2.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__2.onclick=function (e) {
			me.player.openNext("{node2}","$cur");
		}
		this.__2.onmouseover=function (e) {
			me.elementMouseOver['_2']=true;
		}
		this.__2.onmouseout=function (e) {
			me.__2.style[domTransition]='none';
			me.__2.style.opacity='0.5';
			me.__2.style.visibility=me.__2.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_2']=false;
		}
		this.__2.ontouchend=function (e) {
			me.elementMouseOver['_2']=false;
		}
		this.__2.ggUpdatePosition=function (useTransition) {
		}
		this.__.appendChild(this.__2);
		this.__1=document.createElement('div');
		this.__1__img=document.createElement('img');
		this.__1__img.className='ggskin ggskin_button';
		this.__1__img.setAttribute('src',basePath + 'images/_1.png');
		this.__1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__1__img.className='ggskin ggskin_button';
		this.__1__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__1__img);
		this.__1.appendChild(this.__1__img);
		this.__1.ggId="\u041e\u0442\u0434\u0435\u043b\u043a\u0430";
		this.__1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__1.ggVisible=true;
		this.__1.className='ggskin ggskin_button ';
		this.__1.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 125px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 85px;';
		hs+='pointer-events:auto;';
		this.__1.setAttribute('style',hs);
		this.__1.style[domTransform + 'Origin']='50% 50%';
		me.__1.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__1.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__1.onclick=function (e) {
			me.player.openNext("{node3}","$cur");
		}
		this.__1.onmouseover=function (e) {
			me.elementMouseOver['_1']=true;
		}
		this.__1.onmouseout=function (e) {
			me.__1.style[domTransition]='none';
			me.__1.style.opacity='0.5';
			me.__1.style.visibility=me.__1.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_1']=false;
		}
		this.__1.ontouchend=function (e) {
			me.elementMouseOver['_1']=false;
		}
		this.__1.ggUpdatePosition=function (useTransition) {
		}
		this.__.appendChild(this.__1);
		this.__0=document.createElement('div');
		this.__0__img=document.createElement('img');
		this.__0__img.className='ggskin ggskin_button';
		this.__0__img.setAttribute('src',basePath + 'images/_0.png');
		this.__0__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
		this.__0__img.className='ggskin ggskin_button';
		this.__0__img['ondragstart']=function() { return false; };
		me.player.checkLoaded.push(this.__0__img);
		this.__0.appendChild(this.__0__img);
		this.__0.ggId="\u041c\u0435\u0431\u0435\u043b\u044c";
		this.__0.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this.__0.ggVisible=true;
		this.__0.className='ggskin ggskin_button ';
		this.__0.ggType='button';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 49px;';
		hs+='left : 214px;';
		hs+='opacity : 0.5;';
		hs+='position : absolute;';
		hs+='top : 0px;';
		hs+='visibility : inherit;';
		hs+='width : 73px;';
		hs+='pointer-events:auto;';
		this.__0.setAttribute('style',hs);
		this.__0.style[domTransform + 'Origin']='50% 50%';
		me.__0.ggIsActive=function() {
			if ((this.parentNode) && (this.parentNode.ggIsActive)) {
				return this.parentNode.ggIsActive();
			}
			return false;
		}
		me.__0.ggElementNodeId=function() {
			if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
				return this.parentNode.ggElementNodeId();
			}
			return me.player.getCurrentNode();
		}
		this.__0.onclick=function (e) {
			me.player.openNext("{node1}","$cur");
		}
		this.__0.onmouseover=function (e) {
			me.elementMouseOver['_0']=true;
		}
		this.__0.onmouseout=function (e) {
			me.__0.style[domTransition]='none';
			me.__0.style.opacity='0.5';
			me.__0.style.visibility=me.__0.ggVisible?'inherit':'hidden';
			me.elementMouseOver['_0']=false;
		}
		this.__0.ontouchend=function (e) {
			me.elementMouseOver['_0']=false;
		}
		this.__0.ggUpdatePosition=function (useTransition) {
		}
		this.__.appendChild(this.__0);
		this.divSkin.appendChild(this.__);
		this._text_2=document.createElement('div');
		this._text_2__text=document.createElement('div');
		this._text_2.className='ggskin ggskin_textdiv';
		this._text_2.ggTextDiv=this._text_2__text;
		this._text_2.ggId="Text 2";
		this._text_2.ggLeft=-68;
		this._text_2.ggTop=-20;
		this._text_2.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._text_2.ggVisible=true;
		this._text_2.className='ggskin ggskin_text ';
		this._text_2.ggType='text';
		hs ='';
		hs+='cursor : pointer;';
		hs+='height : 20px;';
		hs+='left : -68px;';
		hs+='position : absolute;';
		hs+='top : -20px;';
		hs+='visibility : inherit;';
		hs+='width : 126px;';
		hs+='pointer-events:auto;';
		this._text_2.setAttribute('style',hs);
		this._text_2.style[domTransform + 'Origin']='50% 50%';
		hs ='position:absolute;';
		hs+='left: 0px;';
		hs+='top:  0px;';
		hs+='width: 126px;';
		hs+='height: 20px;';
		hs+='border: 0px solid #000000;';
		hs+='color: rgba(255,255,255,1);';
		hs+='text-align: center;';
		hs+='white-space: nowrap;';
		hs+='padding: 0px 1px 0px 1px;';
		hs+='overflow: hidden;';
		this._text_2__text.setAttribute('style',hs);
		this._text_2__text.innerHTML="\u0421\u0430\u0439\u0442-\u043f\u0440\u043e\u0435\u043a\u0442.\u0420\u0424";
		this._text_2.appendChild(this._text_2__text);
		me._text_2.ggIsActive=function() {
			return false;
		}
		me._text_2.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._text_2.onclick=function (e) {
			me.player.openUrl("http:\/\/89282222840.ru","_blank");
		}
		this._text_2.ggUpdatePosition=function (useTransition) {
			if (useTransition==='undefined') {
				useTransition = false;
			}
			if (!useTransition) {
				this.style[domTransition]='none';
			}
			if (this.parentNode) {
				var w=this.parentNode.offsetWidth;
					this.style.left=(this.ggLeft - 0 + w/2) + 'px';
				var h=this.parentNode.offsetHeight;
					this.style.top=(this.ggTop - 0 + h) + 'px';
			}
		}
		this.divSkin.appendChild(this._text_2);
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.ggHotspotCallChildFunctions=function(functionname) {
		var stack = me.player.getCurrentPointHotspots();
		while (stack.length > 0) {
			var e = stack.pop();
			if (typeof e[functionname] == 'function') {
				e[functionname]();
			}
			if(e.hasChildNodes()) {
				for(var i=0; i<e.childNodes.length; i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	this.changeActiveNode=function(id) {
		me.ggUserdata=me.player.userdata;
		me.__24.ggNodeChange();
		me.__19.ggNodeChange();
		me.__14.ggNodeChange();
		me.__9.ggNodeChange();
		me.__4.ggNodeChange();
		me.__.ggNodeChange();
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		me.ggCurrentTime=new Date().getTime();
		if (me.elementMouseOver['_28']) {
			if (me.player.transitionsDisabled) {
				me.__28.style[domTransition]='none';
			} else {
				me.__28.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__28.style.opacity='1';
			me.__28.style.visibility=me.__28.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_27']) {
			if (me.player.transitionsDisabled) {
				me.__27.style[domTransition]='none';
			} else {
				me.__27.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__27.style.opacity='1';
			me.__27.style.visibility=me.__27.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_26']) {
			if (me.player.transitionsDisabled) {
				me.__26.style[domTransition]='none';
			} else {
				me.__26.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__26.style.opacity='1';
			me.__26.style.visibility=me.__26.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_25']) {
			if (me.player.transitionsDisabled) {
				me.__25.style[domTransition]='none';
			} else {
				me.__25.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__25.style.opacity='1';
			me.__25.style.visibility=me.__25.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_23']) {
			if (me.player.transitionsDisabled) {
				me.__23.style[domTransition]='none';
			} else {
				me.__23.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__23.style.opacity='1';
			me.__23.style.visibility=me.__23.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_22']) {
			if (me.player.transitionsDisabled) {
				me.__22.style[domTransition]='none';
			} else {
				me.__22.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__22.style.opacity='1';
			me.__22.style.visibility=me.__22.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_21']) {
			if (me.player.transitionsDisabled) {
				me.__21.style[domTransition]='none';
			} else {
				me.__21.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__21.style.opacity='1';
			me.__21.style.visibility=me.__21.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_20']) {
			if (me.player.transitionsDisabled) {
				me.__20.style[domTransition]='none';
			} else {
				me.__20.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__20.style.opacity='1';
			me.__20.style.visibility=me.__20.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_18']) {
			if (me.player.transitionsDisabled) {
				me.__18.style[domTransition]='none';
			} else {
				me.__18.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__18.style.opacity='1';
			me.__18.style.visibility=me.__18.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_17']) {
			if (me.player.transitionsDisabled) {
				me.__17.style[domTransition]='none';
			} else {
				me.__17.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__17.style.opacity='1';
			me.__17.style.visibility=me.__17.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_16']) {
			if (me.player.transitionsDisabled) {
				me.__16.style[domTransition]='none';
			} else {
				me.__16.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__16.style.opacity='1';
			me.__16.style.visibility=me.__16.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_15']) {
			if (me.player.transitionsDisabled) {
				me.__15.style[domTransition]='none';
			} else {
				me.__15.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__15.style.opacity='1';
			me.__15.style.visibility=me.__15.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_13']) {
			if (me.player.transitionsDisabled) {
				me.__13.style[domTransition]='none';
			} else {
				me.__13.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__13.style.opacity='1';
			me.__13.style.visibility=me.__13.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_12']) {
			if (me.player.transitionsDisabled) {
				me.__12.style[domTransition]='none';
			} else {
				me.__12.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__12.style.opacity='1';
			me.__12.style.visibility=me.__12.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_11']) {
			if (me.player.transitionsDisabled) {
				me.__11.style[domTransition]='none';
			} else {
				me.__11.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__11.style.opacity='1';
			me.__11.style.visibility=me.__11.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_10']) {
			if (me.player.transitionsDisabled) {
				me.__10.style[domTransition]='none';
			} else {
				me.__10.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__10.style.opacity='1';
			me.__10.style.visibility=me.__10.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_8']) {
			if (me.player.transitionsDisabled) {
				me.__8.style[domTransition]='none';
			} else {
				me.__8.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__8.style.opacity='1';
			me.__8.style.visibility=me.__8.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_7']) {
			if (me.player.transitionsDisabled) {
				me.__7.style[domTransition]='none';
			} else {
				me.__7.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__7.style.opacity='1';
			me.__7.style.visibility=me.__7.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_6']) {
			if (me.player.transitionsDisabled) {
				me.__6.style[domTransition]='none';
			} else {
				me.__6.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__6.style.opacity='1';
			me.__6.style.visibility=me.__6.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_5']) {
			if (me.player.transitionsDisabled) {
				me.__5.style[domTransition]='none';
			} else {
				me.__5.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__5.style.opacity='1';
			me.__5.style.visibility=me.__5.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_3']) {
			if (me.player.transitionsDisabled) {
				me.__3.style[domTransition]='none';
			} else {
				me.__3.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__3.style.opacity='1';
			me.__3.style.visibility=me.__3.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_2']) {
			if (me.player.transitionsDisabled) {
				me.__2.style[domTransition]='none';
			} else {
				me.__2.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__2.style.opacity='1';
			me.__2.style.visibility=me.__2.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_1']) {
			if (me.player.transitionsDisabled) {
				me.__1.style[domTransition]='none';
			} else {
				me.__1.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__1.style.opacity='1';
			me.__1.style.visibility=me.__1.ggVisible?'inherit':'hidden';
		}
		if (me.elementMouseOver['_0']) {
			if (me.player.transitionsDisabled) {
				me.__0.style[domTransition]='none';
			} else {
				me.__0.style[domTransition]='all 500ms ease-out 0ms';
			}
			me.__0.style.opacity='1';
			me.__0.style.visibility=me.__0.ggVisible?'inherit':'hidden';
		}
		me.ggHotspotCallChildFunctions('ggUpdateConditionTimer');
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		this.ggUserdata=this.skin.player.getNodeUserdata(nodeId);
		this.elementMouseDown=[];
		this.elementMouseOver=[];
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		{
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 67px;';
			hs+='position : absolute;';
			hs+='top : 57px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_node_image=document.createElement('div');
			this._ht_node_image__img=document.createElement('img');
			this._ht_node_image__img.className='ggskin ggskin_svg';
			this._ht_node_image__img.setAttribute('src',basePath + 'images/ht_node_image.svg');
			this._ht_node_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_node_image__img['ondragstart']=function() { return false; };
			this._ht_node_image.appendChild(this._ht_node_image__img);
			this._ht_node_image__imgo=document.createElement('img');
			this._ht_node_image__imgo.className='ggskin ggskin_svg';
			this._ht_node_image__imgo.setAttribute('src',basePath + 'images/ht_node_image__o.svg');
			this._ht_node_image__imgo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;');
			this._ht_node_image__imgo['ondragstart']=function() { return false; };
			this._ht_node_image.appendChild(this._ht_node_image__imgo);
			this._ht_node_image.ggId="ht_node_image";
			this._ht_node_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_node_image.ggVisible=true;
			this._ht_node_image.className='ggskin ggskin_svg ';
			this._ht_node_image.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 42px;';
			hs+='left : -22px;';
			hs+='position : absolute;';
			hs+='top : -33px;';
			hs+='visibility : inherit;';
			hs+='width : 42px;';
			hs+='pointer-events:auto;';
			this._ht_node_image.setAttribute('style',hs);
			this._ht_node_image.style[domTransform + 'Origin']='50% 50%';
			me._ht_node_image.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_node_image.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_node_image.onmouseover=function (e) {
				me._ht_node_image__img.style.visibility='hidden';
				me._ht_node_image__imgo.style.visibility='inherit';
			}
			this._ht_node_image.onmouseout=function (e) {
				me._ht_node_image__img.style.visibility='inherit';
				me._ht_node_image__imgo.style.visibility='hidden';
			}
			me._ht_node_image.ggCurrentLogicStateScaling = -1;
			this._ht_node_image.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_node_image.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_node_image.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_node_image.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_node_image.ggCurrentLogicStateScaling == 0) {
						me._ht_node_image.ggParameter.sx = 1.1;
						me._ht_node_image.ggParameter.sy = 1.1;
						me._ht_node_image.style[domTransform]=parameterToTransform(me._ht_node_image.ggParameter);
					}
					else {
						me._ht_node_image.ggParameter.sx = 1;
						me._ht_node_image.ggParameter.sy = 1;
						me._ht_node_image.style[domTransform]=parameterToTransform(me._ht_node_image.ggParameter);
					}
				}
			}
			this._ht_node_image.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_node_image);
			this._text_1=document.createElement('div');
			this._text_1__text=document.createElement('div');
			this._text_1.className='ggskin ggskin_textdiv';
			this._text_1.ggTextDiv=this._text_1__text;
			this._text_1.ggId="Text 1";
			this._text_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_1.ggVisible=true;
			this._text_1.className='ggskin ggskin_text ';
			this._text_1.ggType='text';
			hs ='';
			hs+='height : 18px;';
			hs+='left : -51px;';
			hs+='position : absolute;';
			hs+='top : 14px;';
			hs+='visibility : inherit;';
			hs+='width : 96px;';
			hs+='pointer-events:auto;';
			this._text_1.setAttribute('style',hs);
			this._text_1.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #3b4650;';
			hs+='background: rgba(59,70,80,0.470588);';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,255,255,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 6px 7px 6px 7px;';
			hs+='overflow: hidden;';
			this._text_1__text.setAttribute('style',hs);
			this._text_1__text.innerHTML=me.hotspot.title;
			this._text_1.appendChild(this._text_1__text);
			me._text_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._text_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._text_1.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this.__div.appendChild(this._text_1);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_node_image.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	this.addSkin();
};