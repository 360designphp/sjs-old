// Garden Gnome Software - Skin
// Pano2VR 5.2.0/15969
// Filename: My.ggsk
// Generated Чт май 31 10:52:33 2018

function pano2vrSkin(player,base) {
	var ggSkinVars = [];
	ggSkinVars['ht_ani'] = false;
	var me=this;
	var flag=false;
	this.player=player;
	this.player.skinObj=this;
	this.divSkin=player.divSkin;
	this.ggUserdata=me.player.userdata;
	this.lastSize={ w: -1,h: -1 };
	var basePath="";
	// auto detect base path
	if (base=='?') {
		var scripts = document.getElementsByTagName('script');
		for(var i=0;i<scripts.length;i++) {
			var src=scripts[i].src;
			if (src.indexOf('skin.js')>=0) {
				var p=src.lastIndexOf('/');
				if (p>=0) {
					basePath=src.substr(0,p+1);
				}
			}
		}
	} else
	if (base) {
		basePath=base;
	}
	this.elementMouseDown=[];
	this.elementMouseOver=[];
	var cssPrefix='';
	var domTransition='transition';
	var domTransform='transform';
	var prefixes='Webkit,Moz,O,ms,Ms'.split(',');
	var i;
	if (typeof document.body.style['transform'] == 'undefined') {
		for(var i=0;i<prefixes.length;i++) {
			if (typeof document.body.style[prefixes[i] + 'Transform'] !== 'undefined') {
				cssPrefix='-' + prefixes[i].toLowerCase() + '-';
				domTransition=prefixes[i] + 'Transition';
				domTransform=prefixes[i] + 'Transform';
			}
		}
	}
	
	this.player.setMargins(0,0,0,0);
	
	this.updateSize=function(startElement) {
		var stack=[];
		stack.push(startElement);
		while(stack.length>0) {
			var e=stack.pop();
			if (e.ggUpdatePosition) {
				e.ggUpdatePosition();
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	
	parameterToTransform=function(p) {
		var hs='translate(' + p.rx + 'px,' + p.ry + 'px) rotate(' + p.a + 'deg) scale(' + p.sx + ',' + p.sy + ')';
		return hs;
	}
	
	this.findElements=function(id,regex) {
		var r=[];
		var stack=[];
		var pat=new RegExp(id,'');
		stack.push(me.divSkin);
		while(stack.length>0) {
			var e=stack.pop();
			if (regex) {
				if (pat.test(e.ggId)) r.push(e);
			} else {
				if (e.ggId==id) r.push(e);
			}
			if (e.hasChildNodes()) {
				for(var i=0;i<e.childNodes.length;i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
		return r;
	}
	
	this.addSkin=function() {
		var hs='';
		this.ggCurrentTime=new Date().getTime();
		this._ht_node_timer=document.createElement('div');
		this._ht_node_timer.ggTimestamp=this.ggCurrentTime;
		this._ht_node_timer.ggLastIsActive=true;
		this._ht_node_timer.ggTimeout=500;
		this._ht_node_timer.ggId="ht_node_timer";
		this._ht_node_timer.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
		this._ht_node_timer.ggVisible=true;
		this._ht_node_timer.className='ggskin ggskin_timer ';
		this._ht_node_timer.ggType='timer';
		hs ='';
		hs+='height : 32px;';
		hs+='left : 62px;';
		hs+='position : absolute;';
		hs+='top : 23px;';
		hs+='visibility : inherit;';
		hs+='width : 32px;';
		hs+='pointer-events:none;';
		this._ht_node_timer.setAttribute('style',hs);
		this._ht_node_timer.style[domTransform + 'Origin']='50% 50%';
		me._ht_node_timer.ggIsActive=function() {
			return (me._ht_node_timer.ggTimestamp==0 ? false : (Math.floor((me.ggCurrentTime - me._ht_node_timer.ggTimestamp) / me._ht_node_timer.ggTimeout) % 2 == 0));
		}
		me._ht_node_timer.ggElementNodeId=function() {
			return me.player.getCurrentNode();
		}
		this._ht_node_timer.ggActivate=function () {
			ggSkinVars['ht_ani'] = true;
		}
		this._ht_node_timer.ggDeactivate=function () {
			ggSkinVars['ht_ani'] = false;
		}
		this._ht_node_timer.ggUpdatePosition=function (useTransition) {
		}
		this.divSkin.appendChild(this._ht_node_timer);
		this.divSkin.ggUpdateSize=function(w,h) {
			me.updateSize(me.divSkin);
		}
		this.divSkin.ggViewerInit=function() {
		}
		this.divSkin.ggLoaded=function() {
		}
		this.divSkin.ggReLoaded=function() {
		}
		this.divSkin.ggLoadedLevels=function() {
		}
		this.divSkin.ggReLoadedLevels=function() {
		}
		this.divSkin.ggEnterFullscreen=function() {
		}
		this.divSkin.ggExitFullscreen=function() {
		}
		this.skinTimerEvent();
	};
	this.hotspotProxyClick=function(id) {
	}
	this.hotspotProxyOver=function(id) {
	}
	this.hotspotProxyOut=function(id) {
	}
	this.ggHotspotCallChildFunctions=function(functionname) {
		var stack = me.player.getCurrentPointHotspots();
		while (stack.length > 0) {
			var e = stack.pop();
			if (typeof e[functionname] == 'function') {
				e[functionname]();
			}
			if(e.hasChildNodes()) {
				for(var i=0; i<e.childNodes.length; i++) {
					stack.push(e.childNodes[i]);
				}
			}
		}
	}
	this.changeActiveNode=function(id) {
		me.ggUserdata=me.player.userdata;
	}
	this.skinTimerEvent=function() {
		setTimeout(function() { me.skinTimerEvent(); }, 10);
		me.ggCurrentTime=new Date().getTime();
		if (me._ht_node_timer.ggLastIsActive!=me._ht_node_timer.ggIsActive()) {
			me._ht_node_timer.ggLastIsActive=me._ht_node_timer.ggIsActive();
			if (me._ht_node_timer.ggLastIsActive) {
				ggSkinVars['ht_ani'] = true;
			} else {
				ggSkinVars['ht_ani'] = false;
			}
		}
		me.ggHotspotCallChildFunctions('ggUpdateConditionTimer');
	};
	function SkinHotspotClass(skinObj,hotspot) {
		var me=this;
		var flag=false;
		this.player=skinObj.player;
		this.skin=skinObj;
		this.hotspot=hotspot;
		var nodeId=String(hotspot.url);
		nodeId=(nodeId.charAt(0)=='{')?nodeId.substr(1, nodeId.length - 2):'';
		this.ggUserdata=this.skin.player.getNodeUserdata(nodeId);
		this.elementMouseDown=[];
		this.elementMouseOver=[];
		
		this.findElements=function(id,regex) {
			return me.skin.findElements(id,regex);
		}
		
		if (hotspot.skinid=='ht_node') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 78px;';
			hs+='position : absolute;';
			hs+='top : 39px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_node_image1=document.createElement('div');
			this._ht_node_image1__img=document.createElement('img');
			this._ht_node_image1__img.className='ggskin ggskin_svg';
			this._ht_node_image1__img.setAttribute('src',basePath + 'images/ht_node_image1.svg');
			this._ht_node_image1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_node_image1__img['ondragstart']=function() { return false; };
			this._ht_node_image1.appendChild(this._ht_node_image1__img);
			this._ht_node_image1__imgo=document.createElement('img');
			this._ht_node_image1__imgo.className='ggskin ggskin_svg';
			this._ht_node_image1__imgo.setAttribute('src',basePath + 'images/ht_node_image1__o.svg');
			this._ht_node_image1__imgo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;');
			this._ht_node_image1__imgo['ondragstart']=function() { return false; };
			this._ht_node_image1.appendChild(this._ht_node_image1__imgo);
			this._ht_node_image1.ggId="ht_node_image";
			this._ht_node_image1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_node_image1.ggVisible=true;
			this._ht_node_image1.className='ggskin ggskin_svg ';
			this._ht_node_image1.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 42px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -22px;';
			hs+='visibility : inherit;';
			hs+='width : 42px;';
			hs+='pointer-events:auto;';
			this._ht_node_image1.setAttribute('style',hs);
			this._ht_node_image1.style[domTransform + 'Origin']='50% 50%';
			me._ht_node_image1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_node_image1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_node_image1.onmouseover=function (e) {
				me._ht_node_image1__img.style.visibility='hidden';
				me._ht_node_image1__imgo.style.visibility='inherit';
			}
			this._ht_node_image1.onmouseout=function (e) {
				me._ht_node_image1__img.style.visibility='inherit';
				me._ht_node_image1__imgo.style.visibility='hidden';
			}
			me._ht_node_image1.ggCurrentLogicStateScaling = -1;
			this._ht_node_image1.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_node_image1.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_node_image1.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_node_image1.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_node_image1.ggCurrentLogicStateScaling == 0) {
						me._ht_node_image1.ggParameter.sx = 1.1;
						me._ht_node_image1.ggParameter.sy = 1.1;
						me._ht_node_image1.style[domTransform]=parameterToTransform(me._ht_node_image1.ggParameter);
					}
					else {
						me._ht_node_image1.ggParameter.sx = 1;
						me._ht_node_image1.ggParameter.sy = 1;
						me._ht_node_image1.style[domTransform]=parameterToTransform(me._ht_node_image1.ggParameter);
					}
				}
			}
			this._ht_node_image1.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_node_image1);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_node_image1.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_dom') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_dom";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 221px;';
			hs+='position : absolute;';
			hs+='top : 88px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_dom_image=document.createElement('div');
			this._ht_dom_image__img=document.createElement('img');
			this._ht_dom_image__img.className='ggskin ggskin_svg';
			this._ht_dom_image__img.setAttribute('src',basePath + 'images/ht_dom_image.svg');
			this._ht_dom_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_dom_image__img['ondragstart']=function() { return false; };
			this._ht_dom_image.appendChild(this._ht_dom_image__img);
			this._ht_dom_image.ggId="ht_dom_image";
			this._ht_dom_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_dom_image.ggVisible=true;
			this._ht_dom_image.className='ggskin ggskin_svg ';
			this._ht_dom_image.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 62px;';
			hs+='left : -16px;';
			hs+='position : absolute;';
			hs+='top : -16px;';
			hs+='visibility : inherit;';
			hs+='width : 62px;';
			hs+='pointer-events:auto;';
			this._ht_dom_image.setAttribute('style',hs);
			this._ht_dom_image.style[domTransform + 'Origin']='50% 50%';
			me._ht_dom_image.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_dom_image.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_dom_image.onmouseover=function (e) {
				me._text_12.style[domTransition]='none';
				me._text_12.style.visibility=(Number(me._text_12.style.opacity)>0||!me._text_12.style.opacity)?'inherit':'hidden';
				me._text_12.ggVisible=true;
			}
			this._ht_dom_image.onmouseout=function (e) {
				me._text_12.style[domTransition]='none';
				me._text_12.style.visibility='hidden';
				me._text_12.ggVisible=false;
			}
			me._ht_dom_image.ggCurrentLogicStateScaling = -1;
			this._ht_dom_image.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_dom_image.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_dom_image.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_dom_image.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_dom_image.ggCurrentLogicStateScaling == 0) {
						me._ht_dom_image.ggParameter.sx = 1.1;
						me._ht_dom_image.ggParameter.sy = 1.1;
						me._ht_dom_image.style[domTransform]=parameterToTransform(me._ht_dom_image.ggParameter);
					}
					else {
						me._ht_dom_image.ggParameter.sx = 1;
						me._ht_dom_image.ggParameter.sy = 1;
						me._ht_dom_image.style[domTransform]=parameterToTransform(me._ht_dom_image.ggParameter);
					}
				}
			}
			this._ht_dom_image.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_dom_image);
			this._text_12=document.createElement('div');
			this._text_12__text=document.createElement('div');
			this._text_12.className='ggskin ggskin_textdiv';
			this._text_12.ggTextDiv=this._text_12__text;
			this._text_12.ggId="Text 1";
			this._text_12.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_12.ggVisible=false;
			this._text_12.className='ggskin ggskin_text ';
			this._text_12.ggType='text';
			hs ='';
			hs+='height : 18px;';
			hs+='left : -34px;';
			hs+='position : absolute;';
			hs+='top : -47px;';
			hs+='visibility : hidden;';
			hs+='width : 96px;';
			hs+='pointer-events:auto;';
			this._text_12.setAttribute('style',hs);
			this._text_12.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #ffcc00;';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: #000000;';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 6px 7px 6px 7px;';
			hs+='overflow: hidden;';
			this._text_12__text.setAttribute('style',hs);
			this._text_12__text.innerHTML=me.hotspot.title;
			this._text_12.appendChild(this._text_12__text);
			me._text_12.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._text_12.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._text_12.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this.__div.appendChild(this._text_12);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_dom_image.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_object') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_object";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 438px;';
			hs+='position : absolute;';
			hs+='top : 115px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_node_image0=document.createElement('div');
			this._ht_node_image0__img=document.createElement('img');
			this._ht_node_image0__img.className='ggskin ggskin_svg';
			this._ht_node_image0__img.setAttribute('src',basePath + 'images/ht_node_image0.svg');
			this._ht_node_image0__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_node_image0__img['ondragstart']=function() { return false; };
			this._ht_node_image0.appendChild(this._ht_node_image0__img);
			this._ht_node_image0.ggId="ht_node_image";
			this._ht_node_image0.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_node_image0.ggVisible=true;
			this._ht_node_image0.className='ggskin ggskin_svg ';
			this._ht_node_image0.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 42px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -40px;';
			hs+='visibility : inherit;';
			hs+='width : 42px;';
			hs+='pointer-events:auto;';
			this._ht_node_image0.setAttribute('style',hs);
			this._ht_node_image0.style[domTransform + 'Origin']='50% 50%';
			me._ht_node_image0.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_node_image0.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			me._ht_node_image0.ggCurrentLogicStateScaling = -1;
			this._ht_node_image0.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_node_image0.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_node_image0.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_node_image0.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_node_image0.ggCurrentLogicStateScaling == 0) {
						me._ht_node_image0.ggParameter.sx = 1.1;
						me._ht_node_image0.ggParameter.sy = 1.1;
						me._ht_node_image0.style[domTransform]=parameterToTransform(me._ht_node_image0.ggParameter);
					}
					else {
						me._ht_node_image0.ggParameter.sx = 1;
						me._ht_node_image0.ggParameter.sy = 1;
						me._ht_node_image0.style[domTransform]=parameterToTransform(me._ht_node_image0.ggParameter);
					}
				}
			}
			this._ht_node_image0.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_node_image0);
			this._text_11=document.createElement('div');
			this._text_11__text=document.createElement('div');
			this._text_11.className='ggskin ggskin_textdiv';
			this._text_11.ggTextDiv=this._text_11__text;
			this._text_11.ggId="Text 1";
			this._text_11.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_11.ggVisible=true;
			this._text_11.className='ggskin ggskin_text ';
			this._text_11.ggType='text';
			hs ='';
			hs+='height : 18px;';
			hs+='left : -49px;';
			hs+='position : absolute;';
			hs+='top : -75px;';
			hs+='visibility : inherit;';
			hs+='width : 96px;';
			hs+='pointer-events:auto;';
			this._text_11.setAttribute('style',hs);
			this._text_11.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #000000;';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,204,0,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 6px 7px 6px 7px;';
			hs+='overflow: hidden;';
			this._text_11__text.setAttribute('style',hs);
			this._text_11__text.innerHTML=me.hotspot.title;
			this._text_11.appendChild(this._text_11__text);
			me._text_11.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._text_11.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._text_11.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this.__div.appendChild(this._text_11);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_node_image0.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		if (hotspot.skinid=='ht_text') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_text";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 347px;';
			hs+='position : absolute;';
			hs+='top : 138px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._image_1=document.createElement('div');
			this._image_1__img=document.createElement('img');
			this._image_1__img.className='ggskin ggskin_image';
			this._image_1__img.setAttribute('src',basePath + 'images/image_1.png');
			this._image_1__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._image_1__img.className='ggskin ggskin_image';
			this._image_1__img['ondragstart']=function() { return false; };
			me.player.checkLoaded.push(this._image_1__img);
			this._image_1.appendChild(this._image_1__img);
			this._image_1.ggId="Image 1";
			this._image_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._image_1.ggVisible=true;
			this._image_1.className='ggskin ggskin_image ';
			this._image_1.ggType='image';
			hs ='';
			hs+='height : 17px;';
			hs+='left : -9px;';
			hs+='position : absolute;';
			hs+='top : -14px;';
			hs+='visibility : inherit;';
			hs+='width : 20px;';
			hs+='pointer-events:auto;';
			this._image_1.setAttribute('style',hs);
			this._image_1.style[domTransform + 'Origin']='50% 50%';
			me._image_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._image_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._image_1.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._image_1);
			this._text_10=document.createElement('div');
			this._text_10__text=document.createElement('div');
			this._text_10.className='ggskin ggskin_textdiv';
			this._text_10.ggTextDiv=this._text_10__text;
			this._text_10.ggId="Text 1";
			this._text_10.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_10.ggVisible=true;
			this._text_10.className='ggskin ggskin_text ';
			this._text_10.ggType='text';
			hs ='';
			hs+='height : 18px;';
			hs+='left : -49px;';
			hs+='position : absolute;';
			hs+='top : -33px;';
			hs+='visibility : inherit;';
			hs+='width : 96px;';
			hs+='pointer-events:auto;';
			this._text_10.setAttribute('style',hs);
			this._text_10.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #061c38;';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(0,255,255,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 6px 7px 6px 7px;';
			hs+='overflow: hidden;';
			this._text_10__text.setAttribute('style',hs);
			this._text_10__text.innerHTML=me.hotspot.title;
			this._text_10.appendChild(this._text_10__text);
			me._text_10.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._text_10.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._text_10.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this.__div.appendChild(this._text_10);
		} else
		if (hotspot.skinid=='ht_copter') {
			this.__div=document.createElement('div');
			this.__div.ggId="ht_copter";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 78px;';
			hs+='position : absolute;';
			hs+='top : 181px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_copter_image=document.createElement('div');
			this._ht_copter_image__img=document.createElement('img');
			this._ht_copter_image__img.className='ggskin ggskin_svg';
			this._ht_copter_image__img.setAttribute('src',basePath + 'images/ht_copter_image.svg');
			this._ht_copter_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_copter_image__img['ondragstart']=function() { return false; };
			this._ht_copter_image.appendChild(this._ht_copter_image__img);
			this._ht_copter_image__imgo=document.createElement('img');
			this._ht_copter_image__imgo.className='ggskin ggskin_svg';
			this._ht_copter_image__imgo.setAttribute('src',basePath + 'images/ht_copter_image__o.svg');
			this._ht_copter_image__imgo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;');
			this._ht_copter_image__imgo['ondragstart']=function() { return false; };
			this._ht_copter_image.appendChild(this._ht_copter_image__imgo);
			this._ht_copter_image.ggId="ht_copter_image";
			this._ht_copter_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_copter_image.ggVisible=true;
			this._ht_copter_image.className='ggskin ggskin_svg ';
			this._ht_copter_image.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 77px;';
			hs+='left : -38px;';
			hs+='position : absolute;';
			hs+='top : -52px;';
			hs+='visibility : inherit;';
			hs+='width : 77px;';
			hs+='pointer-events:auto;';
			this._ht_copter_image.setAttribute('style',hs);
			this._ht_copter_image.style[domTransform + 'Origin']='50% 50%';
			me._ht_copter_image.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_copter_image.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_copter_image.onmouseover=function (e) {
				me._ht_copter_image__img.style.visibility='hidden';
				me._ht_copter_image__imgo.style.visibility='inherit';
			}
			this._ht_copter_image.onmouseout=function (e) {
				me._ht_copter_image__img.style.visibility='inherit';
				me._ht_copter_image__imgo.style.visibility='hidden';
			}
			me._ht_copter_image.ggCurrentLogicStateScaling = -1;
			this._ht_copter_image.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_copter_image.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_copter_image.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_copter_image.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_copter_image.ggCurrentLogicStateScaling == 0) {
						me._ht_copter_image.ggParameter.sx = 1.1;
						me._ht_copter_image.ggParameter.sy = 1.1;
						me._ht_copter_image.style[domTransform]=parameterToTransform(me._ht_copter_image.ggParameter);
					}
					else {
						me._ht_copter_image.ggParameter.sx = 1;
						me._ht_copter_image.ggParameter.sy = 1;
						me._ht_copter_image.style[domTransform]=parameterToTransform(me._ht_copter_image.ggParameter);
					}
				}
			}
			this._ht_copter_image.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_copter_image);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_copter_image.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		} else
		{
			this.__div=document.createElement('div');
			this.__div.ggId="ht_node_text";
			this.__div.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this.__div.ggVisible=true;
			this.__div.className='ggskin ggskin_hotspot ';
			this.__div.ggType='hotspot';
			hs ='';
			hs+='height : 5px;';
			hs+='left : 181px;';
			hs+='position : absolute;';
			hs+='top : 249px;';
			hs+='visibility : inherit;';
			hs+='width : 5px;';
			hs+='pointer-events:auto;';
			this.__div.setAttribute('style',hs);
			this.__div.style[domTransform + 'Origin']='50% 50%';
			me.__div.ggIsActive=function() {
				return me.player.getCurrentNode()==this.ggElementNodeId();
			}
			me.__div.ggElementNodeId=function() {
				return me.hotspot.url.substr(1, me.hotspot.url.length - 2);
			}
			this.__div.onclick=function (e) {
				me.player.openUrl(me.hotspot.url,me.hotspot.target);
				me.skin.hotspotProxyClick(me.hotspot.id);
			}
			this.__div.onmouseover=function (e) {
				me.player.setActiveHotspot(me.hotspot);
				me.skin.hotspotProxyOver(me.hotspot.id);
			}
			this.__div.onmouseout=function (e) {
				me.player.setActiveHotspot(null);
				me.skin.hotspotProxyOut(me.hotspot.id);
			}
			this.__div.ggUpdatePosition=function (useTransition) {
			}
			this._ht_node_image=document.createElement('div');
			this._ht_node_image__img=document.createElement('img');
			this._ht_node_image__img.className='ggskin ggskin_svg';
			this._ht_node_image__img.setAttribute('src',basePath + 'images/ht_node_image.svg');
			this._ht_node_image__img.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;pointer-events:none;');
			this._ht_node_image__img['ondragstart']=function() { return false; };
			this._ht_node_image.appendChild(this._ht_node_image__img);
			this._ht_node_image__imgo=document.createElement('img');
			this._ht_node_image__imgo.className='ggskin ggskin_svg';
			this._ht_node_image__imgo.setAttribute('src',basePath + 'images/ht_node_image__o.svg');
			this._ht_node_image__imgo.setAttribute('style','position: absolute;top: 0px;left: 0px;width: 100%;height: 100%;-webkit-user-drag:none;visibility:hidden;pointer-events:none;');
			this._ht_node_image__imgo['ondragstart']=function() { return false; };
			this._ht_node_image.appendChild(this._ht_node_image__imgo);
			this._ht_node_image.ggId="ht_node_image";
			this._ht_node_image.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._ht_node_image.ggVisible=true;
			this._ht_node_image.className='ggskin ggskin_svg ';
			this._ht_node_image.ggType='svg';
			hs ='';
			hs+='cursor : pointer;';
			hs+='height : 42px;';
			hs+='left : -20px;';
			hs+='position : absolute;';
			hs+='top : -22px;';
			hs+='visibility : inherit;';
			hs+='width : 42px;';
			hs+='pointer-events:auto;';
			this._ht_node_image.setAttribute('style',hs);
			this._ht_node_image.style[domTransform + 'Origin']='50% 50%';
			me._ht_node_image.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._ht_node_image.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._ht_node_image.onmouseover=function (e) {
				me._ht_node_image__img.style.visibility='hidden';
				me._ht_node_image__imgo.style.visibility='inherit';
			}
			this._ht_node_image.onmouseout=function (e) {
				me._ht_node_image__img.style.visibility='inherit';
				me._ht_node_image__imgo.style.visibility='hidden';
			}
			me._ht_node_image.ggCurrentLogicStateScaling = -1;
			this._ht_node_image.ggUpdateConditionTimer=function () {
				var newLogicStateScaling;
				if (
					(ggSkinVars['ht_ani'] == true)
				)
				{
					newLogicStateScaling = 0;
				}
				else {
					newLogicStateScaling = -1;
				}
				if (me._ht_node_image.ggCurrentLogicStateScaling != newLogicStateScaling) {
					me._ht_node_image.ggCurrentLogicStateScaling = newLogicStateScaling;
					me._ht_node_image.style[domTransition]='' + cssPrefix + 'transform 500ms ease 0ms';
					if (me._ht_node_image.ggCurrentLogicStateScaling == 0) {
						me._ht_node_image.ggParameter.sx = 1.1;
						me._ht_node_image.ggParameter.sy = 1.1;
						me._ht_node_image.style[domTransform]=parameterToTransform(me._ht_node_image.ggParameter);
					}
					else {
						me._ht_node_image.ggParameter.sx = 1;
						me._ht_node_image.ggParameter.sy = 1;
						me._ht_node_image.style[domTransform]=parameterToTransform(me._ht_node_image.ggParameter);
					}
				}
			}
			this._ht_node_image.ggUpdatePosition=function (useTransition) {
			}
			this.__div.appendChild(this._ht_node_image);
			this._text_1=document.createElement('div');
			this._text_1__text=document.createElement('div');
			this._text_1.className='ggskin ggskin_textdiv';
			this._text_1.ggTextDiv=this._text_1__text;
			this._text_1.ggId="Text 1";
			this._text_1.ggParameter={ rx:0,ry:0,a:0,sx:1,sy:1 };
			this._text_1.ggVisible=true;
			this._text_1.className='ggskin ggskin_text ';
			this._text_1.ggType='text';
			hs ='';
			hs+='height : 18px;';
			hs+='left : -49px;';
			hs+='position : absolute;';
			hs+='top : -58px;';
			hs+='visibility : inherit;';
			hs+='width : 96px;';
			hs+='pointer-events:auto;';
			this._text_1.setAttribute('style',hs);
			this._text_1.style[domTransform + 'Origin']='50% 50%';
			hs ='position:absolute;';
			hs+='cursor: default;';
			hs+='left: 0px;';
			hs+='top:  0px;';
			hs+='width: auto;';
			hs+='height: auto;';
			hs+='background: #000000;';
			hs+='border: 0px solid #000000;';
			hs+='border-radius: 5px;';
			hs+=cssPrefix + 'border-radius: 5px;';
			hs+='color: rgba(255,204,0,1);';
			hs+='text-align: center;';
			hs+='white-space: nowrap;';
			hs+='padding: 6px 7px 6px 7px;';
			hs+='overflow: hidden;';
			this._text_1__text.setAttribute('style',hs);
			this._text_1__text.innerHTML=me.hotspot.title;
			this._text_1.appendChild(this._text_1__text);
			me._text_1.ggIsActive=function() {
				if ((this.parentNode) && (this.parentNode.ggIsActive)) {
					return this.parentNode.ggIsActive();
				}
				return false;
			}
			me._text_1.ggElementNodeId=function() {
				if ((this.parentNode) && (this.parentNode.ggElementNodeId)) {
					return this.parentNode.ggElementNodeId();
				}
				return me.ggNodeId;
			}
			this._text_1.ggUpdatePosition=function (useTransition) {
				this.style[domTransition]='none';
				this.ggTextDiv.style.left=((98-this.ggTextDiv.offsetWidth)/2) + 'px';
			}
			this.__div.appendChild(this._text_1);
			this.hotspotTimerEvent=function() {
				setTimeout(function() { me.hotspotTimerEvent(); }, 10);
				me._ht_node_image.ggUpdateConditionTimer();
			}
			this.hotspotTimerEvent();
		}
	};
	this.addSkinHotspot=function(hotspot) {
		return new SkinHotspotClass(me,hotspot);
	}
	this.addSkin();
};