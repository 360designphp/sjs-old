function addItem(){
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data:({'intent':'addMenuItem'}),
        dataType: "html",
        error: errorHandler,
        success: function (id) {
            $('#menu_list').append(
                '<tr id="item-'+id+'" onchange="saveItem('+id+')"> ' +
                '<td><input type="text" id="caption-'+id+'"></td>' +
                '<td><input type="checkbox" id="blank-'+id+'"></td>' +
                '<td><input type="text" id="link-'+id+'"></td>' +
                '<td><select name="zone" id="zone-'+id+'">'+
                '<option value="top_menu">Верхнее меню</option>'+
                '<option value="bottom_menu">Нижнее меню</option>'+
                '</select></td>'+
                '<td><input type="number" id="weight-'+id+'" value="1" min="0"></td>' +
               
                '<td><i class="fa fa-trash" onclick="deleteItem('+id+');"></i></td>' +
                '<td><select name="parent" id="parent-'+id+'">'+
                '<option value="0">Без родителя</option>'+
                '</select>'+
                     '</td>' +                                                                            
                '</tr>'
            )
        }
    })
}

function deleteItem(id) {
    if(confirm('Вы уверены?')== true){
        $.ajax({
            url: '/api/ajax',
            type: 'POST',
            data:({'id': id,
                'intent':'deleteMenuItem'}),
            dataType: "html",
            error: errorHandler,
            success: function () {
                $('#item-'+id).fadeOut(400);
            }
        })

    }
}
function saveItem(id) {
    var caption, link, weight,zone, visible, parent;
    caption = $('#caption-'+id).val();
    blank = $('#blank-'+id).prop("checked");
    link = $('#link-'+id).val();
    weight = $('#weight-'+id).val();
    zone = $('#zone-'+id).val();
    visible = $('#visible-'+id).val();
    parent= $('#parent-'+id).val();
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data:({'id': id,
            'caption': caption,
            'blank': blank,
            'link': link,
            'weight': weight,
            'zone': zone,
            'visible': visible,
            'parent':parent,
            'intent':'updateMenuItem'
        }),
        dataType: "html",
        error: errorHandler,
        success: function () {
        }
    })
}