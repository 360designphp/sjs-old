function save() {
    var site_name,description,keywords, email, scripts;
    site_name =  $('#site_name').val();
    keywords =  $('#keywords').val();
    description = $('#description').val();
    email = $('#email').val();
    scripts = $('#scripts').val();
    header_scripts = $('#header_scripts').val();
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data:({
            'site_name':site_name,
            'keywords':keywords,
            'description':description,
            'email':email,
            'scripts':scripts,
            'header_scripts':header_scripts,
            'intent':'saveSettings'}),
        dataType: "html",
        error: errorHandler,
        success: function (data) {
            alert(data);
        }
    })
    function errorHandler(data) {
        alert('Ошибка :' + data.status);
    }
}