function deleteTaxonomy(id) {
    if(confirm('Вы уверены?')== true){
        $.ajax({
            url: '/api/ajax',
            type: 'POST',
            data:({'id': id,
                'intent':'deleteTaxonomy'}),
            dataType: "html",
            error: errorHandler,
            success: function () {
                $('#tax'+id).fadeOut(400);
                
            }
        })

    }
}
function addTaxonomy() {
    var name = $('#taxName').val();
    var description = $('#taxDesc').val();
    var type = $('#taxType').val();
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data: ({
            'name': name,
            'description': description,
            'type': type,
            'intent': 'addTaxonomy'
        }),
        dataType: "html",
        error: errorHandler,
        success: function (param) {

            $('#' + type).append('<li id="tax'+param+'"><label><div class="checkbox-inline icheck"><input type="checkbox" name="taxonomy[]" form="edited_post" value="'+param+'"></div>' + name + '</label></li>');
        }
    })
}
function preparePost() {
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data: ({
            'intent': 'preparePost'
        }),
        dataType: "html",
        error: errorHandler,
        success: function (id) {
            $('#id').val(id);
        }
    })
}


function saveTitle(id) {
    var title = $('#text'+id).val();
    var post_id = $('#id').val();
    $.ajax({
        url: '/api/ajax',
        type: 'POST',
        data: ({
            'id': id,
            'post_id':post_id,
            'title': title,
            'intent': 'saveImageTitle'
        }),
        dataType: "html",
        error: errorHandler,
        success: function (id) {
            $('#id').val(id);
        }
    })
}

function deleteImage(id) {
    if (confirm('Вы уверены?') == true) {
        $.ajax({
            url: '/api/ajax',
            type: 'POST',
            data: ({
                'id': id,
                'intent': 'deleteImage'
            }),
            dataType: "html",
            error: errorHandler,
            success: function () {
                $('#img' + id).fadeOut(400);
                $('#text' + id).fadeOut(400);
            }
        })

    }
}

function errorHandler(data) {
    alert('Ошибка :' + data.status);
}