<?= '<?xml version="1.0" encoding="UTF-8"?>'?>
<complexes>
    @foreach($objects as $object)
    <complex>
        <id>{{$object->domclick_id}}</id>
        <name>{{$object->name}}</name>
        <latitude>{{$object->latitude}}</latitude>
        <longitude>{{$object->longitude}}</longitude>
        <address>{{$object->address}}</address>
         <images>
            <image>http://sjs.su/front/objects/{{$object->image}}</image>
        </images>
        <description_main>
            <title>{{$object->name}}</title>
            <text> {{strip_tags(preg_replace("/&#?[a-z0-9]+;/i"," ",str_ireplace("&nbsp;", ' ',$object->description )) )}}</text>
        </description_main>
        <!-- description_secondary -->
        <infrastructure>
            <!-- parking -->
            <security>{{$object->security}}</security>
            <sports_ground>{{$object->sports_ground}}</sports_ground>
            <playground>{{$object->playground}}</playground>
            <school>{{$object->school}}</school>
            <kindergarten>{{$object->kindergarten}}</kindergarten>
            <fenced_area>{{$object->fenced_area}}</fenced_area>
        </infrastructure>
        <!-- videos-->
        <profits_main>
            <profit_main>
                <title>{{$object->name}}</title>
                <text>{{$object->utp}}</text>
                <image>http://sjs.su/front/objects/{{$object->image}}</image>
            </profit_main>
        </profits_main>
        <!-- profits_secondary -->
        <buildings>
            @foreach($object->buildings->where('available', 1) as $building)
            <building>
                <id>{{$building->domclick_id}}</id>
                <fz_214>{{$building->fz_214}}</fz_214>
                <name>{{$building->name}}</name>
                <floors>{{$building->floors->count()}}</floors>
                <?php //<floors_ready>{{$building->floors->where('ready_status', 1)->count()}}</floors_ready>?>
                <building_state>{{$building->building_state}}</building_state>
                <built_year>{{$building->built_year}}</built_year>
                <ready_quarter>{{$building->ready_quarter}}</ready_quarter>
				@if(!empty($building->building_phase))
                <building_phase>{{$building->building_phase}}</building_phase>
                @endif
                <building_type>{{$building->building_type}}</building_type>
                <!-- image -->
                <flats>
                    @foreach($building->livingFlats() as $flat) 
                    <flat>
                        <flat_id>{{$flat->id}}</flat_id> 
                        <apartment>{{$flat->number}}</apartment>
                        <floor>{{$flat->floor->level}}</floor>
                        <room>{{$flat->rooms}}</room>
                        <plan>http://sjs.su/front/flats/{{$flat->image}}</plan> 

                        <balcony>{{$flat->balcony}}</balcony>
                        <renovation>{{$flat->renovation}}</renovation>

                        <price>{{$flat->price}}</price>
                        <area>{{$flat->area}}</area>
                        @if(!empty($flat->kitchen_area))
                        <kitchen_area>{{$flat->kitchen_area}}</kitchen_area>
                        @endif
						@if(!empty($flat->living_area))
                        <living_area>{{$flat->living_area}}</living_area>
						@endif
                        <window_view>{{$flat->window_view}}</window_view>
                        <bathroom>{{$flat->bathroom}}</bathroom>
                        <!-- rooms_area -->
                    </flat>
                    @endforeach
                </flats>
            </building>
            @endforeach
        </buildings>
       <!-- discounts-->
        <sales_info>
            <sales_phone>+7 (928) 818-50-50</sales_phone>
            <responsible_officer_phone>+7 (928) 818-50-50</responsible_officer_phone>
            <sales_address>ул.Героев Медиков, 56 / Зашкольный пер., 3, Кисловодск, Ставропольский край</sales_address>
            <sales_latitude>43.918707</sales_latitude>
            <sales_longitude>42.712241</sales_longitude>
            <timezone>+3</timezone>
            <work_days>
			    <work_day>
			      <day>пн</day>
			      <open_at>08:00</open_at>
			      <close_at>17:00</close_at>
			    </work_day>
			    <work_day>
                  <day>вт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>ср</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>чт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>пт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
		  </work_days>
        </sales_info>
        <developer>
            <id>243118</id>
            <name>ООО «Стройжилсервис»</name>
            <phone>+7 (800) 550-33-85</phone>
            <site>http://sjs.su/</site>
            <logo>http://sjs.su/front/img/feed_logo.png</logo>
        </developer>
    </complex>
    @endforeach
</complexes>