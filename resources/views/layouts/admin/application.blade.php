@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h3 class="mt0">Заявки</h3>
                </div>


                <div class="container-fluid">
                    <div class="panel">
                        <div class="panel-body panel-no-padding">

                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <!--  <th width="40">
                                      </th>-->
                                    <th>Имя</th>
                                    <th>Телефон</th>
                                    <th>Email</th>
                                    <th width="350">Вопрос</th>
                                    <th width="150">Дата публикации</th>
                                    <!--  <th width="120">Действия</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($applications as $application)
                                    <tr>
                                        <td >{!! $application->created_at !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection