@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование категории</h1>&nbsp;<a href="/category/{!!$post->slug!!}" target="_blank" style="margin-top: 5px;">посмотреть на сайте</a>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/save_category" name="edited_post" method="post" id="edited_post" enctype="multipart/form-data">
                                <input type="text" class="form-control input-lg mb20" name="name" value="{!! ($post->name=='Новая категория') ? "":$post->name !!}" placeholder="Введите заголовок..." required>
                                <input type="hidden" class="form-control input-lg mb20" name="type" value="category">
								<input type="hidden" class="form-control input-lg mb20" name="hiddenslug" value="@if(isset($post->slug)){!!$post->slug!!}"@endif>

                                <textarea class="editor" name="description" id="editor" cols="30" rows="10">
                                    {!!$post->description!!}
                                </textarea>
                                <br>
                                <br>
                                <hr>
                                <span class="h2">SEO - настройки</span>
                                <hr>

                                <span class="h3">Ключевые слова</span>
                                <input type="text" name="keywords" id="keywords" class="form-control" value="{!!$post->keywords!!}">
                                {!!csrf_field()!!}
                                <input type="hidden"  name="id" id="id" value="{!! $post->id !!}">
                                <input type="hidden"  name="parent_id" value="1">
                                <input type="hidden"  name="type" value="category">
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <!--dt>Статус</dt>
                                        <dd>Published</dd>

                                        <dt>Видимость</dt>
                                        <dd>Опубликовано</dd>

                                        <dt>Редакции</dt>
                                        <dd>2 <a href="">Browse</a></dd-->

                                        <dt>Время публикации</dt>
                                        <dd>{!! $post->created_at !!}</dd>

                                        <dt>Последняя редакция</dt>
                                        <dd>{!! $post->updated_at !!}</dd>
                                    </dl>

                                    <div class="panel-footer" >
                                        <input type="submit" class="pull-right btn btn-info" form="edited_post" value="Сохранить">
                                         @if($post->slug != '')    
                                          <button type="button" class=" btn btn-danger" onclick="deleteCategory('{{$post->id}}')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Удалить </button>
										 @endif 
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <!-- Modal images -->
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;">360 CMS</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection