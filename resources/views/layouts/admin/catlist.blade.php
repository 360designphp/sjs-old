@extends('layouts.admin.index')
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Список категорий</h1>
                      
                       
                </div>
                <div class="container-fluid">
  <style>
 
  p#ramka {
    background-color:#E4E4E4;
    opacity: 0.6;
    text-align: center;
    color: black;
  border-radius: 5px;
  padding:3px ;
 
 margin-bottom: 15px;
 
 

}
  
  .buttonPublish { 
  display: inline-block;
  margin: 10px;
  width: 60px;
  height: 60px;
  background: url(http://subtlepatterns.com/patterns/extra_clean_paper.png);
  cursor: pointer;
  border-radius: 50%;

  box-shadow: 0 3px 20px rgba(0,0,0,.25),
  inset 0 2px 0 rgba(255,255,255,.6),
  0 2px 0 rgba(0,0,0,.1),
  inset 0 0 20px rgba(0,0,0,.1);
 }
.buttonPublish:hover {
  box-shadow: inset 0 0 20px rgba(0,0,0,.2),
  0 2px 0 rgba(255,255,255,.4),
  inset 0 2px 0 rgba(0,0,0,.1);
}





  .buttonDell {
  display: inline-block;
  margin: 10px;
  width: 60px;
  height: 60px;
  background: url(http://subtlepatterns.com/patterns/extra_clean_paper.png);
  cursor: pointer;
  border-radius: 50%;

  box-shadow: 0 3px 20px rgba(0,0,0,.25),
  inset 0 2px 0 rgba(255,255,255,.6),
  0 2px 0 rgba(0,0,0,.1),
  inset 0 0 20px rgba(0,0,0,.1);
  }
 .buttonDell:hover {
  box-shadow: inset 0 0 20px rgba(0,0,0,.2),
  0 2px 0 rgba(255,255,255,.4),
  inset 0 2px 0 rgba(0,0,0,.1);
}





  .buttonUnPub {
    
    display: inline-block;
  margin: 10px;
  width: 60px;
  height: 60px;
  background: url(http://subtlepatterns.com/patterns/extra_clean_paper.png);
  cursor: pointer;
  border-radius: 50%;

  box-shadow: 0 3px 20px rgba(0,0,0,.25),
  inset 0 2px 0 rgba(255,255,255,.6),
  0 2px 0 rgba(0,0,0,.1),
  inset 0 0 20px rgba(0,0,0,.1);
  }
  .buttonUnPub:hover{
    
    box-shadow: inset 0 0 20px rgba(0,0,0,.2),
  0 2px 0 rgba(255,255,255,.4),
  inset 0 2px 0 rgba(0,0,0,.1);
    
  }
  
  
  .btnPub {
  background: url(/assets/img/54.png) center center no-repeat;
}

.btnDelll {
  background: url(/assets/img/51.png)center center no-repeat;
}

.btnUnPub {
    word-spacing: -12px;
}
  
    a#dell {
    color: red;
    text-decoration: underline;
   }   
    a#druft {
    color: #CC9724;
     text-decoration: underline;
   } 
    a#publ {
    
    color: #48AC18;
     text-decoration: underline;
   }           
                
                   </style>
                   <div class="row">
                        <div class="col-md-12">
                            <form action="" class="form-inline mb10">
            
                            </form>




                            <div class="panel">
                                <div class="panel-body panel-no-padding">
                                
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Наименование</th>
                                            <th>описание</th>
                                            <th width="150">Дата публикации</th>
                                            <th width="350">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
										
                                        @foreach($posts as $post)
                                        <tr>
                                            <td><a href="/admin/{!!$post->type!!}/edit/{!!$post->slug!!}">{!! $post->name !!}</a></td>
                                            <td>{!!$post->description!!}</td>
                                            <td >{!! $post->created_at !!}   
                                            <p id="ramka">  Изменен</br>  
                                            {!! $post->updated_at !!}
                                           </p>
                                            </td>
                                            <td>
                                   <div class = "btn-group" role="group" aria-label="...">

            <a  href="/admin/{!!$post->type!!}/edit/{!!$post->slug!!}"> <button type="button" class=" btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Редактировать </button> </a>
                              
           <button type="button" class=" btn btn-danger" onclick="deleteCategory('{{$post->id}}')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Удалить </button>
                                            
                        
                 </div>  
                                            </td>
                                         <!--   <td>
                                                <a href="/{!!$post->type!!}/{!!$post->slug!!}" class="btn btn-success btn-xs btn-label" target="_blank"><i class="fa fa-search"></i>Посмотреть</a><br>
                                                <a href="/admin/{!!$post->type!!}/edit/{!!$post->slug!!}" class="btn btn-default btn-xs btn-label"><i class="fa fa-pencil"></i>Редактактировать</a><br>
                                                <a href="/admin/delete_post/{!! $post->slug !!}" class="btn btn-danger btn-xs btn-label"><i class="fa fa-trash-o"></i>Удалить</a>
                                            </td>-->
                                        </tr>
                                         @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection