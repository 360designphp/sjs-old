@include('layouts.admin.common.header')
@include('layouts.admin.common.sidebar')
@yield('content')
@include('layouts.admin.common.footer')