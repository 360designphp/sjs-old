@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование FAQ</h1>&nbsp;&nbsp;&nbsp;@if($post->type =='building_page')<a href="/objects/building_progress/{!!$post->parent_id!!}" target="_blank" style="margin-top: 5px;">посмотреть на сайте</a>
                    @else <a href="/{!!$post->type!!}/{!!$post->slug!!}" target="_blank" style="margin-top: 5px;">посмотреть на сайте</a>
                    @endif

                </div>
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/save_post" name="edited_post" method="post" id="edited_post" enctype="multipart/form-data">
                                <input type="text" class="form-control input-lg mb20" name="title" value="{!! ($post->title=='Новая запись') ? "":$post->title !!}" placeholder="Введите заголовок..." required>
                                <input type="text" class="form-control input-lg mb20" name="slug" value="{!! ($post->title=='Новая запись') ? "":$post->slug !!}" placeholder="Введите ссылку..." required>
                                <input type="hidden" class="form-control input-lg mb20" name="type" value="{!! $type!!}">

                                <textarea class="editor" name="text" id="editor" cols="30" rows="10">
                                    {!! $post->content !!}
                                </textarea>
                                <br>
                                <br>
                                <hr>
                                <span class="h2">SEO - настройки</span>
                                <hr>
                                <span class="h3">Мета-описание</span>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{!!$post->description!!}</textarea>
                                <br>
                                <span class="h3">Ключевые слова</span>
                                <input type="text" name="keywords" id="keywords" class="form-control" value="{!!$post->keywords!!}">
                                {!!csrf_field()!!}
                                <input type="hidden"  name="id" id="id" value="{!! $post->id !!}">
                                <input type="hidden"  name="parent_id" value="@if(isset($parent_id)){{$parent_id}}@else{{$post->id}}@endif">
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <!--dt>Статус</dt>
                                        <dd>Published</dd>

                                        <dt>Видимость</dt>
                                        <dd>Опубликовано</dd>

                                        <dt>Редакции</dt>
                                        <dd>2 <a href="">Browse</a></dd-->

                                        <dt>Время публикации</dt>
                                        <dd>{!! $post->created_at !!}</dd>

                                        <dt>Последняя редакция</dt>
                                        <dd>{!! $post->updated_at !!}</dd>
                                    </dl>

                                    <div class="panel-footer" >

                                        <button class="btn btn-primary" onclick="$('#myModal').modal('show');">Изображения</button>



                                        <input type="submit" class="pull-right btn btn-info" form="edited_post" value="Сохранить">


                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Миниатюра</h3>
                                    @if(isset($thumbnail))
                                        <img src=" /front/images/{!!$thumbnail->slug!!}" alt="" class="img-responsive" height="100">

                                    @else
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">

                                    @endif

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="thumbnail" form="edited_post" value="Обновить">

                                        @if(isset($thumbnail->id))
                                            <button type="button" class="pull-right btn btn-danger"  onclick="reloadPage()">Удалить</button>
                                            <script>


                                                function reloadPage(){
                                                    deleteImage({!!$thumbnail->id!!})
                                                    window.location.reload();

                                                }

                                            </script>




                                        @endif


                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0 mb20">Категории</h3>

                                    <div class="tab-container tab-default mb0">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#all-cat" data-toggle="tab">Категории</a></li>
                                            <!--   <li><a href="#tags" data-toggle="tab">Теги</a></li>-->
                                            <li><a data-toggle="modal" href="#taxModal" style="background-color: #008898; color: #fff;">➕</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="all-cat">
                                                <ul class="list-unstyled mb0" id="category">
                                                    @foreach($categories as $category)
                                                        <li id="tax{!!$category->id!!}"><label><div class="checkbox-inline icheck"><input type="checkbox" name="taxonomy[]" form="edited_post" @if(isset($post_categories))@foreach($post_categories as $p_cat)@if(in_array($category->id, $p_cat))checked @endif @endforeach @endif value="{!!$category->id!!}"></div>{!! $category->name !!}</label></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <!-- Modal images -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog" style="min-width:1000px;">
                <div class="modal-content">
                    <div class="modal-header" style="border:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title">Изображения</h2>
                        <span class="bg-warning">Поддерживаемые форматы &laquo;Jpeg , Png, Gif&raquo; </span>
                    </div>
                    <div class="modal-body clearfix" >

                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist" style="padding-left: 20px;padding-right: 20px;margin-left: -20px;margin-right: -20px;">
                                <li role="presentation" class="active">
                                    <a href="#gallery" aria-controls="tab" role="tab" data-toggle="tab">Галерея</a>
                                </li>

                            </ul>
                            <style type="text/css">
                                #gallery .img-holda {height: 140px;width: 100%;background-color: rgba(200,200,200,0.1);position: relative;margin-bottom: 20px;}
                                #gallery .img-holda .xxx{height: 30px;width: 30px; background: rgba(255,255,255,.5);right: 0;top: 0;color:#000;text-align: center;font-size: 16px;font-weight: 700;display: none;}
                                #gallery img{position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;max-height: 140px;}
                                #gallery .img-holda:hover .xxx{display: block;right: 0;top: 0;position: absolute;cursor: pointer;z-index: 1022;}
                                #gallery {padding-top: 20px;}

                            </style>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="gallery">
                                    <div class="control-group clearfix">
                                        <?php $galleries = $post->gallery();?>
                                        @foreach($galleries as $gal)
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 clearfix">
                                                <div class="img-holda" id="img{!! $gal->id !!}">
                                                    <div class="xxx" onclick="deleteImage({!!$gal->id!!})">x</div>
                                                    <img src="/front/thumbnail/{!! $gal->slug !!}" alt="" class="img-responsive">
                                                </div>
                                                <div style="margin: -16px 0 20px 0;">
                                                    <input type="text" value="{!!$gal->title!!}" onchange="saveTitle({!!$gal->id!!});" id="text{!!$gal->id!!}" style="width: 100%;">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Фотогалерея</label>
                                        <div class="controls">
                                            <form action="/admin/save_images" class="dropzone" id="post_gallery">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="parent_id" value="{!! $post->id !!}">
                                                <input type="hidden" name="type" value="gallery">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal add taxonomy-->

        <div class="modal fade" id="taxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="max-width: 500px; margin: 0 auto;">
                    <div class="modal-header" style="border:none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title">Создание категорий и тегов</h2>
                    </div>

                    <div class="modal-body clearfix" >

                        <label for="taxName">Имя </label>
                        <input type="text" id="taxName" class="form-control">

                        <label for="taxDesc" >Описание </label>
                        <input type="text" id="taxDesc" class="form-control">

                        <label for="taxType">Тип </label>
                        <select name="" id="taxType" class="form-control">
                            <option value="tag">Тег</option>
                            <option value="category">Категория</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn" style="background: #008898; color: #fff;" onclick="addTaxonomy()">Создать</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;">360 CMS</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection