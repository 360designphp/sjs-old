@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Настройки сайта</h1>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body">
                                <form action="" class="form-horizontal row-border">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Название сайта</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="site_name" placeholder="Мой сайт" value="{{$settings->site_name}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ключевые слова</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="keywords" placeholder="слова, помогающие, в, продвижении, сайта" value="{{$settings->keywords}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Мета-описание сайта</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control fullscreen" placeholder="Наш сайт - лучший сайт в рунете..." id="description">{{$settings->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">email для уведомлений</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" id="email" placeholder="mail@mail.ru" value="{{$settings->email}}">
                                        </div>
                                    </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="scripts">Скрипты и счётчики</label>
                                      <div class="col-sm-8">
                                          <textarea class="form-control fullscreen" id="scripts">{!! $settings->scripts !!}</textarea>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="header_scripts">Код для вставки в head</label>
                                      <div class="col-sm-8">
                                          <textarea class="form-control fullscreen" id="header_scripts">{!! $settings->header_scripts !!}</textarea>
                                      </div>
                                  </div>
                                </form>
                                    {!!csrf_field()!!}
                                    <button class="btn-primary btn" onclick="save()">Сохранить</button>
                                    <br>
                                    <br>
                        </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection