@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Управление меню</h1>
                </div>
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body panel-no-padding">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Надпись</th>
                                            <th>Открывать в новом окне</th>
                                            <th>Ссылка</th>
                                            <th>Зона меню</th>
                                            <th>Порядок</th>                                            
                                            <th>Удалить</th>
                                              <th>Родитель</th>
                                        </tr>
                                        </thead>
                                        
                                        <tbody id="menu_list">
                                        @if(isset($menu_items))
                                        @foreach($menu_items as $item)
                                        <tr id="item-{!!$item->id!!}" onchange="saveItem({!! $item->id !!})">
                                            <td><input type="text" id="caption-{!!$item->id!!}" value="{!! $item->caption !!}"></td>
                                            <td><input type="checkbox" id="blank-{!!$item->id!!}" name="blank-{!!$item->id!!}" @if($item->blank == 1)checked @endif></td>
                                            <td><input type="text" id="link-{!! $item->id !!}" value="{!! $item->link !!}"></td>
                                            <td><select name="zone" id="zone-{!! $item->id !!}">
                                                    <option value="top_menu" @if($item->zone == 'top_menu')selected @endif>Верхнее меню</option>
                                                    <option value="bottom_menu" @if($item->zone == 'bottom_menu')selected @endif>Нижнее меню</option>
                                                   
                                                </select></td>
                                            <td><input type="number" id="weight-{!! $item->id !!}" value="{!!$item->weight!!}" min="0"></td>
                                          
                                            <!--<td><select name="parent" class="parent">
                                                    @foreach($menu_items as $m_item)
                                                        <option value="{!! $m_item->id !!}">{!!$m_item->caption!!}</option>
                                                    @endforeach
                                                </select></td>-->
                                            <!--<td><input type="checkbox" id="visible-{!! $item->id !!}" @if($item->checked == true)checked @endif></td>-->
                                            <td><i class="fa fa-trash" onclick="deleteItem({!!$item->id!!});"></i></td>
                                            
                                              <td> 
                                              @if(isset($parent))
                                            <select name="parent" id="parent-{!!$item->id!!}">
                                             <option value="0">Без родителя</option>
                                            @foreach($parent as $itemParent)
                                            @if($itemParent->id==$item->id) 
                                            @continue 
                                            @endif 
                            <option value="{!!$itemParent->id!!}" @if($item->parent_id == $itemParent->id) selected @endif)>{!!$itemParent->caption!!} </option>
                                        
                                                     @endforeach
                                                     @endif 
                                                </select> 
                                            </td>
                                        </tr>
                                         @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <button class="btn btn-primary" onclick="addItem();">Добавить</button>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection