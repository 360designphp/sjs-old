@if (Auth::check())


    @if(Auth::user()->role=="User")
        <script language="JavaScript">
            window.location.href = "/logout"
        </script>
    @endif

@endif
@extends('layouts.admin.index')
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>
                    </h1>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @if($Type!='faq')
                            <form action="" class="form-inline mb10">
                                <div class="form-group">
                                    <select name="category" id="" class="form-control">
                                        <option value="">Посмотреть все категории</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->slug}}">{!! $category->name !!}</option>
                                        @endforeach
                                    </select>

                                    <button type="submit" class="btn btn-default">Фильтровать</button>
                                </div>
                            </form>
                            @endif
                            <div class="panel">
                                <div class="panel-body panel-no-padding">
                                    <table class=" sortable table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            @if(isset ($Type))
                                                @if($Type!='faq')
                                                    <th>Заголовок</th>
                                                    <th>Категория</th>
                                                    <th width="150">Дата публикации</th>
                                                    <th width="350">Действия</th>
                                                @elseif($Type=='faq')
                                                    <th width="150">Заголовок</th>
                                                    <th width="350">Вопрос</th>
                                                    <th>Дата публикации</th>
                                                    <th>Действия</th>
                                                @endif
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset ($Type))
                                            @if($Type!='faq')
                                                @foreach($posts as $post)
                                                    <tr>
                                                        <td>
                                                            <a href="/admin/{!!$post->type!!}/edit/{!!$post->id!!}">{!! $post->title !!}</a><br>
                                                            <small><a href="/{!!$post->type!!}/{!!$post->slug!!}"
                                                                      target="_blank">посмотреть на сайте</a></small>
                                                        </td>
                                                        <td>@foreach($post->taxonomy->where('type', 'category') as $cat)
                                                                <a href="/admin/{{$post->type}}/list?category={!!$cat->slug!!}">{{$cat->name}}</a>
                                                                <br>
                                                            @endforeach
                                                        </td>
                                                        <td>{!! $post->created_at !!}
                                                            <p id="ramka"> Изменен</br>
                                                                {!! $post->updated_at !!}
                                                            </p>
                                                        </td>
                                                        <td>

                                                            <div class="  btn-group" role="group" aria-label="...">
                                                                <a href="/admin/deletePost/{!!$post->id!!}">
                                                                    <button type="button" class=" btn btn-danger"><span
                                                                                class="glyphicon glyphicon-remove"
                                                                                aria-hidden="true"></span> Удалить
                                                                    </button>
                                                                </a>
                                                            </div>
                                                        </td>
                                                @endforeach
                                            @elseif($Type=='faq')
                                                @foreach($posts as $post)
                                                    <tr>

                                                        <!--  <td><div class="icheck checkbox-inline"><input type="checkbox"></div></td>-->
                                                        <td>
                                                            <a href="/admin/{!!$post->type!!}/edit/{!!$post->id!!}">{!! $post->title !!}</a><br>
                                                            <small><a href="/faq"
                                                                      target="_blank">посмотреть на сайте</a></small>
                                                        </td>
                                                        <td>
                                                            {!! $post->content !!}
                                                        </td>
                                                        <td>{!! $post->created_at !!}
                                                            <p id="ramka"> Изменен</br>
                                                                {!! $post->updated_at !!}
                                                            </p>
                                                        </td>


                                                        <td>

                                                            <div class="  btn-group" role="group" aria-label="...">
                                                                <a href="/admin/deletePost/{!!$post->id!!}">
                                                                    <button type="button" class=" btn btn-danger">
                                                                            <span class="glyphicon glyphicon-remove"
                                                                                  aria-hidden="true"></span> Удалить
                                                                    </button>
                                                                </a>
                                                            </div>


                                                        </td>
                                                    </tr>
                                        </tbody>
                                        @endforeach
                                        @endif
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->
@endsection