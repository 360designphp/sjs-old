@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Фотогалерея</h1>
                </div>
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><h2>Галерея</h2></div>
                                <div class="panel-body pb0">

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="clearfix">
                                                <div class="btn-group pull-left" id="galleryfilter">
                                                    <button class="btn btn-default active" data-group="all">All</button>
                                                    <button class="btn btn-default" data-group="industrial">Industrial</button>
                                                    <button class="btn btn-default" data-group="nature">Nature</button>
                                                    <button class="btn btn-default" data-group="architecture">Architecture</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="clearfix">
                                                <div class="btn-group pull-right" id="gallerysort">
                                                    <button class="btn btn-default" data-order="desc"><i class="fa fa-sort-alpha-asc"></i><span class="hidden-xs"> Name</span></button>
                                                    <button class="btn btn-default" data-order="asc"><i class="fa fa-sort-alpha-desc"></i><span class="hidden-xs"> Name</span></button>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">

                                            <hr style="mt0 mb10">


                                            <ul class="gallery row">

                                                <div data-groups='["industrial"]' class="item-wrapper col-md-3" data-name="Rusty">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive"/>
                                                        <h3>Communication</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Enchanted Creek">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["architecture"]' class="item-wrapper col-md-3" data-name="Building">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Beauty</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Mill">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["industrial"]' class="item-wrapper col-md-3" data-name="Machiery">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["architecture"]' class="item-wrapper col-md-3" data-name="Fire Escape">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Mossy Tree">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["industrial"]' class="item-wrapper col-md-3" data-name="Demolition">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["architecture"]' class="item-wrapper col-md-3" data-name="Fountain">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Communication</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Rider">
                                                    <div class="item">
                                                        <img src="http://placehold.it/300&text=Placeholder" alt="" class="img-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="River">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Beauty</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["industrial"]' class="item-wrapper col-md-3" data-name="Corrosion Hazard">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Blog</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Gardens">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Portfolio</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["industrial"]' class="item-wrapper col-md-3" data-name="Code">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Portfolio</h3>
                                                    </div>
                                                </div>

                                                <div data-groups='["nature"]' class="item-wrapper col-md-3" data-name="Trees">
                                                    <div class="item">
                                                        <img src="http://placehold.it/250x250" class="image-responsive">
                                                        <h3>Music</h3>
                                                    </div>
                                                </div>


                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
        </div>
        <footer role="contentinfo">
            <div class="clearfix">
                <ul class="list-unstyled list-inline pull-left">
                    <li><h6 style="margin: 0;"> &copy; 2015 Avenger</h6></li>
                </ul>
                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
            </div>
        </footer>
    </div>
@endsection