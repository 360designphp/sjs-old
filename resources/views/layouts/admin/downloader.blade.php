<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="/admin/save_images" method="post" enctype="multipart/form-data" id="upload">
    <input type="file" name="file[]" multiple>
    {!! csrf_field() !!}
    <input type="submit">
</form>
<script>
    var form = document.getElementById('upload');
    var request = new XMLHttpRequest();

    form.addEventListener('submit', function (e) {
        e.preventDefault();
        var formdata = new FormData(form);
        request.open('post', '/admin/save_images');
        request.addEventListener('load', transferComplete)
        request.send(formdata);
    });
    
    function transferComplete(data) {
       console.log(data.currentTarget.response);
        
    }
</script>
</body>
</html>