@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Настройки пользователя</h1>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body">
                                <form action="/admin/update_user/{{$user->id}}" class="form-horizontal row-border" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Имя пользователя</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_name" name="user_name" value="{!! $user->name !!}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email-пользователя</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_email"  name="user_email"  value="{!! $user->email !!}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Новый пароль</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_password" name="new_password" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Повторите пароль</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="password_repeat" name="confirm_new_password" value="">
                                        </div>
                                    </div>
                                    <select name="role" id="">
                                        <option value="User" @if($user->role == 'User')selected @endif>Пользователь</option>
                                        <option value="Admin" @if($user->role == 'Admin')selected @endif>Администратор</option>
                                    </select>
                                    {!!csrf_field()!!}
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <input type="submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </div>
                                </form>
                        </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection