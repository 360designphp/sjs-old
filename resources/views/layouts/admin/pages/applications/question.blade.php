@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h3 class="mt0">Заявка вопрос-ответ</h3>
                </div>
                <div class="container-fluid">
                    <div class="panel">
                        <div class="panel-body panel-no-padding">

                            <table class="sortable table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <!--  <th width="40">
                                      </th>-->
                                    <th width="175">Дата создания заявки</th>
                                    <th width="120">Регион</th>
                                    <th style="max-width:100px">Имя</th>
                                    <th>Телефон</th>
                                    <th>Email</th>
                                    <th width="350">Вопрос</th>
                                    <th style="max-width: 150px;">Примечания</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($applications as $application)
                                    <tr @if($application->status == 'unread') class="danger"
                                        @endif id="appl-row-{{$application->id}}">
                                        <td>{!! $application->created_at !!}</td>
                                        <td>{!! $application->region !!}
                                        </td>
                                        <td style="max-width:100px">{!! $application->name !!}
                                        </td>
                                        <td>{!! $application->phone !!}
                                        </td>
                                        <td>{!! $application->email !!}
                                        </td>
                                        <td class="text-left">{!! $application->text !!}
                                        </td>
                                        <td>
                                            <div class="note-overlay-{{$application->id}}">
                                            <textarea class="note-{{$application->id}}" id="" cols="30" onchange="storeNote({{$application->id}})"
                                                      rows="10" style="max-width: 150px;">{{ $application->note }}</textarea>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class=" btn btn-primary btn-sm"
                                                    onclick="changeReadStatus({{$application->id}})" Изменить статус>
                                                <span id="read-{{$application->id}}"
                                                      class="glyphicon @if($application->status == 'unread')glyphicon-eye-open @else glyphicon-eye-close @endif "
                                                      aria-hidden="true"></span></button>
                                            <a href="/admin/archive_application/{!!$application->id!!}">
                                                <button type="button" class=" btn btn-warning btn-sm" title="В архив">
                                                    <span class="glyphicon glyphicon-folder-close"
                                                          aria-hidden="true"></span></button>
                                            </a><br>
                                            <a href="/admin/deleteapp/{!!$application->id!!}"
                                               style="color:red; font-weight: bold;">Удалить</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function changeReadStatus(id) {

            $.ajax({
                url: '/api/change_read_status',
                type: 'POST',
                data: ({
                    'id': id
                }),
                dataType: "html",
                error: errorHandler,
                success: function (data) {
                    if (data == 'read') {
                        $('#read-' + id).removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                        $('#appl-row-' + id).removeClass('danger');
                    } else {
                        $('#read-' + id).removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                        $('#appl-row-' + id).addClass('danger');
                    }
                }
            })
        }

        function storeNote(id) {
            $.ajax({
                url: '/api/store_note',
                type: 'POST',
                data: ({
                    'id': id,
                    'note': $('.note-' + id).val()
                }),
                dataType: "html",
                error: errorHandler,
                success: function (data) {
                    if (data == 'ok') {
                        $('.note-overlay-' + id).addClass('has-success');
                    } else {
                        $('.note-overlay-' + id).addClass('has-error');
                    }
                }
            })
        }
    </script>
@endsection