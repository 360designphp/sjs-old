@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h3 class="mt0">Архив заявок</h3>
                </div>
                <div class="container-fluid">
                    <div class="panel">
                        <div class="panel-body panel-no-padding">

                            <table class="sortable table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <!--  <th width="40">
                                      </th>-->
                                    <th width="150">Дата поступления</th>
                                    <th width="120">
                                        Регион
                                    </th>
                                    <th width="150">Имя</th>
                                    <th width="100">Телефон</th>
                                    <th width="100">Email</th>
                                    <th>Текст</th>
                                    <th>Тип заявки</th>
                                    <th width="225">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($applications as $application)
                                    <tr>
                                        <td >{!! $application->created_at !!}</td>
                                        <td>{!! $application->region !!}
                                        </td>
                                        <td>{!! $application->name !!}
                                        </td>
                                        <td>{!! $application->phone !!}
                                        </td>
                                        <td>{!! $application->email !!}
                                        </td>
                                        <td>{!! $application->text !!}
                                        </td>
                                        <td>
                                        @if($application->type == 'recall')
                                                    Заказ обратного звонка
                                        @endif
                                            @if($application->type == 'question')
                                                Вопрос
                                            @endif
                                            @if($application->type == 'booking')
                                                    <a href="/admin/objects/flat/{!! $application->flat_id !!}">Бронь квартиры</a>
                                            @endif
                                        </td>
                                        <td><a href="/admin/deleteapp/{!!$application->id!!}/archive">  <button type="button" class=" btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Удалить</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection