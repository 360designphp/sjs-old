@extends('layouts.admin.index')
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Список пользователей</h1>

                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" class="form-inline mb10">

                            </form>

                            <div class="panel">
                                <div class="panel-body panel-no-padding">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Почта</th>
                                            <th>Статус</th>
                                            <th width="150">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->role == 'Admin'?'Администратор':'Пользователь'}}</td>
                                                <td>
                                                    <a href="/admin/delete_user/{{$user->id}}" class="btn btn-danger">Удалить</a>
                                                    <a href="/admin/user/{{$user->id}}" class="btn btn-warning">Редакт.</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection
