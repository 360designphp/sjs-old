@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование корпуса</h1>@if($building->id)
                        &nbsp;<a href="/flat/{!!$building->id !!}" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    @endif
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/building/save" name="edited_building" method="post" id="edited_building"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="{{$building->id}}" name="id" >
                                {{csrf_field()}}
                                <label class="col-xs-12">№ корпуса
                                    <input type="text" class="form-control  mb20"
                                           value="{!! ($building->number)?$building->number:"" !!}"
                                           name="number"
                                           placeholder="Введите № корпуса..." required>
                                </label>
                                <label class="col-xs-12">Дополнительная информация
                                    <textarea class="editor" name="additional" id="editor" cols="30" rows="10">
                                        {!!$building->additional!!}
                                    </textarea>
                                </label>
                                <label class="col-xs-12">Статус
                                    <input type="text" class="form-control  mb20"
                                           value="{!! ($building->status)?$building->status:"" !!}"
                                           name="status"
                                           placeholder="Укажите статус корпуса..." required>
                                </label>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionDomclick">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionDomclick"
                                                       href="#collapseDomclick"><h2>Информация для домклик</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseDomclick" class="collapse">
                                                            <div class="form-group" style="width: 30%">
                                                                <label for="domclick_id">Domclick ID<input class="form-control" type="text" name="domclick_id" id="domclick_id"
                                                                 value="{{$building->domclick_id}}"></label>   
                                                            </div>
                                                            <div class="form-group" style="width: 40%">
                                                              <span>Cответствие ФЗ-214</span>
                                                                <label for="fz_214_no"><input type="radio" name="fz_214" id="fz_214_no"
                                                                 value="1" @if($building->fz_214 == 1)checked @endif>Да</label>
                                                                <label for="fz_214_yes"><input type="radio" name="fz_214" id="fz_214_yes"
                                                                 value="0" @if($building->fz_214 == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Статус стройки</span>
                                                              <select name="building_state">
                                                                <option></option>
                                                                @foreach($building->building_states as $value => $caption)
                                                                <option value="{{$value}}" @if($building->building_state == $value) selected @endif >{{$caption}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Материал стен</span>
                                                              <select name="building_type">
                                                                <option></option>
                                                                @foreach($building->building_types as $building_type)
                                                                <option value="{{$building_type}}" @if($building->building_type == $building_type) selected @endif >{{$building_type}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Очередь строительства</span>
                                                              <select name="building_phase">
                                                                <option></option>
                                                                @for($building_phase = 1; $building_phase < 4; $building_phase++)
                                                                <option value="{{$building_phase}}" @if($building->building_phase == $building_phase) selected @endif >{{$building_phase}}</option>
                                                                @endfor                                                              
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Квартал сдачи</span>
                                                              <select name="ready_quarter">
                                                                <option></option>
                                                                @for($ready_quarter = 1; $ready_quarter < 5; $ready_quarter++)
                                                                <option value="{{$ready_quarter}}" @if($building->ready_quarter == $ready_quarter) selected @endif >{{$ready_quarter}}</option>
                                                                @endfor                                                              
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                                <label for="built_year">Год сдачи<input class="form-control" type="text" name="built_year" id="built_year"
                                                                 value="{{$building->built_year}}"></label>   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO{{$building->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO{{$building->id}}"
                                                       href="#collapseObject{{$building->id}}"><h2>Служебная информация</h2></a>
                                                    <div class="accordion-body">
                                                        <table class="table mb0">
                                                            <thead>
                                                            </thead>
                                                        </table>
                                                        <div id="collapseObject{{$building->id}}" class="collapse">
                                                                <h3 class="mt0">Идентефикатор объекта</h3>
                                                                <input type="text" name="object_id" id="keywords" class="form-control"
                                                                       value="{!!$building->object_id!!}">
                                                                <br>
                                                                <h3 class="mt0">Код SVG</h3>
                                                                <textarea name="svg" class="form-control" cols="10" rows="20">{{$building->svg}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd>{!! $building->created_at !!}</dd>

                                        <dt>Последняя редакция</dt>
                                        <dd>{!! $building->updated_at !!}</dd>
                                    </dl>

                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_building"
                                               value="Сохранить">
                                        @if($building->id)
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('{{$building->id}}')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Изображение корпуса</h3>
                                    @if($building->image != null)
                                        <img src=" /front/buildings/{{$building->image}}" alt="" class="img-responsive"
                                             height="100">
                                    @else
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                    @endif

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="file"
                                               form="edited_building" value="Обновить">
                                        {{--@if($building->image)
                                            <button type="button" class="pull-right btn btn-danger"
                                                    onclick="deleteFloorImage({!! $building->id !!})">Удалить
                                            </button>
                                        @endif--}}
                                    </div>
                                </div>
                            </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer role="contentinfo">
                    <div class="clearfix">
                        <ul class="list-unstyled list-inline pull-left">
                            <li><h6 style="margin: 0;">360 CMS</h6></li>
                        </ul>
                        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                                    class="fa fa-arrow-up"></i></button>
                    </div>
                </footer>
            </div>
            @endsection
            @section('script')
                <script>
                    function deleteObjectImage(id) {
                        if (confirm('Вы уверены?') == true) {
                            $.ajax({
                                url: '/api/ajax',
                                type: 'POST',
                                data: ({
                                    'id': id,
                                    'intent': 'deleteObjectImage'
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function () {
                                    $('#img' + id).fadeOut(400);
                                    $('#text' + id).fadeOut(400);
                                }
                            })
                        }
                        window.location.reload();
                    }
                    function errorHandler(data) {
                        alert('Ошибка :' + data.status);
                    }
                </script>
@endsection

