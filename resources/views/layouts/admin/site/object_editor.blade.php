@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование объекта</h1>@if($object->id)
                        &nbsp;<a href="/objects/{!!$object->id !!}" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    @endif
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/save" name="edited_object" method="post" id="edited_object"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="{{$object->id}}" name="id">
                                <label class="col-xs-12">Название объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="name"
                                           value="{!! ($object->name)?$object->name:"" !!}"
                                           placeholder="Введите название объекта..." required></label>
                                <label class="col-xs-12">Адрес объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="address"
                                           value="{!! ($object->address) ? $object->address:"" !!}"
                                           placeholder="Введите адрес объекта..." required></label>
                                <label class="col-xs-12">Описание объекта
                                    <textarea class="editor" name="description" id="editor" cols="30" rows="10">
                                        {!!$object->description!!}
                                    </textarea>
                                </label>

                                <label class="col-xs-12">Строка срока сдачи объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="completion_date"
                                           value="{{$object->completion_date}}"
                                           placeholder="Укажите информацию о сроках сдачи объекта." required></label>

                                <br>
                                <div class="col-xs-6">
                                    <label>Материал
                                        <div class="radio"><label><input type="radio" name="material"
                                                                         value="concrete"
                                                                         @if($object->material == 'concrete' || $object->id == 'new') checked @endif
                                                >Бетон</label></div>
                                        <div class="radio"><label><input type="radio" name="material" value="bricks"
                                                                         @if($object->material == 'bricks') checked @endif>Кирпич</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="material" value="foam"
                                                                         @if($object->material == 'foam') checked @endif>Пенобетон</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <label>Тип отопления
                                        <div class="radio"><label><input type="radio" name="heating_type"
                                                                         value="own"
                                                                         @if($object->heating_type == 'own' || $object->id == 'new') checked @endif>Собственное</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="heating_type"
                                                                         value="central"
                                                                         @if($object->heating_type == 'central') checked @endif>Центральное</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <label>Статус
                                        <div class="radio"><label><input type="radio" name="status"
                                                                         value="building"
                                                                         @if($object->status == 'building' || $object->id == 'new') checked @endif>Строится</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="status" value="ready"
                                                                         @if($object->status == 'ready') checked @endif>Сдано</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="status" value="sold"
                                                                         @if($object->status == 'sold') checked @endif>Распродано</label>
                                        </div>
                                    </label>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionDomclick">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionDomclick"
                                                       href="#collapseDomclick"><h2>Информация для домклик</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseDomclick" class="collapse">
                                                            <label class="form-group" for="" style="width: 30%;" >Domclick ID
                                                                <input class="form-control" type="number" value="{{$object->domclick_id}}" name="domclick_id">
                                                            </label>
                                                            <label class="form-group" for="" style="width: 30%;">Широта
                                                                <input class="form-control" type="text" value="{{$object->latitude}}" name="latitude">
                                                            </label>
                                                            <label class="form-group" for="" style="width: 30%;" >Долгота
                                                                <input class="form-control" type="text" value="{{$object->longitude}}" name="longitude">
                                                            </label>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие охраны</span>
                                                                <label for="security_no"><input type="radio" name="security" id="security_no"
                                                                 value="1" @if($object->security == 1)checked @endif>Да</label>
                                                                <label for="security_yes"><input type="radio" name="security" id="security_yes"
                                                                 value="0" @if($object->security == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие паркинга</span>
                                                                <label for="parking_no"><input type="radio" name="parking" id="parking_no"
                                                                 value="1" @if($object->parking == 1)checked @endif>Да</label>
                                                                <label for="parking_yes"><input type="radio" name="parking" id="parking_yes"
                                                                 value="0" @if($object->parking == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие спорт. площадки</span>
                                                                <label for="sports_ground_no"><input type="radio" name="sports_ground" id="sports_ground_no"
                                                                 value="1" @if($object->sports_ground == 1)checked @endif>Да</label>
                                                                <label for="sports_ground_yes"><input type="radio" name="sports_ground" id="sports_ground_yes"
                                                                 value="0" @if($object->sports_ground == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие детской площадки</span>
                                                                <label for="playground_no"><input type="radio" name="playground" id="playground_no"
                                                                 value="1" @if($object->playground == 1)checked @endif>Да</label>
                                                                <label for="playground_yes"><input type="radio" name="playground" id="playground_yes"
                                                                 value="0" @if($object->playground == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие детского сада</span>
                                                                <label for="kindergarten_no"><input type="radio" name="kindergarten" id="kindergarten_no"
                                                                 value="1" @if($object->kindergarten == 1)checked @endif>Да</label>
                                                                <label for="kindergarten_yes"><input type="radio" name="kindergarten" id="kindergarten_yes"
                                                                 value="0" @if($object->kindergarten == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие школы</span>
                                                                <label for="school_no"><input type="radio" name="school" id="school_no"
                                                                 value="1" @if($object->school == 1)checked @endif>Да</label>
                                                                <label for="school_yes"><input type="radio" name="school" id="school_yes"
                                                                 value="0" @if($object->school == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Огороженная територия</span>
                                                                <label for="fenced_area_no"><input type="radio" name="fenced_area" id="fenced_area_no"
                                                                 value="1" @if($object->fenced_area == 1)checked @endif>Да</label>
                                                                <label for="fenced_area_yes"><input type="radio" name="fenced_area" id="fenced_area_yes"
                                                                 value="0" @if($object->fenced_area == 0)checked @endif>Нет</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <span>УТП</span>
                                                                <textarea class="form-control" name="utp" id="" cols="30" rows="10">{{$object->utp}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO{{$object->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO{{$object->id}}"
                                                       href="#collapseObject{{$object->id}}"><h2>Служебная
                                                            информация</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseObject{{$object->id}}" class="collapse">
                                                            <h3 class="mt0">Код SVG</h3>
                                                            <textarea name="svg" class="form-control" cols="10"
                                                                      rows="1">{{$object->svg}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!!csrf_field()!!}
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionB{{$object->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionB{{$object->id}}"
                                                       href="#collapseBuilding{{$object->id}}"><h2>SEO - настройки</h2>
                                                    </a>
                                                    <div class="accordion-body">
                                                        <div id="collapseBuilding{{$object->id}}" class="collapse">
                                                            <span class="h3">Ключевые слова</span>
                                                            <input type="text" name="seo_keywords" id="keywords"
                                                                   class="form-control"
                                                                   value="{!!$object->seo_keywords!!}"><br>
                                                            <span class="h3">Мета-описание</span>
                                                            <textarea name="seo_description" class="form-control"
                                                                      cols="30"
                                                                      rows="10">{{$object->seo_description}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd>{!! $object->created_at !!}</dd>

                                        <dt>Последняя редакция</dt>
                                        <dd>{!! $object->updated_at !!}</dd>
                                    </dl>

                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_object"
                                               value="Сохранить">
                                        @if($object->id)
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('{{$object->id}}')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Миниатюра</h3>
                                    @if($object->image != null)
                                        <img src=" /front/objects/{{$object->image}}" alt="" class="img-responsive"
                                             height="100">

                                    @else
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                    @endif

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="file"
                                               form="edited_object" value="Обновить">

                                        @if($object->image)
                                            <button type="button" class="pull-right btn btn-danger"
                                                    onclick="deleteObjectImage({!! $object->id !!})">Удалить
                                            </button>


                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Дополнительно</h3>
                                    <a href="#!" onclick="$('#docModal').modal('show');">Объектная документация</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- .container-fluid -->
        </div> <!-- #page-content -->
    </div>
    <!-- Modal images -->
    <!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
         <div class="modal-dialog" style="min-width:1000px;">
             <div class="modal-content">
                 <div class="modal-header" style="border:none;">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title">Изображения</h2>
                     <span class="bg-warning">Поддерживаемые форматы &laquo;Jpeg , Png, Gif&raquo; </span>
                 </div>
                 <div class="modal-body clearfix" >

                     <div role="tabpanel">
                         <!-- Nav tabs -->
    <!-- <ul class="nav nav-tabs" role="tablist" style="padding-left: 20px;padding-right: 20px;margin-left: -20px;margin-right: -20px;">
         <li role="presentation" class="active">
             <a href="#gallery" aria-controls="tab" role="tab" data-toggle="tab">Галерея</a>
         </li>

     </ul>
     <style type="text/css">
         #gallery .img-holda {height: 140px;width: 100%;background-color: rgba(200,200,200,0.1);position: relative;margin-bottom: 20px;}
         #gallery .img-holda .xxx{height: 30px;width: 30px; background: rgba(255,255,255,.5);right: 0;top: 0;color:#000;text-align: center;font-size: 16px;font-weight: 700;display: none;}
         #gallery img{position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;max-height: 140px;}
         #gallery .img-holda:hover .xxx{display: block;right: 0;top: 0;position: absolute;cursor: pointer;z-index: 1022;}
         #gallery {padding-top: 20px;}

     </style>-->
    <!-- Modal images -->

    <!-- Modal documents -->
    <!-- Modal images -->
    <div class="modal fade" id="docModal" tabindex="-1" role="dialog" aria-labelledby="docModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="min-width:1000px;">
            <div class="modal-content">
                <div class="modal-header" style="border:none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Документы</h2>
                    <span class="bg-warning">Поддерживаемые форматы &laquo;pdf , jpg, png&raquo; </span>
                </div>
                <div class="modal-body clearfix">

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist"
                            style="padding-left: 20px;padding-right: 20px;margin-left: -20px;margin-right: -20px;">
                            <li role="presentation" class="active">
                                <a href="#documents" aria-controls="tab" role="tab" data-toggle="tab">Документы</a>
                            </li>
                            <li role="presentation">
                                <a href="#object_gallery" aria-controls="tab" role="tab" data-toggle="tab">Галерея</a>
                            </li>

                            <li role="presentation">
                                <a href="#albums" aria-controls="tab" role="tab" data-toggle="tab">Ход строительства</a>
                            </li>

                        </ul>
                        <style type="text/css">
                            #gallery .img-holda {
                                height: 140px;
                                width: 100%;
                                background-color: rgba(200, 2 00, 200, 0.1);
                                position: relative;
                                margin-bottom: 20px;
                            }

                            #gallery .img-holda .xxx {
                                height: 30px;
                                width: 30px;
                                background: rgba(255, 255, 255, .5);
                                right: 0;
                                top: 0;
                                color: #000;
                                text-align: center;
                                font-size: 16px;
                                font-weight: 700;
                                display: none;
                            }

                            #gallery img {
                                position: absolute;
                                top: 0;
                                bottom: 0;
                                left: 0;
                                right: 0;
                                margin: auto;
                                max-height: 140px;
                            }

                            #gallery .img-holda:hover .xxx {
                                display: block;
                                right: 0;
                                top: 0;
                                position: absolute;
                                cursor: pointer;
                                z-index: 1022;
                            }

                            #gallery {
                                padding-top: 20px;
                            }

                        </style>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="documents">
                                <div class="control-group clearfix">
                                    <table style="width: 100%;">
                                        <th style="width: 600px;">Имя</th>
                                        <th>Очередь строит.</th>
                                        <th>Очередь вывода</th>
                                        <th>Дата загрузки</th>
                                        <th>Управление</th>
                                        @foreach($object->documents() as $document)
                                        <tr onchange="updateDocument({{$document->id}})">
                                            <td><input type="text" value="{!! $document->title!!}"
                                                       id="text{!! $document->id!!}">
                                                <small><a
                                                        href="/front/documents/{{$document->slug}}"
                                                        target="_blank">открыть</a></small>      
                                            </td>
                                            <td>
                                                <input type="number" value="{{$document->queue}}"
                                                onchange="saveQueue({{$document->id}})"
                                                id="queue{!! $document->id!!}" style="width: 50px;">
                                            </td>
                                            <td>
                                                <input type="number" value="{{$document->order}}"
                                                id="order{!! $document->id!!}" style="width: 50px;">
                                            </td>
                                            <td>
                                                <input type="text" id="created{!! $document->id!!}"
                                                value="{{$document->created_at}}">
                                            </td>
                                            <td>    
                                                <span class="btn btn-danger"onclick="deleteDocument({!!$document->id!!})">x</span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Загрузка документов</label>
                                    <div class="controls">
                                        <form action="/admin/save_documents" class="dropzone" id="object_documentation">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="parent_id" value="{!! $object->id !!}">
                                            <input type="hidden" name="type" value="document">
                                            <input type="hidden" name="parent_type" value="object">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="object_gallery">
                                <div class="control-group clearfix">
                                    @foreach($object->gallery() as $gal)
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 clearfix">
                                            <div class="img-holda" id="img{!! $gal->id !!}">
                                                <div class="xxx" onclick="deleteImage({!! $gal->id!!})">x</div>
                                                <img src="/front/thumbnail/{!! $gal->slug !!}" alt=""
                                                     class="img-responsive">
                                            </div>
                                            <div style="margin: -16px 0 20px 0;">
                                                <input type="text" value="{!! $gal->title!!}"
                                                       onchange="saveTitle({!! $gal->id!!});" id="text{!! $gal->id!!}"
                                                       style="max-width: 200px;">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Фотогалерея</label>
                                    <div class="controls">
                                        <form action="/admin/save_images" class="dropzone" id="post_gallery">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="parent_id" value="{!!$object->id !!}">
                                            <input type="hidden" name="type" value="object_gallery">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="albums">
                                <div class="control-group">
                                    <label class="control-label">Ход строительства</label>
                                    <div class="controls">
                                        <a href="/admin/object_album/edit/new/{{$object->id}}">Создать альбом</a>
                                        <hr>
                                        @foreach($object->albums() as $album)
                                            <a href="/admin/object_album/edit/{{$album->id}}">{{$album->title}} -
                                                редактировать</a><br><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <!-- Modal documents -->
    </div>
@endsection
@section('script')
    <script>
        function deleteDocument(id) {
            if (confirm('Вы уверены?') == true) {
                $.ajax({
                    url: '/api/ajax',
                    type: 'POST',
                    data: ({
                        'id': id,
                        'intent': 'deleteDocument'
                    }),
                    dataType: "html",
                    error: errorHandler,
                    success: function () {
                        $('#doc' + id).fadeOut(400);
                    }
                })

            }
        }

        function deleteObjectImage(id) {
            if (confirm('Вы уверены?') == true) {
                $.ajax({
                    url: '/api/ajax',
                    type: 'POST',
                    data: ({
                        'id': id,
                        'intent': 'deleteObjectImage'
                    }),
                    dataType: "html",
                    error: errorHandler,
                    success: function () {
                        $('#img' + id).fadeOut(400);
                        $('#text' + id).fadeOut(400);
                    }
                })
            }
            window.location.reload();
        }
         function updateDocument(id) {
            var title = $('#text' + id).val();
            var queue = $('#queue' + id).val();
            var order = $('#order' + id).val();
            var created_at = $('#created' + id).val();
            var post_id = $('#id').val();
            $.ajax({
                url: '/api/ajax',
                type: 'POST',
                data: ({
                    'id': id,
                    'post_id': post_id,
                    'title': title,
                    'queue': queue,
                    'order': order,
                    'created_at': created_at,
                    'intent': 'updateDocument'
                }),
                dataType: "html",
                error: errorHandler,
                success: function (id) {
                    $('#id').val(id);
                }
            })
        }

        function saveTitle(id) {
            var title = $('#text' + id).val();
            var post_id = $('#id').val();
            $.ajax({
                url: '/api/ajax',
                type: 'POST',
                data: ({
                    'id': id,
                    'post_id': post_id,
                    'title': title,
                    'intent': 'saveImageTitle'
                }),
                dataType: "html",
                error: errorHandler,
                success: function (id) {
                    $('#id').val(id);
                }
            })
        }

        function errorHandler(data) {
            alert('Ошибка :' + data.status);
        }

    </script>

@endsection

