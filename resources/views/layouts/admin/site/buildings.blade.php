@extends('layouts.admin.index')
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Список объектов</h1>&nbsp;&nbsp;&nbsp;<a href="/admin/objects/new" class="btn btn-success"
                                                                 title="Добавить объект">+</a>
                </div>
                <div class="container-fluid">
                    @foreach($objects as $object)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion-group" id="accordionO{{$object->id}}">
                                    <div class="panel accordion-item">
                                        <a class="accordion-title collapsed" data-toggle="collapse"
                                           data-parent="accordionO{{$object->id}}"
                                           href="#collapseObject{{$object->id}}"><h2>{{$object->name}}
                                                , {{$object->address}}</h2></a>
                                        <button class="btn btn-warning addbtn "
                                                title="Редактировать объект"
                                                onclick="window.open('/admin/objects/{{$object->id}}')"><i
                                                    class="fa fa-pencil" aria-hidden="true"></i></button>
                                        <div class="accordion-body">
                                            <table class="table mb0">
                                                <thead>
                                                <tr>
                                                    <th >Готовность объекта</th>
                                                    <th>Всего квартир</th>
                                                    <th>Забронированных квартир</th>
                                                    <th>Проданных квартир</th>
                                                    <th>Свободных квартир</th>
                                                    <th>Корпусов
                                                        <button class="btn btn-success btn-sm "
                                                                title="Добавить корпус"
                                                                onclick="prepareModal('Building',{{$object->id}})">+
                                                        </button>
                                                    </th>
                                                </tr>
                                                <tboy>
                                                    <tr>
                                                        <td>@if($object->status == 'building'){{'Строится'}}@elseif($object->status == 'ready'){{'Сдано'}}@else{{'Распродано'}} @endif
                                                        </td>
                                                        <td>{{$object->flats()->count()}}</td>
                                                        <td>{{$object->bookedFlats()->count()}}</td>
                                                        <td>{{$object->soldFlats()->count()}}</td>
                                                        <td>{{$object->freeFlats()->count()}}</td>
                                                        <td>{{$object->buildings()->count()}}

                                                        </td>
                                                    </tr>
                                                </tboy>
                                                </thead>
                                            </table>
                                        </div>

                                        <div id="collapseObject{{$object->id}}" class="collapse">
                                            <div class="accordion-body" id="BuildingFor{{$object->id}}">
                                                @foreach($object->buildings as $building)
                                                    <div class="accordion-group" id="accordionB{{$building->id}}">
                                                        <div class="panel accordion-item">
                                                            <a class="accordion-title collapsed" data-toggle="collapse"
                                                               href="#collapseBuilding{{$building->id}}"><h2>Корпус&nbsp;{{$building->number}}</h2>
                                                            </a>
                                                            <div class="accordion-body">
                                                                <table class="table mb0">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Секций в корпусе
                                                                            <button class="btn btn-success btn-sm "
                                                                                    title="Добавить секцию"
                                                                                    onclick="prepareModal('Section',{{$building->id}});">
                                                                                +
                                                                            </button>
                                                                        </th>

                                                                        <th>Этажей в корпусе
                                                                        </th>

                                                                    </tr>
                                                                    <tboy>
                                                                        <tr>
                                                                            <td>{{$building->sections()->count()}}</td>
                                                                            <td>{{$building->floors()->count()}}</td>
                                                                        </tr>
                                                                    </tboy>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                            <button class="btn btn-warning addbtn  "
                                                                    title="Редактировать корпус"
                                                                    onclick="window.open('/admin/objects/building/{{$building->id}}')">
                                                                <i class="fa fa-pencil"
                                                                   aria-hidden="true"></i>
                                                            </button>
                                                            <div id="collapseBuilding{{$building->id}}"
                                                                 class="collapse ">
                                                                <div class="accordion-body"
                                                                     id="SectionFor{{$building->id}}">
                                                                    @foreach($building->sections as $section)
                                                                        <div class="accordion-group"
                                                                             id="accordionS{{$section->id}}">
                                                                            <div class="panel accordion-item">
                                                                                <a class="accordion-title collapsed"
                                                                                   data-toggle="collapse"
                                                                                   href="#collapseSection{{$section->id}}">
                                                                                    <h2>Секция {{$section->symbol}}</h2>
                                                                                </a>
                                                                                <div class="accordion-body">
                                                                                    <table class="table mb0">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Этажей 
                                                                                            	<button class="btn btn-success btn-sm"
					                                                                                    title="Добавить этаж"
					                                                                                    onclick="prepareModal('Floor',{{$section->id}})">
					                                                                                +
					                                                                            </button>
                                                                        					</th> 
                                                                                            <th>Квартир в секции</th>
                                                                                            <th>Проданных квартир</th>
                                                                                            <th>Забронированных
                                                                                                квартир
                                                                                            </th>
                                                                                            <th>Свободных квартир</th>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>{{$section->floors()->count()}}</td>
                                                                                            <td>{{$section->flats()->count()}}</td>
                                                                                            <td>{{$section->soldFlats()->count()}}</td>
                                                                                            <td>{{$section->bookedFlats()->count()}}</td>
                                                                                            <td>{{$section->freeFlats()->count()}}</td>
                                                                                        </tr>
                                                                                        </thead>
                                                                                    </table>
                                                                                </div>
                                                                                <button class="btn btn-warning addbtn  "
                                                                                        title="Редактировать секцию"
                                                                                        onclick="window.open('/admin/objects/section/{{$section->id}}')">
                                                                                    <i class="fa fa-pencil"
                                                                                       aria-hidden="true"></i>
                                                                                </button>
                                                                                
                                                                                <div id="collapseSection{{$section->id}}"
                                                                                     class="collapse ">
                                                                                    <div class="accordion-body">
                                                                                        @foreach($section->floorsOrdered() as $floor)

                                                                                            <div class="accordion-group"
                                                                                                 id="accordionB{{$floor->id}}">
                                                                                                <div class="panel accordion-item">
                                                                                                    <a class="accordion-title collapsed"
                                                                                                       data-toggle="collapse"
                                                                                                       href="#collapseFloor{{$floor->id}}">
                                                                                                        <h2>{{$floor->level}}
                                                                                                            этаж</h2><a
                                                                                                                href="/admin/objects/flat/new/{{$floor->id}}"
                                                                                                                class="btn btn-success addbtn"
                                                                                                                title="Добавить квартиру">+</a>
                                                                                                    </a>
                                                                                                    <button class="btn btn-warning addbtn addbtn2  "
                                                                                                            title="Редактировать этаж"
                                                                                                            onclick="window.open('/admin/objects/floor/{{$floor->id}}')">
                                                                                                        <i class="fa fa-pencil"
                                                                                                           aria-hidden="true"></i>
                                                                                                    </button>
                                                                                                    <div id="collapseFloor{{$floor->id}}"
                                                                                                         class="collapse">
                                                                                                        <div class="accordion-body">
                                                                                                            <table class="table mb0">
                                                                                                                <table class="table mb0">
                                                                                                                    <thead>
                                                                                                                    <tr>
                                                                                                                        <th >
                                                                                                                            №
                                                                                                                        </th>
                                                                                                                        <th width="10" height="50">
                                                                                                                            <i class="fa fa-file-image-o"></i>
                                                                                                                        </th>
                                                                                                                        @if($floor->level>='1')
                                                                                                                            <th>
                                                                                                                                Комнат
                                                                                                                            </th>

                                                                                                                            <th width="100">
                                                                                                                                S&nbsp;<sub>жил</sub>
                                                                                                                            </th>
                                                                                                                            <th width="100">
                                                                                                                                S&nbsp;<sub>кух</sub>
                                                                                                                            </th>
                                                                                                                        @endif
                                                                                                                        <th width="100">
                                                                                                                            S&nbsp;<sub>общ</sub>
                                                                                                                        </th>
                                                                                                                        <th width="100">
                                                                                                                            &#8381
                                                                                                                        </th>
                                                                                                                        <th width="350">
                                                                                                                            Статус
                                                                                                                        </th>
                                                                                                                        <th width="150"></th>
                                                                                                                    </tr>
                                                                                                                    </thead>
                                                                                                                    @foreach($floor->flats as $flat)
                                                                                                                        <tbody>
                                                                                                                        <tr onchange="saveFlat({{$flat->id}})">
                                                                                                                            <td >{{$flat->number}}</td>
                                                                                                                            <td width="150">
                                                                                                                                @if($flat->image != null)
                                                                                                                                    <img data-original=" /front/flats/{{$flat->image}}"
                                                                                                                                         alt=""
                                                                                                                                         class="img-responsive lazy"
                                                                                                                                         height="100">
                                                                                                                                @endif
                                                                                                                            </td>
                                                                                                                            @if($floor->level>='1')
                                                                                                                                <td>{{$flat->rooms}}</td>

                                                                                                                                <td>
                                                                                                                                <input type="number"
                                                                                                                                       style="max-width:50px"
                                                                                                                                       class="living{{$flat->id}}"
                                                                                                                                       value="{{$flat->living_area}}" step="0.01">
                                                                                                                                    &nbsp;м<sup>2</sup>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                <input type="number"
                                                                                                                                      style="max-width:50px"
                                                                                                                                       class="kitchen{{$flat->id}}"
                                                                                                                                       value="{{$flat->kitchen_area}}" step="0.01">
                                                                                                                                    &nbsp;м<sup>2</sup>
                                                                                                                                </td>
                                                                                                                            @endif
                                                                                                                            <td>
                                                                                                                                <input type="number"
                                                                                                                                       style="max-width:50px"
                                                                                                                                       class="area{{$flat->id}}"
                                                                                                                                       value="{{$flat->area}}">
                                                                                                                                &nbsp;м<sup>2</sup>
                                                                                                                            </td>
                                                                                                                            <td width="250">
                                                                                                                                <input type="text"
                                                                                                                                       class="price{{$flat->id}}"
                                                                                                                                       value="{{$flat->price}}" step="0.01">
                                                                                                                            </td>
                                                                                                                            <td >
                                                                                                                                <div>
                                                                                                                                    <label><input
                                                                                                                                                type="radio"
                                                                                                                                                class="radio{{$flat->id}}"
                                                                                                                                                name="kv{{$flat->id}}"
                                                                                                                                                {{$flat->status == 'sold' ? 'checked':''}} value="sold">&nbsp;Продано</label>
                                                                                                                                    <label><input
                                                                                                                                                type="radio"
                                                                                                                                                class="radio{{$flat->id}}"
                                                                                                                                                id="booked"
                                                                                                                                                {{$flat->status == 'booked' ? 'checked':''}}
                                                                                                                                                name="kv{{$flat->id}}"
                                                                                                                                                value="booked">&nbsp;Забронировано</label>
                                                                                                                                    <label><input
                                                                                                                                                type="radio"
                                                                                                                                                class="radio{{$flat->id}}"
                                                                                                                                                id="free"
                                                                                                                                                name="kv{{$flat->id}}"
                                                                                                                                                {{$flat->status == 'free' ? 'checked':''}} value="free">&nbsp;Свободно</label>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                            <td>
                                                                                                                                <button class="btn btn-warning btn-sm "
                                                                                                                                        title="Редактировать квартиру"
                                                                                                                                        onclick="window.open('/admin/objects/flat/{{$flat->id}}')">
                                                                                                                                    <i class="fa fa-pencil"
                                                                                                                                       aria-hidden="true"></i>
                                                                                                                                </button>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td colspan="<?php echo $floor->level >= '1'?9:6; ?>">
                                                                                                                                <table class="table">
                                                                                                                                    <tr @if($flat->status == 'free') class="hidden" @endif id="{{$flat->id}}deal_head">
                                                                                                                                        <th width="350">
                                                                                                                                            ФИО
                                                                                                                                            покупателя
                                                                                                                                        </th>
                                                                                                                                        <th width="100">
                                                                                                                                            Телефон
                                                                                                                                        </th>
                                                                                                                                        <th width="100">
                                                                                                                                            №
                                                                                                                                            договора
                                                                                                                                        </th>
                                                                                                                                        <th width="50">
                                                                                                                                            Дата
                                                                                                                                            продажи
                                                                                                                                        </th>
                                                                                                                                    </tr>
                                                                                                                                    <tr @if($flat->status == 'free') class="hidden" @endif id="{{$flat->id}}deal_data" onchange="saveFlat({{$flat->id}})">

                                                                                                                                        <td width="">
                                                                                                                                            <input type="text"
                                                                                                                                                   class="cust_name{{$flat->id}}"
                                                                                                                                                   value="{{$flat->customer_name}}">
                                                                                                                                        </td>
                                                                                                                                        <td width="">
                                                                                                                                            <input type="text"
                                                                                                                                                   class="cust_phone{{$flat->id}}"
                                                                                                                                                   value="{{$flat->customer_phone}}">
                                                                                                                                        </td>
                                                                                                                                        <td width="">
                                                                                                                                            <input type="text"
                                                                                                                                                   class="contract{{$flat->id}}"
                                                                                                                                                   value="{{$flat->contract_number}}">
                                                                                                                                        </td>

                                                                                                                                        <td width="">
                                                                                                                                            <input type="text"
                                                                                                                                                   class="date{{$flat->id}}"
                                                                                                                                                   value="{{$flat->date_of_sale}}">
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        </tbody>
                                                                                                                    @endforeach
                                                                                                                </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!-- .container-fluid -->
                </div> <!-- #page-content -->
                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content" style="max-width: 500px; margin: 0 auto;">
                            <div class="modal-header" style="border:none;">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h2 class="modal-title" id="modal_title">Создание </h2>
                            </div>

                            <div class="modal-body clearfix">

                                <label for="modal_value">Обозначение </label>
                                <input type="text" id="modal_value" class="form-control">
                                <input type="hidden" id="modal_type">
                                <input type="hidden" id="modal_id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" class="btn" style="background: #008898; color: #fff;"
                                        onclick="saveEntity()">Создать
                                </button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                @endsection
                @section('script')
                    <script>
                        function prepareModal(type, parent_id) {
                            var title;
                            switch (type) {
                                case 'Building':
                                    title = 'Добавить корпус';
                                    break;
                                case 'Section':
                                    title = 'Добавить секцию';
                                    break;
                                case 'Floor':
                                    title = 'Добавить этаж';
                                    break;
                            }

                            $('#modal_title').text(title);
                            $('#modal_id').val(parent_id);
                            $('#modal_type').val(type);
                            $('#addModal').modal('show');
                        }

                        function saveEntity() {
                            var type = $('#modal_type').val();
                            var parent_id = $('#modal_id').val();
                            var value = $('#modal_value').val();
                            var parent;
                            var child;
                            switch (type) {
                                case 'Building':
                                    parent = 'Object';
                                    break;
                                case 'Section':
                                    parent = 'Building';
                                    break;
                                case 'Floor':
                                    parent = 'Section';
                                    break;
                            }
                            $.ajax({
                                url: '/admin/save_entity',
                                type: 'POST',
                                data: ({
                                    _token: '{{ csrf_token() }}',
                                    'parent_id': parent_id,
                                    'type': type,
                                    'value': value
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function (data) {
                                    data = JSON.parse(data);
                                    if (type == 'Floor') {
                                        location.reload();
                                    } else {
                                        $('#' + type + 'For' + parent_id).append(
                                            '<div class="accordion-group" id="accordionB' + parent_id + '">' +
                                            '<div class="panel accordion-item">' +
                                            '<a class="accordion-title collapsed" data-toggle="collapse" href="#collapse' + type + data.id + '"><h2>' + data.title + '</h2>' +
                                            '</a>' +
                                            '<button class="btn btn-success addbtn"' +
                                            'onclick="prepareModal(\'' + data.child + '\',' + data.id + ')">+</button>' +
                                            '<div id="collapse' + type + data.id + '" class="collapse ">' +
                                            '<div class="accordion-body" id="' + type + 'For1">' +
                                            '</div>' +
                                            '</div>' +
                                            '</div>' +
                                            '</div>');
                                    }
                                    $('#addModal').modal('hide');
                                }
                            })
                        }

                        function saveFlat(id) {
                            var status = $('input[name=kv' + id + ']:checked').val(),
                                price = $('.price' + id).val(),
                                area = $('.area' + id).val(),
                                living_area = $('.living' + id).val(),
                                kitchen_area = $('.kitchen' + id).val(),
                                customer_name = $('.cust_name' + id).val(),
                                customer_phone = $('.cust_phone' + id).val(),
                                contract_number = $('.contract' + id).val(),
                                date_of_sale = $('.date' + id).val();
                            if(price == '') {
                                price = 0;
                            }

                            $.ajax({
                                url: '/api/update_flat',
                                type: 'POST',
                                data: ({
                                    'id': id,
                                    'status': status,
                                    'price': price,
                                    'area': area,
                                    'living_area': living_area,
                                    'kitchen_area': kitchen_area,
                                    'customer_name': customer_name,
                                    'customer_phone': customer_phone,
                                    'date_of_sale': date_of_sale,
                                    'contract_number': contract_number,
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function () {
                                }
                            })
                            changeStatus(id, status);
                        }
                        function errorHandler(data) {
                            alert('Ошибка: ' + data.status);
                        }

                        function changeStatus(id, status){
                            if(status != 'free'){
                                $('#'+id+'deal_head').removeClass('hidden');
                                $('#'+id+'deal_data').removeClass('hidden');
                            } else {
                                $('#'+id+'deal_head').addClass('hidden');
                                $('#'+id+'deal_data').addClass('hidden');
                            }
                        }
                        $(function() {
                            $("img.lazy").lazyload();
                        });
                    </script>
@endsection