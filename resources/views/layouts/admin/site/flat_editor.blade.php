@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование @if($flat->floor->level > 0)квартиры @endif @if($flat->floor->level == 0)офиса @endif @if($flat->floor->level < 0)парковочного места @endif</h1>
                    @if($flat->id)
                        <a href="/objects/{{$flat->getObject()->id}}/{{$flat->building()->id}}/{{$flat->section()->id}}/{!!$flat->floor->id !!}#kv{{$flat->number}}" target="_blank"
                           style="margin-top: 5px;">посмотреть на сайте</a>
                        @endif
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/flat/save" name="edited_flat" method="post" id="edited_flat"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="{{$flat->id}}" name="id" >
                                {{csrf_field()}}
                                <label class="col-xs-12">№ @if($flat->floor->level > 0)квартиры@endif @if($flat->floor->level == 0)офиса@endif @if($flat->floor->level < 0)парковочного места@endif
                                    <input type="number" class="form-control  mb20"
                                           value="{!! ($flat->number)?$flat->number:"" !!}"
                                           name="number"
                                           placeholder="Введите № квартиры..." required>
                                </label>
                                <label class="col-xs-12">S<sub>общ</sub>
                                    <input type="text" class="form-control  mb20"
                                           value="{!! $flat->area ? $flat->area : 0 !!}"
                                           name="area"
                                           placeholder="Введите общую площадь ..." step="0.01">
                                </label>
                                @if($flat->floor->type == 'living')
                                <label class="col-xs-12">S<sub>жил</sub>
                                    <input type="number" class="form-control  mb20"
                                           value="{!!$flat->living_area ? $flat->living_area : 0 !!}"
                                           name="living_area"
                                           placeholder="Введите жилую площадь.." step="0.01">
                                </label>
                                <label class="col-xs-12">S<sub>кух</sub>
                                    <input type="number" class="form-control  mb20"
                                           value="{!!$flat->kitchen_area ? $flat->kitchen_area : 0!!}"
                                           name="kitchen_area"
                                           placeholder="Введите  площадь кухни..." step="0.01">
                                </label>

                                <label class="col-xs-12">Комнат
                                    <input type="number" class="form-control  mb20"
                                           value="{!! ($flat->rooms)?$flat->rooms:"1" !!}"
                                           name="rooms"
                                           placeholder="Введите  кол-во комнат..." >
                                </label>
                                @endif
                                <label class="col-xs-12">Цена
                                    <input type="number" class="form-control  mb20"
                                           value="{!! ($flat->price)?$flat->price:"0" !!}"
                                           name="price"
                                           min="0"
                                           placeholder="Введите  цену ..." >
                                </label>
                                <!-- <label class="col-xs-12">Ссылка на панораму
                                    <input type="text" class="form-control  mb20"
                                           value="{!! $flat->panorama_url!!}"
                                           name="panorama_url"
                                           placeholder="http://sjs.su/panorama/..." >
                                </label> -->
                                @if($flat->floor->type != 'living')
                                    <label class="col-xs-12">Цена
                                        <input type="number" class="form-control  mb20"
                                               value="{!! ($flat->rent_price)?$flat->rent_price:"0" !!}"
                                               name="rent_price"
                                               min="0"
                                               placeholder="Введите арендную плату..." >
                                    </label>
                                @endif
                                
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO{{$flat->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO{{$flat->id}}"
                                                       href="#collapseObject{{$flat->id}}"><h2>Служебная информация</h2></a>
                                                    <div class="accordion-body">
                                                        <table class="table mb0">
                                                            <thead>
                                                            </thead>
                                                        </table>
                                                        <div id="collapseObject{{$flat->id}}" class="collapse">
                                                            <label class="mt0">Идентификатор этажа
                                                                <input type="text" class="form-control  mb20"
                                                                       value="{!! ($flat->floor_id)?$flat->floor_id:"" !!}"
                                                                       name="floor_id"
                                                                       placeholder="Введите  идентификатор..." >
                                                            </label><br>
                                                            <label class="mt0">Зона изображения
                                                                <input type="text" class="form-control  mb20"
                                                                       value="{!! ($flat->image_zone)?$flat->image_zone:"" !!}"
                                                                       name="image_zone"
                                                                       placeholder="Введите  image_zone..." >
                                                            </label>
                                                                <h3 class="mt0">Код SVG</h3>
                                                                <textarea name="svg" class="form-control" cols="1" rows="1">{{$flat->svg}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-xs-6">Статус
                                    <div class="radio"><label><input type="radio" name="status" class="status"
                                                                     value="free"
                                                                     @if($flat->status == 'free' || !$flat->status || $flat->id == 'new') checked  @endif>Свободно</label>
                                    </div>
                                    <div class="radio"><label><input type="radio" name="status" class="status" value="sold"
                                                                     @if($flat->status == 'sold') checked  @endif>Продано</label>
                                    </div>
                                    <div class="radio"><label><input type="radio" name="status" class="status" value="booked"
                                                                     @if($flat->status == 'booked') checked  @endif>Забронировано</label>
                                    </div>
                                </label>
                                <div class="col-xs-6">
                                  <div class="form-group">
                                                              <span>Панорама</span>
                                                              <select name="panorama_url" class="form-control" style="max-width: 250px;">
                                                                <option></option>
                                                                @foreach($panoramas as $panorama)
                                                                <option value="/front/objects/parkovy_360/{{$panorama}}" @if($flat->panorama_url == '/front/objects/parkovy_360/' . $panorama) selected @endif >{{$panorama}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                </div>
                                <div class="sold @if($flat->status == 'free')hidden @endif">
                                    <label class="col-xs-12">ФИО покупателя
                                        <input type="text" class="form-control  mb20"
                                               value="{!! ($flat->customer_name)?$flat->customer_name:"" !!}"
                                               name="customer_name"
                                               placeholder="Введите ФИО покупателя..." >
                                    </label>
                                    <label class="col-xs-12">Телефон
                                        <input type="text" class="form-control  mb20"
                                               value="{!! ($flat->customer_phone)?$flat->customer_phone:"" !!}"
                                               name="customer_phone"
                                               placeholder="Введите телефон покупателя..." >
                                    </label>
                                    <label class="col-xs-12">№ договора
                                        <input type="text" class="form-control  mb20"
                                               value="{!! ($flat->contract_number)?$flat->contract_number:"" !!}"
                                               name="contract_number"
                                               placeholder="Введите № договора..." >
                                    </label>
                                    <label class="col-xs-12">Дата продажи
                                        <input type="text" class="form-control  mb20"
                                               value="{!! ($flat->date_of_sale)?$flat->date_of_sale:"" !!}"
                                               name="date_of_sale"
                                               placeholder="Введите  дату продажи..." >
                                    </label>
                                    <label class="col-xs-12" for="payment_graph">График оплаты
                                    </label>
                                    <textarea name="payment_graph" id="payment_graph" cols="30" rows="20" class="form-control">{{($flat->payment_graph)?$flat->payment_graph:"" }}</textarea>
                                       <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionDomclick">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionDomclick"
                                                       href="#collapseDomclick"><h2>Информация для домклик</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseDomclick" class="collapse">
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Отделка</span>
                                                              <select name="renovation">
                                                                <option></option>
                                                               @foreach($flat->renovations as $renovation)
                                                                <option value="{{$renovation}}" @if($flat->renovation == $renovation) selected @endif >{{$renovation}}</option>
                                                                @endforeach 
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие балкона</span>
                                                              <select name="balcony">
                                                                <option></option>
                                                                @foreach($flat->balconies as $balcony)
                                                                <option value="{{$balcony}}" @if($flat->balcony == $balcony) selected @endif >{{$balcony}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Вид из окна</span>
                                                              <select name="window_view">
                                                                <option></option>
                                                                @foreach($flat->window_views as $view)
                                                                <option value="{{$view}}" @if($flat->window_view == $view) selected @endif >{{$view}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Санузел</span>
                                                              <select name="bathroom">
                                                                <option></option>
                                                                @foreach($flat->bathrooms as $bathroom)
                                                                <option value="{{$bathroom}}" @if($flat->bathroom == $bathroom) selected @endif >{{$bathroom}}</option>
                                                                @endforeach                                                                
                                                              </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                @if($flat->floor->level <= 0)
                                    <label class="col-xs-6">Тип сделки
                                        <div class="radio"><label><input type="radio" name="deal" class="deal"
                                                                         value="buy"
                                                                         @if($flat->deal == 'buy' || !$flat->deal || $flat->id == 'new') checked  @endif>Покупка</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="deal" class="deal" value="rent"
                                                                         @if($flat->deal == 'rent') checked  @endif>Аренда</label>
                                        </div>
                                    </label>
                                @endif
                            </form>
                        </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="panel panel-inverse">
                                    <div class="panel-heading"></div>
                                    <div class="panel-body">
                                        <h3 class="mt0">Публикация</h3>

                                        <dl class="dl-horizontal mb20">
                                            <dt>Время публикации</dt>
                                            <dd>{!! $flat->created_at !!}</dd>

                                            <dt>Последняя редакция</dt>
                                            <dd>{!! $flat->updated_at !!}</dd>
                                        </dl>

                                        <div class="panel-footer">
                                            <input type="submit" class="pull-right btn btn-info" form="edited_flat"
                                                   value="Сохранить">
                                            @if($flat->id)
                                                <button type="button" class=" btn btn-danger"
                                                        onclick="deleteCategory('{{$flat->id}}')"><span
                                                            class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                    Удалить
                                                </button>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="panel panel-inverse">
                                    <div class="panel-heading"></div>
                                    <div class="panel-body">
                                        <h3 class="mt0">Планировка</h3>
                                        @if($flat->image != null)
                                            <img src=" /front/flats/{{$flat->image}}" alt="" class="img-responsive"
                                                 height="100">

                                        @else
                                            <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                        @endif

                                        <div class="panel-footer">
                                            <input type="file" class="pull-right btn btn-info" name="file"
                                                   form="edited_flat" value="Обновить">

                                            @if($flat->image)
                                                <button type="button" class="pull-right btn btn-danger"
                                                        onclick="deleteObjectImage({!! $flat->id !!})">Удалить
                                                </button>


                                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function deleteObjectImage(id) {
            if (confirm('Вы уверены?') == true) {
                $.ajax({
                    url: '/api/ajax',
                    type: 'POST',
                    data: ({
                        'id': id,
                        'intent': 'deleteObjectImage'
                    }),
                    dataType: "html",
                    error: errorHandler,
                    success: function () {
                        $('#img' + id).fadeOut(400);
                        $('#text' + id).fadeOut(400);
                    }
                })
            }
            window.location.reload();
        }

        $('.status').on('click', function () {
            var status = $('.status:checked').val();
            if (status != 'free') {
                $('.sold').removeClass('hidden');
            } else {
                //$('.access .row').css('display', 'block');
                $('.sold').addClass('hidden');
            }
        });

        function errorHandler(data) {
            alert('Ошибка :' + data.status);
        }

    </script>
@endsection

