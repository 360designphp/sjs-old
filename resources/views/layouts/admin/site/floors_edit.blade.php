@extends('layouts.admin.index')
@section('content')
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование этажа</h1>@if($floor->id)
                        &nbsp;<a href="/objects/{{$floor->object()->id}}/{{$floor->building()->id}}/{{$floor->section->id}}/{!!$floor->id !!}" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    @endif
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/floor/save" name="edited_flat" method="post" id="edited_floor"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="{{$floor->id}}" name="id">
                                {{csrf_field()}}
                                <label class="col-xs-12">Этаж
                                    <input type="text" class="form-control  mb20"
                                           value="{!! ($floor->level)?$floor->level:"0" !!}"
                                           name="level"
                                           placeholder="Введите этаж..." required>
                                </label>
                                <label class="col-xs-12">Номер секции
                                    <input type="text" class="form-control  mb20"
                                           value="{!! ($floor->section_id)?$floor->section_id:"" !!}"
                                           name="section_id"
                                           placeholder="Введите номер секции..." required>
                                </label>
                                <div class="col-xs-6">
                                    <label>Тип этажа
                                        <div class="radio"><label><input type="radio" name="type"
                                                                         value="living"
                                                                         @if($floor->type == 'living' || $floor->id == 'new') checked @endif
                                                >Жилой</label></div>
                                        <div class="radio"><label><input type="radio" name="type" value="commercial"
                                                                         @if($floor->type == 'commercial') checked @endif>Коммерческий</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="type" value="parking"
                                                                         @if($floor->type == 'parking') checked @endif>Паркинг</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6" style="width: 40%">
                                     <label>Этаж готов
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="ready_status" value="1" @if($floor->ready_status == 1|| $floor->id == 'new') checked @endif>
                                                Да</label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="ready_status" value="0" @if($floor->ready_status == 0) checked @endif>
                                            Нет</label>
                                        </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO{{$floor->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO{{$floor->id}}"
                                                       href="#collapseObject{{$floor->id}}"><h2>Служебная
                                                            информация</h2></a>
                                                    <div class="accordion-body">
                                                        <table class="table mb0">
                                                            <thead>
                                                            </thead>
                                                        </table>
                                                        <div id="collapseObject{{$floor->id}}" class="collapse">
                                                            <h3 class="mt0">Код SVG</h3>
                                                            <textarea name="svg" class="form-control" cols="1"
                                                                      rows="1">{{$floor->svg}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionB{{$floor->id}}">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionB{{$floor->id}}"
                                                       href="#collapseBuilding{{$floor->id}}"><h2>SEO - настройки</h2>
                                                    </a>
                                                    <div class="accordion-body">
                                                        <div id="collapseBuilding{{$floor->id}}" class="collapse">
                                                            <span class="h3">Ключевые слова</span>
                                                            <input type="text" name="seo_keywords" id="keywords"
                                                                   class="form-control"
                                                                   value="{!!$floor->seo_keywords!!}"><br>
                                                            <span class="h3">Мета-описание</span>
                                                            <textarea name="seo_description" class="form-control"
                                                                      cols="30"
                                                                      rows="10">{{$floor->seo_description}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>
                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd>{!! $floor->created_at !!}</dd>

                                        <dt>Последняя редакция</dt>
                                        <dd>{!! $floor->updated_at !!}</dd>
                                    </dl>
                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_floor"
                                               value="Сохранить">
                                        @if($floor->id)
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('{{$floor->id}}')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">План этажа</h3>
                                    @if($floor->image != null)
                                        <img src=" /front/floors/{{$floor->image}}" alt="" class="img-responsive"
                                             height="100">
                                    @else
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                    @endif

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="file"
                                               form="edited_floor" value="Обновить">
                                        @if($floor->image)
                                            <button type="button" class="pull-right btn btn-danger"
                                                    onclick="deleteFloorImage({!! $floor->id !!})">Удалить
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer role="contentinfo">
                    <div class="clearfix">
                        <ul class="list-unstyled list-inline pull-left">
                            <li><h6 style="margin: 0;">360 CMS</h6></li>
                        </ul>
                        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                                    class="fa fa-arrow-up"></i></button>
                    </div>
                </footer>
            </div>
            @endsection
            @section('script')
                <script>
                    function deleteFloorImage(id) {
                        if (confirm('Вы уверены?') == true) {
                            $.ajax({
                                url: '/api/ajax',
                                type: 'POST',
                                data: ({
                                    'id': id,
                                    'intent': 'deleteFloorImage'
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function () {
                                    $('#img' + id).fadeOut(400);
                                    $('#text' + id).fadeOut(400);
                                }
                            })
                        }
                        window.location.reload();
                    }
                    function errorHandler(data) {
                        alert('Ошибка :' + data.status);
                    }
                </script>
@endsection

