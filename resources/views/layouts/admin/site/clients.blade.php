@extends('layouts.admin.index')
@section('content')

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>База клиентов&nbsp;<small><a href="/admin/clients/printable?<?=$parameters?>" target="_blank">версия для
                                печати</a></small>
                    </h1>

                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" class="form-inline mb10">
                                <div class="form-group">
                                    <select name="object" id="" class="form-control">
                                        <option value="all">Все объекты</option>
                                        @foreach($objects as $object)
                                            <option value="{{$object->id}}" @if(isset($_GET['object']) && $_GET['object'] == $object->id) selected @endif>{!! $object->name !!}</option>
                                        @endforeach
                                    </select>
                                    <select name="status" id="" class="form-control">
                                        <option value="all" @if(!isset($_GET['status']) || $_GET['status'] == 'all') selected @endif>Все статусы</option>
                                        <option value="booked"
                                                @if(isset($_GET['status']) && $_GET['status'] == 'booked') selected @endif>
                                            Забронировно
                                        </option>
                                        <option value="sold"
                                                @if(isset($_GET['status']) && $_GET['status'] == 'sold') selected @endif>
                                            Продано
                                        </option>
                                    </select>
                                    <button type="submit" class="btn btn-default">Фильтровать</button>
                                </div>
                            </form>

                            <div class="panel">
                                <div class="panel-body panel-no-padding">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Телефон</th>
                                            <th width="150">Квартира</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($flats as $flat)
                                            <tr>
                                                <td>{{$flat->customer_name}}</td>
                                                <td>{{$flat->customer_phone}}</td>
                                                <td>
                                                    <a href="/admin/objects/flat/{{$flat->id}}">{{$flat->getObject()->name}}
                                                        {{$flat->getFloorSymbol()}}: {{$flat->number}}</a> <br><br>  <span class="label label-default">{{$flat->getStrStatus()}}</span></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
@endsection