@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <h2 class="text-center">{{$title}}</h2>
        <div class="small-vertical-devider"></div>
        <div class="row searches_list">
            @foreach($posts  as $post)
                <div class="col-xs-6 col-sm-4 search_item">
                    <div target="_blank" class="search_item_inner_wrap clearfix">
                        <div class="search_item_inner_top">
                            <div class="news-grid-title">
                                <?php if (strlen($post->title) <= 90) {
                                    echo $post->title;
                                          } else {
                                    echo substr($post->title, 0, 90) . "...";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="search_item_inner_img">
                            @if(is_object($post->thumbnail()))
                                <img src="/front/thumbnail/{{$post->thumbnail()->slug}}" alt="" class="" onerror="this.src = '/front/img/21455132o.jpg'">
                            @else
                                <img src="/front/img/21455132o.jpg" class="img-responsive">
                            @endif
                        </div>
                        <a href="/{{$post->type}}/{{$post->slug}}" class="search_item_inner_bottom">
                            Подробнее <span class="shev_right"></span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="vertical-devider"></div>
    </main>
@endsection