@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_p404">
        <img src="img/bg404.svg" alt="" class="bg404">
        <div class="text-center">
            <h1>Страница не найдена</h1>
            <h2>Ошибка 404</h2>
        </div>
    </main>
    </div>
    </div>
    @endsection