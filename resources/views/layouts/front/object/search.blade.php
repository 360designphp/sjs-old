@extends('layouts.front.index')
@section('content')
<main class="col-xs-6 col-xs-offset-6 col-sm-10 col-sm-offset-2">
    <div class="slick_houses">
        <div><img src="/front/img/slide1.jpg" class="img-responsive">
            <div class="slick_caption"><h2>Многоквартирный<br>жилой&nbsp;дом</h2>
                <small>г.Кисловодск, ул. Островского, 33.</small>
            </div>
        </div>
        <div><img src="/front/img/21455132o.jpg" class="img-responsive">
            <div class="slick_caption"><h2>Многоквартирный<br>жилой&nbsp;дом</h2>
                <small>г.Кисловодск, ул. Островского, 33.</small>
            </div>
        </div>
        <div><img src="/front/img/fon3.jpg" class="img-responsive">
            <div class="slick_caption"><h2>Многоквартирный<br>жилой&nbsp;дом</h2>
                <small>г.Кисловодск, ул. Островского, 33.</small>
            </div>
        </div>
    </div>
    <nav class="main_filter selected1">
        <form action="" method="POST" class="form-inline" role="form">
            <div class="line_vertical"></div>
            <div class="line_vertical"></div>
            <div class="line_vertical"></div>
            <div class="line_vertical"></div>
            <div class="line_vertical"></div>
            <div class="line_vertical"></div>
            <div class="select_cutom main_filter_selection"><select name="selcom" id="inputselcom" class="form-control">
                    <option value="1">Жилая недвижимость</option>
                    <option value="2">Коммерческая</option>
                    <option value="3">Паркинг</option>
                </select></div>
            <div class="select_cutom main_filter_rooms"><select name="selcom" id="selrooms" class="form-control">
                    <option value="">1-комната</option>
                    <option value="">2-комнаты</option>
                    <option value="">3-комнаты</option>
                    <option value="">4-комнаты</option>
                </select></div>
            <div class="main_filter_squares">Площадь&nbsp;от&nbsp;<input type="text" name="" id=""> до&nbsp;<input
                        type="text" name="" id="">&nbsp;м<sup>2</sup></div>
            <div class="main_filter_etaj">Этаж&nbsp;от&nbsp;<input type="text" name="" id=""> до&nbsp;<input type="text"
                                                                                                             name=""
                                                                                                             id="">
            </div>
            <div class="select_cutom main_filter_place"><select name="seladdr" id="selrooms" class="form-control">
                    <option value="">Объект</option>
                    <option value="">ул. Островского, д.33</option>
                    <option value="">ул. Островского, д.33</option>
                    <option value="">ул. Островского, д.33</option>
                </select></div>
            <div class="select_cutom main_filter_sdelka">Тип сделки:&nbsp;&nbsp;<select name="seladdr" id="selsdelka"
                                                                                        class="form-control">
                    <option value="">Покупка</option>
                    <option value="">Аренда</option>
                </select></div>
            <div class="select_cutom main_filter_carplace"><select name="selcom" id="selrooms" class="form-control">
                    <option value="">1-машиноместо</option>
                    <option value="">2-машиноместа</option>
                    <option value="">3-машиноместа</option>
                    <option value="">4-машиноместа</option>
                </select></div>
            <div class="nextline"></div>
            <div class="rangeslider_wrap">
                <div class="rangeslider_cap"><p>Стоимость от</p>
                    <small>млн.руб.</small>
                </div>
                <span id="range_sl1-label-2a" class="hidden">Example low value</span> <span id="range_sl1-label-2b"
                                                                                            class="hidden">Example high value</span>
                <input id="range_sl1" type="text"> <span id="range_sl1_a">12</span> <span id="range_sl1_b">99</span>
            </div>
        </form>
        <div class="main_filter_button">
            <button type="submit" class="btn btn-primary"></button>
        </div>
    </nav>
    <div class="row searches_list">
        <div class="col-xs-6 col-sm-3 search_item voikova">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item voikova">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item voikova">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item voikova">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item voikova">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item otherobj">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Островского</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
        <div class="col-xs-6 col-sm-3 search_item">
            <div class="search_item_inner_wrap"><a href="kvartplan.html#kv44" target="_blank" class="search_item_inner"><span>Ул. Войкова</span>
                    <span>Этаж: 3</span> <span>Квартира: 44</span> <span>Площадь: 33м<sup>2</sup></span> <span>Цена: 2 млн.<span
                                class="ruble">q</span></span></a></div>
        </div>
    </div>
    <div class="vertical-devider"></div>
</main></div></div>
@endsection