@extends('layouts.front.index')
@section('content')
    <main class="col-xs-6 col-xs-offset-6 col-sm-10 col-sm-offset-2 main_etaj">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12"><h2>Выберите этаж</h2></div>
            <div class="col-xs-12 etaj_svg_wrap">
                <div class="etaj_svg_holder">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px" viewBox="0 0 1152 542.9" style="enable-background:new 0 0 1152 542.9"
                         xml:space="preserve"><style type="text/css">.etaj {
                                opacity: 0.6;
                                fill: transparent;
                            }</style>
                        <g id="Слой_2">
                            <image style="overflow:visible" width="4800" height="2262" xlink:href="/front/img/70188531.jpg"
                                   transform="matrix(0.24 0 0 0.24 0 0)"></image>
                        </g>
                        <g id="Слой_3">
                            <polyline class="etaj et0106" points="144.3,252.3 180.8,252.1 187,252.1 203.3,252 203.3,255.6 222.9,255.4 222.9,251.9 234.5,251.9
                                                234.5,250.3 296.2,249.1 296.2,250.2 311.8,249.8 317.9,250.2 321.1,249.9 321.2,252.5 343,252.2 343,249.7 357.2,249.8
                                                357.2,247.1 390.2,246.6 390.2,226.3 356,226.9 356.1,228.8 303.3,230.6 296.6,229.6 233.7,231.7 233.7,233.5 187.2,234.7
                                                180.8,233.8 144.5,235.2 144.3,252.5     "/>
                            <polygon class="etaj et0208" points="493.1,200.6 493.1,203.9 433.8,205.4 427.5,204.4 390.9,205.9 390.9,228.9 426.7,228 426.7,229.2
                                                443.8,228.5 449.3,229 453.1,229 453.1,231.1 477.3,230.7 477.4,227.8 494.3,227.3 494.3,225.4 526.5,224.6 536.8,224.1
                                                570.8,223.1 576,223.5 599.3,223.6 599.3,226.2 626.4,225.7 626.4,222.2 646.8,222 646.8,220.1 690,218.9 690,192.2 645.6,193.8
                                                645.7,195.7 576,198.7 571.1,197.3   "/>
                            <polygon class="etaj et0308" points="920.1,234.6 923.9,235.1 953.8,234.7 953.9,238.4 988.8,237.8 988.8,234.1 1018.5,233.4 1018.6,230.1
                                                1088.6,228.3 1100.8,231.6 1100.8,229.5 1099.5,228.6 1099.1,209.3 1100.4,209.2 1100.4,204.4 1087.9,198.9 1016.7,201.1
                                                1016.8,203 923.8,206.1 920.8,204.4 820.7,207.6 820.8,210.9 740.6,212.8 736.5,212.5 691,213.8 691,240.4 736.1,239.5
                                                736.1,240.8 758.2,240.3 762,240.6 766.9,240.6 766.9,243.1 797.5,242.8 797.5,239.3 822.1,239.3 822.1,237.1   "/>
                            <polygon class="etaj et0307" points="691,240.4 736.1,239.5 736.1,240.8 758.2,240.3 762,240.6 766.9,240.6 766.9,243.1 797.5,242.8
                                                797.5,239.3 822.1,239.3 822.1,237.1 920.1,234.6 923.9,235.1 953.8,234.7 953.9,238.4 988.8,237.8 988.8,234.1 1018.5,233.4
                                                1018.6,230.1 1088.6,228.3 1100.8,231.6 1100.8,235.1 1099.8,235.1 1099.8,254.7 1101.4,255.4 1101.4,260.4 1088.9,257.8
                                                1018.6,258.7 1018.6,259.8 988.8,259.8 988.8,263.2 954,263.6 954,259.7 924.3,260 920.5,260.6 822.1,262.4 822.1,263.7
                                                797.5,263.9 797.5,266.6 766.9,266.9 766.9,264 739.2,264 736.1,264 691,264.9     "/>
                            <polyline class="etaj et0306" points="691,264.9 736.1,264 739.2,264 766.9,264 766.9,266.9 797.5,266.6 797.5,263.9 822.1,263.7
                                                822.1,262.4 920.5,260.6 924.3,260 954,259.7 954,263.6 988.8,263.2 988.8,259.8 1018.6,259.8 1018.6,258.7 1088.9,257.7
                                                1101.4,260.4 1100.1,260.7 1100.4,280.5 1102.1,281 1102.1,286 1089.6,284.8 1019.2,284.9 1019.2,286 989.5,286.1 989.5,289.7
                                                954.5,289.9 954.5,287.4 949.1,287.4 921.6,287.5 920.8,286.1 822.5,287 822.6,288.4 798,288.5 798,291.2 767,291.5 767,287.4
                                                739.2,287.4 736.2,287.8 691,288.3 691,264.6     "/>
                            <polygon class="etaj et0305" points="691,288.3 736.2,287.8 739.2,287.4 767,287.4 767,291.5 798,291.2 798,288.5 822.6,288.4 822.5,287
                                                920.8,286.1 921.6,287.5 949.1,287.4 954.5,287.4 954.5,289.9 989.5,289.7 989.5,286.1 1019.2,286 1019.2,284.9 1089.6,284.8
                                                1102.1,286 1102.1,286.4 1100.9,286.3 1100.9,306.7 1102.6,306.8 1102.6,311.7 1090.1,311.4 1019.5,311.8 1019.5,313.4
                                                989.5,313.5 989.5,315.6 954.8,315.6 954.8,311.4 924.5,311.3 921.1,311.7 822.5,311.7 822.6,313 798,313 798,315 767,315
                                                767,313.3 736.6,313.4 736.5,311.7 691,311.9     "/>
                            <polygon class="etaj et0304" points="691,311.9 736.5,311.7 736.6,313.4 767,313.3 767,315 798,315 798,313 822.6,313 822.5,311.7
                                                921.1,311.7 924.5,311.3 954.8,311.4 954.8,315.6 989.5,315.6 989.5,313.5 1019.5,313.4 1019.5,311.8 1090.1,311.4 1102.6,311.7
                                                1102.6,312.3 1101.6,312.3 1101.6,332.6 1102.8,332.6 1102.8,337.8 1090.6,338.4 1019.9,338.6 990,338.6 990,341.5 955.3,341.5
                                                955.2,337.3 925.1,337.3 921.6,337.4 822.5,336.8 822.6,337.6 798,337.6 798,340 767.6,340 767.6,335.9 739.6,335.9 736.5,336
                                                691,335.7   "/>
                            <polygon class="etaj et0303" points="691,335.7 736.5,336 739.6,335.9 767.6,335.9 767.6,340 798,340 798,337.6 822.6,337.6 822.5,336.8
                                                921.6,337.4 925.1,337.3 955.2,337.3 955.3,341.5 990,341.5 990,338.6 1019.9,338.6 1090.6,338.4 1102.8,337.8 1102.8,338.2
                                                1102.1,338.2 1102.1,358.3 1103.5,358.3 1103.5,363.8 1091.4,365.7 1020.6,365.3 990.7,365.4 990.7,368 955.4,367.5 955.4,364.8
                                                947.3,365.3 921.9,364.8 921.9,363.4 823.2,361.8 823.2,362.3 798.5,362.1 798.5,364.5 767.6,363.9 767.6,359.8 739.6,359.8
                                                736.5,359.8 691,359.6   "/>
                            <polygon class="etaj et0302" points="691,359.6 736.5,359.8 739.6,359.8 767.6,359.8 767.6,363.9 798.5,364.5 798.5,362.1 823.2,362.2
                                                823.2,361.7 921.9,363.4 921.9,364.8 947.3,365.3 955.4,364.7 955.4,367.5 990.7,368 990.7,365.4 1020.6,365.2 1091.4,365.7
                                                1103.5,363.8 1103.5,364.2 1102.3,364.4 1102.4,384.2 1104,384.2 1104,386.8 1091.8,392 1069.1,391.9 1069.1,392.9 1020.9,391.7
                                                1020,393.4 994.1,392.8 994.1,392.3 991,392.3 955.9,392.3 955.9,389.6 926,389.2 922.3,389.2 823.4,386.5 798.7,386.3
                                                798.7,387.2 767.7,386.6 767.7,383.9 741.3,383.8 737,384.5 691,383.4     "/>
                            <!-- <path class="st4" d="M691,404.8"/> -->
                            <polygon class="etaj et0207" points="390.9,228.8 426.7,227.9 426.7,229.1 443.8,228.4 449.3,228.9 453.1,228.9 453.1,231 477.3,230.6
                                                477.4,227.7 494.3,227.2 494.3,225.3 526.5,224.5 536.8,224 570.8,223 576,223.4 599.3,223.5 599.3,226.1 626.4,225.6 626.4,222.1
                                                646.8,221.9 646.8,220 690,218.8 690,242.7 646.8,243.7 646.8,245.7 626.4,245.6 626.4,247.8 599.3,248.3 599.3,246 576,246
                                                570.7,245.8 494.2,247.8 494.2,249.1 477.2,249.3 477.3,252.1 452.9,252.3 452.9,249.3 432.4,249.3 426.7,249.5 390.9,250.4     "/>
                            <polygon class="etaj et0206" points="390.9,250.3 426.7,249.4 432.4,249.3 452.9,249.3 452.9,252.3 477.3,252.1 477.2,249.2 494.2,249
                                                494.2,247.7 570.7,245.8 576,246 599.3,245.9 599.3,248.2 626.4,247.7 626.4,245.6 646.8,245.6 646.8,243.7 690,242.6 690,266.4
                                                646.8,266.8 646.8,268.8 626.4,268.7 626.4,271.2 599.3,271.4 599.3,269.4 570.7,269.6 570.7,267.5 494.2,269.5 494.2,270.8
                                                477.2,271 477.3,273.9 452.9,274 452.9,271 432.4,271.1 426.7,271.2 390.9,271.4   "/>
                            <polygon class="etaj et0205" points="391,271.4 426.7,271.2 432.5,271.1 452.9,271 452.9,274 477.3,273.9 477.3,271 494.3,270.8
                                                494.3,269.5 570.7,267.5 570.8,269.6 599.4,269.4 599.4,271.4 626.5,271.2 626.5,268.7 646.9,268.8 646.9,266.8 690,266.4
                                                690,289.8 646.9,290.1 646.9,291.9 626.4,291.8 626.4,294.1 599.3,294.2 599.3,291.3 575.1,291.3 570.7,290.8 494.3,291.2
                                                494.3,292.5 477.3,292.7 477.3,294.8 452.7,294.9 452.7,293.4 426.3,293.6 426.3,292.1 391,292.3   "/>
                            <polygon class="etaj et0204" points="391,292.3 426.3,292.1 426.3,293.5 452.7,293.4 452.7,294.9 477.3,294.8 477.3,292.6 494.3,292.5
                                                494.3,291.2 570.7,290.7 575.1,291.3 599.3,291.3 599.3,294.2 626.4,294.1 626.4,291.8 646.9,291.9 646.9,290.1 690,289.7
                                                690,313.3 646.9,313.3 646.9,315 626.6,314.9 626.6,316.7 599.3,316.8 599.3,313.4 575.2,313.5 570.7,313.4 493.8,313 493.8,314.3
                                                476.9,314.5 477,316.7 452.7,316.9 452.7,313.5 430.9,313.6 426.3,313.4 391,313.6     "/>
                            <!-- <path class="st6 v" d="M144.3,293.5"/> -->
                            <polygon class="etaj et0105" points="268.3,269.4 234.6,269.8 234.1,270 222.6,270 222.6,273.5 203,273.7 203,272.3 199.8,272.5
                                                193.5,272.5 179.8,272.5 179.7,270.2 144.1,270.6 144.3,252.3 180.8,252 187,251.9 203.3,251.9 203.3,255.4 222.9,255.3
                                                222.9,251.8 234.5,251.7 234.5,250.1 296.2,248.9 296.2,250 311.8,249.6 317.9,250 321.1,249.7 321.2,252.3 343,252 343,249.5
                                                357.2,249.6 357.2,247 390.2,246.4 390.2,267.1 356.8,267.5 356.8,269.9 342.8,269.9 342.8,271.5 320.9,271.6 320.8,270.8
                                                302.4,270.8 302.4,268.8 295.9,268.8     "/>
                            <polygon class="etaj et0104" points="144.1,270.6 179.7,270.2 179.8,272.5 193.5,272.5 199.8,272.5 203,272.3 203,273.7 222.6,273.5
                                                222.6,270 234.1,270 234.6,269.8 268.3,269.4 295.9,268.8 302.4,268.8 302.4,270.8 320.8,270.8 320.9,271.6 342.8,271.5
                                                342.8,269.9 356.8,269.9 356.8,267.5 390.2,267.1 390.2,288.4 356.8,288.8 356.8,289.8 342.5,289.8 342.5,292.1 320.6,292.2
                                                320.6,290.7 302.1,290.7 302.1,289.5 295.7,289.6 268.4,290 234.1,290.2 234.1,291.4 222.6,291.4 222.6,294 202.8,294.1
                                                202.8,291.7 184.3,291.8 184.3,289.2 143.9,289.6     "/>
                            <polygon class="etaj et0103" points="143.9,289.6 184.3,289.2 184.3,291.8 202.8,291.7 202.8,294.1 222.6,294 222.6,291.4 234.1,291.4
                                                234.1,290.2 268.4,290 295.7,289.6 302.1,289.5 302.1,290.7 320.6,290.7 320.6,292.2 342.5,292.1 342.5,289.8 356.8,289.8
                                                356.8,288.8 390.2,288.4 390.2,308.8 356.8,309.1 356.8,310.1 342.5,310.1 342.5,312.9 320.6,313 320.6,310.1 311.3,310.4
                                                295.7,310.4 295.7,309.1 267.8,309.1 234.1,309 234.1,310.2 222.3,310.4 222.3,313 202.5,313 202.5,310.8 184.3,310.7 184.3,308
                                                143.6,308   "/>
                            <polygon class="etaj et0102" points="390.2,308.8 357,309.1 357,310.1 342.7,310.1 342.7,312.9 320.8,313 320.8,310.1 311.5,310.4
                                                295.9,310.4 295.9,309.1 268,309.1 234.3,309 234.4,310.2 222.5,310.4 222.5,313 202.7,313 202.7,310.8 184.5,310.7 184.5,308
                                                143.6,308 143.4,327.3 184.3,327.4 184.3,331.1 202.5,331.1 202.5,333.3 222.3,333.3 222.3,330.7 234.2,330.6 234.1,329.4
                                                267.8,329.5 295.7,329.6 302.1,329.4 302.1,331.2 320.6,331.2 320.6,334 342.5,333.9 342.5,331.4 356.6,331.4 356.6,330.1
                                                390.2,330.3     "/>
                            <polygon class="etaj et0101" points="143.4,327.3 184.1,327.3 184.1,331.1 202.3,331.1 202.3,333.3 222.1,333.3 222.1,330.7 234,330.6
                                                233.9,329.4 267.6,329.5 295.5,329.6 301.9,329.3 301.9,331.2 320.4,331.2 320.4,334 342.3,333.8 342.3,331.3 356.4,331.4
                                                356.4,330.1 390.2,330.2 390.2,349.7 355.3,349.2 301.7,348.3 295.5,348.5 256.7,347.9 233.9,347.5 234,349.5 222.1,349.6
                                                222.1,352.2 202.3,352.2 202.3,350 184.1,350 184.1,346.2 143.2,346   "/>
                            <!-- <path class="st8 v" d="M142.9,346.8"/> -->
                            <polyline class="etaj et0203" points="391,335 391,313.6 426.3,313.4 430.9,313.6 452.7,313.5 452.7,316.9 477,316.7 476.9,314.5
                                                493.8,314.3 493.8,313 570.7,313.4 575.2,313.5 599.3,313.4 599.3,316.8 626.6,316.7 626.6,315 646.9,315 646.9,313.3 690,313.3
                                                690,337.3 647.2,337 647.3,338.7 626.6,338.6 626.6,340 599.5,340 599.5,337.4 590.4,337.7 570.8,337.7 570.7,336.3 493.9,335.4
                                                493.9,336.5 476.9,336.7 477,337.9 452.4,338.1 452.4,335 430.9,334.8 426.3,334.6 391,334.8   "/>
                            <polygon class="etaj et0202" points="391,334.8 426.3,334.6 430.9,334.8 452.4,335 452.4,338.1 477,337.9 476.9,336.7 493.9,336.5
                                                493.9,335.4 570.7,336.3 570.8,337.7 590.4,337.7 599.5,337.4 599.5,340 626.6,340 626.6,338.6 647.3,338.7 647.2,337 690,337.3
                                                690.4,360.9 646.9,360.4 646.9,361.8 646.2,361.8 626.3,361.2 599.5,361.2 599.5,359.3 590.6,359.3 576,359 570.6,359 494,357.5
                                                494,358.6 477.3,358.4 477.3,358.8 452.5,358.6 452.5,356.3 430.3,356.2 425.8,356.3 391,356   "/>
                            <!-- <path class="st9" d="M391,374.9"/> -->
                            <polygon class="etaj et0201" points="690.4,360.9 690.3,360.9 646.9,360.4 646.9,361.8 646.2,361.8 626.3,361.2 599.5,361.2 599.5,359.2
                                                590.6,359.2 576,359 570.6,359 494,357.5 494,358.6 477.3,358.3 477.3,358.7 452.5,358.6 452.5,356.3 430.3,356.2 425.8,356.2
                                                391,356 391,374.9 425.8,375.6 433,375.6 492.4,377.3 571.2,379.4 575.2,379 646.1,381.1 646.1,381.5 690.4,382.6   "/>
                            <polygon class="etaj et0301" points="691,383.4 737,384.5 741.3,383.8 767.7,383.9 767.7,386.6 798.7,387.2 798.7,386.3 823.4,386.5
                                                922.3,389.2 926,389.2 955.9,389.6 955.9,392.3 991,392.3 994.1,392.3 994.1,392.8 1020,393.4 1020.9,391.7 1069.1,392.9
                                                1069.1,391.9 1091.8,392 1104,386.8 1104,389.8 1103.1,390.2 1103.1,410.1 1104.4,410.2 1104.4,412.4 1105.6,412.5 1093.1,418.2
                                                1020.3,415.9 1020.3,414.9 926.4,411.8 923.7,412.5 822.2,409.2 822.2,408.5 741.8,405.6 737.8,406.4 691,404.8     "/>
                        </g></svg>
                    <div class="etaj_svg_popover">
                        <div>Этаж <span class="etaj_popover_etaj">5</span></div>
                        <div>Количество квартир <span class="etaj_popover_kol_kvart">20</span></div>
                        <div>Стоимость <span class="etaj_popover_price">от <span>8.1</span> млн.</span></div>
                        <div class="etaj_svg_popover_bgwrap"></div>
                    </div>
                </div>
                <section class="about_house text-center"><p>Сдача дома — июнь 2017 г. договор беспроцентной рассрочки
                        (ДДУ), ипотека с государственным со финансированием.</p>
                    <h1 class="">Войкова 13</h1>
                    <div class="col-xs-12 col-sm-5 col-sm-offset-1">Многоквартирный жилой дом расположен по ул. Войкова,
                        13 в спокойном районе города-курорта Кисловодск. Хорошее транспортное сообщение.В шаговой
                        доступности парковая зона отдыха. Развитая сформированная инфраструктура: школа, магазины,
                        детский сад.
                    </div>
                    <div class="col-xs-12 col-sm-5 col-sm-offset-1">Многоквартирный жилой дом расположен по ул. Войкова,
                        13 в спокойном районе города-курорта Кисловодск. Хорошее транспортное сообщение.В шаговой
                        доступности парковая зона отдыха. Развитая сформированная инфраструктура: школа, магазины,
                        детский сад.
                    </div>
                    <div class="small-vertical-devider"></div>
                    <a href="#!">Посмотреть ход строительства</a>
                    <div class="small-vertical-devider"></div>
                    <div class="col-xs-12 about_bigimg"><img src="/front/img/voikova13.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="col-xs-12 about_opisanie">
                        <div class="row">
                            <div class="vertical-devider"></div>
                            <div class="col-xs-6"><h2>Описание дома</h2>
                                <ul>
                                    <li>Многоквартирный 3-х этажный жилой дом, количество квартир в доме – 59, 1-но, 2-х
                                        и 3-х комнатные квартиры, свободная планировка.
                                    </li>
                                    <li>Высота этажа – 3 метра, все несущие стены кирпичные, шатровая кровля.</li>
                                    <li>Дом газифицирован, в каждой квартире индивидуальная система отопления,
                                        центральное водоснабжение.
                                    </li>
                                    <li>Благоустроенная придомовая территория, развитая инфраструктура.</li>
                                </ul>
                            </div>
                            <div class="col-xs-6 padlr0">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2874.6042166603547!2d42.73230431550589!3d43.90546327911355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582b1bad454c4d%3A0x9c01f1e2e6ab8904!2z0YPQuy4g0JLQvtC50LrQvtCy0LAsIDEzLCDQmtC40YHQu9C-0LLQvtC00YHQuiwg0KHRgtCw0LLRgNC-0L_QvtC70YzRgdC60LjQuSDQutGA0LDQuSwgMzU3NzAz!5e0!3m2!1sru!2sru!4v1491142511474"
                                        width="600" height="230" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                                <div class="row about_map_additional">
                                    <div class="col-xs-3">г.Кисловодск, ул.Войкова, д.13</div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn btn-default">Заказать обратный звонок</button>
                                    </div>
                                    <div class="col-xs-3"><a
                                                href="https://www.google.com/maps?ll=43.905912,42.736746&z=16&t=m&hl=ru-RU&gl=RU&mapclient=embed&q=%D1%83%D0%BB.+%D0%92%D0%BE%D0%B9%D0%BA%D0%BE%D0%B2%D0%B0,+13+%D0%9A%D0%B8%D1%81%D0%BB%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D0%BA+%D0%A1%D1%82%D0%B0%D0%B2%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9+357703">Посмотреть
                                            на большой карте</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main></div></div>
@endsection