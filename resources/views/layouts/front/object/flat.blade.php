@extends('layouts.front.index')
@section('content')
    <main class="col-xs-6 col-xs-offset-6 col-sm-10 col-sm-offset-2 main_kvartplan">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-3 linkleft"><a href="#!" class="kvartplan_korpus_link">
                    <div class="arrow_kvart arrleft"></div>
                    Корпус 1</a></div>
            <div class="col-xs-6 text-center"><h2>Корпус 2, 5 этаж</h2></div>
            <div class="col-xs-3 linkright"><a href="#" class="kvartplan_korpus_link">Корпус 3
                    <div class="arrow_kvart arrright"></div>
                </a></div>
            <div class="col-xs-12 kvart_svg_wrap">
                <div class="kvart_svg_holder">
                    <svg version="1.1" id="svg1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1524 631.2"
                         style="enable-background:new 0 0 1524 631.2" xml:space="preserve"><style type="text/css">.kv5 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv4 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv3 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv42 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv2 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv43 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv1 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv41 {
                                opacity: 0.5;
                                fill: transparent;
                            }

                            .kv44 {
                                opacity: 0.5;
                                fill: transparent;
                            }</style>
                        <image style="overflow:visible" width="6350" height="2630" xlink:href="/front/img/C297C907.jpg"
                               transform="matrix(0.24 0 0 0.24 0 0)"></image>
                        <polygon class="kvart kv5 canbuy"
                                 points="188,242.3 188,313.3 3.6,313.3 3.6,9.3 238.9,9.3 238.9,51.3 343.8,51.3 343.8,242.7 "/>
                        <polygon class="kvart kv4 zabron" points="47.8,313.3 47.8,491.7 143.3,491.7 142.9,534.2 238.3,534.2 238.7,479.3 236.2,479.3 236.2,300.5
                                            188,300.5 188,313.3 "/>
                        <polygon class="kvart kv3 zabron"
                                 points="236.2,300.5 414.7,300.5 414.3,534.2 238.3,534.2 238.7,479.3 236.2,479.3 "/>
                        <polygon class="kvart kv42 kupl" points="1193.6,300.5 1298.3,300.5 1298.3,307.7 1390.3,307.7 1390.3,534.2 1199.1,534.2 1199.1,491.8
                                            1193.6,491.8 1193.6,479.3 "/>
                        <polygon class="kvart kv2 canbuy"
                                 points="414.7,300.5 682.3,300.5 682.3,491.7 680,491.7 679.6,534.2 414.3,534.2 "/>
                        <polygon class="kvart kv43 kupl" points="861.4,307.5 998.8,307.5 998.8,300.6 1129.6,300.6 1129.6,491.7 1129.6,534.2 1126.7,534.2
                                            861.4,534.2 "/>
                        <polygon class="kvart kv1 canbuy" points="682.3,300.5 538.5,300.5 538.5,242.3 478.7,242.3 478.7,51.4 584.3,51.4 584.3,8.8 771.9,8.8
                                            771.9,307 771.9,491.6 682.3,491.6 "/>
                        <polygon class="kvart kv41 kupl" points="1390.3,307.7 1298.3,307.7 1298.3,242.6 1196.2,242.6 1196.2,51.4 1298.4,51.4 1298.4,8.9 1489.4,8.9
                                            1489.4,307 1489.4,491.6 1390.3,491.6 "/>
                        <polygon class="kvart kv44 canbuy" points="861.4,307.5 998.8,307.5 998.8,242.3 1058.9,242.3 1058.9,51.4 953.4,51.4 953.4,8.8 771.9,8.8
                                            771.9,307 771.9,491.6 861.4,491.6 "/></svg>
                </div>
                <div class="kvartplan_etaji"><p>Этаж</p>
                    <ul class="list-unstyled">
                        <li class="arrup"><a href="#!" class="arrow_kvart"></a></li>
                        <li><a href="#1" class="etaj_link">6</a></li>
                        <li><a href="#1" class="etaj_link">5</a></li>
                        <li><a href="#1" class="etaj_link">4</a></li>
                        <li><a href="#1" class="etaj_link">3</a></li>
                        <li><a href="#1" class="etaj_link">2</a></li>
                        <li class="active"><a href="#1" class="etaj_link">1</a></li>
                        <li class="arrdown"><a href="#!" class="arrow_kvart"></a></li>
                    </ul>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12 kvart_legend"><span class="leg_green">Свободна</span> <span class="leg_orange">Забронирована</span>
                <span class="leg_red">Продана</span>
                <div class="small-vertical-devider"></div>
            </div>
            <div class="col-xs-12 kvart_about">
                <div>Комнат <span class="kvart_amount_rooms">-</span></div>
                <div>Площадь, кв.м<span class="kvart_amount_squares">-</span></div>
                <div>Стоимость, млн.руб<span class="kvart_amount_price">-</span></div>
                <div>Номер квартиры<span class="kvart_amount_number">-</span></div>
                <div>Статус<span class="kvart_amount_status"></span></div>
                <div><a type="button" class="btn btn-default" href="img/44.jpg" data-lightbox="test-kvart"
                        data-title="Тестовая квартира">Посмотреть план</a></div>
                <div>
                    <button type="button" class="btn btn-default btn_call_zakaz" data-toggle="modal"
                            href="#modal_zakaz">Оставить заявку
                    </button>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <section class="about_house text-center"><p>Сдача дома — июнь 2017 г. договор беспроцентной рассрочки (ДДУ),
                    ипотека с государственным со финансированием.</p>
                <h1 class="">Войкова 13</h1>
                <div class="col-xs-12 col-sm-5 col-sm-offset-1">Многоквартирный жилой дом расположен по ул. Войкова, 13
                    в спокойном районе города-курорта Кисловодск. Хорошее транспортное сообщение.В шаговой доступности
                    парковая зона отдыха. Развитая сформированная инфраструктура: школа, магазины, детский сад.
                </div>
                <div class="col-xs-12 col-sm-5 col-sm-offset-1">Многоквартирный жилой дом расположен по ул. Войкова, 13
                    в спокойном районе города-курорта Кисловодск. Хорошее транспортное сообщение.В шаговой доступности
                    парковая зона отдыха. Развитая сформированная инфраструктура: школа, магазины, детский сад.
                </div>
                <div class="small-vertical-devider"></div>
                <a href="#!">Посмотреть ход строительства</a>
                <div class="small-vertical-devider"></div>
                <div class="col-xs-12 about_bigimg"><img src="/front/img/voikova13.jpg" alt="" class="img-responsive"></div>
                <div class="col-xs-12 about_opisanie">
                    <div class="row">
                        <div class="vertical-devider"></div>
                        <div class="col-xs-6"><h2>Описание дома</h2>
                            <ul>
                                <li>Многоквартирный 3-х этажный жилой дом, количество квартир в доме – 59, 1-но, 2-х и
                                    3-х комнатные квартиры, свободная планировка.
                                </li>
                                <li>Высота этажа – 3 метра, все несущие стены кирпичные, шатровая кровля.</li>
                                <li>Дом газифицирован, в каждой квартире индивидуальная система отопления, центральное
                                    водоснабжение.
                                </li>
                                <li>Благоустроенная придомовая территория, развитая инфраструктура.</li>
                            </ul>
                        </div>
                        <div class="col-xs-6 padlr0">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2874.6042166603547!2d42.73230431550589!3d43.90546327911355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582b1bad454c4d%3A0x9c01f1e2e6ab8904!2z0YPQuy4g0JLQvtC50LrQvtCy0LAsIDEzLCDQmtC40YHQu9C-0LLQvtC00YHQuiwg0KHRgtCw0LLRgNC-0L_QvtC70YzRgdC60LjQuSDQutGA0LDQuSwgMzU3NzAz!5e0!3m2!1sru!2sru!4v1491142511474"
                                    width="600" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
                            <div class="row about_map_additional">
                                <div class="col-xs-3">г.Кисловодск, ул.Войкова, д.13</div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-default">Заказать обратный звонок</button>
                                </div>
                                <div class="col-xs-3"><a
                                            href="https://www.google.com/maps?ll=43.905912,42.736746&z=16&t=m&hl=ru-RU&gl=RU&mapclient=embed&q=%D1%83%D0%BB.+%D0%92%D0%BE%D0%B9%D0%BA%D0%BE%D0%B2%D0%B0,+13+%D0%9A%D0%B8%D1%81%D0%BB%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D0%BA+%D0%A1%D1%82%D0%B0%D0%B2%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9+357703">Посмотреть
                                        на большой карте</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main></div></div>
@endsection
