@extends('layouts.front.index')
@section('content')

    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <div class="row">
            <div class="col-xs-12">
        <div class="small-vertical-devider"></div>
                <h1 class="text-center">{{$post->title }}</h1>
                <div class="small-vertical-devider"></div>
            </div>
            @if($post->thumbnail())
            <div class="col-xs-12 col-sm-6">
                <img src="/front/images/{{$post->thumbnail()->slug}}" class="img-responsive">
            </div>
            @endif

            <div class="col-xs-12">
                {!! $post->additional!!}
                <div class="col-xs-12 @if($post->thumbnail())col-sm-7 @endif">
                    {!! $post->content!!}

                    <div class="doc">
                        @if($post->documents() != null)
                            <ul class="about_opisanie_docs list-unstyled">
                                @foreach($post->documents() as $document)
                                    <li class="file_icon_{{mb_strtolower($document->content)}}"><a href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                @if(count($post->gallery()) != 0)
                <div class='main_text_slick'>
                    @foreach($post->gallery() as $gal)
                    <div>
                        <a class="main_text_slick_image_link" href="/front/images/{!! $gal->slug !!}" title="{{$gal->title}}"  data-title="{{$gal->title}}" rel="lightbox[gallery]"><img class="img-responsive" src="/front/thumbnail/{!! $gal->slug !!}" alt=""/>
                        </a>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="vertical-devider"></div>
        </div>
    </main>

@endsection