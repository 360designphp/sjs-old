@extends('layouts.front.index')
@section('content')
    <main class="col-xs-6 col-xs-offset-1 col-sm-10 col-sm-offset-0 col-md-offset-2">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Примеры отображения контента на странице</h1>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12 col-sm-6">
                <img data-src="holder.js/100px300/?&random=yes&outline=yes&" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-6">
                <p>Тема: «Креативный медиаплан: основные моменты»<br>
                    Анализ зарубежного опыта синхронизирует формат события. Социальная ответственность, пренебрегая деталями, очевидна не для всех. Исходя из структуры пирамиды Маслоу, нишевый проект ригиден как никогда. Стимулирование коммьюнити существенно обуславливает ребрендинг, не считаясь с затратами. Тем не менее, структура рынка откровенна.
                </p>
                <p>
                    Рекламный макет  без оглядки на авторитеты тормозит общественный портрет потребителя, невзирая на действия конкурентов. Рекламный блок изменяет целевой сегмент рынка. Медийный канал регулярно концентрирует ролевой ребрендинг.
                </p>
                <p>
                    Рекламная акция масштабирует системный анализ, полагаясь на инсайдерскую информацию. Селекция бренда традиционно развивает рейтинг. Практика однозначно показывает, что медиапланирование искажает рыночный рекламоноситель. Повышение жизненных стандартов разнородно притягивает инвестиционный продукт.
                </p>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12">
                <h2>Подзаголовок</h2>
                <div class="vertical-devider"></div>
                <p>Тема: «Креативный медиаплан: основные моменты»<br>
                    Анализ зарубежного опыта синхронизирует формат события. Социальная ответственность, пренебрегая деталями, очевидна не для всех. Исходя из структуры пирамиды Маслоу, нишевый проект ригиден как никогда. Стимулирование коммьюнити существенно обуславливает ребрендинг, не считаясь с затратами. Тем не менее, структура рынка откровенна.
                </p>
                <p>
                    Рекламный макет  без оглядки на авторитеты тормозит общественный портрет потребителя, невзирая на действия конкурентов. Рекламный блок изменяет целевой сегмент рынка. Медийный канал регулярно концентрирует ролевой ребрендинг.
                </p>
                <p>
                    Рекламная акция масштабирует системный анализ, полагаясь на инсайдерскую информацию. Селекция бренда традиционно развивает рейтинг. Практика однозначно показывает, что медиапланирование искажает рыночный рекламоноситель. Повышение жизненных стандартов разнородно притягивает инвестиционный продукт.
                </p>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12">
                <h2>Небольшая галерея</h2>
                <div class="vertical-devider"></div>
                <div class='main_text_slick'>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>

                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="/front/img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                    <div>
                        <a class="main_text_slick_image_link" href="/front/img/21455132o.jpg" data-lightbox="Галерея тестовая" data-title="Фотка 1"><img class="img-responsive" src="img/21455132o.jpg" alt=""/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="vertical-devider"></div>
        </div>
    </main>
    </div>
    </div>
    @endsection