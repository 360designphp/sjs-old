<!doctype html>
<html lang="ru">
<head>
    <title>Журнал документов</title>
</head>
<body>
<style>
    body {
        font-size: 14px;
        font-family: tahoma;
    }

    h2 {
        text-align: center;
    }

    table {
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
    }

    td {
        border: 1px solid #000;
        padding: 1em;
        margin: 0;
    }

    thead td {
        font-weight: bold;
    }

</style>
<h2>Электронный журнал учета операций, выполненных с помощью программного обеспечения и технологических средств ведения
    официального сайта застройщика, позволяющие обеспечивать учет всех действий в отношении информации на официальном
    сайте застройщика, подтверждающих размещение соответствующей информации<br>
    @if(isset($object))
    Объект: {{$object->name}}  @if(!is_null($queue)) очередь: {{$queue}} @endif 
    @endif
</h2>

<table>
    <thead>
    <tr>
        <td>Вид размещаемой информации</td>
        <td>Дата опубликования на сайте</td>
        <td>Лицо, разместившее информацию</td>
    </tr>
    </thead>
    <tbody>
    @foreach($documents as $document)
        <tr>
            <td>
                <a href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a>
            </td>
            <td>{{$document->created_at}}</td>
            <td>@if(is_object($document->getAuthor())){{$document->getAuthor()->name}}@endif</td>
        </tr>
    @endforeach
</table>
</body>
</html>