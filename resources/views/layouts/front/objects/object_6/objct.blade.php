@extends('layouts.front.index')
@section('content')
<?php
$building = $object->buildings->first();
?>

    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_etaj">
        <div class="row">
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12">
                <h2 class="text-center">Выберите этаж, oфисы или парковку.</h2>
                <div class="row">
                    <div class="col-xs-12 text-center breadcrumbs">
                        <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                        <a href="/objects/{{$object->id}}"><span class="h4">{{$object->name}}</span></a> >
                        <span class="h4">Корпус {{$building->number}}</span>
                    </div>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12 etaj_svg_wrap">
                <div class="etaj_svg_holder">
                    <input type="hidden" value="/front/buildings/{{$building->image}}" class="building-image">
                    {!!$building->svg!!}
                    <div class="etaj_svg_popover">
                        <div class="etaj_svg_popover_etaj_div">
                            <span class="etaj_popover_first_name">Этаж</span>
                            <span class="etaj_popover_etaj">5</span>
                        </div>
                        <div>
                            <span class="etaj_popover_second_name">Стоимость</span>
                            <span class="etaj_popover_price">от <span>8.1</span> ₽</span>
                        </div>
                        <div>
                            <span class="etaj_popover_third_name">Продано</span>
                            <span class="etaj_popover_kol_prod"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fourth_name">Забронировано</span>
                            <span class="etaj_popover_kol_zabr"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fifth_name">Свободно</span>
                            <span class="etaj_popover_kol_svob"></span>
                        </div>
                        <div class="etaj_svg_popover_arenda_div">
                            <span class="etaj_popover_sixth_name">Аренда</span>
                            <span class="etaj_popover_price_arr">от <span>8.1</span> ₽</span>
                        </div>
                        <div class="etaj_svg_popover_bgwrap"></div>
                    </div>
                </div>
                <section class="about_house text-center">
                    @include('layouts.front.objects.object_gallery')
                    @include('layouts.front.objects.object_'.$object->id.'.description')
                </section>
            </div>
        </div>
    </main>
    </div>
    </div>
@endsection
@section('data')
    <span class="etaj_holder hidden">{@foreach($building->sections->first()->floors->where('type', 'living') as $floor)"{{$floor->level}}": [<?=$floor->flats->min('price')?>, {{$floor->soldFlats()->count()}}, {{$floor->bookedFlats()->count()}}, {{$floor->freeFlats()->count()}},<?=is_int($floor->flats->min('rent_price'))?$floor->flats->min('rent_price'):0 ?>, "/objects/{{$object->id.'/'.$building->id.'/'.$floor->section->id.'/'.$floor->id}}"] @if(!$loop->last),@endif @endforeach}</span>
@endsection

