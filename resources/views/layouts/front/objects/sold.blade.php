@extends('layouts.front.index')
@section('content')

    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_text">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">{{$post->title }}</h1>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12 col-sm-5">
                @foreach($post->gallery() as $gal)
                        <a href="/front/images/{!! $gal->slug !!}" title="{{$gal->title}}"  data-title="{{$gal->title}}" rel="lightbox[gallery]"><img class="img-responsive mgb20" src="/front/thumbnail/{!! $gal->slug !!}" alt=""/></a>
                @endforeach
            </div>
            <div class="col-xs-12 col-sm-7">
                {!! $post->content!!}
                <div class="doc">
                    @if($post->documents()->count() != 0)<a href="/documents/{{$post->id}}/post" target="_blank">Журнал изменений</a>@endif

                @if($post->documents() != null)
                        <ul class="about_opisanie_docs list-unstyled">
                            @foreach($post->documents() as $document)
                                <li class="file_icon_{{mb_strtolower($document->content)}}"><a href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>

            <div class="vertical-devider"></div>
            <div class="col-xs-12">
                {!! $post->additional!!}
            </div>
            <div class="vertical-devider"></div>
        </div>
    </main>
@endsection