@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        @include('layouts.front.objects.slider')
        @include('layouts.front.objects.filter')
        <div class="row searches_list">
            @foreach($objects  as $object)
                @if(!$object->title)
                    <div class="col-xs-6 col-sm-4 search_item">
                        <div target="_blank" class="search_item_inner_wrap clearfix">
                            <a href="/objects/{{$object->id}}" class="no_underline">
                                <div class="search_item_inner_top">
                                    <div>{{$object->name}}</div>
                                </div>
                                <div class="search_item_inner_img">
                                    @if($object->status == 'building')<span class="search_item_inner_label color1">Строится</span>@endif
                                    @if($object->status == 'ready')<span class="search_item_inner_label color4">Сдан</span>@endif
                                    @if($object->status == 'sold')<span class="search_item_inner_label color2">Продано</span>@endif
                                    <img src="/front/objects/{{$object->image}}" alt="" class="">
                                </div>
                                <div class="search_item_inner_bottom no_underline">
                                    Подробнее <span class="shev_right"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                @else
                    <div class="col-xs-6 col-sm-4 search_item">
                        <div target="_blank" class="search_item_inner_wrap clearfix">
                            <a href="/page/{{$object->slug}}" class="no_underline">
                            <div class="search_item_inner_top">
                                <div>{{$object->title}}</div>
                            </div>
                            <div class="search_item_inner_img">
                                @if(!empty($object->badge_text))<span class="search_item_inner_label color5">{{$object->badge_text}}</span>@endif
                                @if(is_object($object->thumbnail()))
                                    <img src="/front/thumbnail/{{$object->thumbnail()->slug}}" alt="" class="">
                                @else
                                    <img src="/front/img/link3_fon.jpg" alt="" class="">
                                @endif
                            </div>
                            <div class="search_item_inner_bottom">
                                Подробнее <span class="shev_right"></span>
                            </div>
                            </a>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="vertical-devider"></div>
    </main>
@endsection