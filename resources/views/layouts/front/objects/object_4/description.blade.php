<span class="h3">{{$object->completion_date}}</span>
<h1 class="">{{$object->name}}</h1>

<div class="col-xs-12 about_opisanie">
    <div class="row">
        <div class="vertical-devider"></div>
        <div class="row">
            @if(count($object->albums()) != 0)
                <div class="main_process_back">
                    <a href="/objects/building_progress/{{$object->id}}" class="btn btn-default btn-kvart">Посмотреть ход строительства</a>
                </div>
            @endif
        </div>
        <div class="col-xs-12 text-center">
            <h3>Документация по объекту</h3>
            @if($object->documents()->count() != 0)<a href="/documents/{{$object->id}}/object" target="_blank">Журнал изменений</a>@endif
            <ul class="about_opisanie_docs list-unstyled">
                <?php $documents = $object->documents();?>

                @if($documents->count() <= 7)
                    @foreach($documents as $document)
                        <li class="file_icon_{{mb_strtolower($document->content)}}"><a
                                    href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a>
                            <small>{{$document->created_at}}</small> <small>ответственное лицо: {{$document->getAuthor()->name}}</small>
                        </li>
                    @endforeach
                @else
                    <?php $i = 0;?>
                    @while($i++ <= 7)
                        <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                    href="/front/documents/{{$documents[$i]->slug}}" target="_blank">{{$documents[$i]->title}}</a>
                            <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                        </li>
                    @endwhile
                    @if($documents->count() > 8)
                        <button data-toggle="collapse" data-target="#docsmore" class="">Показать ещё документы...
                        </button>
                        <div id="docsmore" class="collapse" style="">
                            @for($i = 8; $i <= count($documents->toArray())-1; $i++)
                                <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                            href="/front/documents/{{$documents[$i]->slug}}"
                                            target="_blank">{{$documents[$i]->title}}</a>
                                    <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                                </li>
                            @endfor
                        </div>
                    @endif
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-7">
    <div class="small-vertical-devider"></div>
    {!! $object->description !!}
</div>
<div class="col-xs-12 col-sm-5 padlr0">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1033.2491412262284!2d42.703062922249146!3d43.919394440980284!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582bb0e681b4a7%3A0x90824ab1555fddfc!2z0YPQuy4g0JzQsNGA0YbQuNC90LrQtdCy0LjRh9CwLCA5NtCwLCDQmtC40YHQu9C-0LLQvtC00YHQuiwg0KHRgtCw0LLRgNC-0L_QvtC70YzRgdC60LjQuSDQutGA0LDQuSwg0KDQvtGB0YHQuNGPLCAzNTc3MzY!5e0!3m2!1sru!2s!4v1505905995404" width="600" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="row about_map_additional">
        <div class="col-xs-3">
            {{$object->address}}
        </div>
        <div class="col-xs-6">
            <button type="button" class="btn btn-default">Заказать обратный звонок</button>
        </div>
        <div class="col-xs-3">
            <a href="https://www.google.com/maps/place/ул.+Марцинкевича,+96а,+Кисловодск,+Ставропольский+край,+Россия,+357736/@43.920005,42.703383,17z/data=!4m5!3m4!1s0x40582bb0e681b4a7:0x90824ab1555fddfc!8m2!3d43.91941!4d42.7030611?hl=ru-RU" target="_blank">Посмотреть на большой карте</a>
        </div>
    </div>
</div>
<div class="small-vertical-devider"></div>
