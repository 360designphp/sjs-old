@if(count($object->gallery()))
    <div class="col-xs-12">
        <h2>Галерея объекта</h2>
        <div class="vertical-devider"></div>
        <div class='main_text_slick'>
            @foreach($object->gallery() as $image)
                <div>
                    <a class="main_text_slick_image_link" href="/front/images/{{$image->slug}}" data-lightbox="Галерея объекта" data-title="{{$image->title}}"><img class="img-responsive" src="/front/thumbnail/{{$image->slug}}" alt=""/>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="vertical-devider"></div>
@endif