<h1 class="">{{$object->name}}</h1>

<div class="col-xs-12 about_opisanie">
    <div class="row">
        <div class="vertical-devider"></div>
        <div class="col-xs-12">
            <div class="row">
                @if(count($object->albums()) != 0)
                    <div class="main_process_back">
                        <a href="/objects/building_progress/{{$object->id}}" class="btn btn-default btn-kvart">Посмотреть ход строительства</a>
                        <!--<a href="https://rtsp.me/embed/pjfTnk53/" target="_blank" class="btn btn-default btn-kvart no-border"><i class="fa fa-video-camera" aria-hidden="true"></i> <span class="bordered-link">Онлайн-трансляция</span></a>-->
                        <a href="https://rtsp.me/embed/tGYbFvU2/" target="_blank" class="btn btn-default btn-kvart no-border"><i class="fa fa-video-camera" aria-hidden="true"></i> <span class="bordered-link">Онлайн-трансляция</span></a>
                    </div>
                    
                @endif
            </div>
           <div class="row">
            <div class="col-xs-4 text-center">
                <p class="h3">I Очередь</p>
                <p class="h4"> Сдача - первый квартал 2020 года</p>
                @if($object->documents()->count() != 0)<a href="/documents/{{$object->id}}/object/1" target="_blank">Журнал изменений</a>@endif
                <ul class="descripion-docs list-unstyled">

                <?php $documents = $object->documents(1);?>

                @if(count($documents->toArray()) <= 3)
                    @foreach($documents as $document)
                        <li class="file_icon_{{mb_strtolower($document->content)}}"><a
                                    href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a>
                            <small>{{$document->created_at}}</small> <small>ответственное лицо: {{$document->getAuthor()->name}}</small>
                        </li>
                    @endforeach
                @else
                    <?php $i = -1;?>
                    @while($i++ < 3)
                        <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                    href="/front/documents/{{$documents[$i]->slug}}" target="_blank">{{$documents[$i]->title}}</a>
                            <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                        </li>
                    @endwhile
				        <div id="docsmore" class="collapse docsmore" style="">
                            @for($i = 4; $i <= count($documents->toArray())-1; $i++)
                                <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                            href="/front/documents/{{$documents[$i]->slug}}"
                                            target="_blank">{{$documents[$i]->title}}</a>
                                    <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                                </li>
                            @endfor
                        </div>
                @endif
            </ul>
            </div>

            <div class="col-xs-4 text-center">
                <p class="h3">II Очередь</p>
                <p class="h4">Сдача - первый квартал 2021 года</p>
                @if($object->documents()->count() != 0)<a href="/documents/{{$object->id}}/object/2" target="_blank">Журнал изменений</a>@endif
                <ul class="descripion-docs list-unstyled">

                <?php $documents = $object->documents(2);?>

                @if(count($documents->toArray()) <= 3)
                    @foreach($documents as $document)
                        <li class="file_icon_{{mb_strtolower($document->content)}}"><a
                                    href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a>
                            <small>{{$document->created_at}}</small> <small>ответственное лицо: {{$document->getAuthor()->name}}</small>
                        </li>
                    @endforeach
                @else
                    <?php $i = -1;?>
                    @while($i++ < 3)
                        <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                    href="/front/documents/{{$documents[$i]->slug}}" target="_blank">{{$documents[$i]->title}}</a>
                            <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                        </li>
                    @endwhile
				        <div id="docsmore" class="collapse docsmore" style="">
                            @for($i = 4; $i <= count($documents->toArray())-1; $i++)
                                <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                            href="/front/documents/{{$documents[$i]->slug}}"
                                            target="_blank">{{$documents[$i]->title}}</a>
                                    <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                                </li>
                            @endfor
                        </div>
                @endif
            </ul>
            </div>

            <div class="col-xs-4 text-center">
                <p class="h3">III Очередь</p>
                <p class="h4">Сдача - первый квартал 2022 года</p>
                @if($object->documents()->count() != 0)<a href="/documents/{{$object->id}}/object/3" target="_blank">Журнал изменений</a>@endif

                <ul class="descripion-docs list-unstyled">

                <?php $documents = $object->documents(3);?>

                @if(count($documents->toArray()) <= 3)
                    @foreach($documents as $document)
                        <li class="file_icon_{{mb_strtolower($document->content)}}"><a
                                    href="/front/documents/{{$document->slug}}" target="_blank">{{$document->title}}</a>
                            <small>{{$document->created_at}}</small> <small>ответственное лицо: {{$document->getAuthor()->name}}</small>
                        </li>
                    @endforeach
                @else
                    <?php $i = -1;?>
                    @while($i++ < 3)
                        <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                    href="/front/documents/{{$documents[$i]->slug}}" target="_blank">{{$documents[$i]->title}}</a>
                            <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                        </li>
                    @endwhile
				        <div id="docsmore" class="collapse docsmore" style="">
                            @for($i = 4; $i <= count($documents->toArray())-1; $i++)
                                <li class="file_icon_{{mb_strtolower($documents[$i]->content)}}"><a
                                            href="/front/documents/{{$documents[$i]->slug}}"
                                            target="_blank">{{$documents[$i]->title}}</a>
                                    <small>{{$documents[$i]->created_at}}</small> <small>ответственное лицо: {{$documents[$i]->getAuthor()->name}}</small>
                                </li>
                            @endfor
                        </div>
                @endif
            </ul>
            </div>
          
           </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-default btn-kvart btn-docs" data-toggle="collapse" data-target=".docsmore" class="">Показать ещё документы
                        </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-7">
    <div class="small-vertical-devider"></div>
    {!! $object->description !!}
</div>
<div class="col-xs-12 col-sm-5 padlr0">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2417.303932041352!2d42.73947735557779!3d43.9039967399111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582b193ec183ad%3A0xccf04399fb984c7!2z0YPQuy4g0JLQvtC50LrQvtCy0LAsINCa0LjRgdC70L7QstC-0LTRgdC6LCDQodGC0LDQstGA0L7Qv9C-0LvRjNGB0LrQuNC5INC60YDQsNC5LCAzNTc3MDM!5e0!3m2!1sru!2sru!4v1498720558807"
            width="600" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="row about_map_additional">
        <div class="col-xs-3">
            {{$object->address}}
        </div>
        <div class="col-xs-6">
            <button type="button" class="btn btn-default">Заказать обратный звонок</button>
        </div>
        <div class="col-xs-3">
            <a href="https://www.google.com/maps?ll=43.903582,42.741163&z=17&t=m&hl=ru-RU&gl=RU&mapclient=embed&q=%D1%83%D0%BB.+%D0%92%D0%BE%D0%B9%D0%BA%D0%BE%D0%B2%D0%B0+%D0%9A%D0%B8%D1%81%D0%BB%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D0%BA+%D0%A1%D1%82%D0%B0%D0%B2%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9+357703" target="_blank">Посмотреть
                на большой карте</a>
        </div>
    </div>
</div>
<div class="small-vertical-devider"></div>
