<div class="slick_houses">
    @if(isset($slides))
        @foreach($slides as $slide)
            <div>
                <a href="{!!$slide->slug!!}">
                    @if(is_object($slide->thumbnail()))
                        <img src="/front/images/{{$slide->thumbnail()->slug}}" class="img-responsive">
                    @else
                        <img src="/front/img/slide1.jpg" class="img-responsive">
                    @endif
                </a>
                <div class="slick_caption">
                    <h2>{{$slide->title}}</h2>
                    <small>{!! $slide->content !!}</small>
                </div>
            </div>
        @endforeach
    @endif
</div>