<nav class="main_filter
@if(!isset($param['type']) || $param['type'] == 1){{'selected1'}}@endif
@if(isset($param['type']) && $param['type'] == 2){{'selected2'}}@endif
@if(isset($param['type']) && $param['type'] == 3){{'selected3'}}@endif
        ">
    <form action="/objects/filtered" method="GET" class="form-inline" role="form" id="filter">
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="select_cutom main_filter_selection">
            <select name="type" id="inputselcom" class="form-control">
                <option value="1"  @if(isset($param['type']) && $param['type'] == 1){{'selected'}}@endif>Жилая недвижимость</option>
                <option value="2" @if(isset($param['type']) && $param['type'] == 2){{'selected'}}@endif>Коммерческая</option>
                <option value="3" @if(isset($param['type']) && $param['type'] == 3){{'selected'}}@endif>Паркинг</option>
            </select>
        </div>
        <div class="select_cutom main_filter_rooms">
            <select name="rooms" id="selrooms" class="form-control">
                <option value="all" @if(isset($param['rooms']) && $param['rooms'] == 'all'){{'selected'}}@endif>Количество комнат</option>
                <option value="1" @if(isset($param['rooms']) && $param['rooms'] == 1){{'selected'}}@endif>1-комната</option>
                <option value="2" @if(isset($param['rooms']) && $param['rooms'] == 2){{'selected'}}@endif>2-комнаты</option>
                <option value="3" @if(isset($param['rooms']) && $param['rooms'] == 3){{'selected'}}@endif>3-комнаты</option>
            </select>
        </div>

        <div class="main_filter_squares">
            Площадь&nbsp;от&nbsp;<input type="number" name="area_from" id="" value="@if(isset($param['area_from'])){{$param['area_from']}}@else{{1}}@endif"> до&nbsp;<input type="number" name="area_to" id="" value="@if(isset($param['area_to'])){{$param['area_to']}}@else{{500}}@endif">&nbsp;м<sup>2</sup>
        </div>

        <div class="main_filter_etaj">
            Этаж&nbsp;от&nbsp;<input type="number" name="floor_from" id="" value="@if(isset($param['floor_from'])){{$param['floor_from']}}@else{{1}}@endif"> до&nbsp;<input type="number" name="floor_to" id="" value="@if(isset($param['floor_to'])){{$param['floor_to']}}@else{{20}}@endif">
        </div>

        <div class="select_cutom main_filter_place">
            <select name="object" id="selrooms" class="form-control">
                <option value="">Объект</option>
                @foreach($objects as $object)
                <option value="{{$object->id}}" @if(isset($param['object']) && $param['object'] == $object->id){{'selected'}}@endif>{{$object->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="select_cutom main_filter_sdelka">
            Тип сделки:&nbsp;&nbsp;
            <select name="deal" id="selsdelka" class="form-control">
                <option value="buy">Покупка</option>
                <option value="rent">Аренда</option>
            </select>
        </div>
        <div class="select_cutom main_filter_carplace">
            <select name="parking_places" id="selrooms" class="form-control">
                <option value="1" @if(isset($param['parking_places']) && $param['parking_places'] == 1){{'selected'}}@endif>1-машиноместо</option>
                <option value="2" @if(isset($param['parking_places']) && $param['parking_places'] == 2){{'selected'}}@endif>2-машиноместа</option>
                <option value="3" @if(isset($param['parking_places']) && $param['parking_places'] == 3){{'selected'}}@endif>3-машиноместа</option>
                <option value="4" @if(isset($param['parking_places']) && $param['parking_places'] == 4){{'selected'}}@endif>4-машиноместа</option>
            </select>
        </div>
        <div class="nextline"></div>
        <div class="rangeslider_wrap">
            <div class="rangeslider_cap">
                <p>Стоимость,<span class="rub">a</span> </p>
            </div>
            <span id="range_sl1-label-2a" class="hidden">Example low value</span>
            <span id="range_sl1-label-2b" class="hidden">Example high value</span>
            <input id="range_sl1" type="text" />
            <span id='range_sl1_a'>0</span>
            <span id='range_sl1_b'>7 000 000</span>
        </div>
        <input type="hidden" class="price_from" name="price_from" @if(isset($param['price_from'])) value="{{$param['price_from']}}" @else value="0" @endif >
        <input type="hidden" class="price_to" name="price_to" @if(isset($param['price_to'])) value="{{$param['price_to']}}" @else value="7000000" @endif >
    </form>
    <div class="main_filter_button"><button type="submit" class="btn btn-primary" form="filter"></button></div>
</nav>