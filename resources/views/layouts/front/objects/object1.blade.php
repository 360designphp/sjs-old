@extends('layouts.front.index')
@section('content')
sh       Многоквартирный 3-х этажный жилой дом, количество квартир в доме – 59, 1-но, 2-х и 3-х комнатные квартиры, свободная планировка.
                        </li>
                        <li>
                            Высота этажа – 3 метра, все несущие стены кирпичные, шатровая кровля.
                        </li>
                        <li>
                            Дом газифицирован, в каждой квартире индивидуальная система отопления, центральное водоснабжение.
                        </li>
                        <li>
                            Благоустроенная придомовая территория, развитая инфраструктура.
                        </li>
                    </ul>
                    <ul class="about_opisanie_docs list-unstyled">
                        <li class="file_icon_xls"><a href="#дайскачать">Plan.xls</a><small>3.9 Мб, 03.01.2017 в 15:01</small></li>
                        <li class="file_icon_pdf"><a href="#дайскачать">Plan.pdf</a><small>3.9 Мб, 03.01.2017 в 15:01</small></li>
                        <li class="file_icon_doc"><a href="#дайскачать">Plan.doc</a><small>3.9 Мб, 03 февраля в 15:01</small></li>
                        <li class="file_icon_jpg"><a href="#дайскачать">Plan.jpg</a><small>3.9 Мб, 03 февраля в 15:01</small></li>
                        <li class="file_icon_zip"><a href="#дайскачать">Plan.zip</a><small>3.9 Мб, 03 февраля в 15:01</small></li>
                    </ul>
                </div>
                <div class="col-xs-6 padlr0">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2874.6042166603547!2d42.73230431550589!3d43.90546327911355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582b1bad454c4d%3A0x9c01f1e2e6ab8904!2z0YPQuy4g0JLQvtC50LrQvtCy0LAsIDEzLCDQmtC40YHQu9C-0LLQvtC00YHQuiwg0KHRgtCw0LLRgNC-0L_QvtC70YzRgdC60LjQuSDQutGA0LDQuSwgMzU3NzAz!5e0!3m2!1sru!2sru!4v1491142511474" width="100%" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <div class="row about_map_additional">
                        <div class="col-xs-3">
                            г.Кисловодск, ул.Войкова, д.13
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-default">Заказать обратный звонок</button>
                        </div>
                        <div class="col-xs-3">
                            <a href="https://www.google.com/maps?ll=43.905912,42.736746&z=16&t=m&hl=ru-RU&gl=RU&mapclient=embed&q=%D1%83%D0%BB.+%D0%92%D0%BE%D0%B9%D0%BA%D0%BE%D0%B2%D0%B0,+13+%D0%9A%D0%B8%D1%81%D0%BB%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D0%BA+%D0%A1%D1%82%D0%B0%D0%B2%D1%80%D0%BE%D0%BF%D0%BE%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9+%D0%BA%D1%80%D0%B0%D0%B9+357703">Посмотреть на большой карте</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 korpus_news">
            <div class="vertical-devider"></div>
            <h2>Важные события</h2>
            <div class="slick_news">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/21455132o.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 1</h4>
                                    <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/fon3.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 2</h4>
                                   f <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/slide1.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 1</h4>
                                    <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/21455132o.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 2</h4>
                                    <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/fon3.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 1</h4>
                                    <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-3">
                                <img src="/front/img/21455132o.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-9">
                                <div>
                                    <small>15 мая 2017</small>
                                    <h4>СТРОИТЕЛЬСТВО ЖИЛОГО КОМПЛЕКСА 2</h4>
                                    <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vertical-devider"></div>
        </div>
    </section>

                </div>
            </div>
        </div>
    </main>
    @endsection