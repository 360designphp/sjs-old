@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_process">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Ход строительства: {{$object->name}}</h1>
            </div>
            <div class="vertical-devider"></div>
            @foreach($queues as $number => $albums)
            @if(count($albums) != null)
                @if(count($queues) > 1)
                    <div class="container">
                        <a class="h4" role="button" data-toggle="collapse" href="#collapseQueue{{$number}}"
                           aria-expanded="false" aria-controls="collapseExample">
                            Ход строительства {{$number}}-й очереди
                        </a>

                        <div class="collapse" id="collapseQueue{{$number}}">
                            <div>
                                @foreach($albums as $album)
                                    <div class="col-xs-4 main_process_item">
                                        <a class="main_process_image_link"
                                           href="/objects/building_progress/{{$object->id}}/{{$album->slug}}"><img
                                                    class="img-responsive"
                                                    @if($album->thumbnail()) src="/front/thumbnail/{{$album->thumbnail()->slug}}"
                                                    @endif alt=""/>
                                        </a>
                                        <div>{{$album->title}}@if($album->additional)
                                                <span>{!!$album->additional!!}</span>@endif</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                     <div>
                                @foreach($albums as $album)
                                    <div class="col-xs-4 main_process_item">
                                        <a class="main_process_image_link"
                                           href="/objects/building_progress/{{$object->id}}/{{$album->slug}}"><img
                                                    class="img-responsive"
                                                    @if($album->thumbnail()) src="/front/thumbnail/{{$album->thumbnail()->slug}}"
                                                    @endif alt=""/>
                                        </a>
                                        <div>{{$album->title}}@if($album->additional)
                                                <span>{!!$album->additional!!}</span>@endif</div>
                                    </div>
                                @endforeach
                            </div>
                @endif
            @endif
            @endforeach
            <div class="vertical-devider"></div>
            <div class="main_process_back">
                <a href="/objects/{{$object->id}}" class="btn btn-default btn-kvart">Вернуться к объекту</a>
            </div>
            <div class="vertical-devider"></div>
        </div>
        </div>
    </main>
    </div>
    </div>
@endsection