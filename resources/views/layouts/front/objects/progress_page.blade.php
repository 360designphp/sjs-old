\@extends('layouts.front.index')
@section('content')
<main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_process">
    <div class="vertical-devider"></div>
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center">Ход строительства: {{$album->title}}</h1>
        </div>
        <div class="vertical-devider"></div>
        @foreach($album->gallery() as $image)
        <div class="col-xs-4 main_process_item">
            <a class="main_process_image_link" href="/front/images/{{$image->content}}" data-lightbox="{{$album->title}}" data-title="{{$image->title}}"><img class="img-responsive" src="/front/thumbnail/{{$image->content}}" alt=""/>
            </a>
            <div>{{$image->title}}</div>
        </div>
        @endforeach
        <div class="vertical-devider"></div>
        <div class="main_process_back">
            <a href="/objects/building_progress/{{$object->id}}" class="btn btn-default btn-kvart">Вернуться назад</a>
        </div>
        <div class="vertical-devider"></div>
    </div>
    </div>
</main>
</div>
</div>
@endsection