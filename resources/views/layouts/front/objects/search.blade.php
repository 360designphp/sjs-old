@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        @include('layouts.front.objects.slider')
        @include('layouts.front.objects.filter')
        <div class="row searches_list">
            @if(count($flats) == 0)
                <h1 class="align-">По данным параметрам не найдено предложений</h1>
            @endif
            @foreach($flats as $flat)
                <div class="col-xs-6 col-sm-4 col-lg-3 search_item">
                    <div target="_blank" class="search_item_inner_wrap clearfix">
                        <a href="/objects/{{$flat->getObject()->id}}/{{$flat->building()->id}}/{{$flat->section()->id}}/{{$flat->floor->id}}/#kv{{$flat->number}}"
                           class="no_underline" target="_blank">
                            <div class="search_item_inner_top">
                                <div>{{$flat->getObject()->name}}</div>
                            </div>
                            <div class="search_item_inner_img">
                                @if($flat->status == 'free')<span class="search_item_inner_label color1">Свободно</span>@endif
                                @if($flat->status == 'ready')<span class="search_item_inner_label color4">Забронировано</span>@endif
                                @if($flat->status == 'sold')<span class="search_item_inner_label color2">Продано</span>@endif
                                @if($flat->floor->type !='parking')
                                    <img src="/front/flats/{{$flat->image}}" alt="" class="img-responsive">
                                @else
                                    @if($flat->price == 350000 || $flat->price <= 100)
                                        <img src="/front/img/park_sm.png" alt="" class="img-responsive">
                                    @endif
                                    @if($flat->price == 450000)
                                        <img src="/front/img/park_md.png" alt="" class="img-responsive">
                                    @endif
                                    @if($flat->price == 500000)
                                        <img src="/front/img/parklg.png" alt="" class="img-responsive">
                                    @endif
                                @endif

                                <div class="search_item_inner_info">
                                <!-- <span class="search_item_inner_span_obj">{{$flat->getObject()->name}}</span> -->
                                    <span class="search_item_inner_span_sect">Секция: {{$flat->section()->symbol}}</span>
                                    <span class="search_item_inner_span_etaj">Этаж: {{$flat->floor->level}}</span>
                                    @if($flat->floor->type == 'living')<span class="search_item_inner_span_kvart">Квартира: {{$flat->number}}</span>@endif
                                    @if($flat->floor->type == 'commercial')<span class="search_item_inner_span_kvart">Офис: {{$flat->number}}</span>@endif
                                    @if($flat->floor->type == 'parking')<span class="search_item_inner_span_kvart">Парковочное место: {{$flat->number}}</span>@endif
                                    @if($flat->floor->type != 'parking') <span class="search_item_inner_span_area">Площадь: {{$flat->area}}
                                        м<sup>2</sup></span>@endif
                                    <span class="search_item_inner_span_price">Цена: {{$flat->price}}<span class="ruble"> q</span></span>
                                </div>
                            </div>
                            <div class="search_item_inner_bottom no_underline">
                                Подробнее <span class="shev_right"></span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
            <div class="vertical-devider"></div>
    </main>
    </div>
    </div>
@endsection