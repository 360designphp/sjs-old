@section('modal')
<div class="modal fade" id="modal_zakaz">
    <div class="modal-dialog modal-lg modal_zakaz_dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center">Заказать квартиру</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <img src="img/44.jpg" class="img-responsive modal_img_kvart center-block" alt="квартира 44">
                    </div>
                    <div class="col-xs-12 col-sm-6 modal_zakaz_dialog_forms">
                        <h4>Оставьте заявку,<br> и мы вам перезвоним</h4>
                        <div class="vertical-devider"></div>
                        <label for="input_vash_tel2">Ваш телефон:</label><input type="text" name="vash_tel2" id="input_vash_tel2" class="form-control"  required="required" title="" placeholder="+7 (123) 456-78-90">
                        <label for="input_vash_name2">Как к вам обращаться:</label><input type="text" name="vash_name2" id="input_vash_name2" class="form-control"  required="required" title="">
                        <label for="input_vash_date2">Когда вам удобно:</label><input type="text" name="vash_date2" id="input_vash_date2" class="form-control timepicker"  required="required" title="" placeholder="Нажмите тут">
                        <label for="input_vash_email2">Ваш e-mail адрес:</label><input type="email" name="vash_email2" id="input_vash_name2" class="form-control"  required="required" title="">
                        <div class="vertical-devider"></div>
                        Задайте ваши вопросы нам!
                        <textarea name="vash_text" id="inputvash_text" class="form-control" rows="3" required="required"></textarea>
                        <input type="text" id="kvartid">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary">Заказать</button>
                <button type="button" class="btn btn-default btn_kvart" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
@endsection