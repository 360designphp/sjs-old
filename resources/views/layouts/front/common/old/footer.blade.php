<svg id="offices_x5F_view" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 1481.3 599.8">
    <style>.kvart {
            opacity: .5;
            fill: transparent
        }</style>
    <image width="6172" height="2499" xlink:href="img/officeplan1.jpg" transform="scale(.24)" overflow="visible"/>
    <path class="kvart kv1" d="M5.3 82.8h191.2v62.4h104.9v369.9H104.3V436h-99z"/>
    <path class="kvart kv3" d="M780.4 135.7l-.1-53.4h118.3v433.5H722.9v-380z"/>
    <path class="kvart kv4" d="M1016.3 150.9l.1-60.2H905.7v425.1H1080V232h-26l-.2-81.1z"/>
    <path class="kvart kv5" d="M1156.1 150.9l-.1-60.2h282.6v425.1h-351.9V232h26.2l.1-81.1z"/>
    <path class="kvart kv2" d="M443.3 90.7h279.6v425.1H441.8V344.9h-5.1V90.7z"/>
</svg>