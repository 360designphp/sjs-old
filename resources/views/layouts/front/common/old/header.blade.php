<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@if(isset($pageDescription)){!!$pageDescription!!}@else @if(isset($settings)){!!$settings->description!!}@endif @endif ">
    <meta name="keywords" content="@if(isset($pageKeywords)){!!$pageKeywords!!}@else @if(isset($settings)){!!$settings->keywords!!}@endif @endif ">
    <title>@if(isset($pageTitle)){!!$pageTitle!!}@else @if(isset($settings)){!!$settings->site_name!!}@endif @endif </title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="/front/img/favicon.ico">
    <link rel="icon" type="image/png" href="/front/img/favicon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/front/css/bootstrap.min.css">
    <link rel="stylesheet" href="/front/css/style.css">
    <link rel="stylesheet" href="/front/css/gallerybox.css">
    <link rel="stylesheet" href="/front/css/lightbox.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('main')}}">
                    <img src="/front/img/toplogo.svg" class="logos img-responsive">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
				@if(isset($top_menu))
                           @foreach($top_menu as $menu)
                               <li class=""><a href="{!!$menu->link!!}" @if($menu->blank == 1)target="_blank"@endif>{!! $menu->caption !!}</a></li>
                           @endforeach
                       @endif
                    
                    <li class="top-phone">
                        <p>8793 31–64–65 <br>
                8793 31–64–51</p>
                    </li>
                    <li class="lihelper"></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    </div>