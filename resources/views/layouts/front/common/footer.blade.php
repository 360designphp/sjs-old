</div>
</div>
<footer class="container-fluid">
    <div class="row">
        <div class="col-xs-7 col-xs-offset-1 col-md-9 col-md-offset-3 col-sm-10 col-sm-offset-0">
            <div class="row">
                <div class="col-xs-12 col-sm-11 col-sm-offset-1 footer_360">
                    @if(isset($bottom_menu))
                        @foreach($bottom_menu->where('parent_id',0)->get() as $menu_item)
                            @if($menu_item->childs())
                                <div>
                                    <ul class="list-unstyled">
                                        @foreach($menu_item->childs() as $item)
                                        <li>
                                            <a href="{{$item->link}}" @if($item->blank == 1)target="_blank"@endif>{{$item->caption}}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        @endforeach
                    @endif
                    <div>
                        <a href="http://360design.ru" target="_blank">
                            <img src="/front/img/360logo.svg"  width="130px" class='foot-logo' alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div
<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->
<!-- Bootstrap JavaScript -->
@yield('data')
<script type="text/javascript" src="/front/js/main.js"></script>
<script type="text/javascript" src="/front/js/volodya.js"></script>
<!--<script type="text/javascript" src="/front/js/common.js"></script>-->
<script type="text/javascript">
  // Some Slider resizes
  $(function(){
    $('.slick_houses').height($('.slick_houses').width()/1.6744186);

    $(window).resize(function(){
      $('.slick_houses').height($('.slick_houses').width()/1.6744186);
    });
  });
  
  
  
    function requestRecall() {
        $('input').removeClass('error');
        $('select').removeClass('error');

        var phone = $('.recall_phone').val();
        var name = $('.recall_name').val();
        var time = $('.recall_time').val();
        var email = $('.recall_email').val();
        var region = $('.recall_region').val();
        var text = $('.recall_questions').val();
        var has_error = false;
        if(phone == ''){
            $('.recall_phone').addClass('error');
            has_error = true;

        }
        if(name == ''){
            $('.recall_name').addClass('error');
            has_error = true;
        }
        if(region == '— Выберите регион —'){
            $('.recall_region').addClass('error');
            has_error = true;
        }

        if(!$('.recall_approve').is(':checked') ){
            $('.recall_approve').addClass('error');
            has_error = true;
        }

        if(has_error) {
            return false;
        }

        $.ajax({
            url: '/api/recall_request',
            type: 'post',
            data: {
                'name': name,
                'phone': phone,
                'time': time,
                'email': email,
                'region': region,
                'text':text,
                'type':'recall'
            },
            success: function () {
                alert('Спасибо, наш менеджер свяжется с вами в ближайшее время');
                $('#modal_callus').modal('toggle');
            }
        })
    }
    $('#range_sl1_a').bind("DOMSubtreeModified",function(){
        $('.price_from').val($('#range_sl1_a').text());
    });
    $('#range_sl1_b').bind("DOMSubtreeModified",function(){
        $('.price_to').val($('#range_sl1_b').text());
    });
</script>
@yield('scripts')
@if(isset($settings)){!! $settings->scripts !!}@endif
            
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'bo0vfLOXIg';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</body>

</html>