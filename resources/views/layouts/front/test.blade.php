@extends('layouts.front.index')
@section('content')
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        @include('layouts.front.objects.slider')
        @include('layouts.front.objects.filter')
        <div class="row main_links_houses">
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/building">
                    <div class="main_links_houses_img2"></div>
                    <span>Строящиеся дома</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/ready">
                    <div class="main_links_houses_img1"></div>
                    <span>Сданные дома</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/">
                    <div class="main_links_houses_img3"></div>
                    <span>Все объекты с 1993 года</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/page/hot-offer">
                    <div class="main_links_houses_img4"></div>
                    <span>Горячее предложение</span>
                </a>
            </div>
        </div>
        <div class="vertical-devider"></div>
        <div class="main_banks row">
            <div class="col-xs-2 col-xs-offset-1">
                <a href="http://sberbank.ru/ru/person/credits/home">
                    <img src="/front/img/sbrbnk.png" class="img-responsive" alt="Сбербанк">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="http://www.vbank.ru/personal/credits/product_novostroy/?utm_source=partners&utm_medium=logo&utm_campaign=stavropol-partners-ipoteka" target="_blank" rel="nofollow">
                    <img src="/front/img/vzrjdn.png" class="img-responsive" alt="Банк Возрождение">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="http://www.vtb24.ru/" target="_blank" rel="nofollow">
                    <img src="/front/img/vtb.png" class="img-responsive" alt="ВТБ 24">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="http://www.rshb.ru/" target="_blank" rel="nofollow">
                    <img src="/front/img/rshlzbnk.png" class="img-responsive" alt="Россельхозбанк">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="https://www.sviaz-bank.ru/service/hypotec-new/" target="_blank" rel="nofollow">
                    <img src="/front/img/sb_220x60.gif" class="img-responsive" alt="Сбербанк">
                </a>
            </div>
        </div>
        <div class="vertical-devider"></div>
        <div class="main_news">
        @if(count($press_centre) != 0)
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#news-pane" aria-controls="news-pane" role="tab" data-toggle="tab"><h2 class="mauto">Новости</h2></a></li>
            <li role="presentation"><a href="#press-pane" aria-controls="press-pane" role="tab" data-toggle="tab" class="press-tab"><h2 class="mauto">Пресс-центр</h2></a></li>
          </ul>
          <!-- Tab panes -->
            <div class="tab-content">
            <div role="tabpanel" class="tab-pane active pt5" id="news-pane">
                <a href="/news" class="more-news-link">Все новости</a>
                <div class="slick_news slick_style">
                @for($i = 0; $i < count($news); $i +=3)
                <div class="row">
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($news[$i]->thumbnail() != null) /front/images/{{$news[$i]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$news[$i]->created_at}}</small>
                                    <h4>{{$news[$i]->title}}</h4>
                                    {!! $news[$i]->additional!!}
                                    <a href="/{{$news[$i]->type}}/{{$news[$i]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($news[$i+1]))
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($news[$i+1]->thumbnail() != null) /front/images/{{$news[$i+1]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$news[$i+1]->created_at}}</small>
                                    <h4>{{$news[$i+1]->title}}</h4>
                                    {!! $news[$i+1]->additional !!}
                                    <a href="/{{$news[$i+1]->type}}/{{$news[$i+1]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @endif
                    @if(isset($news[$i+2]))
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($news[$i+2]->thumbnail() != null) /front/images/{{$news[$i+2]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$news[$i+2]->created_at}}</small>
                                    <h4>{{$news[$i+2]->title}}</h4>
                                    {!! $news[$i+2]->additional !!}
                                    <a href="/{{$news[$i+2]->type}}/{{$news[$i+2]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endfor
            </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="press-pane">
                <a href="/press-centre" class="more-news-link more-press-link">Все публикации</a>
                <div class="slick_press slick_style"> 
                @for($i = 0; $i < count($press_centre); $i +=3)
                <div class="row">
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($press_centre[$i]->thumbnail() != null) /front/images/{{$press_centre[$i]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$press_centre[$i]->created_at}}</small>
                                    <h4>{{$press_centre[$i]->title}}</h4>
                                    {!! $press_centre[$i]->additional!!}
                                    <a href="/{{$press_centre[$i]->type}}/{{$press_centre[$i]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(isset($press_centre[$i+1]))
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($press_centre[$i+1]->thumbnail() != null) /front/images/{{$press_centre[$i+1]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$press_centre[$i+1]->created_at}}</small>
                                    <h4>{{$press_centre[$i+1]->title}}</h4>
                                    {!! $press_centre[$i+1]->additional !!}
                                    <a href="/{{$press_centre[$i+1]->type}}/{{$press_centre[$i+1]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                      @if(isset($press_centre[$i+2]))
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="@if($press_centre[$i+2]->thumbnail() != null) /front/images/{{$press_centre[$i+2]->thumbnail()->slug}} @else /front/img/21455132o.jpg @endif" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small>{{$press_centre[$i+2]->created_at}}</small>
                                    <h4>{{$press_centre[$i+2]->title}}</h4>
                                    {!! $press_centre[$i+2]->additional !!}
                                    <a href="/{{$press_centre[$i+2]->type}}/{{$press_centre[$i+2]->slug}}" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                @endfor
            </div>
            </div>
          </div>
        </div>
        @endif
        <div class="vertical-devider"></div>
        <div class="main_about row">
            <div class="col-xs-7 col-xs-offset-1 aboutcom">
                <h2>О компании</h2>
                <div>Строительная компания «Стройжилсервис» организована в 1993 году под руководством директора Леонида Евсеевича Пихельсона.</div>
                <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края.</p>
                <p>В настоящее время на предприятии созданы все условия для работы административного, управленческого и линейного инженерно-технического персонала во вновь обустроенных собственных административных помещениях.</p>
                <button type="button" class="btn" onclick="window.location='/page/o-kompanii'">Подробнее</button>
            </div>
            <div class="col-xs-3 padlr0">
                <iframe src="https://player.vimeo.com/video/169973642" width="100%" height="200px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </main>
    </div>
    </div>
@endsection