<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden = [
    'remember_token',
    ];

    public function updateUser(Request $request){

        //dd($request->all());
        $this->name = $request->user_name;
        $this->email = $request->user_email;
        $this->role = $request->role;

        if($request->new_password == $request->confirm_new_password){
            $this->password = bcrypt($request->new_password);
            //$this->name = $request->new_password;
        }

        $this->save();
    }
}
