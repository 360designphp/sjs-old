<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function building()
    {
        return $this->belongsTo('App\Building');
    }

    public function floors()
    {
        return $this->hasMany('App\Floor');
    }

    public function flats()
    {
        return $this->hasManyThrough('App\Flat', 'App\Floor');
    }
    public function livingFlats() {
        $floors = $this->floors()->where('type', 'living')->get();
        $flats_array = [];
        foreach($floors as $floor) {
            $flats_array[] = $floor->flats->where('status', 'free');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function freeFlats()
    {
        return $this->flats->where('status', 'free');
    }

    public function bookedFlats()
    {

        return $this->flats->where('status', 'booked');
    }

    public function soldFlats()
    {
        return $this->flats->where('status', 'sold');
    }

    public function floorsOrdered()
    {
        $floors = Floor::where('section_id', $this->id)->orderBy('level')->get();
        return $floors;
    }

    public function floorsOrderedDesc()
    {
        $floors = Floor::where('section_id', $this->id)->orderBy('level', 'desc')->get();
        return $floors;
    }

    public function previousSectionLink(Floor $floor)
    {
        $level = $floor->level;
        switch ($this->number){
            case 1:
                return 'minimal';
                break;
            default:
                $sect = Section::where('building_id', $this->building_id)->where('number',(int)$this->number - 1)->first();
                break;
        }
        if(is_object($sect)) {
            $new_floor = Floor::where('section_id', $sect->id)->where('level', $level)->first();
            if (!is_object($new_floor)) {
                if($floor->level > Floor::where('section_id', $sect->id)->min('level')){
                    $level = Floor::where('section_id', $sect->id)->max('level');
                } else {
                    $level = Floor::where('section_id', $sect->id)->min('level');
                }
                $new_floor = Floor::where('section_id', $sect->id)->where('level', $level)->first();
            }
            return ' <a href="'.'/objects/' . $floor->object()->id . '/' . $this->building->id . '/' . $sect->id . '/' . $new_floor->id.'" class="kvartplan_korpus_link">
                    <div class="arrow_kvart arrleft"></div>
                   Секция '. $sect->symbol.'
                </a>';
        }

    }

    public function nextSectionLink(Floor $floor)
    {
        $level = $floor->level;
        $max_number = Section::where('building_id',$this->building_id)->max('number');
        if ($this->number  >= $max_number) {
            return 'maximal';
        } else {
                $sect = Section::where('building_id', $this->building_id)->where('number',(int)$this->number + 1)->first();
        }
        if(is_object($sect)) {
            $new_floor = Floor::where('section_id', $sect->id)->where('level', $level)->first();
            if (!is_object($new_floor)) {
                if($floor->level > Floor::where('section_id', $sect->id)->min('level')){
                    $level = Floor::where('section_id', $sect->id)->max('level');
                } else {
                    $level = Floor::where('section_id', $sect->id)->min('level');
                }
                $new_floor = Floor::where('section_id', $sect->id)->where('level', $level)->first();
            }
            return ' <a href="'.'/objects/' . $floor->object()->id . '/' . $this->building->id . '/' . $sect->id . '/' . $new_floor->id.'" class="kvartplan_korpus_link">
                    <div class="arrow_kvart arrright"></div>
                   Секция '. $sect->symbol.'
                </a>';
        }

    }

    public function getPrice($rooms)
    {
        $price = $this->flats()->where('rooms', $rooms)->min('price');
        if (!$price) {
            return 1;
        }
        return $price;
    }
}


