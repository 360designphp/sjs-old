<?php

namespace App;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

class Object extends Model
{

    protected $guarded = ['_token'];
    
    public function buildings() {
        return $this->hasMany('App\Building');
    }
    public function sections()
    {
        return $this->hasManyThrough('App\Section', 'App\Building');
    }
    public function floors()
    {
        return $this->hasManyThrough('App\Section', 'App\Building');
    }
    public function  flats() {

        $sections = $this->sections;
        $flats_array = [];
         foreach($sections as $section) {
            $flats_array[] = $section->flats;
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }


    public function  freeFlats() {
        $flats_array = [];
        foreach($this->sections as $section) {
            $flats_array[] = $section->flats->where('status', 'free');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function  bookedFlats() {
        $flats_array = [];
        foreach($this->sections as $section) {
            $flats_array[] = $section->flats->where('status', 'booked');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function  soldFlats() {
        $flats_array = [];
        foreach($this->sections as $section) {
            $flats_array[] = $section->flats->where('status', 'sold');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }
    public function childs() {
        return Post::where('parent_id', $this->id);
    }

    //Получаем все документы объекта
    public function documents($queue = null ) {
        if(!is_null($queue)) {
            return $this->childs()->where('type', 'document')->where('parent_type', 'object')->where('queue', $queue)->orderBy('queue','asc')->orderBy('order','asc')->get();
        } else {
            return $this->childs()->where('type', 'document')->where('parent_type', 'object')->orderBy('queue','asc')->orderBy('order','asc')->get(); 
        }
    }

    public function gallery() {
        return $this->childs()->where('type', 'object_gallery')->get();
    }

    public function albums() {
        return $this->childs()->where('type', 'object_album')->get();
    }
    public function firstBuilding() {
        return $this->buildings()->first();
    }
}
