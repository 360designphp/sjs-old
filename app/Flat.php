<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flat extends Model
{


    protected $guarded = ['id', '_token', 'file'];
    public $renovations = ['Да', 'Нет', 'Косметический ремонт', 'Евроремонт', 'Дизайнерский ремонт'];
    public $balconies = ['Нет', 'Балкон', 'Лоджия', 'Больше двух'];
    public $window_views = ['Во двор', 'На улицу', 'Во двор и на улицу'];
    public $bathrooms = ['Совмещенный', 'Раздельный', 'Более 2'];
    public function floor(){
        return $this->belongsTo('App\Floor');
    }

    public function getIntStatus(){
        $status = 0;
        if($this->status != 'free'){
            if($this->status == 'booked'){
                $status = 1;
            } else {
                $status = 2;
            }
        }
        return $status;
    }
    public function getObject(){
        $floor = $this->floor;
        $section = $floor->section;
        $building = $section->building;
        $object = $building->object;
        return $object;
    }

    public function section(){
        $floor = $this->floor;
        $section = $floor->section;
        return $section;
    }

    public function building(){
        $floor = $this->floor;
        $section = $floor->section;
        $building = $section->building;
        return $building;
    }

    public function getStrStatus()
    {
        $statuses = array('free' => 'свободно', 'booked' => 'забронировано', 'sold' => 'продано');
        return $statuses[$this->status];
    }

    public function getFloorSymbol()
    {
        $types = array('living' => 'кв', 'commercial' => 'оф', 'parking' => 'парк');
        return $types[$this->floor->type];
    }

}
