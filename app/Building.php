<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{

    protected $guarded = ['_token', 'id', 'file'];

    public $building_states = ["built" => "Построен, но не сдан", "hand_over" => "сдан в эксплуатацию", "unfinished" => "Строится", "ready" => "Построен и сдан"];

    public $building_types = [ "панельный", "монолитный", "кирпичный", "кирпично-монолитный"];


    public function object(){
        return $this->belongsTo('App\Object');
    }
    public function sections(){
        return $this->hasMany('App\Section');
    }

    public function floors(){
        return $this->hasManyThrough('App\Floor', 'App\Section');
    }

    public function  flats() {

        $sections = $this->sections;
        $flats_array = [];
        foreach($sections as $section) {
            $flats_array[] = $section->flats;
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function  freeFlats() {
        $sections = $this->sections;
        $flats_array = [];
        foreach($sections as $section) {
            $flats_array[] = $section->flats->where('status', 'free');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function  bookedFlats() {
        $sections = $this->sections;
        $flats_array = [];
        foreach($sections as $section) {
            $flats_array[] = $section->flats->where('status', 'booked');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function livingFlats () {
          $sections = $this->sections;
        $flats_array = [];
        foreach($sections as $section) {
            $flats_array[] = $section->livingFlats();
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }

    public function  soldFlats() {
        $sections = $this->sections;
        $flats_array = [];
        foreach($sections as $section) {
            $flats_array[] = $section->flats->where('status', 'sold');
        }
        $flats = collect($flats_array);
        $flats = $flats->collapse();
        return $flats;
    }
    public function getPrice($rooms){
        $price = $this->flats()->where('rooms',$rooms)->min('price');
        return  $price;
    }
}
