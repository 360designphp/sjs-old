<?php

namespace App;

use Barryvdh\Debugbar\Facade;
use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    protected $guarded = ['id', '_token', 'file'];
    public function section(){
        return $this->belongsTo('App\Section');
    }

    public function flats(){
        return $this->hasMany('App\Flat');
    }

    public function freeFlats() {
        return $this->flats->where('status', 'free');
    }

    public function bookedFlats() {
        return $this->flats->where('status', 'booked');
    }

    public function soldFlats() {
        return $this->flats->where('status', 'sold');
    }

    public function building(){
        $section = $this->section;
        $building = $section->building;
        return $building;
    }

    public function object(){
        $section = $this->section;
        $building = $section->building;
        $object = $building->object;
        return $object;
    }
    public function previousFloor(){
        if($this->level == -1){
            return 'minimal';
        }
        elseif($this->level == 0){
            $floor = Floor::where('section_id', $this->section_id)->where('level',-1)->first();
        }
        else {
            $floor = Floor::where('section_id', $this->section_id)->where('level',(int)$this->level- 1)->first();

        }
        if(!is_object($floor)) {
            return 'minimal';
        }
        return $floor->id;
    }

    public function nextFloor(){
        $section = $this->section;
        $max = $section->floors->max('level');
        if($this->level != $max) {
            $floor = Floor::where('section_id', $this->section_id)->where('level',(int)$this->level + 1)->first();
        } else {
            return 'maximal';
        }
        return $floor->id;
    }



}
