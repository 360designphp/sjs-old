<?php

namespace App\Http\Controllers;

use App\Flat;
use App\Menu;

use App\Object;
use App\Section;
use App\User;
use App\Relation;
use App\Taxonomy;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use App\Settings;
use App\Post;
use App\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use DB;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->data = array();
        $this->data['settings'] = Settings::first();
        $this->middleware('auth');
        $this->data['unread'] = Application::where('status', 'unread')->count();
        $this->data['unread_faq'] = Application::where('status', 'unread')->where('type', 'question')->count();
        $this->data['unread_rec'] = Application::where('status', 'unread')->where('type', 'recall')->count();
        $this->data['unread_book'] = Application::where('status', 'unread')->where('type', 'booking')->count();
    }

    //Настройки
    public function settings()
    {
        $this->data['ad_script'] = '/assets/js/ajax/settings_saver.js';

        $this->data['settings'] = Settings::first() == null ? new Settings() : Settings::first();

        return view('layouts.admin.settings', $this->data);
    }

//Заявки на звонок
    public function application($action = null)
    {
        $applications = Application::where('type', 'recall')->where('status', '<>', 'archieved')->orderByRaw('FIELD(status, "unread", "read")')->get();
        if ($action != null) {
            foreach ($applications as $application) {
                $application->status = 'read';
                $application->save();
            }
        }
        $this->data['applications'] = $applications;
        //Костыли
        $this->data['unread'] = Application::where('status', 'unread')->count();
        $this->data['unread_faq'] = Application::where('status', 'unread')->where('type', 'question')->count();
        $this->data['unread_rec'] = 0;
        $this->data['unread_book'] = Application::where('status', 'unread')->where('type', 'booking')->count();
        //Конец костылей
        return view('layouts.admin.pages.applications.recall', $this->data);
    }

    //Заявки на бронирование
    public function application1($action = null)
    {

        $applications = Application::where('type', 'booking')->where('status', '<>', 'archieved')->orderByRaw('FIELD(status, "unread", "read")')->get();
        $this->data['applications'] = $applications;
        //Костыли
        $this->data['unread'] = Application::where('status', 'unread')->count();
        $this->data['unread_faq'] = Application::where('status', 'unread')->where('type', 'question')->count();
        $this->data['unread_rec'] = Application::where('status', 'unread')->where('type', 'recall')->count();
        $this->data['unread_book'] = 0;
        //Конец костылей
        return view('layouts.admin.pages.applications.booking', $this->data);
    }

    //Заявки на вопрос-ответ
    public function application2($action = null)
    {

        $applications = Application::where('type', 'question')->where('status', '<>', 'archieved')->orderByRaw('FIELD(status, "unread", "read")')->get();
        //Костыли
        $this->data['unread'] = Application::where('status', 'unread')->count();
        $this->data['unread_faq'] = 0;
        $this->data['unread_rec'] = Application::where('status', 'unread')->where('type', 'recall')->count();
        $this->data['unread_book'] = Application::where('status', 'unread')->where('type', 'booking')->count();
        //Конец костылей
        $this->data['applications'] = $applications;

        return view('layouts.admin.pages.applications.question', $this->data);
    }

    public function getArchivedApplications()
    {
        $applications = Application::where('status', 'archieved')->get();

        //Костыли
        $this->data['unread'] = Application::where('status', 'unread')->count();
        $this->data['unread_faq'] = Application::where('status', 'unread')->where('type', 'question')->count();
        $this->data['unread_rec'] = Application::where('status', 'unread')->where('type', 'recall')->count();
        $this->data['unread_book'] = Application::where('status', 'unread')->where('type', 'booking')->count();
        //Конец костылей
        $this->data['applications'] = $applications;

        return view('layouts.admin.pages.applications.archive', $this->data);
    }

    //Галерея
    public function gallery()
    {
        $this->data['ad_script'] = '/assets/plugins/shufflejs/jquery.shuffle.min.js';

        return view('layouts.admin.gallery', $this->data);
    }

    //Управление меню
    public function menu()
    {
        $this->data['menu_items'] = Menu::all();
        //$this->data['parent']=DB::select('select id, caption, parent_id from menus');
        $this->data['parent'] = Menu::all();
        $this->data['ad_script'] = '/assets/js/ajax/menu.js';
        return view('layouts.admin.menu', $this->data);
    }

    //Создание/редактирование поста
    public function editPost($type, $id, $parent_id = null)
    {
        $this->data['categories'] = Taxonomy::where('type', 'category')->get();
        $this->data['tags'] = Taxonomy::where('type', 'tag')->get();
        $this->data['type_name'] = '';
        $this->data['parent_type'] = 'post';

        if ($type == 'object_album' && $parent_id != null) {
            $this->data['parent_id'] = $parent_id;
            $this->data['parent_type'] = 'object';
        }
        $this->data['type'] = $type;

        if ($type != 'category') {

            if ($id != 'new') {
                $post = Post::find($id);
                $this->data['post'] = $post;
                $this->data['post_tags'] = $post->taxonomy()->where('type', 'tag')->get()->toArray();
                $this->data['post_categories'] = $post->taxonomy()->where('type', 'category')->get()->toArray();
                $this->data['thumbnail'] = $post->thumbnail();
                $this->data['parent_id'] = $post->parent_id;
                $this->data['parent_type'] = $post->parent_type;
            } else {
                $post = new Post();
                $post->type = $type;
                $post->title = 'Новая запись';
                $post->slug = 'new_page';
                $post->content = '';
                if ($parent_id != null) {
                    $post->parent_id = $parent_id;
                }
                $post->save();
                return redirect('/admin/' . $type . '/edit/' . $post->id);


            }
            $this->data['ad_script'] = '/assets/js/ajax/dropzone.js';
            return view('layouts.admin.editor', $this->data);
        } else {
            if ($id != 'new') {
                $post = Taxonomy::where('slug', $slug)->first();
                $this->data['post'] = $post;

            } else {
                //dd('HEW');
                $post = new Taxonomy();
                $post->type = $type;
                $post->name = 'Новая категория';
                $post->description = '';
                $post->slug = 'new_category';
                $post->save();
                $post->slug = 'new_category_' + $post->id;
                $post->save();
                return redirect('/admin/' . $type . '/edit/' . $post->slug);

            }
            $this->data['ad_script'] = '/assets/js/ajax/cat_list.js';
            return view('layouts.admin.cateditor', $this->data);
        }


    }


    public function archiveApplication($id)
    {
        $application = Application::find($id);
        $application->status = 'archieved';
        $application->save();
        return redirect('/admin/application/' . $application->type . '');
    }

    //Удаление заявки
    public function deleteapp($id, $archive = false)
    {
        $application = Application::find($id);
        $type = $application->type;
        $application->delete();
        if ($archive) {
            return redirect('/admin/application/archive');
        } else {
            return redirect('/admin/application/' . $type);
        }
    }


    //Удаление поста
    public function deletePost($id)
    {
        $post = Post::find($id);
        $type = $post->type;

        if ($post->delete()) {
            return redirect('/admin/' . $type . '/list');
        }
    }

    //Удаление категории
    public function deleteCategory($id)
    {
        $post = Taxonomy::find($id);
        $post->delete();
        return redirect()->to('/admin/category/list');

    }

    //Список постов
    public function postsList(Request $request, $type)
    {

        if ($type == 'category') {
            $this->data['posts'] = Taxonomy::where('type', $type)->get();
            $this->data['Type'] = $type;
            $this->data['ad_script'] = '/assets/js/ajax/cat_list.js';
            return view('layouts/admin/catlist', $this->data);
        } else {
            if ($request->category) {
                $this->data['posts'] = Post::whereHas('taxonomy', function ($query) use ($request) {
                    $query->where('slug', 'LIKE', $request->category);
                })->get();
            } else {
                $this->data['posts'] = Post::where('type', $type)->where('status', '!=', 'deleted')->orderBy('status')->get();
            }

            $this->data['categories'] = Taxonomy::where('type', 'category')->get();
            $this->data['Type'] = $type;


            return view('layouts/admin/list', $this->data);
        }
    }


    public function savePost(Request $request)
    {

        $link = Post::savePost($request);
        return redirect()->to('/admin/' . $request->type . '/edit/' . $link);
    }

    public function saveСategory(Request $request)
    {
        $link = str_slug($request->name);
        Taxonomy::saveTaxonomy($request);
        return redirect()->to('/admin/category/edit/' . $link);
    }

    //Сохранение слайдов/картинок галереи
    public function saveImages(Request $request)
    {

        $final_width_of_image = 360;
        if (isset($_FILES['file'])) {

            if (preg_match('/[.](jpg)|(jpeg)|(gif)|(png)|(JPG)|(JPEG)|(GIF)|(PNG)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                //Вынести в AjaxController!
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/images/";
                $path_to_image_directory = $upDir;
                $test = explode('.', $file);
                $after = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

                $file = str_slug($test[0]);
                $slug = $request->parent_id . "_" . $request->type . "_" . $file . "." . $after;

                ///////////////////////////////////////////////////////////////////////////////
                //Записываем в базу
                $image = new Post();
                $image->slug = $image->content = $slug;
                $image->title = '';
                $image->type = $request->type;
                $image->parent_id = $request->parent_id;
                $image->parent_type = $request->parent_type;
                $image->save();

                $filename = str_slug($test[0]);
                $filename = $request->parent_id . "_" . $request->type . "_" . $filename . "." . $after;
                $source = $_FILES['file']['tmp_name'];
                $target = $path_to_image_directory . $filename;
                move_uploaded_file($source, $target);

                $this->createThumbnail($filename, $upDir, $final_width_of_image);
            } else {
                return false;
            }
        }
    }

    public function userSettings($id)
    {
        $this->data['user'] = User::findOrFail($id);
        return view('layouts.admin.profile_settings', $this->data);
    }

    public function userUpdate($id, Request $request)
    {
        $user = User::find($id);
        $user->updateUser($request);
        return redirect('/admin/user/' . $id);
    }

    public function registerAdmin()
    {
        //if(Auth::user()!= null && Auth::user()->role == 'Admin');


        return view('layouts.admin.reg_form', $this->data);

    }

    public function getClientsList(Request $request, $printable = '')
    {
        $param1 = $param2 = $cont = '';
        $flats = Flat::where('customer_name', '<>', '')->where('customer_name', '<>', '0');
        $description = '';
        if (isset($request->object) && $request->object != 'all') {
            $flats->whereHas('floor', function ($query) use ($request) {
                $query->whereHas('section', function ($query) use ($request) {
                    $query->whereHas('building', function ($query) use ($request) {
                        $query->whereHas('object', function ($query) use ($request) {
                            $query->where('id', $request->object);
                        });
                    });
                });
            });
            $param1 = 'object=' . $request->object;
            $object = Object::find($request->object);
            $description .= 'объект: ' . $object->name;
        }
        if (isset($request->status) && $request->status != 'all') {
            $flats->where('status', $request->status);
            $param2 = 'status=' . $request->status;
            $status = $request == 'booked' ? 'Забронировано' : 'Продано';
            $description .= " статус: $status";
            $this->data['status'] = $request->status;
        }
        if ($param1 != '' && $param2 != '') {
            $cont = '&';
        }

        $this->data['flats'] = $flats->get();
        $this->data['parameters'] = $param1 . $cont . $param2;
        $this->data['objects'] = Object::all();
        $this->data['description'] = $description;
        if (!empty($printable)) {
            return view('layouts.admin.site.clients_printable', $this->data);
        } else {
            return view('layouts.admin.site.clients', $this->data);
        }
    }

    public function createThumbnail($filename, $upDir, $final_width_of_image)
    {

        // require 'config.php'; //Подключаем файл конфигурации
        $path_to_thumbs_directory = "../public/front/thumbnail/";
        if (preg_match('/[.](jpg)|(JPG)$/', $filename)) {
            $im = imagecreatefromjpeg($upDir . $filename);
        } elseif (preg_match('/[.](jpeg)|(JPEG)$/', $filename)) {
            $im = imagecreatefromjpeg($upDir . $filename);
        } elseif (preg_match('/[.](gif)|(GIF)$/', $filename)) {
            $im = imagecreatefromgif($upDir . $filename);
        } elseif (preg_match('/[.](png)|(PNG)$/', $filename)) {
            $im = imagecreatefrompng($upDir . $filename);
        } //Определяем формат изображения
        imageantialias($im, true);
        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $final_width_of_image;
        $ny = floor($oy * ($final_width_of_image / $ox));

        $nm = imagecreatetruecolor($nx, $ny);

        imagecopyresampled($nm, $im, 0, 0, 0, 0, $nx, $ny, $ox, $oy);

        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                die("Возникли проблемы! попробуйте снова!");
            }
        }
        imagejpeg($nm, $path_to_thumbs_directory . $filename);
        // $tn = '<img src="' . $path_to_thumbs_directory . $filename . '" alt="image" />';
        // $tn .= '<br />Поздравляем! Ваше изображение успешно загружено и его миниатюра удачно выполнена. Выше Вы можете просмотреть результат:';
        // echo $tn;
    }//Сжимаем изображение, если есть оишибки, то говорим о них, если их нет, то выводим получившуюся миниатюру

    public function getUsers(Request $request)
    {
        $this->data['users'] = User::all();
        return view('layouts.admin.site.user_list', $this->data);
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/admin/user');
    }
}






