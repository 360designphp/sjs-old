<?php

namespace App\Http\Controllers;

use App\Flat;
use App\Floor;
use App\Object;
use App\Building;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Taxonomy;
use App\Settings;
use App\Menu;

class HomeController extends Controller
{

    public $data = array();

    public function __construct()
    {
        $this->data['top_menu'] = Menu::getMenu('top_menu')->get();
        $this->data['bottom_menu'] = Menu::getMenu('bottom_menu');
        $this->settings = Settings::first();
        $this->data['settings'] = $this->settings;
        $this->data['objects'] = Object::all();
        $this->data['slides'] = Post::where('type', 'slide')->where('title', '<>', 'Новая запись')->get();
        $this->data['regions'] = self::getEnumValues('applications', 'region');
    }

    public function index()
    {
        return view('home');
    }

    public function searchPost(Request $request)
    {
        return view('layouts.front.objects.search');
    }

    public function getFaq()
    {
        $this->data['faqs'] = Post::where('type', 'faq')->get();
        return view('layouts.front.faq', $this->data);
    }

    public function getHomepage($test = null)
    {
        $this->data['homepage'] = true;
        $this->data['news'] = Post::where('type', 'news')->orderBy('created_at', 'desc')->take(6)->get();
        $this->data['press_centre'] = Post::where('type', 'press-centre')->orderBy('created_at', 'desc')->take(6)->get();
        if(is_null($test)) {
            return view('layouts.front.home', $this->data);
        } else {
            return view('layouts.front.test', $this->data); 
        }
    }

    public function getPost($type = 'post', $slug = null)
    {
        $post = Post::where('slug', $slug)->where('status', 'published')->first();

        if($type != 'post') {
            $post = Post::where('slug', $slug)->where('type', $type)->where('status', 'published')->first();
        }
    
        if ($post == null) { 
            return redirect()->to('/error404');
        }

        $this->data['pageTitle'] = $post->title; // . ' | '. $this->data['settings']->site_name;

        if ($post->description != null) $this->data['pageDescription'] = $post->description;
        if ($post->keywords != null) $this->data['pageKeywords'] = $post->keywords;
        $this->data['post'] = $post;
        $tax = $post->taxonomy->first();
        if ($tax != null && $tax->slug == 'obekty') {
            return view('layouts.front.objects.sold', $this->data);
        }
        return view('layouts.front.page', $this->data);
    }

    public function getPostList($type)
    {
        $title = 'Страницы';
        switch ($type) {
            case 'news':
                $title = 'Новости';
                break;
            case 'press-centre':
                $title = 'Пресс-центр';
                break;
            case 'action':
                $title = 'Акции';
                    break;
        }
        $this->data['posts'] = Post::where('type', $type)->where('status', 'published')->orderBy('created_at', 'desc')->get();
        $this->data['pageTitle'] =  $title . ' | ' . $this->data['settings']->site_name;
        $this->data['title'] = $title;
        return view('layouts.front.news_list', $this->data);
    }

    public function getCategory($category)
    {
        $this->data['tax'] = Taxonomy::where('slug', $category)->first();
        if (Taxonomy::where('slug', $category)->count() == 0) {
            return redirect()->to('/error404');
        }
        $this->data['posts'] = Post::whereHas('taxonomy', function ($query) use ($category) {
            $query->where('slug', 'LIKE', $category);
        })->get();
        return view('layouts.front.news_list', $this->data);
    }

    public function getObject($id)
    {
          //Костыль для Коллективной
        if($id == 5) {
        $this->data['object'] = Object::find($id);
        $this->data['buildings'] = $this->data['object']->buildings;
        $this->data['building'] = Building::find(5);
        $this->data['sections'] = Section::where('building_id', 5);
        return view('layouts.front.objects.object_' . $id . '.section', $this->data);
        }

        //Конец костыля
        $this->data['object'] = Object::findOrFail($id);
        $path = 'object_' . $id . '.object';
        return view('layouts.front.objects.' . $path, $this->data);
    }

    public function getSection($object_id, $building_id)
    {

        $this->data['object'] = Object::find($object_id);
        $this->data['buildings'] = $this->data['object']->buildings;
        $this->data['building'] = Building::find($building_id);
        $this->data['sections'] = Section::where('building_id', $building_id);
        return view('layouts.front.objects.object_' . $object_id . '.section', $this->data);
    }

    public function getFloors($object_id, $building_id, $section_id)
    {
        $this->data['object'] = Object::find($object_id);
        $this->data['buildings'] = $this->data['object']->buildings;
        $this->data['building'] = Building::find($building_id);
        $this->data['sections'] = Section::where('building_id', $building_id);
        $this->data['section'] = Section::find($section_id);
        $this->data['livingFloors'] = Floor::where('section_id', $section_id)->where('level', '>=', '1')->get();
        $this->data['zeroFloors'] = Floor::where('section_id', $section_id)->where('level', '0')->get();
        $this->data['minusFloors'] = Floor::where('section_id', $section_id)->where('level', '<=', '-1')->get();
        return view('layouts.front.objects.object_' . $object_id . '.section_' . $section_id, $this->data);
    }

    public function getFloor($object_id, $building_id, $section_id, $floor_id)
    {
        $this->data['floor'] = Floor::find($floor_id);
        $this->data['object_id'] = $object_id;
        $this->data['object'] = Object::find($object_id);
        $this->data['building_id'] = $building_id;
        $this->data['section'] = Section::find($section_id);
        return view('layouts.front.objects.floor', $this->data);
    }

    public function getObjectsList()
    {
        $collection = new Collection();
        $objects = Object::all();
        $pages = Post::whereHas('taxonomy', function ($query) {
            $query->where('name', 'Like', 'Объекты');
        })->get();

        $this->data['objects'] = $collection->merge($objects)->merge($pages);
        return view('layouts.front.objects.list', $this->data);
    }

    public function getSearch()
    {
        return view('layouts.front.objects.search', $this->data);
    }

    public function getBuildingProgressPage($object_id, $slug = null)
    {
        $this->data['object'] = Object::find($object_id);
        if ($slug == null) {
            $queues = DB::table('posts')->where('parent_id', $object_id)->distinct()->pluck('queue');
            $all_queues = array();

            foreach ($queues as $queue){
                if($queue != null) {
                    $all_queues[$queue] = Post::where('type', 'object_album')->where('parent_id', $object_id)->where('queue', $queue)->get();
                }
            }
            $this->data['queues'] = $all_queues;

            return view('layouts.front.objects.progress_list', $this->data);
        } else {
            $this->data['album'] = Post::where('type', 'object_album')->where('slug', $slug)->first();
            return view('layouts.front.objects.progress_page', $this->data);
        }

    }

    public function showReadyList()
    {
        $objects = Object::where('status', 'ready')->get();
        $collection = new Collection();
        $pages = Post::whereHas('taxonomy', function ($query) {
            $query->where('name', 'Like', 'Архив');
        })->where('display', 1)->get();
        $this->data['objects'] = $collection->merge($objects)->merge($pages);
        return view('layouts.front.objects.list', $this->data);
    }

    public function showBuildingList()
    {

        $this->data['pageTitle'] = "Строящиеся дома | Стройжилсервис";
        $this->data['objects'] = Object::where('status', 'building')->get();

        return view('layouts.front.objects.list', $this->data);
    }

    public function showFilteredList(Request $data)
    {
        $flats = Flat::where('status', '<>', 'sold')->where('price', '>=', (int)$data->price_from)->where('price', '<=', (int)$data->price_to);

        if ($data->type == '1') {
            $flats->whereHas('floor', function ($query) use ($data) {
                $query->where('level', '>=', $data->floor_from);
                $query->where('level', '<=', $data->floor_to);
                $query->where('type', 'living');
            });

        }

        if ($data->type == '2') {
            $flats->whereHas('floor', function ($query) use ($data) {
                $query->where('type', 'commercial');
            });
        }

        if ($data->type == '3') {
            $flats->whereHas('floor', function ($query) use ($data) {
                $query->where('type', 'parking');
            });
        }


        if ($data->type != '1') {
            $flats->where('deal', $data->deal);
        }

        if ($data->type != '3') {
            $flats->where('area', '>=', (int)$data->area_from)->where('area', '<=', (int)$data->area_to);
            if ($data->rooms != 'all') {
                $flats->where('rooms', $data->rooms);
            }
        }

        if ($data->object != '') {
            $flats->whereHas('floor', function ($query) use ($data) {
                $query->whereHas('section', function ($query) use ($data) {
                    $query->whereHas('building', function ($query) use ($data) {
                        $query->whereHas('object', function ($query) use ($data) {
                            $query->where('id', $data->object);
                        });
                    });
                });
            });
        }

        $req = clone ($flats);
        if ($data->type == 3 && isset($data->parking_places) && $req->count() < (int)$data->parking_places) {
            $this->data['flats'] = array();
        } else {
            $this->data['flats'] = $flats->get();
        }
        $this->data['param'] = $data->all();
        $this->data['pageTitle'] = "Отфильтрованые квартиры | Стройжилсервис";
        return view('layouts.front.objects.search', $this->data);
    }

    public function getEnumValues($table, $column)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '$column'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

    public function getDocumentsTable($id, $type, $queue = null)
    {
        $documents = Post::where('type', 'document')->where('parent_id', $id)->where('parent_type', $type);
        if(!is_null($queue)) {
            $documents->where('queue', $queue);
        }
        if($type == 'object') {
            $this->data['object'] = Object::find($id);
        }
        $this->data['queue'] = $queue;
        $this->data['documents'] = $documents->get();
        return view('layouts.front.documents_list', $this->data);
    }

}