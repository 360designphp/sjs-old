<?php
/*
Ну ты, Вова, и дурик!
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Object;
use App\Flat;
use App\Floor;
use App\Section;
use App\Post;
use App\Building;
use App\Application;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ObjectController extends Controller
{
     public function __construct()
    {
        $this->data = array();
        $this->data['settings'] = Settings::first();
        $this->data['unread']= Application::where('status', 'unread')->count();
        $this->data['unread_faq']= Application::where('status', 'unread')->where('type','question')->count();
        $this->data['unread_rec']= Application::where('status', 'unread')->where('type','recall')->count();
        $this->data['unread_book']= Application::where('status', 'unread')->where('type','booking')->count();
    }
    
    public function showList(){
        $this->data['objects'] = Object::all();
        return view('layouts/admin/site/buildings', $this->data);
    }

    public function editObject($id){
        if($id == 'new'){
            $this->data['object'] = new Object();
        } else {
            $this->data['object'] = Object::find($id);
        }
        $this->data['ad_script']='/assets/js/ajax/dropzone.js';
        return view('layouts/admin/site/object_editor', $this->data);
    }
    public function editFlat($id, $floor = null){
        if($id == 'new'){
            $flat = new Flat();
            if($floor) {
                $floor = Floor::find($floor);
                $flat->floor_id = $floor->id;
                $this->data['flat'] = $flat;
            }
            $this->data['flat'] = $flat;
        } else {
            $flat = Flat::find($id);
            $this->data['flat'] = $flat;
        }

        //Папки с панорамами войкова - костыль. В голове у тебя костыль!
        $this->data['panoramas'] = ["1a","1a-z","1b","1b-z","1d","1g","1v","1x",
"2a","2a-z","2b","2b-z","2g","2v","2v-z",
"3a","3a-balkon","3a-z","3a-z-balkon","3b","3b-balkon","3b-z","3b-z-balkon",
"3d","3d-balkon","3d-z","3d-z-balkon","3e","3e-balkon",
"3g","3g-balkon","3g-z","3g-z-balkon","3i","3i-balkon",
"3j","3j-balkon","3k","3k-balkon","3v"];
        return view('layouts/admin/site/flat_editor', $this->data);
    }
    public function  saveFlat(Request $request) {
        if($request->id == ""){
            $flat = new Flat();
        } else {
            $flat = Flat::find($request->id);
        }

        $flat->fill($request->all());

        $flat->save();
        if(isset($_FILES['file'])) {

            if(preg_match('/[.](jpg)|(jpeg)|(gif)|(png)|(JPG)|(JPEG)|(GIF)|(PNG)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/flats/";
                $path_to_image_directory=$upDir;
                $test= explode('.', $file);
                $after=$test[1];

                $file=str_slug($test[0]);
                $slug=$flat->id."_".$file;
                $file=$upDir.$flat->id."_".$file.".".$after; 

                ///////////////////////////////////////////////////////////////////////////////
                //Записываем в базу
                $flat->image=$slug.'.'.$after;
                $flat->save();

                $source = $_FILES['file']['tmp_name'];

                move_uploaded_file($source, $file);

                //$this-> createThumbnail($filename,$upDir,$final_width_of_image);
            }
        }


        return redirect()->to('admin/objects/flat/'.$flat->id);
    }
    public function  saveObject(Request $request) {
        if($request->id == ""){
            $object = new Object();
        } else {
            $object = Object::find($request->id);
        }
        $object->fill($request->all());
        $object->save();
        if(isset($_FILES['file'])) {

            if(preg_match('/[.](jpg)|(jpeg)|(gif)|(png)|(JPG)|(JPEG)|(GIF)|(PNG)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/objects/";
                $test= explode('.', $file);
                $after=$test[1];

                $file=str_slug($test[0]);
                $slug=$object->id."_".$file;

                //Записываем в базу
                $object->image=$slug.'.'.$after;
                $object->save();
                Image::make($_FILES['file']['tmp_name'])->save($upDir.  $object->image);
               //Со сжатием Image::make($_FILES['file']['tmp_name'])->fit(2000)->save($upDir.  $object->image);
            }
        }


        return redirect()->to('admin/objects/'.$object->id);
    }
    public function editBuilding($id){
        if($id == 'new'){
            $this->data['building'] = new Building();
        } else {
            $this->data['building'] = Building::find($id);
        }
        return view('layouts/admin/site/building_editor', $this->data);
    }
    public function  saveBuilding(Request $request) {

        if($request->id == ""){
            $building = new Building();
        } else {
            $building = Building::find($request->id);
        }
        $building->fill($request->all());
        $building->save();
        if(isset($_FILES['file'])) {

            if(preg_match('/[.](jpg)|(jpeg)|(gif)|(png)|(JPG)|(JPEG)|(GIF)|(PNG)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/buildings/";
                $path_to_image_directory=$upDir;
                $test= explode('.', $file);
                $after=$test[1];

                $file=str_slug($test[0]);
                $slug=$building->id."_".$file; 
                $file=$upDir.$building->id."_".$file.".".$after;

                ///////////////////////////////////////////////////////////////////////////////
                //Записываем в базу
                $building->image=$slug.'.'.$after;
                $building->save();

                $source = $_FILES['file']['tmp_name'];

                move_uploaded_file($source, $file);
            }
        }
        return redirect()->to('admin/objects/building/'.$building->id);
    }
    public function editSection($id){
        if($id == 'new'){
            $this->data['section'] = new Section();
        } else {
            $this->data['section'] = Section::find($id);
        }
        return view('layouts/admin/site/section_editor', $this->data);
    }
    public function  saveSection(Request $request) {
        if($request->id == ""){
            $section = new Section();
        } else {
            $section = Section::find($request->id);
        }
        $section->number = $request->number;
        $section->symbol = $request->symbol;
        $section->building_id = $request->building_id;
        $section->seo_keywords = $request->seo_keywords;
        $section->seo_description = $request->seo_description;
        $section->save();
        return redirect()->to('admin/objects/section/'.$section->id);
    }
    public function editFloor($id){
        if($id == 'new'){
            $this->data['floor'] = new Floor();
        } else {
            $this->data['floor'] = Floor::find($id);
        }
        return view('layouts/admin/site/floors_edit', $this->data);
    }
    public function  saveFloor(Request $request) {
        if($request->id == ""){
            $floor = new Floor();
        } else {
            $floor = Floor::find($request->id);
        }
        $floor->fill($request->all());
        if($request->image != null) {
            $floor->image = $request->image;
        }
        $floor->save();
        if(isset($_FILES['file'])) {

            if(preg_match('/[.](jpg)|(jpeg)|(gif)|(png)|(JPG)|(JPEG)|(GIF)|(PNG)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/floors/";
                $path_to_image_directory=$upDir;
                $test= explode('.', $file);
                $after= Post::getExtension($_FILES['file']['name']);
                $file=str_slug($test[0]);
                $slug=$floor->id."_".$file;
                $file=$upDir.$floor->id."_".$file.".".$after;

                ///////////////////////////////////////////////////////////////////////////////
                //Записываем в базу
                $floor->image=$slug.'.'.$after;
                $floor->save();

                $source = $_FILES['file']['tmp_name'];

                move_uploaded_file($source, $file);

                //$this-> createThumbnail($filename,$upDir,$final_width_of_image);
            }
        }
        return redirect()->to('admin/objects/floor/'.$floor->id);
    }
    public function saveEntity(Request $request)
    {
        $id = $title = $child = 0;
        switch ($request->type)
        {
            case'Building';
            {
                $building= new Building();
                $building->number=$request->value;
                $building->object_id=$request->parent_id;
                $building->save();
                $id = $building->id;
                $title = 'Корпус №'.$building->number;
                $child = 'Section';
                break;
            }
            case'Section';
            {
                $section= new Section();
                $section->symbol=$request->value;
                $section->building_id=$request->parent_id;
                $section->save();
                $id = $section->id;
                $title = 'Секция '.$section->symbol;
                $child = 'Floor';
                break;
            }
            case'Floor';
            {
                $floor= new Floor();
                $floor->level=$request->value;;
                $floor->section_id=$request->parent_id;
                $floor->save();
                $id = $floor->id;
                $title = $floor->level . ' этаж';
                $child = 'Flat';
                break;
            }

        }
        return response()->json(['id' => $id, 'title' => $title, 'child' => $child]);

    }

    public function updateFlat(Request $request) {
        $flat = Flat::find($request->id);
        $flat->status = $request->status;
        $flat->price = $request->price;
        $flat->area = $request->area;
        $flat->living_area = $request->living_area;
        $flat->kitchen_area = $request->kitchen_area;
        $flat->customer_name = $request->customer_name;
        $flat->customer_phone = $request->customer_phone;
        $flat->date_of_sale = $request->date_of_sale;
        $flat->contract_number = $request->contract_number;

        $flat->save();
    }


    public function bookFlat(Request $request) {
        $flat = Flat::find($request->id);
        $flat->status = $request->status;
        $flat->customer_name = $request->customer_name;
        $flat->customer_phone = $request->customer_phone;
        $flat->save();
    }

    public function saveDocuments(Request $request)
    {
        if(isset($_FILES['file'])) {

            if(preg_match('/[.](jpg)|(jpeg)|(png)|(JPG)|(JPEG)|(PNG)|(pdf)|(PDF)$/', //Ставим допустимые форматы изображений для загрузки
                $_FILES['file']['name'])) {
                //Вынести в AjaxController!
                $file = $_FILES['file']['name'];
                $upDir = "../public/front/documents/";
                $path_to_image_directory=$upDir;
                $test= explode('.', $file);
                $after= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $salt = str_random(4);
                $file=str_slug($test[0]);
                $slug =  $salt. '_' . $request->parent_id."_".$request->type."_".$file.".".$after;

                //Записываем в базу
                $document = new Post();
                $document->slug = $slug;
                $document->content = $after;
                $document->title = $_FILES['file']['name'];
                $document->type = $request->type;
                $document->parent_id = $request->parent_id;
                $document->parent_type = $request->parent_type;
                $document->author_id = 7; //Todo: ИСПРАВИТЬ этот костыль
                $document->save();



                // $filename = $_FILES['file']['name'];
                $filename=str_slug($test[0]);
                $filename= $salt. '_' .$request->parent_id."_".$request->type."_".$filename.".".$after;
                $source = $_FILES['file']['tmp_name'];
                $target = $path_to_image_directory . $filename;
                move_uploaded_file($source, $target);
            }else{
                return false;
            }
    }

}

public function generateFeed() {
    $objects = Object::where('available',1)->get();
    //$output=View::make('feed')->with(compact('objects'))->render();
    return response()
            ->view('feed', compact('objects'), 200)
            ->header('Content-Type', 'text/xml');
}

}

