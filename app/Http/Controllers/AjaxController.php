<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Floor;
use App\Application;
use App\Menu;
use App\Post;
use App\Object;
use App\Settings;
use App\Taxonomy;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index(Request $request)
    {
        switch ($request->intent) {
            case 'saveSettings':

                $this->saveSettings($request);
                break;
            case 'deleteTaxonomy':
                Taxonomy::find($request->id)->delete();
                break;
            case 'addTaxonomy':
                return (Taxonomy::addTaxonomy($request));

                break;
            case 'deleteImage':
                $post = Post::find($request->id);
                $post->deleteImage();
                break;
            case 'addMenuItem':
                $menu = new Menu();
                $menu->save();
                return $menu->id;
                break;
            case 'deleteMenuItem':
                Menu::find($request->id)->delete();
                break;
            case 'updateMenuItem':
                $menu = Menu::find($request->id);
                $menu->updateItem($request);
                break;
            case 'saveImageTitle':
                $this->updateImageTitle($request);
                break;
            case'deleteObjectImage':
                $this->deleteObjectImage($request->id);
                break;
            case'updateDocument':
                $this->updateDocument($request);
                break;
            case'deleteDocument':
                $this->deleteDocument($request->id);
                break;
            case 'sendMessage':
                $to = 'szs-kislovodsk@yandex.ru';
                $subject = 'Стройжилсервис | ' . $request->subject;
                $message = 'Новое сообщение от пользователя ' . $request->name . '
                ' . $request->text . '
                E-mail для обратной связи: ' . $request->email;
                mail($to, $subject, $message);
                break;
        }
    }

    public function saveSettings(Request $request)
    {
        $settings = Settings::first();
        if (!$settings) {
            $settings = new Settings();
        }
        $settings->site_name = $request->site_name;
        $settings->keywords = $request->keywords;
        $settings->description = $request->description;
        $settings->email = $request->email;
        $settings->scripts = $request->scripts;
        $settings->header_scripts = $request->header_scripts;


        if ($settings->update()) {
            echo 'Настройки успешно сохранены';
        } else {
            echo 'Возникла ошибка при сохранении настроек';
        }
    }

    public function updateImageTitle(Request $request)
    {
        $image = Post::find($request->id);
        $image->title = $request->title;
        $image->update();
    }

    public function deleteObjectImage($id)
    {
        $object = Object::find($id);
        $upDir = "../public/front/objects/";

        @unlink($upDir . $object->image);
        $object->image = null;
        $object->save();
    }

    public function deleteFloorImage($id)
    {
        $floor = Floor::find($id);
        $upDir = "../public/front/floors/";

        @unlink($upDir . $floor->image);
        $floor->image = null;
        $floor->save();
    }

     public function updateDocument(Request $request)
    {
        $document = Post::find($request->id);
        $document->title = $request->title;
        $document->queue = $request->queue;
        $document->order = $request->order;
        $document->created_at = $request->created_at;

        $document->save();
    }


    public function deleteDocument($id)
    {
        $document = Post::find($id);
        $upDir = "../public/front/documents/";

        @unlink($upDir . $document->slug);
        $document->delete();
    }

    public function changeReadStatus(Request $request) {
        $appl = Application::find($request->id);
        if($appl->status == 'read') {
            $appl->status = 'unread';
        } elseif($appl->status == 'unread') {
            $appl->status = 'read';
        }
        $appl->save();
        return $appl->status;
    }

    public function storeNote(Request $request)
    {
        $application = Application::find($request->id);
        $application->note = $request->note;
        if($application->save()) {
            return 'ok';
        } else {
            return 'error';
        }
    }

}
