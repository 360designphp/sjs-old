<?php

namespace App\Http\Controllers;

use App\Flat;
use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Application;
use Log;

class RequestController extends Controller
{
    public function recallRequest(Request $request)
    {
        $application = new Application($request->all());
        $application->save();

            $this->sendNotification($request);
        return $request->all();
    }

    public function sendNotification(Request $request){
        $settings = Settings::first();
        $headers = "Content-Type: text/html; charset=UTF-8\r\n";
        $title = $text = '';
        $to = $settings->email;
        if($request->flat_id != null) {
            $flat = Flat::find($request->flat_id);
            $object = $flat->getObject();
            $title = "Заявка на бронь в объекте   $object->name ";
            $text = 'Поступила заявка на бронь квартиры №' . $flat->number . ' на объекте &laquo; ' . $object->name . ' &raquo; <br><br><a href="' . env('APP_URL') . 'admin/objects/flat/' . $flat->id . '" target="_blank">Перейти к квартире</a>';
        }
        if($request->type == 'recall') {
            $title = 'Новый запрос обратного звонка';
            $text = 'Пользователь ' . $request->name . ' просит перезвонить ему по номеру ' . $request->phone. '(регион: '.$request->region.')<br><br><a href="' . env('APP_URL') . 'admin/application/recall" target="_blank">Перейти к списку запросов</a>';
        }
        if($request->type == 'question') {
            $title = 'Новый вопрос на сайте sjs.su';
            $text = 'Пользователь ' . $request->name . ' задал следующий вопрос:<br>' . $request->text. ' <br><br> <a href="' . env('APP_URL') . 'admin/application/question" target="_blank">Перейти к списку вопросов</a>';
        }
        mail($to,$title,$text, $headers);
    }
}
