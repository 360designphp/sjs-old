<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;


class Post extends Model
{
    protected $softDelete = true;

    //Возвращает категории и теги к которым относится пост
    public function taxonomy()
    {
        return $this->belongsToMany('App\Taxonomy');
    }

    //Возвращает дочерние посты
    public function childs()
    {
        return $this->where('parent_id', $this->id)->where('parent_type', 'post');
    }

    //Возвращает родительский пост
    public function parent()
    {
        return $this->find($this->parent_id);
    }

    //Получаем миниатюру изображения
    public function thumbnail()
    {
        return $this->childs()->where('type', 'thumbnail')->first();
    }

    //получаем все слайды поста
    public function slides()
    {
        return $this->childs()->where('type', 'slide')->get();
    }

    //Получаем все изображения для галереи поста
    public function gallery()
    {
        return $this->childs()->where('type', 'gallery')->get();
    }

    //Получаем все документы страницы
    public function documents()
    {
        return $this->childs()->where('type', 'document')->get();
    }

    //Сохраняем пост
    public static function savePost(Request $request)
    {

        if ($request->id == '') {
            $post = new Post();
        } else {
            $post = Post::find($request->id);
        }
        $post->type = $request->type;
        $post->title = htmlentities($request->title);
        if (strripos($request->slug, '/')) {
            $post->slug = $request->slug;
        } else {
            $post->slug = str_slug($request->slug);
        }
        $post->content = $request->text;
        $post->additional = $request->additional;

        $post->badge_text = $request->badge_text;
        $post->badge_year = $request->badge_year;

        $post->queue = $request->queue;

        $post->parent_id = $request->parent_id;
        $post->seo_description = $request->description;
        $post->seo_keywords = $request->keywords;
        $post->status = 'published';
        if ($request->display) {
            $post->display = true;
        } else {
            $post->display = false;
        }
        if (isset($request->taxonomy)) {
            $post->taxonomy()->sync($request->taxonomy);
        }
        $post->save();
        //Создаем/изменяем пост миниатюры
        if(!empty($_FILES['thumbnail']['tmp_name'])){
            $thumb = $post->childs()->where('type','thumbnail')->first();
            $save = false;
            if($thumb == ''){
                $thumb = new Post();
                $save = true;
            }
            $filename = $_FILES['thumbnail']['name'];
            $test = explode('.', $filename);
            $after = $after= pathinfo($filename, PATHINFO_EXTENSION);

            $thumbslug=$post->id."_thumbnail_".$test[0].".".$after;

            $upDir = "../public/front/images/";
            $filename = $upDir.$post->id."_thumbnail_".$test[0].".".$after;
            $file = $post->id."_thumbnail_".$test[0].".".$after;
            if($thumb->content != ''){
                @ unlink($upDir . $thumb->content);
            }
            @copy($_FILES['thumbnail']['tmp_name'],$filename);
            $thumb->createThumbnail($file);
            $thumb->content = ($_FILES['thumbnail']['name']);
            $thumb->type = 'thumbnail';
            $thumb->title = $_FILES['thumbnail']['name'] + $post->id;
            $thumb->slug = $thumbslug;
            $thumb->parent_id = $post->id;
            $thumb->parent_type = 'post';
                $thumb->save();

        }
        return $post->id;
    }

    //
    public function deleteImage()
    {
        $upDir = "../public/front/images/";
        $upDirTH = "../public/front/thumbnail/";
        $filename = $this->slug;
        $this->delete();
        // dd($upDir.$filename);
        //unlink($upDir.$filename);
        @unlink($upDir . $filename);
        @unlink($upDirTH . $filename);

    }

    public function createThumbnail($filenameTH)
    {

        $path_to_thumbs_directory = "../public/front/thumbnail/";

        if (!file_exists($path_to_thumbs_directory)) {
            if (!mkdir($path_to_thumbs_directory)) {
                die("Возникли проблемы! попробуйте снова!");
            }
        }
        Image::make($_FILES['thumbnail']['tmp_name'])->fit(550, 400)->save($path_to_thumbs_directory . $filenameTH);

    }//Сжимаем изображение, если есть оишибки, то говорим о них, если их нет, то выводим получившуюся миниатюру

    public function getAuthor()
    {
        return User::find($this->author_id);
    }

    static function getExtension($filename)
    {
        $array = explode(".", $filename);
        return end($array);
    }

    public function getType() {
        $type = 'запись';
        switch ($this->type) {
            case 'page':
                $type = 'страница';
                break;
            case 'news':
                $type = 'новость';
                break;
            case 'action':
                $type = 'акция';
                break;
            case 'faq':
                $type = 'вопрос-ответ';
                break;
            case 'press-centre':
                $type = 'публикация';
                break;   
        }
        return $type;
    }

    public function getUrl() {
        $url = '/';
        switch ($this->type) {
            case 'building-page':
                $url = "/objects/building_progress/{$this->parent_id}/{$this->slug}";
                break;
            case 'press-centre':
                $url = "/press-centre/{$this->id}";
                break;
            case 'faq':
                $url = '/faq';
                break;
            default:
                $url = "/{$this->type}/{$this->slug}";
            break;    
        }
        return $url;
    }
}
