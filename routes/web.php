<?php
use App\Settings;
use App\Post;
use App\Relation;

// 301 Редиректы
Route::get('/about/news/', function(){
    return Redirect::to('/press-centre', 301);
});

Route::get('/about/news/myi_na_sayte_trk_stavropole.html', function(){
    return Redirect::to('/press-centre/my-na-sayte-trk-stavropole', 301);
});

Route::get('/about/news/kislovodskaya_gazeta.html', function(){
    return Redirect::to('/press-centre/kislovodskaya-gazeta', 301);
});

Route::get('/object/', function(){
    return Redirect::to('/objects', 301);
});

Route::get('/kupit_nedvijimost/', function(){
    return Redirect::to('/page/usloviya-pokupki', 301);
});

Route::get('/kupit_nedvijimost/usloviya_pokupki', function(){
    return Redirect::to('/page/usloviya-pokupki', 301);
});

Route::get('/kupit_nedvijimost/uslugi_yurista', function(){
    return Redirect::to('/page/usloviya-pokupki', 301);
});

Route::get('/about/history', function(){
    return Redirect::to('/page/o-kompanii', 301);
});

Route::get('/about/stroitelno-proizvodstvennaya_baza/', function(){
    return Redirect::to('/page/o-kompanii', 301);
});

Route::get('/contacts/', function(){
    return Redirect::to('/page/contacts', 301);
});

Route::get('/about/honors', function(){
    return Redirect::to('/page/nagrady', 301);
});

Route::get('/about/team', function(){
    return Redirect::to('/page/komanda-sjs', 301);
});

Route::get('/about/vacancy', function(){
    return Redirect::to('/page/vakansii', 301);
});

Route::get('/kupit_nedvijimost/{var1}/{var2?}/{var3}', function(){
    return Redirect::to('/', 301);
});


Route::get('/about/uslugi/{var}', function(){
    return Redirect::to('/', 301);
});

Route::get('/feed.xml', 'ObjectController@generateFeed');

Route::group(['prefix' => 'objects'], function () {
    Route::get('/', ['uses' => 'HomeController@getObjectsList']);
    Route::get('/building_progress/{object_id}/{slug?}', ['uses' => 'HomeController@getBuildingProgressPage']);

    Route::get('/ready', ['uses' => 'HomeController@showReadyList']);
    Route::get('/building', ['uses' => 'HomeController@showBuildingList']);
    Route::get('/filtered', ['uses' => 'HomeController@showFilteredList']);
    Route::get('/{object_id}', ['uses' => 'HomeController@getObject']);
    Route::get('/{object_id}/{building_id}', ['uses' => 'HomeController@getSection']);
    Route::get('/{object_id}/{building_id}/{section_id}', ['uses' => 'HomeController@getFloors']);
    Route::get('/{object_id}/{building_id}/{section_id}/{floor_id}', ['uses' => 'HomeController@getFloor']);

});

Route::get('/', ['uses' => 'HomeController@getHomepage', 'as' => 'main']);

Route::get('/documents/{id}/{type}/{queue?}', ['uses' => 'HomeController@getDocumentsTable']);

//Галерея
Route::get('/gallery', function () {
    return view('layouts.front.news_list');
});
Route::get('/section_b', function () {
    return view('layouts.front.objects.object_2.section_b');
});
Route::get('/logout', function () {
    Auth::logout();
    return view('auth/login');
});
//Список страниц
Route::get('/pages', function () {
    return view('layouts.front.news_list');
});
// Faq
Route::get('/faq', ['uses'=> 'HomeController@getFaq']);

//Поиск
Route::any('/search', ['uses' => 'HomeController@searchPost']);

//Список категорий
Route::get('/category/{category}', ['uses' => 'HomeController@getCategory']);
Auth::routes();

Route::get('/home', 'HomeController@index');

/*---------РОУТЫ АДМИН-ПАНЕЛИ------*/


//Todo: Вынести в контроллер
//Route::get('/registeradmin', ['uses' => 'AdminController@registerAdmin']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
// Главная страница панели
    Route::get('/', function () {
        return redirect()->to('/admin/objects');
    });
//Просмотр заявок на звонок
    Route::get('/application/recall/{action?}', ['uses' => 'AdminController@application']);
    //Просмотр заявок на бронь
    Route::get('/application/booking/{action?}', ['uses' => 'AdminController@application1']);
    //Просмотр вопросов
    Route::get('/application/question/{action?}', ['uses' => 'AdminController@application2']);
    //Просмотр архивированных заявок
    Route::get('/application/archive', ['uses' => 'AdminController@getArchivedApplications']);



//Создание/редактирование поста
    Route::get('/{type}/edit/{id}/{parent_id?}', ['uses' => 'AdminController@editPost']);

//Регистрация пользователя
    Route::get('/register', ['uses' => 'AdminController@registerAdmin']);

//Список пользователей
    Route::get('/user', ['uses' => 'AdminController@getUsers']);

// Настройки пользователя
    Route::get('/user/{id}', ['uses' => 'AdminController@userSettings']);

//Сохраняет настройки авторизованного пользователя
    Route::post('/update_user/{id}', ['uses' => 'AdminController@userUpdate']);

    Route::get('/delete_user/{id}', ['uses' => 'AdminController@deleteUser']);

    Route::get('/clients/{printable?}', ['uses' => 'AdminController@getClientsList']);

//Список постов без постов-фотографий
    Route::get('/{type}/list', ['uses' => 'AdminController@postsList']);

// Удаление категории
    Route::get('/delete_category/{id}', ['uses' => 'AdminController@deleteCategory']);

//Управление меню
    Route::get('/menu', ['uses' => 'AdminController@menu']);

//Список постов-фотографий
    Route::get('/gallery', ['uses' => 'AdminController@gallery']);

//Сохранение постов
    Route::post('/save_post', ['uses' => 'AdminController@savePost']);

//Сохранение категории	
    Route::post('/save_category', ['uses' => 'AdminController@saveСategory']);


//Редактирование настроек
    Route::get('/settings', ['uses' => 'AdminController@settings']);

    //Сохранение изображений (удалить?)
    Route::post('/save_images', ['uses' => 'AdminController@saveImages']);

    Route::post('/save_documents', ['uses' => 'ObjectController@saveDocuments']);

    //Список объектов
    Route::group(['prefix' => 'objects'], function () {
        Route::get('/', ['uses' => 'ObjectController@showList']);
        //Редактор объектоа
        Route::get('/{id}', ['uses' => 'ObjectController@editObject']);

        //Сохранение объекта
        Route::post('/save', ['uses' => 'ObjectController@saveObject']);

        //Редактор квартиры
        Route::get('/flat/{id}/{floor?}', ['uses' => 'ObjectController@editFlat']);

        //Сохранение квартиры
        Route::post('/flat/save', ['uses' => 'ObjectController@saveFlat']);

        //Редактор корпуса
        Route::get('/building/{id}', ['uses' => 'ObjectController@editBuilding']);

        //Сохранение корпуса
        Route::post('/building/save', ['uses' => 'ObjectController@saveBuilding']);
        //Редактор этажа
        Route::get('/floor/{id}', ['uses' => 'ObjectController@editFloor']);

        //Сохранение этажа
        Route::post('/floor/save', ['uses' => 'ObjectController@saveFloor']);
        //Редактор секции
        Route::get('/section/{id}', ['uses' => 'ObjectController@editSection']);

        //Сохранение секции
        Route::post('/section/save', ['uses' => 'ObjectController@saveSection']);
    });
    

    Route::get('/archive_application/{id}', ['uses' => 'AdminController@archiveApplication']);

    Route::get('/deletePost/{id}', ['uses' => 'AdminController@deletePost']);
    Route::get('/deleteapp/{id}/{archive?}', ['uses' => 'AdminController@deleteapp']);
    Route::get('/{type}/publishPost/{id}', ['uses' => 'AdminController@publishPostPage']);
    Route::post('/save_entity', ['uses' => 'ObjectController@saveEntity']);
});
//Роуты фронта, вынесены сюда чтобы не блокировать админку
Route::get('/{type}/{slug}', ['uses' => 'HomeController@getPost']);
Route::get('/{type}', ['uses' => 'HomeController@getPostList']);
