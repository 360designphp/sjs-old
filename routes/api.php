<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('/ajax', ['uses' => 'AjaxController@index']);
Route::post('/recall_request', ['uses' => 'RequestController@recallRequest']);
Route::post('/update_flat', ['uses' => 'ObjectController@updateFlat']);
Route::post('/book_flat', ['uses' => 'ObjectController@bookFlat']);
Route::post('/change_read_status', ['uses' => 'AjaxController@changeReadStatus']);
Route::post('/store_note', ['uses' => 'AjaxController@storeNote']);