<?php if(count($object->gallery())): ?>
    <div class="col-xs-12">
        <h2>Галерея объекта</h2>
        <div class="vertical-devider"></div>
        <div class='main_text_slick'>
            <?php $__currentLoopData = $object->gallery(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div>
                    <a class="main_text_slick_image_link" href="/front/images/<?php echo e($image->slug); ?>" data-lightbox="Галерея объекта" data-title="<?php echo e($image->title); ?>"><img class="img-responsive" src="/front/thumbnail/<?php echo e($image->slug); ?>" alt=""/>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
    </div>
    <div class="vertical-devider"></div>
<?php endif; ?>