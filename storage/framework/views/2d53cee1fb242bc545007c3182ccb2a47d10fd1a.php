<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <h2 class="text-center"><?php echo e($title); ?></h2>
        <div class="small-vertical-devider"></div>
        <div class="row searches_list">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="col-xs-6 col-sm-4 search_item">
                    <div target="_blank" class="search_item_inner_wrap clearfix">
                        <div class="search_item_inner_top">
                            <div class="news-grid-title">
                                <?php if (strlen($post->title) <= 90) {
                                    echo $post->title;
                                          } else {
                                    echo substr($post->title, 0, 90) . "...";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="search_item_inner_img">
                            <?php if(is_object($post->thumbnail())): ?>
                                <img src="/front/thumbnail/<?php echo e($post->thumbnail()->slug); ?>" alt="" class="" onerror="this.src = '/front/img/21455132o.jpg'">
                            <?php else: ?>
                                <img src="/front/img/21455132o.jpg" class="img-responsive">
                            <?php endif; ?>
                        </div>
                        <a href="/<?php echo e($post->type); ?>/<?php echo e($post->slug); ?>" class="search_item_inner_bottom">
                            Подробнее <span class="shev_right"></span>
                        </a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
        <div class="vertical-devider"></div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>