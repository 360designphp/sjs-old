<?= '<?xml version="1.0" encoding="UTF-8"?>'?>
<complexes>
    <?php $__currentLoopData = $objects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <complex>
        <id><?php echo e($object->domclick_id); ?></id>
        <name><?php echo e($object->name); ?></name>
        <latitude><?php echo e($object->latitude); ?></latitude>
        <longitude><?php echo e($object->longitude); ?></longitude>
        <address><?php echo e($object->address); ?></address>
         <images>
            <image>http://sjs.su/front/objects/<?php echo e($object->image); ?></image>
        </images>
        <description_main>
            <title><?php echo e($object->name); ?></title>
            <text> <?php echo e(strip_tags(preg_replace("/&#?[a-z0-9]+;/i"," ",str_ireplace("&nbsp;", ' ',$object->description )) )); ?></text>
        </description_main>
        <!-- description_secondary -->
        <infrastructure>
            <!-- parking -->
            <security><?php echo e($object->security); ?></security>
            <sports_ground><?php echo e($object->sports_ground); ?></sports_ground>
            <playground><?php echo e($object->playground); ?></playground>
            <school><?php echo e($object->school); ?></school>
            <kindergarten><?php echo e($object->kindergarten); ?></kindergarten>
            <fenced_area><?php echo e($object->fenced_area); ?></fenced_area>
        </infrastructure>
        <!-- videos-->
        <profits_main>
            <profit_main>
                <title><?php echo e($object->name); ?></title>
                <text><?php echo e($object->utp); ?></text>
                <image>http://sjs.su/front/objects/<?php echo e($object->image); ?></image>
            </profit_main>
        </profits_main>
        <!-- profits_secondary -->
        <buildings>
            <?php $__currentLoopData = $object->buildings->where('available', 1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $building): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <building>
                <id><?php echo e($building->domclick_id); ?></id>
                <fz_214><?php echo e($building->fz_214); ?></fz_214>
                <name><?php echo e($building->name); ?></name>
                <floors><?php echo e($building->floors->count()); ?></floors>
                <?php //<floors_ready>{{$building->floors->where('ready_status', 1)->count()}}</floors_ready>?>
                <building_state><?php echo e($building->building_state); ?></building_state>
                <built_year><?php echo e($building->built_year); ?></built_year>
                <ready_quarter><?php echo e($building->ready_quarter); ?></ready_quarter>
				<?php if(!empty($building->building_phase)): ?>
                <building_phase><?php echo e($building->building_phase); ?></building_phase>
                <?php endif; ?>
                <building_type><?php echo e($building->building_type); ?></building_type>
                <!-- image -->
                <flats>
                    <?php $__currentLoopData = $building->livingFlats(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?> 
                    <flat>
                        <flat_id><?php echo e($flat->id); ?></flat_id> 
                        <apartment><?php echo e($flat->number); ?></apartment>
                        <floor><?php echo e($flat->floor->level); ?></floor>
                        <room><?php echo e($flat->rooms); ?></room>
                        <plan>http://sjs.su/front/flats/<?php echo e($flat->image); ?></plan> 

                        <balcony><?php echo e($flat->balcony); ?></balcony>
                        <renovation><?php echo e($flat->renovation); ?></renovation>

                        <price><?php echo e($flat->price); ?></price>
                        <area><?php echo e($flat->area); ?></area>
                        <?php if(!empty($flat->kitchen_area)): ?>
                        <kitchen_area><?php echo e($flat->kitchen_area); ?></kitchen_area>
                        <?php endif; ?>
						<?php if(!empty($flat->living_area)): ?>
                        <living_area><?php echo e($flat->living_area); ?></living_area>
						<?php endif; ?>
                        <window_view><?php echo e($flat->window_view); ?></window_view>
                        <bathroom><?php echo e($flat->bathroom); ?></bathroom>
                        <!-- rooms_area -->
                    </flat>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </flats>
            </building>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </buildings>
       <!-- discounts-->
        <sales_info>
            <sales_phone>+7 (928) 818-50-50</sales_phone>
            <responsible_officer_phone>+7 (928) 818-50-50</responsible_officer_phone>
            <sales_address>ул.Героев Медиков, 56 / Зашкольный пер., 3, Кисловодск, Ставропольский край</sales_address>
            <sales_latitude>43.918707</sales_latitude>
            <sales_longitude>42.712241</sales_longitude>
            <timezone>+3</timezone>
            <work_days>
			    <work_day>
			      <day>пн</day>
			      <open_at>08:00</open_at>
			      <close_at>17:00</close_at>
			    </work_day>
			    <work_day>
                  <day>вт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>ср</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>чт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
                <work_day>
                  <day>пт</day>
                  <open_at>08:00</open_at>
                  <close_at>17:00</close_at>
                </work_day>
		  </work_days>
        </sales_info>
        <developer>
            <id>243118</id>
            <name>ООО «Стройжилсервис»</name>
            <phone>+7 (800) 550-33-85</phone>
            <site>http://sjs.su/</site>
            <logo>http://sjs.su/front/img/feed_logo.png</logo>
        </developer>
    </complex>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</complexes>