<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/front/img/favicon.ico" type="image/x-icon">
    <meta name="description"
    content="<?php if(isset($pageDescription)): ?><?php echo $pageDescription; ?><?php else: ?> <?php if(isset($settings)): ?><?php echo $settings->description; ?><?php endif; ?> <?php endif; ?> ">
    <meta name="keywords"
    content="<?php if(isset($pageKeywords)): ?><?php echo $pageKeywords; ?><?php else: ?> <?php if(isset($settings)): ?><?php echo $settings->keywords; ?><?php endif; ?> <?php endif; ?> ">
    <title><?php if(isset($pageTitle)): ?><?php echo $pageTitle; ?><?php else: ?> <?php if(isset($settings)): ?><?php echo $settings->site_name; ?><?php endif; ?> <?php endif; ?> </title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700&amp;subset=cyrillic"
    rel="stylesheet">
    <link rel="stylesheet" href="/front/css/main.min.css">
    <link rel="stylesheet" href="/front/css/volodya.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php if(isset($settings)): ?><?php echo $settings->header_scripts; ?><?php endif; ?>
    
</head>
<body>

    <div class="modal fade" id="modal-mobile-menu">
        <div class="mobile-modal-content modal-content">
            <button type="button" class="btn btn-default btn_kvart close-mobile" data-dismiss="modal">╳</button>
            

            <div class="mobtitle-wrap"> 
                <div class="wrap-mobile-logo">

                    <a href="/"><img src="/front/img/sjs_logo.svg" class="mobile-logo"></a>
                </div>
                <div class="mobile-title-text"> 
                    <h1 class="he-mobile">Стройжилсервис</h1>
                    <p>На рынке строительства с 1993 года.</p>
                </div>

                <div class="mobile-phone">          
                    <a href="tel:+78793772029">+7 (87937) 7-20-29</a>
                    <a href="tel:+79288185050">+7 (928) 818-50-50</a>
                    <a href="tel:+78005503385">+7 (800) 550-33-85</a> 
                </div>

                <a data-toggle="modal" href="#modal_callus" class="mobile-modal">Заказать звонок</a>

                <nav class="mobile-menu">
                 <ul>
                    <li><a href="/"  title="Главная"
                     class="main_left_link">Главная</a></li>
                     <li><a href="/faq"  title="Вопросы и ответы"
                         class="main_left_link">Вопросы и ответы</a></li>
                         <li><a href="/page/o-kompanii"  title="О компании"
                             class="main_left_link">О компании</a></li>
                             <li><a href="/page/contacts"  title="Контакты"
                                 class="main_left_link">Контакты</a></li>
                                 <li><a href="/objects"  title="Все объекты с 1993 года"
                                     class="main_left_link">Все объекты с 1993 года</a></li>
                                     <li><a href="/page/hot-offer"  title="Горячее предложение"
                                         class="main_left_link">Горячее предложение</a></li>
                                         <li><a href="/page/usloviya-pokupki"  title="Как купить квартиру?"
                                             class="main_left_link">Как купить квартиру?</a></li>
                                         </ul>
                                     </nav>
                                     <li class="mobile-address">
                                        г.&nbsp;Кисловодск, пер.&nbsp;Зашкольный, д.&nbsp;3
                                        <br>Телефон: +7(87937)&nbsp;7-64-32
                                        <br>E-email: <a href="mailto:szs-kislovodsk@mail.ru">szs-kislovodsk@mail.ru</a>
                                        <div class="vertical-devider"></div>
                                    </li>
                                </div>
    
    

                            </div>

                        </div>


                        <div class="modal fade" id="modal_callus">
                            <div class="modal-dialog modal-lg modal_callme">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title text-center">Оставьте заявку, и мы вам перезвоним</h4>
                                        <small>Поля, обозначенные звездочкой (<span class="red">*</span>), обязательны для заполнения</small>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="input_vash_tel">Ваш телефон <span class="red">*</span></label><input type="text"
                                                name="vash_tel"
                                                id="input_vash_tel"
                                                class="form-control recall_phone"
                                                required="required"
                                                title=""
                                                placeholder="+7 (123) 456-78-90">
                                                <label for="input_vash_name">Как к вам обращаться <span class="red">*</span></label><input
                                                type="text" name="vash_name" id="input_vash_name" class="form-control recall_name"
                                                required="required" title="">
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="vash_email">Ваш e-mail адрес <span class="red">*</span></label><input type="email" name="vash_email"
                                                id="input_vash_name"
                                                class="form-control recall_email"
                                                required="required" title="">
                                                <?php if(isset($regions)): ?>
                                                <label for="modal_callus_region">Ваш регион <span class="red">*</span></label>
                                                <select id="modal_callus_region"
                                                class="selectpicker form-control input_region recall_region"
                                                data-live-search="true">
                                                <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <option><?php echo $val; ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </select>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="vertical-devider"></div>
                                            Примечание
                                            <textarea name="vash_text" id="inputvash_text" class="form-control recall_questions" rows="3"
                                            required="required"></textarea>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="checkbox_input recall_approve" value="on">
                                                    <span class="red">*</span> Я согласен на обработку моих персональных данных
                                                </label>
                                                <a href="/page/legal" target="_blank">Подробнее...</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer text-center">

                                    <button type="button" class="btn  btn-primary" onclick="requestRecall()">Перезвоните мне</button>
                                    <button type="button" class="btn btn-default btn_kvart" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo $__env->yieldContent('modal'); ?>
                    <div id="wrapper">
                        <div class="mobile-button"><a data-toggle="modal" href="#modal-mobile-menu">&#9776</a></div>
                        <div class="container-fluid">
                            <div class="row">
                                <aside class="col-xs-6 col-sm-2 padlr0">
                                    <nav>
                                        <ul class="list-unstyled main_left_list">

                                            <li>
                                                <div class="small-vertical-devider"></div>
                                                <header>
                                                    <a href="/"><img src="/front/img/sjs_logo.svg" class="center-block"></a>
                                                    <h1 class="he">Стройжилсервис</h1>
                                                    <small>На рынке строительства<br class="hidden-lg"> с 1993 года.</small>
                                                </header>
                                            </li>

                                            <li>
                                                <ul class="main_left_phones list-unstyled">
                                                    <li class="small-vertical-devider"></li>
                                                    <li>+7 (87937) 7-20-29</li>
                                                    <li>+7 (928) 818-50-50</li>
                                                    <li>+7 (800) 550-33-85</li>
                                                    <li><small>Звонок по России бесплатный</small></li>
                                                    <li class="small-vertical-devider"></li>
                                                    <li><a data-toggle="modal" href="#modal_callus" class="btn btn-primary modal_callus_btn"><img src="/front/img/phone.png" width="20">&nbsp;<span>Заказать звонок</span></a></li>


                                                    <li class="small-vertical-devider"></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <?php if(isset($top_menu)): ?>
                                                <ul class="list-unstyled main_left_links_list">
                                                    <?php $__currentLoopData = $top_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <li><a href="<?php echo $menu->link; ?>" <?php if($menu->blank == 1): ?>target="_blank"
                                                     <?php endif; ?> title="<?php echo $menu->caption; ?>"
                                                     class="main_left_link"><?php echo $menu->caption; ?></a></li>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> 
                                                 </ul>
                                                 <?php else: ?>
                                                 <ul class="list-unstyled main_left_links_list">
                                                    <li><a href="/"  title="Главная"
                                                     class="main_left_link">Главная</a></li>
                                                     <li><a href="/faq"  title="Вопросы и ответы"
                                                         class="main_left_link">Вопросы и ответы</a></li>
                                                         <li><a href="/page/o-kompanii"  title="О компании"
                                                             class="main_left_link">О компании</a></li>
                                                             <li><a href="/page/contacts"  title="Контакты"
                                                                 class="main_left_link">Контакты</a></li>
                                                                 <li><a href="/objects"  title="Все объекты с 1993 года"
                                                                     class="main_left_link">Все объекты с 1993 года</a></li>
                                                                     <li><a href="/page/hot-offer"  title="Горячее предложение"
                                                                         class="main_left_link">Горячее предложение</a></li>
                                                                         <li><a href="/page/usloviya-pokupki"  title="Как купить квартиру?"
                                                                             class="main_left_link">Как купить квартиру?</a></li>
                                                                         </ul>
                                                                         <?php endif; ?>
                                                                         
                                                                     </li>
                                                                     <li>
                                                                     	<div class="vertical-devider"></div>
								                                        <a href="https://vk.com/sjs_kislovodsk" target="_blank" class="soc_icon_a">
								                                            <img src="/front/img/_soc_vk.svg" class="soc_icon">
								                                        </a>
								                                         <a href="https://ok.ru/profile/554019134452" target="_blank" class="soc_icon_a">
								                                            <img src="/front/img/_soc_ok.svg" class="soc_icon">
								                                        </a>
								                                         <a href="https://www.facebook.com/szs.kislovodsk" target="_blank" class="soc_icon_a">
								                                            <img src="/front/img/_soc_fb.svg" class="soc_icon">
								                                        </a>
								                                         <a href="https://www.instagram.com/strojzhilservis/" target="_blank" class="soc_icon_a">
								                                            <img src="/front/img/_soc_ig.svg" class="soc_icon">
								                                        </a>
								                                    </li> 
                                                                     <li class="main_left_address">
                                                                     	<div class="vertical-devider"></div>
                                                                        г.&nbsp;Кисловодск, пер.&nbsp;Зашкольный, д.&nbsp;3
                                                                        <br>Телефон: +7(87937) 7-64-32
                                                                        <br>E-email: <a href="mailto:szs-kislovodsk@mail.ru">szs-kislovodsk@mail.ru</a>
                                                                        <div class="vertical-devider"></div>
                                                                    </li>
                                                                </ul>
                                                            </nav>

                                                        </aside>



