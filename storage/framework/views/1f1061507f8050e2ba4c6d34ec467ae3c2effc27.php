\
<?php $__env->startSection('content'); ?>
<main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_process">
    <div class="vertical-devider"></div>
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center">Ход строительства: <?php echo e($album->title); ?></h1>
        </div>
        <div class="vertical-devider"></div>
        <?php $__currentLoopData = $album->gallery(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="col-xs-4 main_process_item">
            <a class="main_process_image_link" href="/front/images/<?php echo e($image->content); ?>" data-lightbox="<?php echo e($album->title); ?>" data-title="<?php echo e($image->title); ?>"><img class="img-responsive" src="/front/thumbnail/<?php echo e($image->content); ?>" alt=""/>
            </a>
            <div><?php echo e($image->title); ?></div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        <div class="vertical-devider"></div>
        <div class="main_process_back">
            <a href="/objects/building_progress/<?php echo e($object->id); ?>" class="btn btn-default btn-kvart">Вернуться назад</a>
        </div>
        <div class="vertical-devider"></div>
    </div>
    </div>
</main>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>