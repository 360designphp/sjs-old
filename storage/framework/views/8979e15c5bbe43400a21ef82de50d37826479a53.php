<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование объекта</h1><?php if($object->id): ?>
                        &nbsp;<a href="/objects/<?php echo $object->id; ?>" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    <?php endif; ?>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/save" name="edited_object" method="post" id="edited_object"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo e($object->id); ?>" name="id">
                                <label class="col-xs-12">Название объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="name"
                                           value="<?php echo ($object->name)?$object->name:""; ?>"
                                           placeholder="Введите название объекта..." required></label>
                                <label class="col-xs-12">Адрес объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="address"
                                           value="<?php echo ($object->address) ? $object->address:""; ?>"
                                           placeholder="Введите адрес объекта..." required></label>
                                <label class="col-xs-12">Описание объекта
                                    <textarea class="editor" name="description" id="editor" cols="30" rows="10">
                                        <?php echo $object->description; ?>

                                    </textarea>
                                </label>

                                <label class="col-xs-12">Строка срока сдачи объекта
                                    <input type="text" class="form-control input-lg mb20"
                                           name="completion_date"
                                           value="<?php echo e($object->completion_date); ?>"
                                           placeholder="Укажите информацию о сроках сдачи объекта." required></label>

                                <br>
                                <div class="col-xs-6">
                                    <label>Материал
                                        <div class="radio"><label><input type="radio" name="material"
                                                                         value="concrete"
                                                                         <?php if($object->material == 'concrete' || $object->id == 'new'): ?> checked <?php endif; ?>
                                                >Бетон</label></div>
                                        <div class="radio"><label><input type="radio" name="material" value="bricks"
                                                                         <?php if($object->material == 'bricks'): ?> checked <?php endif; ?>>Кирпич</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="material" value="foam"
                                                                         <?php if($object->material == 'foam'): ?> checked <?php endif; ?>>Пенобетон</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <label>Тип отопления
                                        <div class="radio"><label><input type="radio" name="heating_type"
                                                                         value="own"
                                                                         <?php if($object->heating_type == 'own' || $object->id == 'new'): ?> checked <?php endif; ?>>Собственное</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="heating_type"
                                                                         value="central"
                                                                         <?php if($object->heating_type == 'central'): ?> checked <?php endif; ?>>Центральное</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6">
                                    <label>Статус
                                        <div class="radio"><label><input type="radio" name="status"
                                                                         value="building"
                                                                         <?php if($object->status == 'building' || $object->id == 'new'): ?> checked <?php endif; ?>>Строится</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="status" value="ready"
                                                                         <?php if($object->status == 'ready'): ?> checked <?php endif; ?>>Сдано</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="status" value="sold"
                                                                         <?php if($object->status == 'sold'): ?> checked <?php endif; ?>>Распродано</label>
                                        </div>
                                    </label>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionDomclick">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionDomclick"
                                                       href="#collapseDomclick"><h2>Информация для домклик</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseDomclick" class="collapse">
                                                            <label class="form-group" for="" style="width: 30%;" >Domclick ID
                                                                <input class="form-control" type="number" value="<?php echo e($object->domclick_id); ?>" name="domclick_id">
                                                            </label>
                                                            <label class="form-group" for="" style="width: 30%;">Широта
                                                                <input class="form-control" type="text" value="<?php echo e($object->latitude); ?>" name="latitude">
                                                            </label>
                                                            <label class="form-group" for="" style="width: 30%;" >Долгота
                                                                <input class="form-control" type="text" value="<?php echo e($object->longitude); ?>" name="longitude">
                                                            </label>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие охраны</span>
                                                                <label for="security_no"><input type="radio" name="security" id="security_no"
                                                                 value="1" <?php if($object->security == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="security_yes"><input type="radio" name="security" id="security_yes"
                                                                 value="0" <?php if($object->security == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие паркинга</span>
                                                                <label for="parking_no"><input type="radio" name="parking" id="parking_no"
                                                                 value="1" <?php if($object->parking == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="parking_yes"><input type="radio" name="parking" id="parking_yes"
                                                                 value="0" <?php if($object->parking == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие спорт. площадки</span>
                                                                <label for="sports_ground_no"><input type="radio" name="sports_ground" id="sports_ground_no"
                                                                 value="1" <?php if($object->sports_ground == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="sports_ground_yes"><input type="radio" name="sports_ground" id="sports_ground_yes"
                                                                 value="0" <?php if($object->sports_ground == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие детской площадки</span>
                                                                <label for="playground_no"><input type="radio" name="playground" id="playground_no"
                                                                 value="1" <?php if($object->playground == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="playground_yes"><input type="radio" name="playground" id="playground_yes"
                                                                 value="0" <?php if($object->playground == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие детского сада</span>
                                                                <label for="kindergarten_no"><input type="radio" name="kindergarten" id="kindergarten_no"
                                                                 value="1" <?php if($object->kindergarten == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="kindergarten_yes"><input type="radio" name="kindergarten" id="kindergarten_yes"
                                                                 value="0" <?php if($object->kindergarten == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Наличие школы</span>
                                                                <label for="school_no"><input type="radio" name="school" id="school_no"
                                                                 value="1" <?php if($object->school == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="school_yes"><input type="radio" name="school" id="school_yes"
                                                                 value="0" <?php if($object->school == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group" style="width: 30%">
                                                              <span>Огороженная територия</span>
                                                                <label for="fenced_area_no"><input type="radio" name="fenced_area" id="fenced_area_no"
                                                                 value="1" <?php if($object->fenced_area == 1): ?>checked <?php endif; ?>>Да</label>
                                                                <label for="fenced_area_yes"><input type="radio" name="fenced_area" id="fenced_area_yes"
                                                                 value="0" <?php if($object->fenced_area == 0): ?>checked <?php endif; ?>>Нет</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <span>УТП</span>
                                                                <textarea class="form-control" name="utp" id="" cols="30" rows="10"><?php echo e($object->utp); ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO<?php echo e($object->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO<?php echo e($object->id); ?>"
                                                       href="#collapseObject<?php echo e($object->id); ?>"><h2>Служебная
                                                            информация</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseObject<?php echo e($object->id); ?>" class="collapse">
                                                            <h3 class="mt0">Код SVG</h3>
                                                            <textarea name="svg" class="form-control" cols="10"
                                                                      rows="1"><?php echo e($object->svg); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php echo csrf_field(); ?>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionB<?php echo e($object->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionB<?php echo e($object->id); ?>"
                                                       href="#collapseBuilding<?php echo e($object->id); ?>"><h2>SEO - настройки</h2>
                                                    </a>
                                                    <div class="accordion-body">
                                                        <div id="collapseBuilding<?php echo e($object->id); ?>" class="collapse">
                                                            <span class="h3">Ключевые слова</span>
                                                            <input type="text" name="seo_keywords" id="keywords"
                                                                   class="form-control"
                                                                   value="<?php echo $object->seo_keywords; ?>"><br>
                                                            <span class="h3">Мета-описание</span>
                                                            <textarea name="seo_description" class="form-control"
                                                                      cols="30"
                                                                      rows="10"><?php echo e($object->seo_description); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd><?php echo $object->created_at; ?></dd>

                                        <dt>Последняя редакция</dt>
                                        <dd><?php echo $object->updated_at; ?></dd>
                                    </dl>

                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_object"
                                               value="Сохранить">
                                        <?php if($object->id): ?>
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('<?php echo e($object->id); ?>')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Миниатюра</h3>
                                    <?php if($object->image != null): ?>
                                        <img src=" /front/objects/<?php echo e($object->image); ?>" alt="" class="img-responsive"
                                             height="100">

                                    <?php else: ?>
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                    <?php endif; ?>

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="file"
                                               form="edited_object" value="Обновить">

                                        <?php if($object->image): ?>
                                            <button type="button" class="pull-right btn btn-danger"
                                                    onclick="deleteObjectImage(<?php echo $object->id; ?>)">Удалить
                                            </button>


                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Дополнительно</h3>
                                    <a href="#!" onclick="$('#docModal').modal('show');">Объектная документация</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- .container-fluid -->
        </div> <!-- #page-content -->
    </div>
    <!-- Modal images -->
    <!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
         <div class="modal-dialog" style="min-width:1000px;">
             <div class="modal-content">
                 <div class="modal-header" style="border:none;">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h2 class="modal-title">Изображения</h2>
                     <span class="bg-warning">Поддерживаемые форматы &laquo;Jpeg , Png, Gif&raquo; </span>
                 </div>
                 <div class="modal-body clearfix" >

                     <div role="tabpanel">
                         <!-- Nav tabs -->
    <!-- <ul class="nav nav-tabs" role="tablist" style="padding-left: 20px;padding-right: 20px;margin-left: -20px;margin-right: -20px;">
         <li role="presentation" class="active">
             <a href="#gallery" aria-controls="tab" role="tab" data-toggle="tab">Галерея</a>
         </li>

     </ul>
     <style type="text/css">
         #gallery .img-holda {height: 140px;width: 100%;background-color: rgba(200,200,200,0.1);position: relative;margin-bottom: 20px;}
         #gallery .img-holda .xxx{height: 30px;width: 30px; background: rgba(255,255,255,.5);right: 0;top: 0;color:#000;text-align: center;font-size: 16px;font-weight: 700;display: none;}
         #gallery img{position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;max-height: 140px;}
         #gallery .img-holda:hover .xxx{display: block;right: 0;top: 0;position: absolute;cursor: pointer;z-index: 1022;}
         #gallery {padding-top: 20px;}

     </style>-->
    <!-- Modal images -->

    <!-- Modal documents -->
    <!-- Modal images -->
    <div class="modal fade" id="docModal" tabindex="-1" role="dialog" aria-labelledby="docModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="min-width:1000px;">
            <div class="modal-content">
                <div class="modal-header" style="border:none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Документы</h2>
                    <span class="bg-warning">Поддерживаемые форматы &laquo;pdf , jpg, png&raquo; </span>
                </div>
                <div class="modal-body clearfix">

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist"
                            style="padding-left: 20px;padding-right: 20px;margin-left: -20px;margin-right: -20px;">
                            <li role="presentation" class="active">
                                <a href="#documents" aria-controls="tab" role="tab" data-toggle="tab">Документы</a>
                            </li>
                            <li role="presentation">
                                <a href="#object_gallery" aria-controls="tab" role="tab" data-toggle="tab">Галерея</a>
                            </li>

                            <li role="presentation">
                                <a href="#albums" aria-controls="tab" role="tab" data-toggle="tab">Ход строительства</a>
                            </li>

                        </ul>
                        <style type="text/css">
                            #gallery .img-holda {
                                height: 140px;
                                width: 100%;
                                background-color: rgba(200, 2 00, 200, 0.1);
                                position: relative;
                                margin-bottom: 20px;
                            }

                            #gallery .img-holda .xxx {
                                height: 30px;
                                width: 30px;
                                background: rgba(255, 255, 255, .5);
                                right: 0;
                                top: 0;
                                color: #000;
                                text-align: center;
                                font-size: 16px;
                                font-weight: 700;
                                display: none;
                            }

                            #gallery img {
                                position: absolute;
                                top: 0;
                                bottom: 0;
                                left: 0;
                                right: 0;
                                margin: auto;
                                max-height: 140px;
                            }

                            #gallery .img-holda:hover .xxx {
                                display: block;
                                right: 0;
                                top: 0;
                                position: absolute;
                                cursor: pointer;
                                z-index: 1022;
                            }

                            #gallery {
                                padding-top: 20px;
                            }

                        </style>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="documents">
                                <div class="control-group clearfix">
                                    <table style="width: 100%;">
                                        <th style="width: 600px;">Имя</th>
                                        <th>Очередь строит.</th>
                                        <th>Очередь вывода</th>
                                        <th>Дата загрузки</th>
                                        <th>Управление</th>
                                        <?php $__currentLoopData = $object->documents(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <tr onchange="updateDocument(<?php echo e($document->id); ?>)">
                                            <td><input type="text" value="<?php echo $document->title; ?>"
                                                       id="text<?php echo $document->id; ?>">
                                                <small><a
                                                        href="/front/documents/<?php echo e($document->slug); ?>"
                                                        target="_blank">открыть</a></small>      
                                            </td>
                                            <td>
                                                <input type="number" value="<?php echo e($document->queue); ?>"
                                                onchange="saveQueue(<?php echo e($document->id); ?>)"
                                                id="queue<?php echo $document->id; ?>" style="width: 50px;">
                                            </td>
                                            <td>
                                                <input type="number" value="<?php echo e($document->order); ?>"
                                                id="order<?php echo $document->id; ?>" style="width: 50px;">
                                            </td>
                                            <td>
                                                <input type="text" id="created<?php echo $document->id; ?>"
                                                value="<?php echo e($document->created_at); ?>">
                                            </td>
                                            <td>    
                                                <span class="btn btn-danger"onclick="deleteDocument(<?php echo $document->id; ?>)">x</span>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </table>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Загрузка документов</label>
                                    <div class="controls">
                                        <form action="/admin/save_documents" class="dropzone" id="object_documentation">
                                            <?php echo csrf_field(); ?>

                                            <input type="hidden" name="parent_id" value="<?php echo $object->id; ?>">
                                            <input type="hidden" name="type" value="document">
                                            <input type="hidden" name="parent_type" value="object">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="object_gallery">
                                <div class="control-group clearfix">
                                    <?php $__currentLoopData = $object->gallery(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 clearfix">
                                            <div class="img-holda" id="img<?php echo $gal->id; ?>">
                                                <div class="xxx" onclick="deleteImage(<?php echo $gal->id; ?>)">x</div>
                                                <img src="/front/thumbnail/<?php echo $gal->slug; ?>" alt=""
                                                     class="img-responsive">
                                            </div>
                                            <div style="margin: -16px 0 20px 0;">
                                                <input type="text" value="<?php echo $gal->title; ?>"
                                                       onchange="saveTitle(<?php echo $gal->id; ?>);" id="text<?php echo $gal->id; ?>"
                                                       style="max-width: 200px;">
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Фотогалерея</label>
                                    <div class="controls">
                                        <form action="/admin/save_images" class="dropzone" id="post_gallery">
                                            <?php echo csrf_field(); ?>

                                            <input type="hidden" name="parent_id" value="<?php echo $object->id; ?>">
                                            <input type="hidden" name="type" value="object_gallery">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="albums">
                                <div class="control-group">
                                    <label class="control-label">Ход строительства</label>
                                    <div class="controls">
                                        <a href="/admin/object_album/edit/new/<?php echo e($object->id); ?>">Создать альбом</a>
                                        <hr>
                                        <?php $__currentLoopData = $object->albums(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <a href="/admin/object_album/edit/<?php echo e($album->id); ?>"><?php echo e($album->title); ?> -
                                                редактировать</a><br><br>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
        <!-- Modal documents -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        function deleteDocument(id) {
            if (confirm('Вы уверены?') == true) {
                $.ajax({
                    url: '/api/ajax',
                    type: 'POST',
                    data: ({
                        'id': id,
                        'intent': 'deleteDocument'
                    }),
                    dataType: "html",
                    error: errorHandler,
                    success: function () {
                        $('#doc' + id).fadeOut(400);
                    }
                })

            }
        }

        function deleteObjectImage(id) {
            if (confirm('Вы уверены?') == true) {
                $.ajax({
                    url: '/api/ajax',
                    type: 'POST',
                    data: ({
                        'id': id,
                        'intent': 'deleteObjectImage'
                    }),
                    dataType: "html",
                    error: errorHandler,
                    success: function () {
                        $('#img' + id).fadeOut(400);
                        $('#text' + id).fadeOut(400);
                    }
                })
            }
            window.location.reload();
        }
         function updateDocument(id) {
            var title = $('#text' + id).val();
            var queue = $('#queue' + id).val();
            var order = $('#order' + id).val();
            var created_at = $('#created' + id).val();
            var post_id = $('#id').val();
            $.ajax({
                url: '/api/ajax',
                type: 'POST',
                data: ({
                    'id': id,
                    'post_id': post_id,
                    'title': title,
                    'queue': queue,
                    'order': order,
                    'created_at': created_at,
                    'intent': 'updateDocument'
                }),
                dataType: "html",
                error: errorHandler,
                success: function (id) {
                    $('#id').val(id);
                }
            })
        }

        function saveTitle(id) {
            var title = $('#text' + id).val();
            var post_id = $('#id').val();
            $.ajax({
                url: '/api/ajax',
                type: 'POST',
                data: ({
                    'id': id,
                    'post_id': post_id,
                    'title': title,
                    'intent': 'saveImageTitle'
                }),
                dataType: "html",
                error: errorHandler,
                success: function (id) {
                    $('#id').val(id);
                }
            })
        }

        function errorHandler(data) {
            alert('Ошибка :' + data.status);
        }

    </script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>