<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_etaj">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2>Выберите этаж, oфисы или парковку.</h2>
                <div class="row">
                    <div class="col-xs-12 breadcrumbs">
                        <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                        <a href="/objects/<?php echo e($object->id); ?>"><span class="h4"><?php echo e($object->name); ?></span></a> >
                        <span class="h4">Секция <?php echo e($section->symbol); ?></span>
                    </div>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12 etaj_svg_wrap">
                <div class="etaj_svg_holder">
                    <svg version="1.1" id="etaj_marcinkevich_a_g"
                         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="167 111.2 929.5 618.7"
                         style="enable-background:new 167 111.2 929.5 618.7;" xml:space="preserve">
                                <style type="text/css">
                                    .etaj {
                                        opacity: 0.6;
                                        fill: transparent;
                                    }
                                    .offices{
                                        opacity: 0.6;
                                        fill: transparent;
                                    }
                                    .parks .parks_wrap{
                                        fill:#ffffff;
                                        opacity:0.5;
                                    }
                                    .st3{fill:#58595B;}
                                    .st4{fill:none;stroke:#939598;stroke-width:0.4126;stroke-miterlimit:10;}
                                </style>
                        <g id="etaj_marcinkevich_a_img">

                            <image style="overflow:visible;enable-background:new    ;" width="4004" height="2669" xlink:href="/front/img/SJS_site_floors_marcinkevich_sector_A.jpg"  transform="matrix(0.2321 0 0 0.2321 167.1787 110.9207)">
                            </image>
                        </g>
                        <g id="etaj_marcinkevich_a">
                            <polygon class="etaj et0806" points="482.3,260.7 626.8,238.7 626.8,224 687.4,213.5 699.5,226 922.1,191.5 924.1,241.2
                                        749.5,264.2 749.6,271.9 700.4,278.5 690.4,273 482.3,300.9   "/>
                            <polygon class="etaj et0805" points="482.3,300.9 690.4,273 700.4,278.5 749.6,271.9 749.5,264.2 924.1,241.2 924.3,250.2
                                        922.8,250.4 924.8,300.9 751,318.4 750.8,324.8 701.4,330.1 691.4,326 482.3,348.7     "/>
                            <polygon class="etaj et0804" points="482.3,348.7 691.4,326 701.4,330.1 750.8,324.8 751,318.4 924.8,300.9 927,357.6
                                        751.7,371.5 751.9,378.6 702.2,382.5 692.3,379.5 482.3,396.5     "/>
                            <polygon class="etaj et0803" points="482.3,396.5 692.3,379.5 702.2,382.5 751.9,378.6 751.7,371.5 927,357.6 929.3,417.5
                                        753.3,427 753.2,432.5 703.3,435.2 693.2,433 482.3,445.5     "/>
                            <polygon class="etaj et0802" points="482.3,445.5 693.2,433 703.3,435.2 753.2,432.5 753.3,427 929.3,417.5 931.8,477
                                        754.5,483.2 754.3,487.1 704.3,488.7 694,487.8 482.3,494     "/>
                            <polygon class="etaj et0801" points="482.3,493.9 694,487.8 704.3,488.7 754.3,487.1 754.5,483.2 931.8,476.9 934.3,543.4
                                        693,544.1 629.8,543.9 629.9,542.9 483,542.7     "/>
                            <g class="parks park08">
                                <path class="parks_wrap" d="M915.8,573c0,3.1-2.5,5.7-5.7,5.7h-63.5c-3.1,0-5.7-2.5-5.7-5.7v-5.8c0-3.1,2.5-5.7,5.7-5.7h63.5
                                            c3.1,0,5.7,2.5,5.7,5.7V573z"/>
                                <g>
                                    <path class="st3" d="M871.7,565.9h5.6v7h-1.4v-5.8h-2.7v5.8h-1.4L871.7,565.9L871.7,565.9z"/>
                                    <path class="st3" d="M879.7,569.3l-1.2-0.2c0.1-0.5,0.4-0.9,0.7-1.1s0.8-0.4,1.5-0.4c0.6,0,1,0.1,1.3,0.2
                                                c0.3,0.1,0.5,0.3,0.6,0.5s0.2,0.6,0.2,1.2v1.6c0,0.4,0,0.8,0.1,1c0,0.2,0.1,0.4,0.2,0.7h-1.3c0-0.1-0.1-0.2-0.1-0.4
                                                c0-0.1,0-0.1,0-0.2c-0.2,0.2-0.5,0.4-0.7,0.5c-0.3,0.1-0.5,0.2-0.8,0.2c-0.5,0-0.9-0.1-1.2-0.4s-0.5-0.6-0.5-1.1
                                                c0-0.3,0.1-0.5,0.2-0.8c0.1-0.2,0.3-0.4,0.6-0.5c0.2-0.1,0.6-0.2,1.1-0.3c0.6-0.1,1.1-0.2,1.3-0.3v-0.1c0-0.3-0.1-0.4-0.2-0.6
                                                c-0.1-0.1-0.4-0.2-0.7-0.2c-0.2,0-0.4,0-0.6,0.1C879.9,568.9,879.8,569.1,879.7,569.3z M881.5,570.4c-0.2,0.1-0.4,0.1-0.8,0.2
                                                c-0.4,0.1-0.6,0.2-0.7,0.2c-0.2,0.1-0.3,0.3-0.3,0.5s0.1,0.3,0.2,0.5c0.1,0.1,0.3,0.2,0.5,0.2s0.5-0.1,0.7-0.2
                                                c0.2-0.1,0.3-0.3,0.3-0.4c0-0.1,0.1-0.3,0.1-0.7V570.4z"/>
                                    <path class="st3" d="M884.1,567.8h1.3v0.7c0.2-0.3,0.4-0.5,0.7-0.6c0.3-0.2,0.6-0.2,0.9-0.2c0.6,0,1.1,0.2,1.5,0.7
                                                s0.6,1.1,0.6,1.9s-0.2,1.5-0.6,2c-0.4,0.5-0.9,0.7-1.5,0.7c-0.3,0-0.5-0.1-0.8-0.2c-0.2-0.1-0.5-0.3-0.7-0.6v2.6h-1.3v-7H884.1z
                                                 M885.5,570.2c0,0.6,0.1,1,0.3,1.3s0.5,0.4,0.8,0.4s0.6-0.1,0.8-0.4c0.2-0.2,0.3-0.7,0.3-1.2s-0.1-0.9-0.3-1.2s-0.5-0.4-0.8-0.4
                                                s-0.6,0.1-0.8,0.4C885.6,569.4,885.5,569.7,885.5,570.2z"/>
                                    <path class="st3" d="M890.1,567.8h1.3v2.1c0.2,0,0.4-0.1,0.5-0.2c0.1-0.1,0.2-0.4,0.3-0.7c0.2-0.5,0.4-0.8,0.6-1
                                                c0.2-0.1,0.5-0.2,0.8-0.2c0.1,0,0.3,0,0.5,0v0.9c-0.4,0-0.6,0-0.7,0.1c-0.1,0.1-0.2,0.3-0.3,0.6c-0.2,0.5-0.4,0.8-0.7,0.9
                                                c0.4,0.1,0.8,0.5,1.1,1.1c0,0,0,0.1,0.1,0.1l0.7,1.4h-1.4l-0.7-1.5c-0.1-0.3-0.3-0.5-0.4-0.6c-0.1-0.1-0.2-0.1-0.4-0.1v2.2h-1.3
                                                L890.1,567.8L890.1,567.8z"/>
                                    <path class="st3" d="M895,567.8h1.3v3.2l2.1-3.2h1.3v5.1h-1.3v-3.2l-2.1,3.2H895V567.8z"/>
                                    <path class="st3" d="M901,567.8h1.3v1.9h1.9v-1.9h1.3v5.1h-1.3v-2.1h-1.9v2.1H901V567.8z"/>
                                    <path class="st3" d="M906.9,567.8h3.4v1.1h-2.1v4h-1.3L906.9,567.8L906.9,567.8z"/>
                                </g>
                                <path class="st3" d="M858.9,568.5L858.9,568.5L858.9,568.5c0.2,0.5,0.4,0.9,0.4,1.4v2.5c0,0.3-0.2,0.5-0.5,0.5h-0.7
                                            c-0.3,0-0.5-0.2-0.5-0.5V572h-0.4c0-0.2,0-0.4-0.1-0.6c0.3-0.3,0.5-0.6,0.5-1c0-0.2,0-0.3,0-0.4c0-0.7-0.6-1.3-1.3-1.3H856l0,0
                                            h-0.1c-0.1-0.3-0.3-0.7-0.5-1c-0.4-0.6-1.1-1-1.8-1.1c-0.6-0.1-1.3-0.1-1.9-0.1c-0.1,0-0.3,0-0.4,0c0.1-0.3,0.3-0.5,0.4-0.8
                                            c0.3-0.4,0.7-0.6,1.2-0.7c0.6-0.1,1.2-0.1,1.8-0.1l0,0c0.6,0,1.2,0,1.8,0.1c0.5,0.1,0.9,0.3,1.2,0.7c0.2,0.3,0.5,1,0.8,1.6
                                            c0.2-0.2,0.4-0.3,0.6-0.3h0.2c0.3,0,0.5,0.2,0.5,0.5c0,0.1,0,0.2,0,0.4c0,0.3-0.2,0.5-0.5,0.5L858.9,568.5z M846.7,570.4
                                            c0-0.1,0-0.3,0-0.4c0-0.3,0.2-0.5,0.5-0.5h0.2c0.2,0,0.4,0.1,0.6,0.3c0.3-0.6,0.6-1.3,0.8-1.6c0.3-0.4,0.7-0.6,1.2-0.7
                                            c0.6-0.1,1.2-0.1,1.8-0.1l0,0c0.6,0,1.2,0,1.8,0.1c0.5,0.1,0.9,0.3,1.2,0.7c0.2,0.3,0.5,1,0.8,1.6c0.2-0.2,0.4-0.3,0.6-0.3h0.2
                                            c0.3,0,0.5,0.2,0.5,0.5c0,0.1,0,0.2,0,0.4c0,0.3-0.2,0.5-0.5,0.5H856l0.1,0.1c0.2,0.4,0.4,0.8,0.4,1.3v2.5c0,0.3-0.2,0.5-0.5,0.5
                                            h-0.7c-0.3,0-0.5-0.2-0.5-0.5v-0.4h-5.9v0.4c0,0.3-0.2,0.5-0.5,0.5h-0.7c-0.3,0-0.5-0.2-0.5-0.5v-2.5c0-0.5,0.1-0.9,0.4-1.3
                                            c0,0,0-0.1,0.1-0.1h-0.2C846.9,570.9,846.7,570.7,846.7,570.4z M853.3,572.2c0,0.2,0.2,0.4,0.4,0.4h1c0.2,0,0.4-0.2,0.4-0.4v-0.3
                                            c0-0.2-0.2-0.4-0.4-0.4h-0.1l-1,0.3C853.4,571.8,853.3,572,853.3,572.2L853.3,572.2z M848.4,572.2c0,0.2,0.2,0.4,0.4,0.4l0,0h1
                                            c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.1-0.3-0.3-0.4l-1-0.3c-0.2-0.1-0.4,0.1-0.5,0.3v0.1L848.4,572.2L848.4,572.2z"/>
                                <line class="st4" x1="865.8" y1="561.6" x2="865.8" y2="578.7"/>
                            </g>
                        </g>
                                </svg>
                    <div class="etaj_svg_popover">
                        <div class="etaj_svg_popover_etaj_div">
                            <span class="etaj_popover_first_name">Этаж</span>
                            <span class="etaj_popover_etaj">5</span>
                        </div>
                        <div>
                            <span class="etaj_popover_second_name">Стоимость</span>
                            <span class="etaj_popover_price">от <span>8.1</span> ₽</span>
                        </div>
                        <div>
                            <span class="etaj_popover_third_name">Продано</span>
                            <span class="etaj_popover_kol_prod"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fourth_name">Забронировано</span>
                            <span class="etaj_popover_kol_zabr"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fifth_name">Свободно</span>
                            <span class="etaj_popover_kol_svob"></span>
                        </div>
                        <div class="etaj_svg_popover_arenda_div">
                            <span class="etaj_popover_sixth_name">Аренда</span>
                            <span class="etaj_popover_price_arr">от <span>8.1</span> ₽</span>
                        </div>
                        <div class="etaj_svg_popover_bgwrap"></div>
                    </div>
                </div>
                <section class="about_house text-center">
                    <?php echo $__env->make('layouts.front.objects.object_gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('layouts.front.objects.object_4.description', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </section>
            </div>
        </div>
    </main>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('data'); ?>
    <span class="etaj_holder hidden">{<?php $__currentLoopData = $livingFloors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $floor): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"0<?php echo e($floor->section->id); ?>0<?php echo e($floor->level); ?>": [<?=$floor->flats->min('price')?>, <?php echo e($floor->soldFlats()->count()); ?>, <?php echo e($floor->bookedFlats()->count()); ?>, <?php echo e($floor->freeFlats()->count()); ?>,<?=$floor->flats->min('rent_price')?>, "/objects/<?php echo e($object->id.'/'.$building->id.'/'.$floor->section->id.'/'.$floor->id); ?>"],<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php $__currentLoopData = $zeroFloors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parking): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"<?='park0'.$parking->section->id?>":[<?=$parking->flats->min('price')?>, <?php echo e($parking->soldFlats()->count()); ?>, <?php echo e($parking->bookedFlats()->count()); ?>, <?php echo e($parking->freeFlats()->count()); ?>,<?=$parking->flats->min('rent_price')?>, "/objects/<?php echo e($object->id.'/'.$building->id.'/'.$parking->section->id.'/'.$parking->id); ?>"]<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>}</span>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>