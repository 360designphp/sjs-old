<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <?php echo $__env->make('layouts.front.objects.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('layouts.front.objects.filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row main_links_houses">
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/building">
                    <div class="main_links_houses_img2"></div>
                    <span>Строящиеся дома</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/ready">
                    <div class="main_links_houses_img1"></div>
                    <span>Сданные дома</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/objects/">
                    <div class="main_links_houses_img3"></div>
                    <span>Все объекты с 1993 года</span>
                </a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <a href="/page/actions">
                    <div class="main_links_houses_img4"></div>
                    <span>Акции</span>
                </a>
            </div>
        </div>
        <div class="vertical-devider"></div>
        <div class="main_banks row">
            <div class="col-xs-2  col-xs-offset-1">
                <a href="http://sberbank.ru/ru/person/credits/home" target="_blank">
                    <img src="/front/img/sbrbnk.png" class="img-responsive" alt="Сбербанк">
                </a>
            </div>
             <div class="col-xs-2">
                <a href="http://www.vtb24.ru/" target="_blank" rel="nofollow">
                    <img src="/front/img/vtb.png" class="img-responsive" alt="ВТБ 24">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="http://www.vbank.ru/personal/credits/product_novostroy/?utm_source=partners&utm_medium=logo&utm_campaign=stavropol-partners-ipoteka" target="_blank" rel="nofollow">
                    <img src="/front/img/vzrjdn.png" class="img-responsive" alt="Банк Возрождение">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="http://www.rshb.ru/" target="_blank" rel="nofollow">
                    <img src="/front/img/rshlzbnk.jpg" class="img-responsive" alt="Россельхозбанк">
                </a>
            </div>
            <div class="col-xs-2">
                <a href="https://www.sviaz-bank.ru/service/hypotec-new/" target="_blank" rel="nofollow">
                    <img src="/front/img/sb_220x60.gif" class="img-responsive" alt="Сбербанк">
                </a>
            </div>
             <div class="col-xs-2 col-xs-offset-1 mt-10">
                <a class="btn btn-success" href="https://ipoteka.domclick.ru/?_ga=2.112135812.1440989557.1521102066-528974055.1521102066" target="_blank">
                  Калькулятор ипотеки
                </a>
            </div>
            <div class="col-xs-2 mt-10">
                <a class="btn btn-primary" href="https://www.vtb24.ru/mortgage/" target="_blank">
                  Калькулятор ипотеки
                </a>
            </div>

        </div>
        <div class="vertical-devider"></div>
        <div class="main_news">
        <?php if(count($press_centre) != 0): ?>
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#news-pane" aria-controls="news-pane" role="tab" data-toggle="tab"><h2 class="mauto">Новости</h2></a></li>
            <li role="presentation"><a href="#press-pane" aria-controls="press-pane" role="tab" data-toggle="tab" class="press-tab"><h2 class="mauto">Пресс-центр</h2></a></li>
          </ul>
          <!-- Tab panes -->
            <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="news-pane">
                <a href="/news" class="more-news-link">Все новости</a>
                <div class="slick_news slick_style">
                <?php for($i = 0; $i < count($news); $i +=3): ?>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($news[$i]->thumbnail() != null): ?> /front/images/<?php echo e($news[$i]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($news[$i]->created_at); ?></small>
                                    <h4><?php echo e($news[$i]->title); ?></h4>
                                    <?php echo str_limit($news[$i]->additional,70); ?>

                                    <a href="/<?php echo e($news[$i]->type); ?>/<?php echo e($news[$i]->slug); ?>" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($news[$i+1])): ?>
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($news[$i+1]->thumbnail() != null): ?> /front/images/<?php echo e($news[$i+1]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($news[$i+1]->created_at); ?></small>
                                    <h4><?php echo e($news[$i+1]->title); ?></h4>
                                    <?php echo str_limit($news[$i+1]->additional, 70); ?>

                                    <a href="/<?php echo e($news[$i+1]->type); ?>/<?php echo e($news[$i+1]->slug); ?>" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <?php endif; ?>
                    <?php if(isset($news[$i+2])): ?>
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($news[$i+2]->thumbnail() != null): ?> /front/images/<?php echo e($news[$i+2]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($news[$i+2]->created_at); ?></small>
                                    <h4><?php echo e($news[$i+2]->title); ?></h4>
                                    <?php echo str_limit($news[$i+2]->additional, 70); ?>

                                    <a href="/<?php echo e($news[$i+2]->type); ?>/<?php echo e($news[$i+2]->slug); ?>" type="button" class="btn btn-primary">Подробнее</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endfor; ?>
            </div>
            </div>
            
            <div role="tabpanel" class="tab-pane" id="press-pane">
                <a href="/press-centre" class="more-news-link more-press-link">Все публикации</a>
                <div class="slick_press slick_style"> 
                <?php for($i = 0; $i < count($press_centre); $i +=3): ?>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($press_centre[$i]->thumbnail() != null): ?> /front/images/<?php echo e($press_centre[$i]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($press_centre[$i]->created_at); ?></small>
                                    <h4><?php echo e($press_centre[$i]->title); ?></h4>
                                    <?php echo str_limit($press_centre[$i]->additional, 70); ?>

                                    <a href="/<?php echo e($press_centre[$i]->type); ?>/<?php echo e($press_centre[$i]->slug); ?>" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(isset($press_centre[$i+1])): ?>
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($press_centre[$i+1]->thumbnail() != null): ?> /front/images/<?php echo e($press_centre[$i+1]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($press_centre[$i+1]->created_at); ?></small>
                                    <h4><?php echo e($press_centre[$i+1]->title); ?></h4>
                                    <?php echo str_limit($press_centre[$i+1]->additional, 70); ?>

                                    <a href="/<?php echo e($press_centre[$i+1]->type); ?>/<?php echo e($press_centre[$i+1]->slug); ?>" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                      <?php if(isset($press_centre[$i+2])): ?>
                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-4">
                                <img src="<?php if($press_centre[$i+2]->thumbnail() != null): ?> /front/images/<?php echo e($press_centre[$i+2]->thumbnail()->slug); ?> <?php else: ?> /front/img/21455132o.jpg <?php endif; ?>" class="img-responsive">
                            </div>
                            <div class="col-xs-8">
                                <div>
                                    <small><?php echo e($press_centre[$i+2]->created_at); ?></small>
                                    <h4><?php echo e($press_centre[$i+2]->title); ?></h4>
                                    <?php echo str_limit($press_centre[$i+2]->additional, 70); ?>

                                    <a href="/<?php echo e($press_centre[$i+2]->type); ?>/<?php echo e($press_centre[$i+2]->slug); ?>" type="button" class="btn  btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endfor; ?>
            </div>
            </div>
          </div>
        </div>
        <?php endif; ?>
        <div class="vertical-devider"></div>
        <div class="main_about row">
            <div class="col-xs-7 col-xs-offset-1 aboutcom">
                <h2>О компании</h2>
                <div>Строительная компания «Стройжилсервис» организована в 1993 году под руководством директора Леонида Евсеевича Пихельсона.</div>
                <p>В течение 20 лет вся трудовая деятельность связана со строительством, реконструкцией и капитальным ремонтом объектов жилищно-гражданского, курортного и общественного назначения города – курорта Кисловодска и городов Кавказских Минеральных Вод Ставропольского края.</p>
                <p>В настоящее время на предприятии созданы все условия для работы административного, управленческого и линейного инженерно-технического персонала во вновь обустроенных собственных административных помещениях.</p>
                <button type="button" class="btn" onclick="window.location='/page/o-kompanii'">Подробнее</button>
            </div>
            <div class="col-xs-3 padlr0">
                <iframe src="https://player.vimeo.com/video/169973642" width="100%" height="200px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </main>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>