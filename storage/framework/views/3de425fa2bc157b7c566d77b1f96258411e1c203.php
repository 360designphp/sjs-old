<div class="slick_houses">
    <?php if(isset($slides)): ?>
        <?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <div>
                <a href="<?php echo $slide->slug; ?>">
                    <?php if(is_object($slide->thumbnail())): ?>
                        <img src="/front/images/<?php echo e($slide->thumbnail()->slug); ?>" class="img-responsive">
                    <?php else: ?>
                        <img src="/front/img/slide1.jpg" class="img-responsive">
                    <?php endif; ?>
                </a>
                <div class="slick_caption">
                    <h2><?php echo e($slide->title); ?></h2>
                    <small><?php echo $slide->content; ?></small>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    <?php endif; ?>
</div>