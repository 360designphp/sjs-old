<?php $__env->startSection('content'); ?>

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>База клиентов&nbsp;<small><a href="/admin/clients/printable?<?=$parameters?>" target="_blank">версия для
                                печати</a></small>
                    </h1>

                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" class="form-inline mb10">
                                <div class="form-group">
                                    <select name="object" id="" class="form-control">
                                        <option value="all">Все объекты</option>
                                        <?php $__currentLoopData = $objects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <option value="<?php echo e($object->id); ?>" <?php if(isset($_GET['object']) && $_GET['object'] == $object->id): ?> selected <?php endif; ?>><?php echo $object->name; ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                                    <select name="status" id="" class="form-control">
                                        <option value="all" <?php if(!isset($_GET['status']) || $_GET['status'] == 'all'): ?> selected <?php endif; ?>>Все статусы</option>
                                        <option value="booked"
                                                <?php if(isset($_GET['status']) && $_GET['status'] == 'booked'): ?> selected <?php endif; ?>>
                                            Забронировно
                                        </option>
                                        <option value="sold"
                                                <?php if(isset($_GET['status']) && $_GET['status'] == 'sold'): ?> selected <?php endif; ?>>
                                            Продано
                                        </option>
                                    </select>
                                    <button type="submit" class="btn btn-default">Фильтровать</button>
                                </div>
                            </form>

                            <div class="panel">
                                <div class="panel-body panel-no-padding">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Телефон</th>
                                            <th width="150">Квартира</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $flats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <tr>
                                                <td><?php echo e($flat->customer_name); ?></td>
                                                <td><?php echo e($flat->customer_phone); ?></td>
                                                <td>
                                                    <a href="/admin/objects/flat/<?php echo e($flat->id); ?>"><?php echo e($flat->getObject()->name); ?>

                                                        <?php echo e($flat->getFloorSymbol()); ?>: <?php echo e($flat->number); ?></a> <br><br>  <span class="label label-default"><?php echo e($flat->getStrStatus()); ?></span></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>