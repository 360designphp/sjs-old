<b class="h3"><?php echo e($object->completion_date); ?></b>
<br>
<br>
<div class="col-xs-12 about_opisanie">
    <div class="row">
        <div class="vertical-devider"></div>
        <div class="row">
            <?php if(count($object->albums()) != 0): ?>
                <div class="main_process_back">
                    <a href="/objects/building_progress/<?php echo e($object->id); ?>" class="btn btn-default btn-kvart">Посмотреть ход строительства</a>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-12 text-center">
            <h3>Документация по объекту</h3>
            <?php if($object->documents()->count() != 0): ?><a href="/documents/<?php echo e($object->id); ?>/object" target="_blank">Журнал изменений</a><?php endif; ?>
            <ul class="about_opisanie_docs list-unstyled">
                <?php $documents = $object->documents();?>

                <?php if($documents->count() <= 7): ?>
                    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <li class="file_icon_<?php echo e(mb_strtolower($document->content)); ?>"><a
                                    href="/front/documents/<?php echo e($document->slug); ?>" target="_blank"><?php echo e($document->title); ?></a>
                            <small><?php echo e($document->created_at); ?></small> <small>ответственное лицо: <?php echo e($document->getAuthor()->name); ?></small>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php else: ?>
                    <?php $i = 0;?>
                    <?php while($i++ <= 7): ?>
						<?php if(isset($documents[$i])): ?>
                        <li class="file_icon_<?php echo e(mb_strtolower($documents[$i]->content)); ?>"><a
                                    href="/front/documents/<?php echo e($documents[$i]->slug); ?>" target="_blank"><?php echo e($documents[$i]->title); ?></a>
                            <small><?php echo e($documents[$i]->created_at); ?></small> <small>ответственное лицо: <?php echo e($documents[$i]->getAuthor()->name); ?></small>
                        </li>
                        <?php endif; ?>
                    <?php endwhile; ?>
                    <?php if($documents->count() > 8): ?>
                        <button data-toggle="collapse" data-target="#docsmore" class="">Показать ещё документы...
                        </button>
                        <div id="docsmore" class="collapse" style="">
                            <?php for($i = 8; $i <= count($documents->toArray())-1; $i++): ?>
								<?php if(isset($documents[$i])): ?>
                                <li class="file_icon_<?php echo e(mb_strtolower($documents[$i]->content)); ?>"><a
                                            href="/front/documents/<?php echo e($documents[$i]->slug); ?>"
                                            target="_blank"><?php echo e($documents[$i]->title); ?></a>
                                    <small><?php echo e($documents[$i]->created_at); ?></small> <small>ответственное лицо: <?php echo e($documents[$i]->getAuthor()->name); ?></small>
                                </li>
								<?php endif; ?>
                            <?php endfor; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-7">
    <div class="small-vertical-devider"></div>
    <?php echo $object->description; ?>

</div>
<div class="col-xs-12 col-sm-5 padlr0">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2874.3161182777826!2d42.7263929151676!3d43.911428943712664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40582b03fc5fd9bb%3A0x95bfb67bbcf3d352!2z0JrQvtC70LvQtdC60YLQuNCy0L3QsNGPINGD0LsuLCAxMSwg0JrQuNGB0LvQvtCy0L7QtNGB0LosINCh0YLQsNCy0YDQvtC_0L7Qu9GM0YHQutC40Lkg0LrRgNCw0LksIDM1NzcwMw!5e0!3m2!1sru!2sru!4v1517045102153" width="600" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
    <div class="row about_map_additional">
        <div class="col-xs-3">
            <?php echo e($object->address); ?>

        </div>
        <div class="col-xs-6">
            <button type="button" class="btn btn-default">Заказать обратный звонок</button>
        </div>
        <div class="col-xs-3">
            <a href="https://www.google.ru/maps/place/Коллективная+ул.,+11,+Кисловодск,+Ставропольский+край,+357703/@43.9115188,42.7281873,19z/data=!4m5!3m4!1s0x40582b03fc5fd9bb:0x95bfb67bbcf3d352!8m2!3d43.9114251!4d42.7285816" target="_blank">Посмотреть на большой карте</a>
        </div>
    </div>
</div>
<div class="small-vertical-devider"></div>
