<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_korpus">
        <div class="row">
            <div class="col-xs-12 korpus_svg_wrap">
                <div class="small-vertical-devider"></div>
                <div class="row">
                    <div class="col-xs-12 text-center breadcrumbs">
                        <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                        <span class="h4"><?php echo e($object->name); ?></span>
                    </div>
                </div>
                <div class="small-vertical-devider"></div>
                <div class="korpus_svg_holder">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="0 0 929.5 554.7" style="enable-background:new 0 0 929.5 554.7;" xml:space="preserve">
                                        <style type="text/css">
                                            .korp {
                                                opacity: 0.5;
                                                fill: transparent;
                                            }
                                        </style>
                        <g id="SJS_site_select_marcinkevich_img">
                            <g id="Group">

                                <image style="overflow:visible;" width="4000" height="2387" id="Img"
                                       xlink:href="/front/img/SJS_site_select_marcinkevich_img.jpg"
                                       transform="matrix(0.2324 0 0 0.2324 0 0)">
                                </image>
                            </g>
                        </g>
                        <g id="SJS_site_select_marcinkevich">
                            <a href="/objects/<?php echo e($object->id); ?>/<?php echo e($object ->firstBuilding()->id); ?>/8">
                            <polygon class="korp korpus8" points="206.6,435.2 211.3,435.5 216.3,434.8 228.1,437 419.6,420.6 419.7,420.6 431.3,422.1
                                                514.2,413.8 514.3,413.4 514.3,413.4 514.3,411.3 514.3,411.3 547.1,407.7 542.8,145.6 448.3,132.3 448.3,124.1 387.9,115.5
                                                371.8,121.7 215,99.5 143.7,139.2 143.7,139.4 143.4,139.4 143.4,140.6 143.7,140.6 143.7,145.5 137.8,148.7 137.8,178.6
                                                145.8,179.2 146.2,182.5 187.4,185.8 189.4,185.3 198.4,186 198.4,426.4   "/>
                            </a>
                            <a href="/objects/<?php echo e($object->id); ?>/<?php echo e($object ->firstBuilding()->id); ?>/9" class="b-sec">
                            <polygon class="korp korpus9" points="863.3,365.9 863.3,351.5 863.5,351.5 855.8,114.5 846.9,97.6 825.8,99.9 812.9,79.2
                                                747,87.1 739.2,78.9 670.2,87.8 670.2,97.1 563.8,110.6 541,106.8 456.1,133.4 542.8,145.6 547.1,407.7 567.4,406.1 577.9,406.1
                                                577.9,407.2 582.2,407.2 582.2,409.4 642.1,409.4 655.3,404.6 817.6,402.5     "/>
                            </a>
                            <g id="Group_2_">
                                <g>
                                    <path d="M350.1,101.8c-0.9,0.3-2.7,0.6-5.1,0.3c-5.4-0.6-9.5-4.5-9.6-10.7c-0.1-5.9,4-9.5,10-8.8c2.4,0.3,3.9,1,4.6,1.4l-0.6,1.9
                                                        c-0.9-0.6-2.3-1-3.9-1.2c-4.5-0.5-7.5,2-7.4,7c0,4.6,2.8,7.9,7.5,8.5c1.5,0.2,3.1,0,4-0.3L350.1,101.8z"/>
                                    <path d="M354,96.5c0.1,3.4,2.3,5,4.8,5.3c1.8,0.2,2.9,0,3.8-0.3l0.4,1.8c-0.9,0.3-2.4,0.6-4.6,0.3c-4.3-0.5-6.8-3.5-6.9-7.6
                                                        c0-4.1,2.4-7.1,6.4-6.6c4.5,0.5,5.7,4.6,5.8,7.1c0,0.5,0,0.9-0.1,1.1L354,96.5z M361.4,95.6c0-1.6-0.7-4.1-3.5-4.4
                                                        c-2.5-0.3-3.6,1.9-3.8,3.6L361.4,95.6z"/>
                                    <path d="M369.4,91l0.1,5.9L370,97l5.1-5.3l3.1,0.3l-6,5.5c2.2,0.5,3.6,2.1,4.7,4.7c0.4,0.9,0.9,2.3,1.5,3.5l-2.6-0.3
                                                        c-0.4-0.6-0.9-1.8-1.3-2.9c-1-2.3-2.3-3.5-4.6-3.7l-0.5-0.1l0.1,6l-2.5-0.3l-0.1-13.6L369.4,91z"/>
                                    <path d="M383,92.6l0.1,11.7l6.4,0.7l-0.1-11.7l2.5,0.3l0.1,11.8l1.4,0.2l-0.1,6.1l-1.9-0.2l-0.2-4.3l-10.6-1.2l-0.1-13.6
                                                        L383,92.6z"/>
                                    <path d="M398.6,94.4l0.1,5.7c0,2.2,0,3.5-0.1,5.4l0.1,0c0.8-1.6,1.3-2.5,2.2-3.9l4.2-6.5l2.9,0.3l0.1,13.6l-2.4-0.3l-0.1-5.8
                                                        c0-2.2,0-3.4,0.1-5.6l-0.1,0c-0.9,1.6-1.5,2.7-2.2,4c-1.3,2-2.8,4.4-4.2,6.6l-2.9-0.3l-0.1-13.6L398.6,94.4z"/>
                                    <path d="M422.4,110.7l-2.5-0.3l-0.1-5.7l-1.9-0.2c-1.1-0.1-1.8,0.1-2.3,0.5c-1,0.7-1.4,2.3-1.9,3.6c-0.2,0.4-0.4,0.7-0.6,1.1
                                                        l-2.7-0.3c0.3-0.4,0.6-0.9,0.8-1.5c0.6-1.3,0.9-3,2.1-3.9c0.4-0.3,0.9-0.6,1.6-0.6l0-0.1c-1.8-0.4-3.7-1.7-3.7-3.8
                                                        c0-1.4,0.7-2.3,1.8-2.7c1.1-0.5,2.8-0.6,4.6-0.4c1.7,0.2,3.3,0.5,4.5,0.8L422.4,110.7z M419.8,98.4c-0.6-0.2-1.2-0.3-2.1-0.4
                                                        c-1.7-0.2-3.7,0-3.7,1.9c0,1.7,2.2,2.6,3.7,2.8c1,0.1,1.6,0.2,2.1,0.2L419.8,98.4z"/>
                                    <path d="M435.7,106.2l-1.9,5.7l-2.6-0.3l6.4-18.2l3,0.3l6.8,19.7l-2.7-0.3l-2.1-6.2L435.7,106.2z M442,105.1l-1.9-5.7
                                                        c-0.4-1.3-0.7-2.4-1-3.6l-0.1,0c-0.3,1.1-0.6,2.2-0.9,3.3l-1.8,5.3L442,105.1z"/>
                                </g>
                            </g>
                            <g id="Group_3_">
                                <g>
                                    <path d="M665.9,78.8c-0.9,0.6-2.8,1.2-5.2,1.5c-5.5,0.7-9.4-2.2-9.1-8.4c0.3-5.9,4.6-10.5,10.5-11.2c2.4-0.3,3.9,0,4.5,0.3
                                                        L666,63c-0.9-0.3-2.3-0.5-3.9-0.3c-4.5,0.6-7.7,3.8-7.9,8.7c-0.2,4.6,2.4,7.3,7.1,6.7c1.5-0.2,3.1-0.7,4.1-1.3L665.9,78.8z"/>
                                    <path d="M670.1,72.5c-0.1,3.3,2,4.5,4.6,4.1c1.8-0.2,2.9-0.7,3.9-1.2l0.4,1.7c-0.9,0.5-2.4,1.1-4.7,1.4c-4.3,0.5-6.7-1.9-6.5-6
                                                        c0.2-4.1,2.8-7.6,6.8-8.1c4.5-0.6,5.6,3.2,5.4,5.7c0,0.5-0.1,0.9-0.1,1.2L670.1,72.5z M677.6,69.9c0.1-1.6-0.5-3.9-3.3-3.6
                                                        c-2.5,0.3-3.8,2.8-4,4.5L677.6,69.9z"/>
                                    <path d="M685.9,63.4l-0.2,5.9l0.6-0.1l5.4-6.5l3.1-0.4l-6.3,6.9c2.2,0,3.5,1.3,4.5,3.6c0.3,0.9,0.8,2.1,1.4,3.1l-2.6,0.3
                                                        c-0.3-0.5-0.8-1.6-1.2-2.6c-0.9-2.1-2.1-2.9-4.5-2.6l-0.5,0.1l-0.2,6l-2.5,0.3l0.6-13.6L685.9,63.4z"/>
                                    <path d="M699.5,61.7L699,73.4l6.4-0.8l0.4-11.7l2.5-0.3l-0.4,11.8l1.4-0.1l-0.4,6.2l-1.9,0.2l0-4.3l-10.6,1.3L697,62L699.5,61.7z
                                                        "/>
                                    <path d="M715.1,59.8l-0.2,5.7c-0.1,2.2-0.1,3.5-0.3,5.5l0.1,0c0.9-1.8,1.4-2.8,2.4-4.5l4.5-7.6l2.9-0.4L724,72.3l-2.4,0.3
                                                        l0.2-5.8c0.1-2.2,0.2-3.4,0.4-5.6l-0.1,0c-0.9,1.9-1.6,3-2.4,4.5c-1.4,2.3-3,5.1-4.6,7.6l-2.9,0.4l0.5-13.6L715.1,59.8z"/>
                                    <path d="M738.3,70.5l-2.5,0.3l0.2-5.7l-1.9,0.2c-1.1,0.1-1.8,0.5-2.3,1c-1.1,0.9-1.5,2.7-2.1,4.1c-0.2,0.4-0.4,0.8-0.6,1.2
                                                        l-2.7,0.3c0.3-0.5,0.6-1,0.9-1.7c0.6-1.4,1-3.2,2.3-4.4c0.4-0.4,1-0.8,1.6-1l0-0.1c-1.8,0-3.6-0.8-3.5-2.9c0-1.4,0.8-2.5,1.9-3.2
                                                        c1.2-0.8,2.9-1.3,4.6-1.5c1.7-0.2,3.2-0.3,4.4-0.3L738.3,70.5z M736.2,58.9c-0.6,0-1.2,0-2.1,0.1c-1.7,0.2-3.8,0.9-3.8,2.8
                                                        c-0.1,1.7,2.1,2.1,3.6,1.9c1-0.1,1.6-0.2,2.1-0.3L736.2,58.9z"/>
                                    <path d="M759.8,49l-0.1,2.1l-8.3,1l-0.1,5.1c0.6-0.2,1.7-0.4,2.4-0.4c1.8-0.2,3.6-0.1,4.9,0.7c1.4,0.7,2.4,2.1,2.3,4.3
                                                        c0,1.8-0.7,3.2-1.7,4.3c-1.6,1.7-4.2,2.6-6.8,2.9c-1.7,0.2-3.1,0.3-3.9,0.3l0.5-18.9L759.8,49z M751.1,67.1c0.6,0,1.2,0,2.1-0.1
                                                        c1.5-0.2,3.1-0.9,4.1-2c0.7-0.8,1.1-1.7,1.1-2.9c0-1.5-0.6-2.4-1.5-2.9c-1-0.6-2.3-0.7-3.6-0.5c-0.6,0.1-1.3,0.2-2,0.4
                                                        L751.1,67.1z"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <div class="korpus_svg_popover">
                        <h4><span class="korpus_number">Секция 1</span></h4>
                        <div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv1">
                                <span>1-но комнатная</span><span>от 3.2 ₽</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv2">
                                <span>2-х комнатная</span><span>от 8.1 ₽</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv3">
                                <span>3-х комнатная</span><span>от 10.2 ₽</span>
                            </div>
                           <div class="korpus_svg_popover_item korpus_svg_popover_item_kv4">
                              <span></span><span></span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_prod">
                                <div><span>12</span>продано</div>
                                <div><span>35</span>забронировано</div>
                                <div><span>93</span>свободно</div>
                            </div>
                            <div class="korpus_svg_popover_item">
                                <span>Начало строительства</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_zaselenie"><span>Заселение в ноябре 2018 года</span>
                            </div>
                        </div>
                    </div>
                    <div class="korpus_svg_hint">
                        <img src="/front/img/handcur.svg" alt="Наведите курсор на корпус"><span><span></span>Выберите корпус для дополнительной информации</span>
                    </div>
                </div>
                <section class="about_house text-center">
                    <?php echo $__env->make('layouts.front.objects.object_gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('layouts.front.objects.object_4.description', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </section>
            </div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('data'); ?>
    <span class="korp_holder hidden">{<?php $__currentLoopData = $object->sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"<?php echo $section->id; ?>":[<?php echo e($section->getPrice(1)); ?>,<?php echo e($section->getPrice(2)); ?>,<?php echo e($section->getPrice(3)); ?>, 0,<?php echo e($section->soldFlats()->count()); ?> ,<?php echo e($section->bookedFlats()->count()); ?>, <?php echo e($section->freeFlats()->count()); ?>, "<?php echo $section->status; ?>", "<?php echo $section->additional; ?>","Секция <?php echo e($section->symbol); ?>"]<?php if($section->id != 9): ?>, <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>}</span>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        $('.b-sec').hover(
        function(){
            $('.korpus_svg_popover_item_kv3').hide();
        },
        function(){
            $('.korpus_svg_popover_item_kv3').show();
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>