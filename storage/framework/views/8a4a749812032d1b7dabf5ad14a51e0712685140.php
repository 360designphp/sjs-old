<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование этажа</h1><?php if($floor->id): ?>
                        &nbsp;<a href="/objects/<?php echo e($floor->object()->id); ?>/<?php echo e($floor->building()->id); ?>/<?php echo e($floor->section->id); ?>/<?php echo $floor->id; ?>" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    <?php endif; ?>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/floor/save" name="edited_flat" method="post" id="edited_floor"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo e($floor->id); ?>" name="id">
                                <?php echo e(csrf_field()); ?>

                                <label class="col-xs-12">Этаж
                                    <input type="text" class="form-control  mb20"
                                           value="<?php echo ($floor->level)?$floor->level:"0"; ?>"
                                           name="level"
                                           placeholder="Введите этаж..." required>
                                </label>
                                <label class="col-xs-12">Номер секции
                                    <input type="text" class="form-control  mb20"
                                           value="<?php echo ($floor->section_id)?$floor->section_id:""; ?>"
                                           name="section_id"
                                           placeholder="Введите номер секции..." required>
                                </label>
                                <div class="col-xs-6">
                                    <label>Тип этажа
                                        <div class="radio"><label><input type="radio" name="type"
                                                                         value="living"
                                                                         <?php if($floor->type == 'living' || $floor->id == 'new'): ?> checked <?php endif; ?>
                                                >Жилой</label></div>
                                        <div class="radio"><label><input type="radio" name="type" value="commercial"
                                                                         <?php if($floor->type == 'commercial'): ?> checked <?php endif; ?>>Коммерческий</label>
                                        </div>
                                        <div class="radio"><label><input type="radio" name="type" value="parking"
                                                                         <?php if($floor->type == 'parking'): ?> checked <?php endif; ?>>Паркинг</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-xs-6" style="width: 40%">
                                     <label>Этаж готов
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="ready_status" value="1" <?php if($floor->ready_status == 1|| $floor->id == 'new'): ?> checked <?php endif; ?>>
                                                Да</label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="ready_status" value="0" <?php if($floor->ready_status == 0): ?> checked <?php endif; ?>>
                                            Нет</label>
                                        </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO<?php echo e($floor->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO<?php echo e($floor->id); ?>"
                                                       href="#collapseObject<?php echo e($floor->id); ?>"><h2>Служебная
                                                            информация</h2></a>
                                                    <div class="accordion-body">
                                                        <table class="table mb0">
                                                            <thead>
                                                            </thead>
                                                        </table>
                                                        <div id="collapseObject<?php echo e($floor->id); ?>" class="collapse">
                                                            <h3 class="mt0">Код SVG</h3>
                                                            <textarea name="svg" class="form-control" cols="1"
                                                                      rows="1"><?php echo e($floor->svg); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionB<?php echo e($floor->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionB<?php echo e($floor->id); ?>"
                                                       href="#collapseBuilding<?php echo e($floor->id); ?>"><h2>SEO - настройки</h2>
                                                    </a>
                                                    <div class="accordion-body">
                                                        <div id="collapseBuilding<?php echo e($floor->id); ?>" class="collapse">
                                                            <span class="h3">Ключевые слова</span>
                                                            <input type="text" name="seo_keywords" id="keywords"
                                                                   class="form-control"
                                                                   value="<?php echo $floor->seo_keywords; ?>"><br>
                                                            <span class="h3">Мета-описание</span>
                                                            <textarea name="seo_description" class="form-control"
                                                                      cols="30"
                                                                      rows="10"><?php echo e($floor->seo_description); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>
                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd><?php echo $floor->created_at; ?></dd>

                                        <dt>Последняя редакция</dt>
                                        <dd><?php echo $floor->updated_at; ?></dd>
                                    </dl>
                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_floor"
                                               value="Сохранить">
                                        <?php if($floor->id): ?>
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('<?php echo e($floor->id); ?>')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">План этажа</h3>
                                    <?php if($floor->image != null): ?>
                                        <img src=" /front/floors/<?php echo e($floor->image); ?>" alt="" class="img-responsive"
                                             height="100">
                                    <?php else: ?>
                                        <img src=" http://placehold.it/350x200" alt="" class="img-responsive">
                                    <?php endif; ?>

                                    <div class="panel-footer">
                                        <input type="file" class="pull-right btn btn-info" name="file"
                                               form="edited_floor" value="Обновить">
                                        <?php if($floor->image): ?>
                                            <button type="button" class="pull-right btn btn-danger"
                                                    onclick="deleteFloorImage(<?php echo $floor->id; ?>)">Удалить
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer role="contentinfo">
                    <div class="clearfix">
                        <ul class="list-unstyled list-inline pull-left">
                            <li><h6 style="margin: 0;">360 CMS</h6></li>
                        </ul>
                        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                                    class="fa fa-arrow-up"></i></button>
                    </div>
                </footer>
            </div>
            <?php $__env->stopSection(); ?>
            <?php $__env->startSection('script'); ?>
                <script>
                    function deleteFloorImage(id) {
                        if (confirm('Вы уверены?') == true) {
                            $.ajax({
                                url: '/api/ajax',
                                type: 'POST',
                                data: ({
                                    'id': id,
                                    'intent': 'deleteFloorImage'
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function () {
                                    $('#img' + id).fadeOut(400);
                                    $('#text' + id).fadeOut(400);
                                }
                            })
                        }
                        window.location.reload();
                    }
                    function errorHandler(data) {
                        alert('Ошибка :' + data.status);
                    }
                </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>