<?php $__env->startSection('content'); ?>

    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_text">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center"><?php echo e($post->title); ?></h1>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12 col-sm-5">
                <?php $__currentLoopData = $post->gallery(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <a href="/front/images/<?php echo $gal->slug; ?>" title="<?php echo e($gal->title); ?>"  data-title="<?php echo e($gal->title); ?>" rel="lightbox[gallery]"><img class="img-responsive mgb20" src="/front/thumbnail/<?php echo $gal->slug; ?>" alt=""/></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </div>
            <div class="col-xs-12 col-sm-7">
                <?php echo $post->content; ?>

                <div class="doc">
                    <?php if($post->documents()->count() != 0): ?><a href="/documents/<?php echo e($post->id); ?>/post" target="_blank">Журнал изменений</a><?php endif; ?>

                <?php if($post->documents() != null): ?>
                        <ul class="about_opisanie_docs list-unstyled">
                            <?php $__currentLoopData = $post->documents(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <li class="file_icon_<?php echo e(mb_strtolower($document->content)); ?>"><a href="/front/documents/<?php echo e($document->slug); ?>" target="_blank"><?php echo e($document->title); ?></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>

            <div class="vertical-devider"></div>
            <div class="col-xs-12">
                <?php echo $post->additional; ?>

            </div>
            <div class="vertical-devider"></div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>