<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Информация о клиентах - версия для печати</title>
    <style>
        body {
            font-size: 14px;
            font-family: tahoma;
        }

        h2, h3{
            text-align: center;
        }

        table {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

        td {
            border: 1px solid #000;
            padding: 5px 10px;
            margin: 0;
        }

        thead td {
            font-weight: bold;
        }
        .flat-link {
            float:left;
            font-size: 14px;
        }

    </style>
</head>
<body onload="printit()">
<h3 style="margin:0 auto;"><?php echo e($description); ?></h3>
<hr>
<table>
    <tr>
        <th style="width: 450px;">Имя</th>
        <th style="width: 150px;">Телефон</th>
        <th> Квартира</th>
    </tr>
    <?php $__currentLoopData = $flats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr>
            <td><?php echo e($flat->customer_name); ?></td>
            <td><?php echo e($flat->customer_phone); ?></td>
            <td>
                <div class="flat-link">
                    <a href="/admin/objects/flat/<?php echo e($flat->id); ?>"><?php echo e($flat->getObject()->name); ?> <?php echo e($flat->getFloorSymbol()); ?>: <?php echo e($flat->number); ?></a>
                </div> 
                <?php if(!isset($status)): ?>
                <div style="text-align:right;">
                    <b><?php echo e($flat->getStrStatus()); ?></b>
                </div>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</table>
<script>
    function printit() {
        if (window.print) {
            window.print();
        } else {
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(6, 2);//Use a 1 vs. a 2 for a prompting dialog box WebBrowser1.outerHTML = "";
        }
    }
</script>
</body>
</html>