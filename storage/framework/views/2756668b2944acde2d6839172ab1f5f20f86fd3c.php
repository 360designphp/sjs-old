<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <?php echo $__env->make('layouts.front.objects.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('layouts.front.objects.filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row searches_list">
            <?php if(count($flats) == 0): ?>
                <h1 class="align-">По данным параметрам не найдено предложений</h1>
            <?php endif; ?>
            <?php $__currentLoopData = $flats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="col-xs-6 col-sm-4 col-lg-3 search_item">
                    <div target="_blank" class="search_item_inner_wrap clearfix">
                        <a href="/objects/<?php echo e($flat->getObject()->id); ?>/<?php echo e($flat->building()->id); ?>/<?php echo e($flat->section()->id); ?>/<?php echo e($flat->floor->id); ?>/#kv<?php echo e($flat->number); ?>"
                           class="no_underline" target="_blank">
                            <div class="search_item_inner_top">
                                <div><?php echo e($flat->getObject()->name); ?></div>
                            </div>
                            <div class="search_item_inner_img">
                                <?php if($flat->status == 'free'): ?><span class="search_item_inner_label color1">Свободно</span><?php endif; ?>
                                <?php if($flat->status == 'ready'): ?><span class="search_item_inner_label color4">Забронировано</span><?php endif; ?>
                                <?php if($flat->status == 'sold'): ?><span class="search_item_inner_label color2">Продано</span><?php endif; ?>
                                <?php if($flat->floor->type !='parking'): ?>
                                    <img src="/front/flats/<?php echo e($flat->image); ?>" alt="" class="img-responsive">
                                <?php else: ?>
                                    <?php if($flat->price == 350000 || $flat->price <= 100): ?>
                                        <img src="/front/img/park_sm.png" alt="" class="img-responsive">
                                    <?php endif; ?>
                                    <?php if($flat->price == 450000): ?>
                                        <img src="/front/img/park_md.png" alt="" class="img-responsive">
                                    <?php endif; ?>
                                    <?php if($flat->price == 500000): ?>
                                        <img src="/front/img/parklg.png" alt="" class="img-responsive">
                                    <?php endif; ?>
                                <?php endif; ?>

                                <div class="search_item_inner_info">
                                <!-- <span class="search_item_inner_span_obj"><?php echo e($flat->getObject()->name); ?></span> -->
                                    <span class="search_item_inner_span_sect">Секция: <?php echo e($flat->section()->symbol); ?></span>
                                    <span class="search_item_inner_span_etaj">Этаж: <?php echo e($flat->floor->level); ?></span>
                                    <?php if($flat->floor->type == 'living'): ?><span class="search_item_inner_span_kvart">Квартира: <?php echo e($flat->number); ?></span><?php endif; ?>
                                    <?php if($flat->floor->type == 'commercial'): ?><span class="search_item_inner_span_kvart">Офис: <?php echo e($flat->number); ?></span><?php endif; ?>
                                    <?php if($flat->floor->type == 'parking'): ?><span class="search_item_inner_span_kvart">Парковочное место: <?php echo e($flat->number); ?></span><?php endif; ?>
                                    <?php if($flat->floor->type != 'parking'): ?> <span class="search_item_inner_span_area">Площадь: <?php echo e($flat->area); ?>

                                        м<sup>2</sup></span><?php endif; ?>
                                    <span class="search_item_inner_span_price">Цена: <?php echo e($flat->price); ?><span class="ruble"> q</span></span>
                                </div>
                            </div>
                            <div class="search_item_inner_bottom no_underline">
                                Подробнее <span class="shev_right"></span>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="vertical-devider"></div>
    </main>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>