<?php $__env->startSection('content'); ?>

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Список пользователей</h1>

                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" class="form-inline mb10">

                            </form>

                            <div class="panel">
                                <div class="panel-body panel-no-padding">

                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Имя</th>
                                            <th>Почта</th>
                                            <th>Статус</th>
                                            <th width="150">Действия</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <tr>
                                                <td><?php echo e($user->name); ?></td>
                                                <td><?php echo e($user->email); ?></td>
                                                <td><?php echo e($user->role == 'Admin'?'Администратор':'Пользователь'); ?></td>
                                                <td>
                                                    <a href="/admin/delete_user/<?php echo e($user->id); ?>" class="btn btn-danger">Удалить</a>
                                                    <a href="/admin/user/<?php echo e($user->id); ?>" class="btn btn-warning">Редакт.</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>