<div class="static-sidebar-wrapper sidebar-midnightblue">
    <div class="static-sidebar">
        <div class="sidebar">
            <div class="widget stay-on-collapse" id="widget-welcomebox">
                <div class="widget-body welcome-box tabular">
                    <div class="tabular-row">
                       
                        <div class="tabular-cell welcome-options">
                            <span class="welcome-text">Здравствуйте,</span>
                        <a href="/admin/user/<?php echo e(Auth::user()->id); ?>/" class="name"><?php echo Auth::user()->name; ?></a>
                        <a href="/logout" class=""> Выход </a>
                        
                        
                      
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="widget stay-on-collapse" id="widget-sidebar">
                <nav role="navigation" class="widget-body">
                    <ul class="acc-menu">
                        <li class="nav-separator">Меню</li>
                        <!--<li><a href="/admin"><i class="fa fa-inbox"></i><span>Главная</span></a></li>-->
                        <li><a href="javascript:;"><i class="fa fa-book"></i><span>Заявки</span><?php if(isset($unread) && $unread > 0): ?><span class="badge badge-danger"><?php echo e($unread); ?></span><?php endif; ?></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/application/recall">Звонок <?php if(isset($unread_rec) && $unread_rec > 0): ?><span class="badge badge-danger"><?php echo e($unread_rec); ?></span><?php endif; ?></a></li>
                                <li><a href="/admin/application/booking">Бронь <?php if(isset($unread_book) && $unread_book > 0): ?><span class="badge badge-danger"><?php echo e($unread_book); ?></span><?php endif; ?></a></li>
                                <li><a href="/admin/application/question">Вопрос <?php if(isset($unread_faq) && $unread_faq > 0): ?><span class="badge badge-danger"><?php echo e($unread_faq); ?></span><?php endif; ?></a></li>
                                <li><a href="/admin/application/archive">Архив</a></li>
                            </ul>
                        </li>
                        <li><a href="/admin/objects"><i class="fa fa-building"></i><span>Объекты</span></a></li>
                        <li><a href="/admin/clients"><i class="fa fa-address-card" aria-hidden="true"></i><span>Клиентская база</span></a></li>
                        <li><a href="javascript:;"><i class="fa fa-folder-open"></i><span>Новости</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/news/edit/new">Создать новость</a></li>
                                <li><a href="/admin/news/list">Новости</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-folder-open"></i><span>Пресс-центр</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/press-centre/edit/new">Создать публикацию</a></li>
                                <li><a href="/admin/press-centre/list">Публикации</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-newspaper-o"></i><span>Акции</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/action/edit/new">Создать акцию</a></li>
                                <li><a href="/admin/action/list">Все акции</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-newspaper-o"></i><span>Страницы</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/page/edit/new">Создать  страницу</a></li>
                                <li><a href="/admin/page/list">Все  страницы</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-file-text-o"></i><span>Категории</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/category/edit/new">Создать категорию</a></li>
                                <li><a href="/admin/category/list">Список категорий</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-comment-o"></i><span>Вопрос-ответ</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/faq/list">Все вопросы</a></li>
                                <li><a href="/admin/faq/edit/new">Добавить вопрос</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-picture-o"></i><span>Слайды</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/slide/edit/new">Создать слайд</a></li>
                                <li><a href="/admin/slide/list">Все Слайды</a></li>
                            </ul>
                        </li>
                        <!--li><a href="/admin/gallery"><i class="fa fa-camera-retro"></i><span>Фотогалерея</span></a></li-->
                        <li><a href="/admin/menu"><i class="fa fa-th-list"></i><span>Меню</span></a></li>
                        <li><a href="/admin/settings"><i class="fa fa-wrench"></i><span>Настройки</span></a></li>
                        <li><a href="javascript:;"><i class="fa fa-users"></i><span>Пользователи</span></a>
                            <ul class="acc-menu">
                                <li><a href="/admin/user">Список пользователей</a></li>
                                <li><a href="/admin/register">Создать пользователя</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>