<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_korpus">
        <div class="row">
            <div class="col-xs-12 korpus_svg_wrap">
                <div class="small-vertical-devider"></div>
                <div class="row">
                    <div class="col-xs-12 text-center breadcrumbs">
                        <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                        <span class="h4"><?php echo e($object->name); ?></span>
                    </div>
                </div>
                <div class="small-vertical-devider"></div>
                <div class="korpus_svg_holder">
                    <img src="/front/objects/prospect_img.jpg" class="image-responsive" alt="">
               <!--      <svg version="1.1" class="collectivnaya_layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                x="0px" y="0px" viewBox="0 0 1800 1173" style="enable-background:new 0 0 1800 1173;" xml:space="preserve">
                               <image style="overflow:visible;" width="1800" height="1173" class="prospect_img" xlink:href="/front/objects/prospect_img.jpg" >
                               </image>
                               </svg> -->

                    <div class="korpus_svg_popover">
                        <h4><span class="korpus_number">Секция 1</span></h4>
                        <div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv1">
                                <span>1-но комнатная</span><span>от 3.2 ₽</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv2">
                                <span>2-х комнатная</span><span>от 8.1 ₽</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_kv3">
                                <span>3-х комнатная</span><span>от 10.2 ₽</span>
                            </div>
                           <div class="korpus_svg_popover_item korpus_svg_popover_item_kv4">
                              <span></span><span></span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_prod">
                                <div><span>12</span>продано</div>
                                <div><span>35</span>забронировано</div>
                                <div><span>93</span>свободно</div>
                            </div>
                            <div class="korpus_svg_popover_item">
                                <span>Начало строительства</span>
                            </div>
                            <div class="korpus_svg_popover_item korpus_svg_popover_item_zaselenie"><span>Заселение в ноябре 2018 года</span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="korpus_svg_hint">
                        <img src="/front/img/handcur.svg" alt="Наведите курсор на корпус"><span><span></span>Выберите корпус для дополнительной информации</span>
                    </div> -->
                </div>
                <section class="about_house text-center">
                    <?php echo $__env->make('layouts.front.objects.object_gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('layouts.front.objects.object_6.description', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </section>
            </div>
        </div>
    </main>
<?php $__env->stopSection(); ?>
<?php if(false): ?>
<?php $__env->startSection('data'); ?>
    <span class="korp_holder hidden">{<?php $__currentLoopData = $object->sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"<?php echo $section->id; ?>":[<?php echo e($section->getPrice(1)); ?>,<?php echo e($section->getPrice(2)); ?>,<?php echo e($section->getPrice(3)); ?>, 0,<?php echo e($section->soldFlats()->count()); ?> ,<?php echo e($section->bookedFlats()->count()); ?>, <?php echo e($section->freeFlats()->count()); ?>, "<?php echo $section->status; ?>", "<?php echo $section->additional; ?>","Секция <?php echo e($section->symbol); ?>"]<?php if($section->id != 9): ?>, <?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>}</span>
<?php $__env->stopSection(); ?>
<?php endif; ?>
<?php $__env->startSection('scripts'); ?>
    <script>
        $('.b-sec').hover(
        function(){
            $('.korpus_svg_popover_item_kv3').hide();
        },
        function(){
            $('.korpus_svg_popover_item_kv3').show();
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>