<nav class="main_filter
<?php if(!isset($param['type']) || $param['type'] == 1): ?><?php echo e('selected1'); ?><?php endif; ?>
<?php if(isset($param['type']) && $param['type'] == 2): ?><?php echo e('selected2'); ?><?php endif; ?>
<?php if(isset($param['type']) && $param['type'] == 3): ?><?php echo e('selected3'); ?><?php endif; ?>
        ">
    <form action="/objects/filtered" method="GET" class="form-inline" role="form" id="filter">
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="line_vertical"></div>
        <div class="select_cutom main_filter_selection">
            <select name="type" id="inputselcom" class="form-control">
                <option value="1"  <?php if(isset($param['type']) && $param['type'] == 1): ?><?php echo e('selected'); ?><?php endif; ?>>Жилая недвижимость</option>
                <option value="2" <?php if(isset($param['type']) && $param['type'] == 2): ?><?php echo e('selected'); ?><?php endif; ?>>Коммерческая</option>
                <option value="3" <?php if(isset($param['type']) && $param['type'] == 3): ?><?php echo e('selected'); ?><?php endif; ?>>Паркинг</option>
            </select>
        </div>
        <div class="select_cutom main_filter_rooms">
            <select name="rooms" id="selrooms" class="form-control">
                <option value="all" <?php if(isset($param['rooms']) && $param['rooms'] == 'all'): ?><?php echo e('selected'); ?><?php endif; ?>>Количество комнат</option>
                <option value="1" <?php if(isset($param['rooms']) && $param['rooms'] == 1): ?><?php echo e('selected'); ?><?php endif; ?>>1-комната</option>
                <option value="2" <?php if(isset($param['rooms']) && $param['rooms'] == 2): ?><?php echo e('selected'); ?><?php endif; ?>>2-комнаты</option>
                <option value="3" <?php if(isset($param['rooms']) && $param['rooms'] == 3): ?><?php echo e('selected'); ?><?php endif; ?>>3-комнаты</option>
            </select>
        </div>

        <div class="main_filter_squares">
            Площадь&nbsp;от&nbsp;<input type="number" name="area_from" id="" value="<?php if(isset($param['area_from'])): ?><?php echo e($param['area_from']); ?><?php else: ?><?php echo e(1); ?><?php endif; ?>"> до&nbsp;<input type="number" name="area_to" id="" value="<?php if(isset($param['area_to'])): ?><?php echo e($param['area_to']); ?><?php else: ?><?php echo e(500); ?><?php endif; ?>">&nbsp;м<sup>2</sup>
        </div>

        <div class="main_filter_etaj">
            Этаж&nbsp;от&nbsp;<input type="number" name="floor_from" id="" value="<?php if(isset($param['floor_from'])): ?><?php echo e($param['floor_from']); ?><?php else: ?><?php echo e(1); ?><?php endif; ?>"> до&nbsp;<input type="number" name="floor_to" id="" value="<?php if(isset($param['floor_to'])): ?><?php echo e($param['floor_to']); ?><?php else: ?><?php echo e(20); ?><?php endif; ?>">
        </div>

        <div class="select_cutom main_filter_place">
            <select name="object" id="selrooms" class="form-control">
                <option value="">Объект</option>
                <?php $__currentLoopData = $objects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <option value="<?php echo e($object->id); ?>" <?php if(isset($param['object']) && $param['object'] == $object->id): ?><?php echo e('selected'); ?><?php endif; ?>><?php echo e($object->name); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <div class="select_cutom main_filter_sdelka">
            Тип сделки:&nbsp;&nbsp;
            <select name="deal" id="selsdelka" class="form-control">
                <option value="buy">Покупка</option>
                <option value="rent">Аренда</option>
            </select>
        </div>
        <div class="select_cutom main_filter_carplace">
            <select name="parking_places" id="selrooms" class="form-control">
                <option value="1" <?php if(isset($param['parking_places']) && $param['parking_places'] == 1): ?><?php echo e('selected'); ?><?php endif; ?>>1-машиноместо</option>
                <option value="2" <?php if(isset($param['parking_places']) && $param['parking_places'] == 2): ?><?php echo e('selected'); ?><?php endif; ?>>2-машиноместа</option>
                <option value="3" <?php if(isset($param['parking_places']) && $param['parking_places'] == 3): ?><?php echo e('selected'); ?><?php endif; ?>>3-машиноместа</option>
                <option value="4" <?php if(isset($param['parking_places']) && $param['parking_places'] == 4): ?><?php echo e('selected'); ?><?php endif; ?>>4-машиноместа</option>
            </select>
        </div>
        <div class="nextline"></div>
        <div class="rangeslider_wrap">
            <div class="rangeslider_cap">
                <p>Стоимость,<span class="rub">a</span> </p>
            </div>
            <span id="range_sl1-label-2a" class="hidden">Example low value</span>
            <span id="range_sl1-label-2b" class="hidden">Example high value</span>
            <input id="range_sl1" type="text" />
            <span id='range_sl1_a'>0</span>
            <span id='range_sl1_b'>7 000 000</span>
        </div>
        <input type="hidden" class="price_from" name="price_from" <?php if(isset($param['price_from'])): ?> value="<?php echo e($param['price_from']); ?>" <?php else: ?> value="0" <?php endif; ?> >
        <input type="hidden" class="price_to" name="price_to" <?php if(isset($param['price_to'])): ?> value="<?php echo e($param['price_to']); ?>" <?php else: ?> value="7000000" <?php endif; ?> >
    </form>
    <div class="main_filter_button"><button type="submit" class="btn btn-primary" form="filter"></button></div>
</nav>