<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Настройки пользователя</h1>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body">
                                <form action="/admin/update_user/<?php echo e($user->id); ?>" class="form-horizontal row-border" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Имя пользователя</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $user->name; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email-пользователя</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_email"  name="user_email"  value="<?php echo $user->email; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Новый пароль</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="user_password" name="new_password" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Повторите пароль</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="password_repeat" name="confirm_new_password" value="">
                                        </div>
                                    </div>
                                    <select name="role" id="">
                                        <option value="User" <?php if($user->role == 'User'): ?>selected <?php endif; ?>>Пользователь</option>
                                        <option value="Admin" <?php if($user->role == 'Admin'): ?>selected <?php endif; ?>>Администратор</option>
                                    </select>
                                    <?php echo csrf_field(); ?>

                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <input type="submit" class="btn btn-primary">
                                        </div>
                                    </div>
                                </div>
                                </form>
                        </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>