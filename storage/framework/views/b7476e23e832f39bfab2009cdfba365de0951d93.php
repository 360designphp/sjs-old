<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_process">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Ход строительства: <?php echo e($object->name); ?></h1>
            </div>
            <div class="vertical-devider"></div>
            <?php $__currentLoopData = $queues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $number => $albums): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <?php if(count($albums) != null): ?>
                <?php if(count($queues) > 1): ?>
                    <div class="container">
                        <a class="h4" role="button" data-toggle="collapse" href="#collapseQueue<?php echo e($number); ?>"
                           aria-expanded="false" aria-controls="collapseExample">
                            Ход строительства <?php echo e($number); ?>-й очереди
                        </a>

                        <div class="collapse" id="collapseQueue<?php echo e($number); ?>">
                            <div>
                                <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <div class="col-xs-4 main_process_item">
                                        <a class="main_process_image_link"
                                           href="/objects/building_progress/<?php echo e($object->id); ?>/<?php echo e($album->slug); ?>"><img
                                                    class="img-responsive"
                                                    <?php if($album->thumbnail()): ?> src="/front/thumbnail/<?php echo e($album->thumbnail()->slug); ?>"
                                                    <?php endif; ?> alt=""/>
                                        </a>
                                        <div><?php echo e($album->title); ?><?php if($album->additional): ?>
                                                <span><?php echo $album->additional; ?></span><?php endif; ?></div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                     <div>
                                <?php $__currentLoopData = $albums; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $album): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <div class="col-xs-4 main_process_item">
                                        <a class="main_process_image_link"
                                           href="/objects/building_progress/<?php echo e($object->id); ?>/<?php echo e($album->slug); ?>"><img
                                                    class="img-responsive"
                                                    <?php if($album->thumbnail()): ?> src="/front/thumbnail/<?php echo e($album->thumbnail()->slug); ?>"
                                                    <?php endif; ?> alt=""/>
                                        </a>
                                        <div><?php echo e($album->title); ?><?php if($album->additional): ?>
                                                <span><?php echo $album->additional; ?></span><?php endif; ?></div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <div class="vertical-devider"></div>
            <div class="main_process_back">
                <a href="/objects/<?php echo e($object->id); ?>" class="btn btn-default btn-kvart">Вернуться к объекту</a>
            </div>
            <div class="vertical-devider"></div>
        </div>
        </div>
    </main>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>