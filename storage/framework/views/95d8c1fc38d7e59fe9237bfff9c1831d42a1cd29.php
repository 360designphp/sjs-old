<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_kvartplan">
        <div class="vertical-devider"></div>
        <div class="row bottom_border">

            <div class="col-xs-3 linkleft">
                <?php if($section->previousSectionLink($floor) != 'minimal'): ?>
               <?php echo $section->previousSectionLink($floor); ?>

                <?php endif; ?>
            </div>

            <div class="col-xs-6 text-center breadcrumbs">
                <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                <a href="/objects/<?php echo e($floor->object()->id); ?>"><span class="h4"><?php echo e($floor->object()->name); ?></span></a> >
                <?php if($floor->building()->number != 0): ?>
                <a href="/objects/<?php echo e($floor->object()->id); ?>/<?php echo e($floor->building()->id); ?>"><span class="h4">Корпус <?php echo e($floor->building()->number); ?></span></a> >
                <?php endif; ?>
                <?php if(View::exists('layouts.front.objects.object_' . $floor->object()->id . '.section_'.$floor->section->id)): ?>
                <a href="/objects/<?php echo e($floor->object()->id); ?>/<?php echo e($floor->building()->id); ?>/<?php echo e($floor->section->id); ?>"><span class="h4">Секция <?php echo e($floor->section->symbol); ?></span></a> > <?php echo e($floor->level); ?> этаж</span>
                <?php else: ?>
                <span class="h4">Секция <?php echo e($section->symbol); ?>, <?php echo e($floor->level); ?> этаж</span>
                <?php endif; ?>
            </div>


            <div class="col-xs-3 linkright">
                <?php if($section->nextSectionLink($floor) != 'maximal'): ?>
                    <?php echo $section->nextSectionLink($floor); ?>

                <?php endif; ?>
            </div>
            <div class="small-vertical-devider"></div>
        </div>
            <div class="col-xs-12 kvart_svg_wrap">
                <div class="kvart_svg_holder">
                        <?php echo $floor->svg; ?>

                </div>
                <div class="kvartplan_etaji">
                    <p>Этаж</p>
                    <ul class="list-unstyled">
                        <li class="arrup"><a href="/objects/<?php echo e($object->id); ?>/<?php echo e($section->building->id); ?>/<?php echo e($section->id); ?>/<?php echo e($floor->nextFloor()); ?>" class="arrow_kvart" <?php if($floor->nextFloor() == 'maximal'): ?>style="pointer-events:none;"<?php endif; ?>></a></li>
                        <?php $__currentLoopData = $section->floorsOrderedDesc(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $selector_floor): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <li <?php if($floor->level == $selector_floor->level): ?>class="active"<?php endif; ?>><a href="/objects/<?php echo e($object->id); ?>/<?php echo e($section->building->id); ?>/<?php echo e($section->id); ?>/<?php echo e($selector_floor->id); ?>" class="etaj_link"><?php echo e($selector_floor->level); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <li class="arrdown"><a href="/objects/<?php echo e($object->id); ?>/<?php echo e($section->building->id); ?>/<?php echo e($section->id); ?>/<?php echo e($floor->previousFloor()); ?>" class="arrow_kvart" <?php if($floor->previousFloor() == 'minimal'): ?>style="pointer-events:none;"<?php endif; ?>></a></li>

                    </ul>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12 kvart_legend">
                <span class=leg_green>Свободно</span>
                <span class=leg_orange>Забронировано</span>
                <span class=leg_red>Продано</span>
                <div class="small-vertical-devider"></div>
            </div>
            <div class="col-xs-12 kvart_about">
                <?php if($floor->type != 'parking'): ?><div>Комнат <span class="kvart_amount_rooms">-</span></div><?php endif; ?>
                <div>Площадь, кв.м<span class="kvart_amount_squares">-</span></div>
                <div>Стоимость<span class="kvart_amount_price">-</span></div>
                <?php if($floor->type != 'living'): ?><div>Арендная плата<span class="kvart_amount_rent"></span></div><?php endif; ?>
                <div>Номер  <?php if($floor->type == 'living'): ?>квартиры <?php endif; ?>  <?php if($floor->type =='commercial'): ?>помещения <?php endif; ?>  <?php if($floor->type == 'parking'): ?>парковки <?php endif; ?><span class="kvart_amount_number">-</span></div>
                <div>Статус<span class="kvart_amount_status"></span></div>
                <?php if($floor->type != 'parking'): ?><div><a type="button" class="btn btn-default" href="" data-lightbox="test-kvart">Посмотреть план</a></div> <?php endif; ?>
                <div><a type="button" class="btn btn-default btn-panorama" disabled target="_blank">3D тур по квартире</a></div>
                <?php if(Auth::check() && Auth::user()->role == 'Admin'): ?>
                    <div><button type="button" class="btn btn_call_admin_zakaz btn-primary" data-toggle="modal" href='#modal_admin_zakaz'>Забронировать</button></div>
                <?php else: ?>
                    <div><button type="button" class="btn btn-default btn_call_zakaz" data-toggle="modal" href='#modal_zakaz'>Забронировать</button></div>
                <?php endif; ?>    
            </div>
            <div class="small-vertical-devider"></div>
            <section class="about_house text-center">
                <?php echo $__env->make('layouts.front.objects.object_'.$floor->object()->id.'.description', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->make('layouts.front.objects.object_gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </section>
        </div>
    </main>
    </div>
    </div>
    <span class="kv_holder hidden">
        {<?php $__currentLoopData = $floor->flats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            "kv<?php echo e($flat->number); ?>":[<?php echo e($flat->rooms); ?>,<?php echo e($flat->area); ?>,"<?php echo e($flat->price); ?> ₽",<?=$flat->getIntStatus()?>,"/front/flats/<?php echo e($flat->image); ?>",<?php echo e($flat->id); ?>, <?php echo e($flat->panorama_url ? '"'. $flat->panorama_url .'"' : '"null"'); ?>],
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> "image":"/front/floors/<?php echo e($floor->image); ?>"}
    </span>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('modal'); ?>
    <?php if(Auth::check() && Auth::user()->role == 'Admin'): ?>
    <div class="modal fade" id="modal_admin_zakaz">
        <div class="modal-dialog modal-lg modal_zakaz_dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center">Оформить бронь</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php if($floor->type != 'parking'): ?>
                        <div class="col-xs-12 col-sm-6">
                            <img src="img/44.jpg" class="img-responsive modal_img_kvart center-block" alt="изображение квартиры">
                        </div>
                        <div class="col-xs-12 col-sm-6 modal_zakaz_dialog_forms">
                        <?php else: ?>
                                <div class="col-xs-12 col-sm-12 modal_zakaz_dialog_forms">
                        <?php endif; ?>
                            <div class="vertical-devider"></div>
                            <label for="input_client_tel2">Телефон клиента<span class="red">*</span></label><input type="text" name="client_tel2" id="client_tel2" class="form-control client_phone"  required="required" title="" placeholder="+7 (123) 456-78-90">
                            <label for="input_client_name2">ФИО Клиента <span class="red">*</span></label><input type="text" name="client_name2" id="client_name2" class="form-control client_name"  required="required" title="">                
                            <input type="text" id="kvartid">
                            <div>
                                <label><input type="radio" class="kv_booking" name="kv_booking" value="sold">Продано</label>
                                <label><input type="radio" class="kv_booking" name="kv_booking" value="booked">Забронировано</label>
                                <label><input type="radio" class="kv_booking" name="kv_booking" value="free">Свободно</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn  btn-primary book" onclick="bookFlat()">Забронировать</button>
                    <button type="button" class="btn btn-default btn_kvart" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function bookFlat() {

            var phone = $('.client_phone').val();
            var name = $('.client_name').val();
            var status = $('.kv_booking').val();
            var flat_id = $('#kvartid').val();
            var has_error = false;

            if(phone == ''){
                $('.booking_phone').addClass('error');
                has_error = true;

            }
            if(name == ''){
                $('.booking_name').addClass('error');
                has_error = true;
            }
            alert(flat_id); 
            return false;
            if(has_error) {
                return false;
            }

            $.ajax({
                url: '/api/book_flat',
                type: 'post',
                data: {
                    'name': name,
                    'phone': phone,
                    'id':flat_id,
                    'status':'status
                },
                success: function () {
                    alert('Статус квартиры изменен');
                    $('#modal_admin_zakaz').modal('toggle');
                }
            })
        }
    </script>
    <?php else: ?>
        <div class="modal fade" id="modal_zakaz">
        <div class="modal-dialog modal-lg modal_zakaz_dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <?php if($floor->type == 'living'): ?>
                    <h4 class="modal-title text-center">Забронировать квартиру</h4>
                    <?php endif; ?>
                    <?php if($floor->type == 'commercial'): ?>
                    <h4 class="modal-title text-center">Забронировать помещение</h4>
                    <?php endif; ?>
                    <?php if($floor->type == 'parking'): ?>
                    <h4 class="modal-title text-center">Забронировать парковку</h4>
                    <?php endif; ?>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php if($floor->type != 'parking'): ?>
                        <div class="col-xs-12 col-sm-6">
                            <img src="img/44.jpg" class="img-responsive modal_img_kvart center-block" alt="изображение квартиры">
                        </div>
                        <div class="col-xs-12 col-sm-6 modal_zakaz_dialog_forms">
                        <?php else: ?>
                                <div class="col-xs-12 col-sm-12 modal_zakaz_dialog_forms">
                        <?php endif; ?>
                                    <h4>Оставьте заявку,<br> и мы с вами свяжемся</h4>
                                    <small>Поля, обозначенные звездочкой (<span class="red">*</span>), обязательны для заполнения</small>
                            <div class="vertical-devider"></div>
                            <label for="input_vash_tel2">Ваш телефон <span class="red">*</span></label><input type="text" name="vash_tel2" id="input_vash_tel2" class="form-control booking_phone"  required="required" title="" placeholder="+7 (123) 456-78-90">
                            <label for="input_vash_name2">Как к вам обращаться <span class="red">*</span></label><input type="text" name="vash_name2" id="input_vash_name2" class="form-control booking_name"  required="required" title="">
                            <?php if(isset($regions)): ?>
                                <label for="input_vash_region">Ваш регион <span class="red">*</span></label>
                                    <select class="selectpicker form-control input_region booking_region" data-live-search="true">
                                        <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <option><?php echo $val; ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>
                            <?php endif; ?>
                            <label for="input_vash_email2">Ваш e-mail адрес <span class="red">*</span></label><input type="email" name="vash_email2" id="input_vash_email2" class="form-control booking_email"  required="required" title="">
                            <div class="vertical-devider"></div>
                            Примечания
                            <textarea name="vash_text" id="inputvash_text" class="form-control booking_notes" rows="3" ></textarea>
                            <input type="text" id="kvartid">

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="checkbox_input booking_approve" value="on">
                                            <span class="red">*</span> Я согласен на обработку моих персональных данных
                                        </label>
                                        <a href="/page/legal" target="_blank">Подробнее...</a>
                                    </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn  btn-primary book" onclick="requestBooking();">Заказать</button>
                    <button type="button" class="btn btn-default btn_kvart" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function requestBooking() {
            $('input').removeClass('error');
            $('select').removeClass('error');

            var phone = $('.booking_phone').val();
            var name = $('.booking_name').val();
            var email = $('.booking_email').val();
            var region = $('.booking_region').val();
            var text = $('.booking_notes').val();
            var flat_id = $('#kvartid').val();
            var has_error = false;

            if(phone == ''){
                $('.booking_phone').addClass('error');
                has_error = true;

            }
            if(name == ''){
                $('.booking_name').addClass('error');
                has_error = true;
            }
            if(email == ''){
                $('.booking_email').addClass('error');
                has_error = true;
            }
            if(region == '— Выберите регион —'){
                $('.booking_region').addClass('error');
                has_error = true;
            }

            if(!$('.booking_approve').is(':checked') ){
                $('.booking_approve').addClass('error');
                has_error = true;
            }

            if(has_error) {
                return false;
            }

            $.ajax({
                url: '/api/recall_request',
                type: 'post',
                data: {
                    'name': name,
                    'phone': phone,
                    'email': email,
                    'region': region,
                    'text':text,
                    'flat_id':flat_id,
                    'type':'booking'
                },
                success: function () {
                    alert('Спасибо, мы получили вашу заявку. Менеджер свяжется с вами в ближайшее время');
                    $('#modal_zakaz').modal('toggle');
                }
            })
        }
    </script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>