<!doctype html>
<html lang="ru">
<head>
    <title>Журнал документов</title>
</head>
<body>
<style>
    body {
        font-size: 14px;
        font-family: tahoma;
    }

    h2 {
        text-align: center;
    }

    table {
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
    }

    td {
        border: 1px solid #000;
        padding: 1em;
        margin: 0;
    }

    thead td {
        font-weight: bold;
    }

</style>
<h2>Электронный журнал учета операций, выполненных с помощью программного обеспечения и технологических средств ведения
    официального сайта застройщика, позволяющие обеспечивать учет всех действий в отношении информации на официальном
    сайте застройщика, подтверждающих размещение соответствующей информации<br>
    <?php if(isset($object)): ?>
    Объект: <?php echo e($object->name); ?>  <?php if(!is_null($queue)): ?> очередь: <?php echo e($queue); ?> <?php endif; ?> 
    <?php endif; ?>
</h2>

<table>
    <thead>
    <tr>
        <td>Вид размещаемой информации</td>
        <td>Дата опубликования на сайте</td>
        <td>Лицо, разместившее информацию</td>
    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $documents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $document): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr>
            <td>
                <a href="/front/documents/<?php echo e($document->slug); ?>" target="_blank"><?php echo e($document->title); ?></a>
            </td>
            <td><?php echo e($document->created_at); ?></td>
            <td><?php if(is_object($document->getAuthor())): ?><?php echo e($document->getAuthor()->name); ?><?php endif; ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</table>
</body>
</html>