<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <div class="vertical-devider"></div>
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center">Вопросы и ответы</h1>
            </div>
            <div class="vertical-devider"></div>
            <div class="panel-group">
                <?php $__currentLoopData = $faqs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faq): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <h4 class="panel-title">
                            <a data-toggle="collapse" class="collapsed" href="#collapse<?php echo $faq->id; ?>"><?php echo e($faq->title); ?></a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $faq->id; ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php echo $faq->content; ?>

                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </div>
            <div class="vertical-devider"></div>
            <div class="col-xs-12 col-sm-12 main_faq_forms">
                <h4>Задайте ваш вопрос</h4>
                <small>Поля, обозначенные звездочкой (<span class="red">*</span>), обязательны для заполнения</small>
                <div class="vertical-devider"></div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <label for="input_vash_tel3">Ваш телефон <span class="red">*</span></label><input type="text" name="vash_tel3" id="input_vash_tel3" class="form-control faq_phone"  required="required" title="" placeholder="+7 (123) 456-78-90">
                        <label for="input_vash_name3">Как к вам обращаться <span class="red">*</span></label><input type="text" name="vash_name3" id="input_vash_name3" class="form-control faq_name"  required="required" title="">
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?php if(isset($regions)): ?>
                                <label for="faq_region">Ваш регион <span class="red">*</span></label>
                                <select class="selectpicker form-control faq_region" data-live-search="true" id="faq_region">
                                    <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option><?php echo $val; ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                        <?php endif; ?>
                        <label for="input_vash_email3">Ваш e-mail адрес <span class="red">*</span></label><input type="email" name="vash_email3" id="input_vash_name3" class="form-control faq_email"  required="required" title="">
                    </div>
                    <div class="col-xs-12">
                        <div class="vertical-devider"></div>
                        Ваш вопрос <span class="red">*</span>
                        <textarea name="vash_text " id="inputvash_text" class="form-control faq_text" rows="3" required="required"></textarea>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="checkbox_input faq_approve" value="on">
                                Я согласен на обработку моих персональных данных <span class="red">*</span>
                            </label>
                            <a href="/page/legal" target="_blank">Подробнее...</a>
                        </div>
                        <div class="vertical-devider"></div>
                        <div class="text-center">
                            <button type="button" class="btn btn-default btn-kvart" onclick="requestAnswer()">Задать вопрос</button>
                        </div>
                        <div class="vertical-devider"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    
    </div>
    </div>
    <script type="text/javascript">
        function requestAnswer() {
            $('input').removeClass('error');
            $('select').removeClass('error');
            $('textarea').removeClass('error');

            var phone = $('.faq_phone').val();
            var name = $('.faq_name').val();
            var email = $('.faq_email').val();
            var region = $('.faq_region').val();
            var text = $('.faq_text').val();
            var has_error = false;

            if(phone == ''){
                $('.faq_phone').addClass('error');
                has_error = true;

            }
            if(name == ''){
                $('.faq_name').addClass('error');
                has_error = true;
            }
            if(region == '— Выберите регион —'){
                $('.faq_region').addClass('error');
                has_error = true;
            }

            if(email == ''){
                $('.faq_email').addClass('error');
                has_error = true;
            }

            if(!$('.faq_approve').is(':checked') ){
                $('.faq_approve').addClass('error');
                has_error = true;
            }

            if(has_error) {
                return false;
            }

            $.ajax({
                url: '/api/recall_request',
                type: 'post',
                data: {
                    'name': name,
                    'phone': phone,
                    'email': email,
                    'region': region,
                    'text':text,
                    'type':'question'
                },
                success: function () {
                    alert('Спасибо за вопрос. Мы постараемся ответить на него на этой странице');
                }
            })
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>