<?php $__env->startSection('content'); ?>

    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2 main_etaj">
        <div class="row">
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12">
                <h2 class="text-center">Выберите этаж, oфисы или парковку.</h2>
                <div class="row">
                    <div class="col-xs-12 text-center breadcrumbs">
                        <a href="/objects/building"><span class="h4">Строящиеся</span></a> >
                        <a href="/objects/<?php echo e($object->id); ?>"><span class="h4"><?php echo e($object->name); ?></span></a> >
                        <span class="h4">Корпус <?php echo e($building->number); ?></span>
                    </div>
                </div>
            </div>
            <div class="small-vertical-devider"></div>
            <div class="col-xs-12 etaj_svg_wrap">
                <div class="etaj_svg_holder">
                    <input type="hidden" value="/front/buildings/<?php echo e($building->image); ?>" class="building-image">
                    <?php echo $building->svg; ?>

                    <div class="etaj_svg_popover">
                        <div class="etaj_svg_popover_etaj_div">
                            <span class="etaj_popover_first_name">Этаж</span>
                            <span class="etaj_popover_etaj">5</span>
                        </div>
                        <div>
                            <span class="etaj_popover_second_name">Стоимость</span>
                            <span class="etaj_popover_price">от <span>8.1</span> ₽</span>
                        </div>
                        <div>
                            <span class="etaj_popover_third_name">Продано</span>
                            <span class="etaj_popover_kol_prod"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fourth_name">Забронировано</span>
                            <span class="etaj_popover_kol_zabr"></span>
                        </div>
                        <div>
                            <span class="etaj_popover_fifth_name">Свободно</span>
                            <span class="etaj_popover_kol_svob"></span>
                        </div>
                        <div class="etaj_svg_popover_arenda_div">
                            <span class="etaj_popover_sixth_name">Аренда</span>
                            <span class="etaj_popover_price_arr">от <span>8.1</span> ₽</span>
                        </div>
                        <div class="etaj_svg_popover_bgwrap"></div>
                    </div>
                </div>
                <section class="about_house text-center">
                    <?php echo $__env->make('layouts.front.objects.object_gallery', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('layouts.front.objects.object_'.$object->id.'.description', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </section>
            </div>
        </div>
    </main>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('data'); ?>
    <span class="etaj_holder hidden">{<?php $__currentLoopData = $building->floors->where('level', '>=','1'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $floor): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"0<?php echo e($floor->section->number); ?>0<?php echo e($floor->level); ?>": [<?=$floor->flats->min('price')?>, <?php echo e($floor->soldFlats()->count()); ?>, <?php echo e($floor->bookedFlats()->count()); ?>, <?php echo e($floor->freeFlats()->count()); ?>,<?=is_int($floor->flats->min('rent_price'))?$floor->flats->min('rent_price'):0 ?>, "/objects/<?php echo e($object->id.'/'.$building->id.'/'.$floor->section->id.'/'.$floor->id); ?>"],<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php $__currentLoopData = $building->floors->where('type','commercial'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $office): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"<?='office0'.$office->section->id?>":[<?=$office->flats->min('price')?>, <?php echo e($office->soldFlats()->count()); ?>, <?php echo e($office->bookedFlats()->count()); ?>, <?php echo e($office->freeFlats()->count()); ?>,<?=$office->flats->min('rent_price')?>, "/objects/<?php echo e($object->id.'/'.$building->id.'/'.$office->section->id.'/'.$office->id); ?>" ],<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?> <?php $__currentLoopData = $building->floors->where('type','parking'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parking): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>"<?='park0'.$parking->section->id?>":[<?=$parking->flats->min('price')?>, <?php echo e($parking->soldFlats()->count()); ?>, <?php echo e($parking->bookedFlats()->count()); ?>, <?php echo e($parking->freeFlats()->count()); ?>,<?=$parking->flats->min('rent_price')? $parking->flats->min('rent_price') : 0 ?>, "/objects/<?php echo e($object->id.'/'.$building->id.'/'.$parking->section->id.'/'.$parking->id); ?>"]<?php if(!$loop->last): ?>,<?php endif; ?> <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>}</span>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>