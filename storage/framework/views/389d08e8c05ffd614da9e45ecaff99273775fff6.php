<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h3 class="mt0">Заявки на ответный звонок</h3>
                </div>
                <div class="container-fluid">
                    <div class="panel">
                        <div class="panel-body panel-no-padding">

                            <table class="sortable table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th width="150">Дата заявки</th>
                                    <th width="120">Регион</th>
                                    <th style="max-width: 100px;">Имя</th>
                                    <th>Телефон</th>
                                    <th>Email</th>
                                    <th width="350" >Вопрос</th>
                                    <th style="max-width: 150px;">Примечания</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr <?php if($application->status == 'unread'): ?> class="danger" <?php endif; ?> id="appl-row-<?php echo e($application->id); ?>">
                                        <td ><?php echo $application->created_at; ?></td>
                                        <td><?php echo $application->region; ?>

                                        </td>
                                        <td><?php echo $application->name; ?>

                                        </td>
                                        <td><?php echo $application->phone; ?>

                                        </td>
                                        <td><?php echo $application->email; ?>

                                        </td>
                                        <td class="text-left"><?php echo $application->text; ?>

                                        </td>
                                        <td>
                                            <div class="note-overlay-<?php echo e($application->id); ?>">
                                            <textarea class="note-<?php echo e($application->id); ?>" id="" cols="30" onchange="storeNote(<?php echo e($application->id); ?>)"
                                                      rows="10" style="max-width: 150px;"><?php echo e($application->note); ?></textarea>
                                            </div>
                                        </td>
                                        <td>
                                            <button type="button" class=" btn btn-primary btn-sm" onclick="changeReadStatus(<?php echo e($application->id); ?>)" Изменить статус><span id="read-<?php echo e($application->id); ?>" class="glyphicon <?php if($application->status == 'unread'): ?>glyphicon-eye-open <?php else: ?> glyphicon-eye-close <?php endif; ?> " aria-hidden="true"></span></button>
                                            <a href="/admin/archive_application/<?php echo $application->id; ?>"> <button type="button" class=" btn btn-warning btn-sm" title="В архив"><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span></button></a><br>
                                            <a href="/admin/deleteapp/<?php echo $application->id; ?>" style="color:red; font-weight: bold;">Удалить</a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        function changeReadStatus(id) {

            $.ajax({
                url: '/api/change_read_status',
                type: 'POST',
                data: ({
                    'id': id
                }),
                dataType: "html",
                error: errorHandler,
                success: function (data) {
                    if(data == 'read') {
                        $('#read-'+id).removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                        $('#appl-row-'+id).removeClass('danger');
                    } else {
                        $('#read-'+id).removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                        $('#appl-row-'+id).addClass('danger');
                    }
                }
            })
        }

        function storeNote(id) {
            $.ajax({
                url: '/api/store_note',
                type: 'POST',
                data: ({
                    'id': id,
                    'note': $('.note-' + id).val()
                }),
                dataType: "html",
                error: errorHandler,
                success: function (data) {
                    if (data == 'ok') {
                        $('.note-overlay-' + id).addClass('has-success');
                    } else {
                        $('.note-overlay-' + id).addClass('has-error');
                    }
                }
            })
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>