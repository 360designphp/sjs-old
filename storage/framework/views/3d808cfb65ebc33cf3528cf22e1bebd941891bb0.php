<?php if(Auth::check()): ?>


    <?php if(Auth::user()->role=="User"): ?>
        <script language="JavaScript">
            window.location.href = "/logout"
        </script>
    <?php endif; ?>

<?php endif; ?>

<?php $__env->startSection('content'); ?>

    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>
                    </h1>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($Type!='faq'): ?>
                            <form action="" class="form-inline mb10">
                                <div class="form-group">
                                    <select name="category" id="" class="form-control">
                                        <option value="">Посмотреть все категории</option>
                                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                            <option value="<?php echo e($category->slug); ?>"><?php echo $category->name; ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                    </select>

                                    <button type="submit" class="btn btn-default">Фильтровать</button>
                                </div>
                            </form>
                            <?php endif; ?>
                            <div class="panel">
                                <div class="panel-body panel-no-padding">
                                    <table class=" sortable table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <?php if(isset ($Type)): ?>
                                                <?php if($Type!='faq'): ?>
                                                    <th>Заголовок</th>
                                                    <th>Категория</th>
                                                    <th width="150">Дата публикации</th>
                                                    <th width="350">Действия</th>
                                                <?php elseif($Type=='faq'): ?>
                                                    <th width="150">Заголовок</th>
                                                    <th width="350">Вопрос</th>
                                                    <th>Дата публикации</th>
                                                    <th>Действия</th>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(isset ($Type)): ?>
                                            <?php if($Type!='faq'): ?>
                                                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <tr>
                                                        <td>
                                                            <a href="/admin/<?php echo $post->type; ?>/edit/<?php echo $post->id; ?>"><?php echo $post->title; ?></a><br>
                                                            <small><a href="/<?php echo $post->type; ?>/<?php echo $post->slug; ?>"
                                                                      target="_blank">посмотреть на сайте</a></small>
                                                        </td>
                                                        <td><?php $__currentLoopData = $post->taxonomy->where('type', 'category'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                                <a href="/admin/<?php echo e($post->type); ?>/list?category=<?php echo $cat->slug; ?>"><?php echo e($cat->name); ?></a>
                                                                <br>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                        </td>
                                                        <td><?php echo $post->created_at; ?>

                                                            <p id="ramka"> Изменен</br>
                                                                <?php echo $post->updated_at; ?>

                                                            </p>
                                                        </td>
                                                        <td>

                                                            <div class="  btn-group" role="group" aria-label="...">
                                                                <a href="/admin/deletePost/<?php echo $post->id; ?>">
                                                                    <button type="button" class=" btn btn-danger"><span
                                                                                class="glyphicon glyphicon-remove"
                                                                                aria-hidden="true"></span> Удалить
                                                                    </button>
                                                                </a>
                                                            </div>
                                                        </td>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            <?php elseif($Type=='faq'): ?>
                                                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <tr>

                                                        <!--  <td><div class="icheck checkbox-inline"><input type="checkbox"></div></td>-->
                                                        <td>
                                                            <a href="/admin/<?php echo $post->type; ?>/edit/<?php echo $post->id; ?>"><?php echo $post->title; ?></a><br>
                                                            <small><a href="/faq"
                                                                      target="_blank">посмотреть на сайте</a></small>
                                                        </td>
                                                        <td>
                                                            <?php echo $post->content; ?>

                                                        </td>
                                                        <td><?php echo $post->created_at; ?>

                                                            <p id="ramka"> Изменен</br>
                                                                <?php echo $post->updated_at; ?>

                                                            </p>
                                                        </td>


                                                        <td>

                                                            <div class="  btn-group" role="group" aria-label="...">
                                                                <a href="/admin/deletePost/<?php echo $post->id; ?>">
                                                                    <button type="button" class=" btn btn-danger">
                                                                            <span class="glyphicon glyphicon-remove"
                                                                                  aria-hidden="true"></span> Удалить
                                                                    </button>
                                                                </a>
                                                            </div>


                                                        </td>
                                                    </tr>
                                        </tbody>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        <?php endif; ?>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .container-fluid -->
    </div> <!-- #page-content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>