<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Настройки сайта</h1>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body">
                                <form action="" class="form-horizontal row-border">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Название сайта</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="site_name" placeholder="Мой сайт" value="<?php echo e($settings->site_name); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Ключевые слова</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="keywords" placeholder="слова, помогающие, в, продвижении, сайта" value="<?php echo e($settings->keywords); ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Мета-описание сайта</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control fullscreen" placeholder="Наш сайт - лучший сайт в рунете..." id="description"><?php echo e($settings->description); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">email для уведомлений</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" id="email" placeholder="mail@mail.ru" value="<?php echo e($settings->email); ?>">
                                        </div>
                                    </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="scripts">Скрипты и счётчики</label>
                                      <div class="col-sm-8">
                                          <textarea class="form-control fullscreen" id="scripts"><?php echo $settings->scripts; ?></textarea>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label" for="header_scripts">Код для вставки в head</label>
                                      <div class="col-sm-8">
                                          <textarea class="form-control fullscreen" id="header_scripts"><?php echo $settings->header_scripts; ?></textarea>
                                      </div>
                                  </div>
                                </form>
                                    <?php echo csrf_field(); ?>

                                    <button class="btn-primary btn" onclick="save()">Сохранить</button>
                                    <br>
                                    <br>
                        </div>
                        </div>
                    </div>

                </div> <!-- .container-fluid -->
            </div> <!-- #page-content -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>