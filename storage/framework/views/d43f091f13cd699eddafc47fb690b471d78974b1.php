<?php $__env->startSection('content'); ?>
    <main class="col-xs-12 col-sm-12 col-md-10  col-md-offset-2">
        <?php echo $__env->make('layouts.front.objects.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('layouts.front.objects.filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="row searches_list">
            <?php $__currentLoopData = $objects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $object): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <?php if(!$object->title): ?>
                    <div class="col-xs-6 col-sm-4 search_item">
                        <div target="_blank" class="search_item_inner_wrap clearfix">
                            <a href="/objects/<?php echo e($object->id); ?>" class="no_underline">
                                <div class="search_item_inner_top">
                                    <div><?php echo e($object->name); ?></div>
                                </div>
                                <div class="search_item_inner_img">
                                    <?php if($object->status == 'building'): ?><span class="search_item_inner_label color1">Строится</span><?php endif; ?>
                                    <?php if($object->status == 'ready'): ?><span class="search_item_inner_label color4">Сдан</span><?php endif; ?>
                                    <?php if($object->status == 'sold'): ?><span class="search_item_inner_label color2">Продано</span><?php endif; ?>
                                    <img src="/front/objects/<?php echo e($object->image); ?>" alt="" class="">
                                </div>
                                <div class="search_item_inner_bottom no_underline">
                                    Подробнее <span class="shev_right"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-xs-6 col-sm-4 search_item">
                        <div target="_blank" class="search_item_inner_wrap clearfix">
                            <a href="/page/<?php echo e($object->slug); ?>" class="no_underline">
                            <div class="search_item_inner_top">
                                <div><?php echo e($object->title); ?></div>
                            </div>
                            <div class="search_item_inner_img">
                                <?php if(!empty($object->badge_text)): ?><span class="search_item_inner_label color5"><?php echo e($object->badge_text); ?></span><?php endif; ?>
                                <?php if(is_object($object->thumbnail())): ?>
                                    <img src="/front/thumbnail/<?php echo e($object->thumbnail()->slug); ?>" alt="" class="">
                                <?php else: ?>
                                    <img src="/front/img/link3_fon.jpg" alt="" class="">
                                <?php endif; ?>
                            </div>
                            <div class="search_item_inner_bottom">
                                Подробнее <span class="shev_right"></span>
                            </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </div>
        <div class="vertical-devider"></div>
    </main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>