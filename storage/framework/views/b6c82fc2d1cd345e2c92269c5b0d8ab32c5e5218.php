<?php $__env->startSection('content'); ?>
    <div class="static-content-wrapper">
        <div class="static-content">
            <div class="page-content">
                <div class="page-heading">
                    <h1>Редактирование секции</h1><?php if($section->id ): ?>
                        &nbsp;<a href="/flat/<?php echo $section->id; ?>" target="_blank"
                                 style="margin-top: 5px;">посмотреть на сайте</a>
                    <?php endif; ?>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <form action="/admin/objects/section/save" name="edited_section" method="post" id="edited_section"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo e($section->id); ?>" name="id" >
                                <?php echo e(csrf_field()); ?>

                                <label class="col-xs-12">Символ
                                    <input type="text" class="form-control  mb20"
                                           value="<?php echo ($section->symbol)?$section->symbol:""; ?>"
                                           name="symbol"
                                           placeholder="Введите символ секции..." required>
                                </label>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionO<?php echo e($section->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionO<?php echo e($section->id); ?>"
                                                       href="#collapseObject<?php echo e($section->id); ?>"><h2>Служебная информация</h2></a>
                                                    <div class="accordion-body">
                                                        <table class="table mb0">
                                                            <thead>
                                                            </thead>
                                                        </table>

                                                        <div id="collapseObject<?php echo e($section->id); ?>" class="collapse">
                                                            <label class="mt0">Идентификатор корпуса
                                                                <input type="text" class="form-control  mb20"
                                                                       value="<?php echo ($section->building_id)?$section->building_id:""; ?>"
                                                                       name="floor_id"
                                                                       placeholder="Введите  идентификатор..." required>
                                                            </label><br>
                                                            <label class="mt0">№ Секции
                                                                <input type="text" class="form-control  mb20"
                                                                       value="<?php echo ($section->number)?$section->number:""; ?>"
                                                                       name="number"
                                                                       placeholder="Введите № секции..." required>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion-group" id="accordionB<?php echo e($section->id); ?>">
                                                <div class="panel accordion-item">
                                                    <a class="accordion-title collapsed" data-toggle="collapse"
                                                       data-parent="accordionB<?php echo e($section->id); ?>"
                                                       href="#collapseBuilding<?php echo e($section->id); ?>"><h2>SEO - настройки</h2></a>
                                                    <div class="accordion-body">
                                                        <div id="collapseBuilding<?php echo e($section->id); ?>" class="collapse">
                                                            <span class="h3">Ключевые слова</span>
                                                            <input type="text" name="seo_keywords" id="keywords" class="form-control"
                                                                   value="<?php echo $section->seo_keywords; ?>"><br>
                                                            <span class="h3">Мета-описание</span>
                                                            <textarea name="seo_description" class="form-control" cols="30" rows="10"><?php echo e($section->seo_description); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="panel panel-inverse">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <h3 class="mt0">Публикация</h3>

                                    <dl class="dl-horizontal mb20">
                                        <dt>Время публикации</dt>
                                        <dd><?php echo $section->created_at; ?></dd>

                                        <dt>Последняя редакция</dt>
                                        <dd><?php echo $section->updated_at; ?></dd>
                                    </dl>

                                    <div class="panel-footer">
                                        <input type="submit" class="pull-right btn btn-info" form="edited_section"
                                               value="Сохранить">
                                        <?php if($section->id): ?>
                                            <button type="button" class=" btn btn-danger"
                                                    onclick="deleteCategory('<?php echo e($section->id); ?>')"><span
                                                        class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                Удалить
                                            </button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer role="contentinfo">
                    <div class="clearfix">
                        <ul class="list-unstyled list-inline pull-left">
                            <li><h6 style="margin: 0;">360 CMS</h6></li>
                        </ul>
                        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i
                                    class="fa fa-arrow-up"></i></button>
                    </div>
                </footer>
            </div>
            <?php $__env->stopSection(); ?>
            <?php $__env->startSection('script'); ?>
                <script>
                    function deleteObjectImage(id) {
                        if (confirm('Вы уверены?') == true) {
                            $.ajax({
                                url: '/api/ajax',
                                type: 'POST',
                                data: ({
                                    'id': id,
                                    'intent': 'deleteObjectImage'
                                }),
                                dataType: "html",
                                error: errorHandler,
                                success: function () {
                                    $('#img' + id).fadeOut(400);
                                    $('#text' + id).fadeOut(400);
                                }
                            })
                        }
                        window.location.reload();
                    }
                    function errorHandler(data) {
                        alert('Ошибка :' + data.status);
                    }
                </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>