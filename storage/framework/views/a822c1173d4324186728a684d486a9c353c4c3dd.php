<?php echo $__env->make('layouts.front.common.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<main class="col-xs-6 col-xs-offset-6 col-sm-10 col-sm-offset-2 main_p404">
    <img src="/front/img/bg404.svg" alt="" class="bg404">
    <div class="text-center">
        <h1>Страница не найдена</h1>
        <h2>Ошибка 404</h2>
    </div>
</main>
</div>
</div>
<?php echo $__env->make('layouts.front.common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>